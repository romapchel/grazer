package nl.impressie.grazer.threads;

import nl.impressie.grazer.util.crawlerUtils.IndexUtil;

import java.util.List;

/**
 * @author Impressie
 */
public class IndexTask implements Runnable {
	List targets;
	String mode;
	boolean updateOnly;
	int globalMaxDepth;

	public IndexTask(List Targets, String Mode, boolean UpdateOnly, int globalMaxDepth) {
		targets = Targets;
		mode = Mode;
		updateOnly = UpdateOnly;
		this.globalMaxDepth = globalMaxDepth;
	}

	@Override
	public void run() {
		IndexUtil.indexPages(targets, mode, System.nanoTime(), updateOnly, globalMaxDepth);
	}
}
