package nl.impressie.grazer.threads;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.DB.Tag;

import java.util.*;

/**
 * @author Impressie
 */
public class ArticleTagUpdater {
	private static Deque<UpdateArticleTags> queue;
	private static ArticleTagUpdater instance;
	private boolean started = false;

	private ArticleTagUpdater() {
		queue = CollectionFactory.createQueue();
	}

	public static void updateTags(List<Article> articles, Map<String, Tag> oldTags, Map<String, Tag> newTags) {
		Set<String> all = CollectionFactory.createSet();
		for(String key: oldTags.keySet()) {
			all.add(key);
		}
		for(String key: newTags.keySet()) {
			all.add(key);
		}

		for(String key: all) {
			if(oldTags.containsKey(key) && newTags.containsKey(key)) {
				oldTags.remove(key);
				newTags.remove(key);
			}
		}

		if(oldTags.size() > 0 || newTags.size() > 0)
			updateTags(articles, oldTags, newTags.values());
	}
	public static void updateTags(List<Article> articles, Map<String, Tag> toRemove, Collection<Tag> toAdd) {
		UpdateArticleTags updater = new UpdateArticleTags(getInstance(), articles, toRemove, toAdd);
		queue.add(updater);

		getInstance().start();
	}

	private static ArticleTagUpdater getInstance() {
		if(null == instance)
			instance = new ArticleTagUpdater();
		return instance;
	}

	private void start() {
		if(started)
			return;

		started = true;
		next();
	}

	public void next() {
		UpdateArticleTags next = queue.pollFirst();
		if(null != next) {
			new Thread(next).start();
		} else {
			instance = null;
		}
	}


	private static class UpdateArticleTags implements Runnable {
		private ArticleTagUpdater updater;
		private List<Article> articles;
		private Collection<Tag> toAdd;
		private Map<String, Tag> toRemove;

		public UpdateArticleTags(ArticleTagUpdater updater, List<Article> articles, Map<String, Tag> toRemove, Collection<Tag> toAdd) {
			this.updater = updater;
			this.articles = articles;
			this.toRemove = toRemove;
			this.toAdd = toAdd;
		}

		@Override
		public void run() {
			if(toAdd.size() > 0 || toRemove.size() > 0) {
				for(Article article: articles) {
					if(toRemove.size() > 0)
						article.removeTags(toRemove);

					if(toAdd.size() > 0)
						article.addTags(toAdd);
				}
			}
			updater.next();
		}
	}
}
