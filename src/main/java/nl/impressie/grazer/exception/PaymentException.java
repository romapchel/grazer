package nl.impressie.grazer.exception;

import nl.stil4m.mollie.ResponseOrError;

import java.util.Map;

/**
 * @author Impressie
 */
public class PaymentException extends Exception {
	private static final String CUSTOMER_UNAVAILABLE = "{type=request, message=The customer is no longer available}";
	private String message;

	public PaymentException(String message) {
		super(message);
		this.message = message;
		System.out.println(String.format("message: [%s]", message));
	}

	public static PaymentException getException(ResponseOrError result) {
		Map errors = result.getError();
		StringBuilder builder = new StringBuilder();
		for(Object error: errors.values()) {
			builder.append(error);
		}

		return new PaymentException(builder.toString());
	}

	public boolean isCustomerError() {
		System.out.println(String.format("message: [%s]", message));
		switch(message) {
			case CUSTOMER_UNAVAILABLE:
				return true;
			default:
				return false;
		}
	}

	@Override
	public void printStackTrace() {
		System.err.println(getMessage());
		super.printStackTrace();
	}
}
