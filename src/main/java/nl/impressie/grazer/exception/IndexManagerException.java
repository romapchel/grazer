package nl.impressie.grazer.exception;

/**
 * @author Impressie
 */
public class IndexManagerException extends Exception {
	public IndexManagerException(String msg) {
		super(msg);
	}
}
