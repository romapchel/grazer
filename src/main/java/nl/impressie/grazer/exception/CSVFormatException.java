package nl.impressie.grazer.exception;

/**
 * @author Impressie
 */
public class CSVFormatException extends Exception {
	public CSVFormatException(String message) {
		super(message);
	}
}
