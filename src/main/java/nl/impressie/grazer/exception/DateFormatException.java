package nl.impressie.grazer.exception;

/**
 * @author Impressie
 */
public class DateFormatException extends Exception {
	private String date, regex, format, originalDate;

	public DateFormatException(String date, String regex, String format, String originalDate) {
		super(getErrorString(format, regex, date, originalDate));
		this.format = format;
		this.regex = regex;
		this.date = date;
		this.originalDate = originalDate;
	}

	public String getErrorString(String page, String originalDate) {
		return getErrorString(format, regex, date, page, originalDate);
	}

	public static String getErrorString(String format, String regex, String date, String originalDate) {
		return getErrorString(format, regex, date, originalDate, "");
	}

	public static String getErrorString(String format, String regex, String date, String originalDate, String page) {
		String error = String.format("Error with dateformat [%1$s] and regex [%2$s] for date [%3$s]([%4$s])", format, regex, date, originalDate);
		if(null != page && 0 < page.length())
			error = String.format("%s on page %s", error, page);

		return error;
	}

	public String getExceptionForMail(String page, String originalDate) {
		return getErrorString(page, originalDate);
	}
}
