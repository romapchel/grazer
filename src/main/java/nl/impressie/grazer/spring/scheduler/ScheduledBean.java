package nl.impressie.grazer.spring.scheduler;

import nl.impressie.grazer.controller.IndexController;
import nl.impressie.grazer.util.LogUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author Impressie
 */
public class ScheduledBean {

	@Value("${indexing.maxdepth}")
	private int globalMaxDepth;
	@Value("${indexing.defaultmode}")
	private String defaultMode;

	@Value("${server.testmode}")
	private Boolean testmode;

	@Scheduled(fixedRateString = "${indexing.scheduled}")
	public void scheduledIndexing() {
		LogUtil.log("Indexing should start at %s", DateTime.now().toString());
		if(!testmode)
			IndexController.makeIndex(defaultMode, globalMaxDepth);

	}
}
