package nl.impressie.grazer.spring.scheduler;

import nl.impressie.grazer.controller.MailingController;

import nl.impressie.grazer.model.Email;
import nl.impressie.grazer.util.WeeklyMailUtil;
import org.springframework.scheduling.annotation.Scheduled;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class ScheduledEmail {
    private final WeeklyMailUtil weeklyMailUtil  = new WeeklyMailUtil();


        @Scheduled(cron = "0 16  11 * * *")
    public void everyDay() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        List<Email> send = MailingController.sortForEmailing("Daily");

        for ( Email email : send) {
            weeklyMailUtil.sendMail(email.getEmail(),  email.getArticleList(), email.getProfile(), email.getUsername());
            System.out.println(email.getEmail() +   email.getArticleList() + email.getProfile() + email.getUsername());
            System.out.println("Email was sent to " + email.getEmail());
        }

    }
    @Scheduled(cron = "0 0 8 */3 * *")
    public void everyThreeDays() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        List<Email> send = MailingController.sortForEmailing("Every 3 days");

        for ( Email email : send) {
            weeklyMailUtil.sendMail(email.getEmail(),  email.getArticleList(), email.getProfile(),email.getUsername());

            System.out.println("Email was sent to " + email.getEmail());
        }

    }
    @Scheduled(cron = "0 0 8 */7 * *")
    public void everyWeek() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        List<Email> send = MailingController.sortForEmailing("Weekly");

        for (Email email : send) {
            weeklyMailUtil.sendMail(email.getEmail(), email.getArticleList(), email.getProfile(), email.getUsername());

            System.out.println("Email was sent to " + email.getEmail());
        }
    }
}
