package nl.impressie.grazer.spring.scheduler;

import nl.impressie.grazer.spring.beans.Captcha;
import nl.impressie.grazer.util.LinkUtil;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.MailUtil;
import nl.impressie.grazer.util.caching.CacheUtil;
import nl.impressie.grazer.util.caching.CacheableFunctions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author Impressie
 */
@Configuration
@EnableScheduling
public class SpringBeans {

	@Bean
	public ScheduledBean scheduledBeans() {
		return new ScheduledBean();
	}

	@Bean
	public ScheduledEmail scheduledEmail(){return new ScheduledEmail();}

	@Bean
	public LinkUtil linkUtil() {
		return new LinkUtil();
	}

	@Bean
	public MailUtil mailUtil() {
		return new MailUtil();
	}

	@Bean
	public LogUtil logUtil() {
		return LogUtil.instantiate();
	}

	@Bean
	public Captcha captcha() {
		return Captcha.instantiate();
	}

	@Bean
	public CacheableFunctions cacheableFunctions() { return new CacheableFunctions(); }

	@Bean
	public CacheUtil cacheableUtil() { return CacheUtil.instantiate(); }
}
