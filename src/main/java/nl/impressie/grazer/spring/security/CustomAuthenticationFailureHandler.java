package nl.impressie.grazer.spring.security;

import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.SessionUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	@Override
	public void onAuthenticationFailure(
			HttpServletRequest Request,
			HttpServletResponse Response,
			AuthenticationException Exception) throws IOException, ServletException {
		String errorName = Exception.getClass().getName();

		if(errorName.equals("org.springframework.security.authentication.BadCredentialsException")) {
			SessionUtil.addToSession(Request, "username", Request.getParameter("username"));
			getRedirectStrategy().sendRedirect(Request, Response, "/login?error");
		} else if(errorName.equals("org.springframework.security.authentication.AuthenticationServiceException")) {
			LogUtil.logException(Exception);
			getRedirectStrategy().sendRedirect(Request, Response, "/login?service");
		} else {
			super.onAuthenticationFailure(Request, Response, Exception);
		}
	}

}