package nl.impressie.grazer.spring.security;

import nl.impressie.grazer.dao.UserDao;
import nl.impressie.grazer.model.User.CustomUser;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author Impressie
 */
public class CustomUserDetailsService implements UserDetailsService {
	@Override
	public CustomUser loadUserByUsername(String username) throws UsernameNotFoundException {
		CustomUser user = UserDao.findByUsername(username);
		if(user == null)
			throw new UsernameNotFoundException(String.format("No user with the username %s exists", username));
		return user;
	}
}
