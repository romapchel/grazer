package nl.impressie.grazer.spring.beans;

import nl.impressie.grazer.factories.CollectionFactory;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * @author Impressie
 */
@Component
public class Captcha {
	@Value("${google.recaptcha.key.secret}")
	private String secret;

	@Value("${google.recaptcha.key.site}")
	private String site;

	private static Captcha instance;

	public static final String PARAM_NAME = "g-recaptcha-response";

	private Captcha() {
		System.out.println();
	}
	public static Captcha instantiate() {
		if(instance == null)
			instance = new Captcha();
		return instance;
	}
	public static boolean verifyCapthcha(Map requestMap) {
		String response = null;
		if(requestMap.containsKey(PARAM_NAME)) {
			Object found = requestMap.get(PARAM_NAME);
			if(found instanceof String[] && ((String[])found).length == 1)
				response = ((String[])found)[0];
			else
				response = found.toString();
		}
		return instance.verifyCapthcha(response);
	}
	public static String getForm() {
		return instance.getForm(true);
	}

	private String getFormV3(boolean includeJS) {
		StringBuilder builder = new StringBuilder();

		if(includeJS) {
			builder.append(String.format("<script src=\"https://www.google.com/recaptcha/api.js?render=%s\"></script>", site));
			builder.append(String.format("<script> grecaptcha.ready(function() { grecaptcha.execute('%s', {action: 'login'}); }); </script>", site));
		}
		builder.append(String.format("<div class=\"g-recaptcha col-sm-5\" th:attr=\"data-sitekey=%s\"></div>", site));
		builder.append("<span id=\"captchaError\" class=\"alert alert-danger col-sm-4\" style=\"display:none\"></span>");

		return builder.toString();
	}
	private String getForm(boolean includeJS) {
		StringBuilder builder = new StringBuilder();

		if(includeJS) {
			builder.append("<script src=\"https://www.google.com/recaptcha/api.js\" async defer></script>");
		}
		builder.append(String.format("<div class=\"g-recaptcha\" data-sitekey=\"%s\"></div>", site));

		return builder.toString();
	}
	private boolean verifyCapthcha(String response) {
		if(response != null) {
			try {
				Map data = CollectionFactory.createMap();
				data.put("secret", secret);
				data.put("response", response);
				Connection.Response googleResponse = Jsoup.connect("https://www.google.com/recaptcha/api/siteverify")
						.data(data)
						.ignoreContentType(true)
						.execute();
				JSONObject json = new JSONObject(googleResponse.body());
				boolean success = json.getBoolean("success");
				return success;
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
