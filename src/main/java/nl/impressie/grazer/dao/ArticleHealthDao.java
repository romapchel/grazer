package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.ArticleHealth;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Impressie
 */
public class ArticleHealthDao extends Dao {
	public static final String TABLE_NAME = "article_health";
	public static final RowMapper ROW_MAPPER = new RowMapper() {
		@Override
		public ArticleHealth mapRow(ResultSet resultSet, int i) throws SQLException {
			String url = resultSet.getString(URL);

			Long indexedOn = resultSet.getLong(INDEXED_ON);
			String indecedContent = resultSet.getString(INDEXED_CONTENT);
			String indecedDate = resultSet.getString(INDEXED_DATE);
			String indexedTitle = resultSet.getString(INDEXED_TITLE);

			Long checkedOn = resultSet.getLong(CHECKED_ON);
			String checkedContent = resultSet.getString(CHECKED_CONTENT);
			String checkedDate = resultSet.getString(CHECKED_DATE);
			String checkedTitle = resultSet.getString(CHECKED_TITLE);

			String skipped = resultSet.getString(SKIPPED);

			boolean heaalthyContent = resultSet.getBoolean(HEALTHY_CONTENT);
			boolean heaalthyDate = resultSet.getBoolean(HEALTHY_DATE);
			boolean heaalthyTitle = resultSet.getBoolean(HEALTHY_TITLE);
			boolean heaalthyOverall = resultSet.getBoolean(HEALTHY_OVERALL);

			int pageId = resultSet.getInt(PAGE_ID);
			String page_url = resultSet.getString(PAGE_URL);

			return new ArticleHealth(url, indexedOn, indecedContent, indecedDate, indexedTitle, checkedOn, checkedContent, checkedDate, checkedTitle, heaalthyContent, heaalthyTitle, heaalthyDate, heaalthyOverall, pageId, page_url, skipped);
		}
	};

	public static final String ID = "id", URL = "url", INDEXED_ON = "indexed_on", INDEXED_CONTENT = "indexed_content", INDEXED_DATE = "indexed_date", INDEXED_TITLE = "indexed_title",
			CHECKED_ON = "checked_on", CHECKED_CONTENT = "checked_content", CHECKED_DATE = "checked_date", CHECKED_TITLE = "checked_title", SKIPPED = "skipped",
			HEALTHY_CONTENT = "healthy_content", HEALTHY_TITLE = "healthy_title", HEALTHY_DATE = "healthy_date", HEALTHY_OVERALL = "healthy_overall", PAGE_ID = "page_id", PAGE_URL = "page_url";

	public static ArticleHealth create(String url, long indexedOnDate, String indexedContent, String indexedDate, String indexedTitle, String checkedContent, String checkedDate, String checkedTitle,
									   boolean compareContent, boolean compareTitle, boolean compareDate, boolean overalHealth, Object pageId, String pageUrl, String skipped) {
		List<Parameter> params = CollectionFactory.createList();
		params.add(new Parameter(ArticleHealthDao.URL, url));
		params.add(new Parameter(ArticleHealthDao.INDEXED_ON, indexedOnDate));
		params.add(new Parameter(ArticleHealthDao.INDEXED_CONTENT, indexedContent));
		params.add(new Parameter(ArticleHealthDao.INDEXED_DATE, indexedDate));
		params.add(new Parameter(ArticleHealthDao.INDEXED_TITLE, indexedTitle));
		params.add(new Parameter(ArticleHealthDao.CHECKED_ON, DateUtil.getTimestamp()));
		params.add(new Parameter(ArticleHealthDao.CHECKED_CONTENT, checkedContent));
		params.add(new Parameter(ArticleHealthDao.CHECKED_DATE, checkedDate));
		params.add(new Parameter(ArticleHealthDao.CHECKED_TITLE, checkedTitle));
		params.add(new Parameter(ArticleHealthDao.HEALTHY_CONTENT, compareContent));
		params.add(new Parameter(ArticleHealthDao.HEALTHY_TITLE, compareTitle));
		params.add(new Parameter(ArticleHealthDao.HEALTHY_DATE, compareDate));
		params.add(new Parameter(ArticleHealthDao.HEALTHY_OVERALL, overalHealth));
		params.add(new Parameter(ArticleHealthDao.PAGE_ID, pageId));
		params.add(new Parameter(ArticleHealthDao.PAGE_URL, pageUrl));
		params.add(new Parameter(ArticleHealthDao.SKIPPED, skipped));

		return (ArticleHealth) create(TABLE_NAME, ROW_MAPPER, ID, params);
	}

	public static ArticleHealth findHealthForPageByUrl(Object page_id, String url) {
		List where = CollectionFactory.createList();
		where.add(new Parameter(PAGE_ID, page_id));
		where.add(new Parameter(URL, url));

		List sort = CollectionFactory.createList();
		sort.add(new Parameter(CHECKED_ON, Parameter.SortOrder.DESCENDING));

		List reports = find(TABLE_NAME, ROW_MAPPER, where, sort);
		if(reports.size() > 0)
			return (ArticleHealth) reports.get(0);
		return null;
	}

	public static int deleteByPageUrl(String pageUrl) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(PAGE_URL, pageUrl));

		return delete(TABLE_NAME, params);
	}
}
