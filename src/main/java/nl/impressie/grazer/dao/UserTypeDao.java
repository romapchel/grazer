package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.User.AccountType;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Role;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * @author Impressie
 */
public class UserTypeDao extends Dao {
	private static final String TABLE_NAME = "user_type";

	private static final String ID = "id";
	private static final String USER_ID = "user_id";
	private static final String TYPE_ID = "type_id";
	private static final String END_DATE = "end_date";

	private final static RowMapper<AccountType> ROWMAP = (resultSet, i) -> {
		AccountType acc = AccountTypeDao.findById(resultSet.getInt(TYPE_ID));
		acc.setUserType(resultSet.getInt(ID), resultSet.getInt(END_DATE));
		return acc;
	};

	public static AccountType getTypeByUserId(Object userId) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(USER_ID, userId));

		Parameter active = new Parameter(END_DATE, DateUtil.getCurrentDateAsInt(), Parameter.Comparator.BEWLOW_OR_EQUAL_TO_STORED);
		active.setStartsSub(true);
		params.add(active);

		Parameter eternal = new Parameter(END_DATE, 0, Parameter.Comparator.ABOVE_OR_EQUAL_TO_STORED);
		eternal.setSeperator(Parameter.Seperator.OR);
		eternal.setEndsSub(true);
		params.add(eternal);

		List<AccountType> res = find(TABLE_NAME, ROWMAP, params);
		AccountType unique = unique(res);
		return unique;
	}

	public static int delete(Object id, int userId) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(ID, id));
		params.add(new Parameter(USER_ID, userId));

		return delete(TABLE_NAME, params);
	}
	public static AccountType update(Object id, int userId, int typeId, int endDate) {
		List set = CollectionFactory.createList();
		set.add(new Parameter(USER_ID, userId));
		set.add(new Parameter(TYPE_ID, typeId));
		set.add(new Parameter(END_DATE, endDate));

		List where = CollectionFactory.createList();
		where.add(new Parameter(ID, id));
		where.add(new Parameter(USER_ID, userId));

		AccountType updated;
		if(null == id || find(TABLE_NAME, ROWMAP, where).size() == 0) {
			updated = create(TABLE_NAME, ROWMAP, ID, set);

		} else {
			List<AccountType> list = update(TABLE_NAME, ROWMAP, where, set);
			updated = unique(list);
		}

		return updated;
	}

	public static AccountType updateType(CustomUser userToUpdate, int typeId, int days, boolean daysIsEndDate, boolean addDays) {
		AccountType currentAccountType = userToUpdate.getAccountType();
		CustomUser currentUser = AuthenticationUtil.getCurrentUser();
		if(
			userToUpdate.getId() == 1 ||
			(
				currentAccountType.hasRole(Role.ADMIN) &&
					(
						(!currentUser.hasRole(Role.ADMIN)) ||
						userToUpdate.getId() == currentUser.getId()
					)
			)
		)
			return currentAccountType;

		int endDate = (daysIsEndDate ? days : daysToDate(days, currentAccountType, typeId, addDays));
		Object id = null;
		int idToDelete = -1;
		int currentEnd = currentAccountType.getEndDate();

		if(currentEnd != 0 && (Integer)currentAccountType.getId() == typeId) {
			id = currentAccountType.getUserTypeId();

		} else {
			if(null != currentAccountType)
				idToDelete = currentAccountType.getUserTypeId();
		}
		AccountType updated = update(id, userToUpdate.getId(), typeId, endDate);


		boolean expired = !addDays && endDate <= DateUtil.getCurrentDateAsInt();
		boolean empty = typeId == AccountTypeDao.DEFAULT_TYPE_ID;

		if(expired || empty)
			delete(updated.getUserTypeId(), userToUpdate.getId());

		if(updated != null && (int)updated.getId() == typeId && idToDelete != -1)
			delete(idToDelete, userToUpdate.getId());

		return updated;
	}

	private static int daysToDate(int days, AccountType accountType, int typeId, boolean addDays) {
		int endDate;
		int currentEnd = accountType.getEndDate();

		int usedDays = (addDays ? days : -1*days);
		if(currentEnd != 0 && (Integer)accountType.getId() == typeId)
			endDate = DateUtil.getDaysAfterAsInt(currentEnd, usedDays);
		else
			endDate = DateUtil.getDaysAfterTodayAsInt(usedDays);

		if(days == -1)
			endDate = 0;

		return endDate;
	}
}
