package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.Crawler.ArticleSearch;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static nl.impressie.grazer.model.DB.Tag.isValidtagType;

/**
 * @author Impressie
 */
public class TagDao extends Dao {

	private static final String TABLE_NAME = "tag";
	private static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
			int idNumber = rs.getInt(ID);
			String type = rs.getString(TYPE);
			String name = rs.getString(NAME);
			String code = rs.getString(CODE);
			String desc = rs.getString(DESCRIPTION);
			boolean disabled = rs.getBoolean(DISABLED);
			Tag tag = new Tag(idNumber, type, name, code, disabled, desc);
			return tag;
		}
	};
	private static Map<String, Parameter.Type> parameter_maps;

	public final static String ID = "id", NAME = "name", TYPE = "type", CODE = "code", DESCRIPTION = "description", DISABLED = "disabled";

	public static List<Tag> getType(String type) {
		List params = CollectionFactory.createList();
		if(null != type && 0 < type.length())
			params.add(toParameter(TYPE, type));

		return find(params);
	}

	public static Tag findById(Object id) {
		return (Tag) unique(find(toParameter(ID, id)));
	}

	public static Tag findByName(String name, String type) {
		name = stripNoSubGroup(name);

		List params = CollectionFactory.createList();
		params.add(toParameter(NAME, name));
		params.add(toParameter(TYPE, type));
		return unique(find(params));
	}

	public static Tag getChildTag(String name) {
		name = stripNoSubGroup(name);

		List params = CollectionFactory.createList();
		Parameter nameParam = toParameter(NAME, "%: " + name.trim());
		nameParam.setComparator(Parameter.Comparator.LIKE);
		params.add(nameParam);
		params.add(toParameter(TYPE, Tag.RECHTSGEBIED));
		return unique(find(params));
	}

	private static String stripNoSubGroup(String name) {
		String noSubGroup = ": Geen subrechtsgebied";
		if(name.toLowerCase().endsWith(noSubGroup.toLowerCase()))
			name = name.substring(0, name.length() - noSubGroup.length()).trim();

		return name;
	}

	public static List<Tag> find(Parameter... params) {
		return find(CollectionFactory.createList(params));
	}

	public static List<Tag> find(List params) {
		List sort = CollectionFactory.createList();
		sort.add(toParameter(NAME, true));

		return find(TABLE_NAME, ROWMAP, params, sort);
	}

	public static List<Tag> findByProfileId(Object profielId) {
		return ProfileTagDao.findTags(profielId);
	}

	public static Tag create(String type, String name) {
		return create(type, name, null);
	}

	public static Tag create(String type, String name, String code) {
		if(name.length() == 0 || type.length() == 0)
			return null;

		name = stripNoSubGroup(name);
		name = name.substring(0, 1).toUpperCase() + name.substring(1);

		Tag tag = findByName(name, type);
		if(tag != null)
			return tag;

		List parameters = CollectionFactory.createList();
		parameters.add(toParameter(NAME, name));
		parameters.add(toParameter(TYPE, type));
		if(null != code) parameters.add(toParameter(CODE, code));

		return create(parameters);
	}

	public static Tag create(List parameters) {
		return (Tag) create(TABLE_NAME, ROWMAP, ID, parameters);
	}


	public static Tag save(HttpServletRequest request) {
		boolean isUpdate = request.getParameterMap().containsKey(ID);
		Object id = (isUpdate ? safeGetParameter(request, ID) : 0);

		String tagType = safeGetParameter(request, TagDao.TYPE);
		if(!isUpdate && !isValidtagType(tagType))
			return null;

		List params = CollectionFactory.createList();

		addParameterToList(params, request, TagDao.NAME);
		addParameterToList(params, request, TagDao.CODE);
		addParameterToList(params, request, TagDao.DESCRIPTION);

		if(isUpdate)
			params.add(new Parameter(TagDao.TYPE, TagDao.findById(id).getType()));
		else
			addParameterToList(params, request, TagDao.TYPE);

		return TagDao.update(id, params);
	}

	private static List addParameterToList(List list, HttpServletRequest request, String key) {
		if(request.getParameterMap().containsKey(key)) list.add(toParameter(key, request.getParameter(key)));

		return list;
	}

	public static Tag update(Object id, List updateParameters) {
		List where = CollectionFactory.createList();
		where.add(toParameter(ID, id));
		Tag t = findById(id);

		if(null == t) return create(updateParameters);

		return (Tag) unique(update(TABLE_NAME, ROWMAP, where, updateParameters));
	}

	public static void deleteType(String type) {
		List<Tag> tags = getType(type);
		deleteTags(tags);
	}

	public static void deleteTags(List<Tag> tags) {
		if(tags.size() == 0)
			return;

		boolean hideinactive = false;

		List ids = CollectionFactory.createList();
		Map tagMap = CollectionFactory.createMap();
		for(Tag tag : tags) {
			String tid = tag.getId();
			tagMap.put(tid, tag);

			ids.add(tid);
		}

		for(Website page : WebsiteDao.getAll(hideinactive)) {
			page.removeTags(tagMap);
		}


		for(Article article : getArticlesForTags(tags)) {
			article.removeTags(tagMap);
		}

		Parameter param = new Parameter(ID, ids, Parameter.Comparator.IN);
		param.setType(Parameter.Type.STRING);
		delete(TABLE_NAME, param);
	}

	public static List<Article> getArticlesForTags(List tags) {
		ArticleSearch search = new ArticleSearch();
		search.addTags(tags).setIgnoreDisabledPages(false);
		Map results = search.search();
		List articles = (List<Article>) results.get("artikels");
		return articles;
	}

	public static Map getAllTagsAsMap() {
		Map tags = CollectionFactory.createMap();
		for(String id : Tag.getTypes()) {
			tags.put(id, getType(id));
		}
		return tags;
	}

	public static String getAsIdString(List<Tag> tags) {
		StringBuilder ids = new StringBuilder();
		for(Tag tag : tags) {
			if(ids.length() > 0)
				ids.append(", ");
			ids.append(tag.getId());
		}
		return ids.toString();
	}

	public static Map getNamesValuesAsString(List<Tag> tags, boolean quoted) {
		StringBuilder names = new StringBuilder();
		StringBuilder ids = new StringBuilder();
		for(Tag tag : tags) {
			if(names.length() > 0) {
				names.append(", ");
				ids.append(", ");
			}
			String format = "%1$s";
			if(quoted)
				format = "\"" + format + "\"";
			names.append(String.format(format, tag.getName()));
			ids.append(String.format(format, tag.getId()));
		}
		Map map = CollectionFactory.createMap();
		map.put("names", names.toString());
		map.put("ids", ids.toString());
		return map;
	}

	private static void setUpParameterTypes() {
		parameter_maps = CollectionFactory.createMap();
		parameter_maps.put(ID, Parameter.Type.STRING);
		parameter_maps.put(NAME, Parameter.Type.STRING);
		parameter_maps.put(TYPE, Parameter.Type.STRING);
		parameter_maps.put(CODE, Parameter.Type.STRING);
		parameter_maps.put(DISABLED, Parameter.Type.BOOLEAN);
	}

	public static Parameter setType(Parameter param) {
		if(null == parameter_maps) setUpParameterTypes();

		String column = param.getColumn();

		if(parameter_maps.containsKey(column)) param.setType(parameter_maps.get(column));

		return param;
	}

	private static Parameter toParameter(String column, Object value) {
		Parameter param = new Parameter(column, value);
		return setType(param);
	}

	public static List<Tag> getAllBut(String... typesToExclude) {
		List params = CollectionFactory.createList();
		if(0 < typesToExclude.length)
			params.add(new Parameter(TYPE, typesToExclude, Parameter.Comparator.NOT_EQUALS));

		return find(params);
	}

	public static Map<String, Tag> createFromString(String string, String type) {
		Map tags = CollectionFactory.createMap();
		for(String name: string.split(",")) {
			if(name.length() == 0)
				continue;

			Tag t = create(type, name);
			if(t == null)
				System.out.println();
			tags.put(t.getId(), t);
		}

		return tags;
	}
}
