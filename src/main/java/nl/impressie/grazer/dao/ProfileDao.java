package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.model.User.Role;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class ProfileDao extends Dao {
	public static final String TABLE_NAME = "profile";
	public static final String ID = "id", PROFILE_NAME = "profile_name", USER_ID = "user_id", CONTENT_SEARCH = "content_search_term", TITLE_SEARCH = "title_search_term",
			PROFILE_SITE = "user_site", PROFILE_TAG = "profile_tag", PROFILE_SEARCH = "profile_search", MAIL_OPTION = "mail_option";

	public static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt(ID);
			String name = rs.getString(PROFILE_NAME);
			String searchContent = rs.getString(CONTENT_SEARCH);
			String searchTitle = rs.getString(TITLE_SEARCH);
			int userId = rs.getInt(USER_ID);
			String mailOption = rs.getString(MAIL_OPTION);
			Map tags = Dao.modelListToMap(ProfileTagDao.findTags(id));
			Map sites = Dao.modelListToMap(ProfileWebsiteDao.findPagesByProfId(id));

			Profile prof = new Profile(id, name, userId, tags, sites, searchContent, searchTitle, mailOption);

			return prof;
		}
	};


	public static Profile create(HttpServletRequest request) {
		String name = safeGetParameter(request, ProfileDao.PROFILE_NAME);
		if(name.trim().length() == 0)
			return null;

		int id = AuthenticationUtil.getCurrentUser().getId();
		Profile currentProfile = findByUserIdAndName(id, name);

		if(null == currentProfile)
			currentProfile = create(name, id);

		return currentProfile;
	}

	public static Profile create(String profileName, Object userId) {
		CustomUser user = UserDao.findById(userId);
		if(null == user) return null;

		List params = CollectionFactory.createList();
		params.add(new Parameter(USER_ID, userId));
		if(0 < profileName.length())
			params.add(new Parameter(PROFILE_NAME, profileName));

		return (Profile) create(TABLE_NAME, ROWMAP, ID, params);
	}

	public static Profile findById(Object id) {
		return (Profile) unique(find(new Parameter(ID, id)));
	}

	public static Profile findByUserIdAndName(Object userId, String name) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(USER_ID, userId));
		params.add(new Parameter(PROFILE_NAME, name));

		return (Profile) unique(find(params));
	}

	public static List findByUserId(Object userId) {
		return find(new Parameter(USER_ID, userId));
	}

	public static List find(Parameter... params) {
		return find(CollectionFactory.createList(params));
	}

	public static List find(List params) {
		return find(TABLE_NAME, ROWMAP, params);
	}

	public static void delete(Object id) {
		delete(TABLE_NAME, new Parameter(ID, id));
	}

	public static Profile update(List<Parameter> whereParameters, List<Parameter> updateParameters) {
		return (Profile) unique(update(TABLE_NAME, ROWMAP, whereParameters, updateParameters));
	}

	public static Profile update(Object id, boolean globalState) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		if(user != null && user.hasRole(Role.GLOBAL_EDIT)) {
			List where = CollectionFactory.createList();
			where.add(new Parameter(ID, id));

			int newUserId = 0;
			if(!globalState)
				newUserId = user.getId();

			List updates = CollectionFactory.createList();
			updates.add(new Parameter(USER_ID, newUserId));
			return update(where, updates);
		}
		return findById(id);
	}
	public static Profile update(Object id, String name) {
		if(0 < name.length()) {
			List where = CollectionFactory.createList();
			where.add(new Parameter(ID, id));

			List updates = CollectionFactory.createList();
			updates.add(new Parameter(PROFILE_NAME, name));
			return update(where, updates);
		}
		return findById(id);
	}

	public static Profile updateMail(Object id, String weeklyMail){
		if (weeklyMail.length() > 0){

			List where = CollectionFactory.createList();
			where.add(new Parameter(ID, id));

			List updates = CollectionFactory.createList();
			updates.add(new Parameter(MAIL_OPTION, weeklyMail));
			return update(where, updates);
		}
		return findById(id);
	}

	public static Profile update(Object id, String newTitleSearch, String newContentSearch) {
		Profile profile = findById(id);
		if(null == profile)
			return null;

		List updates = CollectionFactory.createList();
		updates.add(new Parameter(TITLE_SEARCH, newTitleSearch));
		updates.add(new Parameter(CONTENT_SEARCH, newContentSearch));

		List where = CollectionFactory.createList();
		where.add(new Parameter(ID, id));

		return update(where, updates);
	}

	public static Profile update(String id, Map newTags, Map newSites) {
		Profile profile = findById(id);
		if(null == profile)
			return null;

		List tagIdsToDelete = CollectionFactory.createList();

		Map<String, Tag> currentTags = profile.getTags();
		for(String tagId : currentTags.keySet()) {
			if(newTags.containsKey(tagId))
				newTags.remove(tagId);
			else
				tagIdsToDelete.add(tagId);
		}
		for(String tagId : (Collection<String>) newTags.keySet()) {
			ProfileTagDao.create(id, tagId);
		}
		ProfileTagDao.delete(tagIdsToDelete);

		List pageIdsToDelete = CollectionFactory.createList();
		Map<Object, Website> currentPages = profile.getSites();
		for(Object pageId : currentPages.keySet()) {
			if(newSites.containsKey(pageId))
				newSites.remove(pageId);
			else
				pageIdsToDelete.add(pageId);
		}
		for(Object pageId : newSites.keySet()) {
			ProfileWebsiteDao.create(id, pageId);
		}
		ProfileWebsiteDao.delete(pageIdsToDelete);

		return findById(id);
	}

	public static Profile updateTags(int profileId, Map tagsToRemove, List tagsToAdd) {
		for(Object tagId : tagsToAdd) {
			ProfileTagDao.create(profileId, tagId);
		}

		ProfileTagDao.delete(tagsToRemove.keySet());

		return findById(profileId);
	}

	public static Profile updateSites(int profileId, Map pagesToRemove, List pagesToAdd) {
		for(Object tagId : pagesToAdd) {
			ProfileWebsiteDao.create(profileId, tagId);
		}

		ProfileWebsiteDao.delete(pagesToRemove.keySet());

		return findById(profileId);
	}

	public static Profile copyProfile(Profile profile, CustomUser user) {
		Profile newProfile = ProfileDao.create(String.format("Standaardprofiel %s", profile.getName()), user.getId());

		newProfile = newProfile.updateTags(profile.getTags().keySet());
		newProfile = newProfile.updateSites(profile.getSites().keySet());
		newProfile = newProfile.updateSearch(profile.getTitleSearchString(), profile.getContentSearchString());

		return findById(newProfile.getId());
	}

	public static Map validateCreate(Map requestParameters, boolean ignoreMissing, boolean checkUnique) {
		Map errors = CollectionFactory.createMap();
		errors = validateLength(ignoreMissing, PROFILE_NAME, requestParameters, errors, 1, 500);

		return errors;
	}
}
