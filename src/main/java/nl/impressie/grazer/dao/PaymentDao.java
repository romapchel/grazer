package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.CustomPayment;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * @author Impressie
 */
public class PaymentDao extends Dao {
	public static final String ID = "id", USER_ID = "user_id", PAYMENT_ID = "initial_payment_id", SUBSCRIPTION_ID = "subscription_payment_id", TIMESTAMP = "timestamp", BUNDLE_ID = "bundle_id";

	private static final String TABLE_NAME = "payment";
	private static final RowMapper<CustomPayment> ROWMAP = (rs, rowNum) -> {
		int id = rs.getInt(ID);
		int userId = rs.getInt(USER_ID);
		int bundleId = rs.getInt(BUNDLE_ID);
		String paymentId = rs.getString(PAYMENT_ID);
		String subscriptionId = rs.getString(SUBSCRIPTION_ID);
		long timestamp = rs.getLong(TIMESTAMP);

		CustomPayment payment = new CustomPayment(id, userId, bundleId, paymentId, subscriptionId, timestamp);

		return payment;
	};

	public static CustomPayment getById(int id) {
		return unique(find(TABLE_NAME, ROWMAP, ID, id));
	}

	public static CustomPayment getBySubscription(String subscription_payment_id) {
		return unique(find(TABLE_NAME, ROWMAP, SUBSCRIPTION_ID, subscription_payment_id));
	}

	public static CustomPayment getByInitialPayment(String initial_payment_id) {
		return unique(find(TABLE_NAME, ROWMAP, PAYMENT_ID, initial_payment_id));
	}

	public static CustomPayment getByUserId(int userId) {
		return unique(find(TABLE_NAME, ROWMAP, USER_ID, userId));
	}

	public static CustomPayment getLastPayment() {
		List<CustomPayment> all = find(TABLE_NAME, ROWMAP, CollectionFactory.createList(), CollectionFactory.createList(new Parameter(TIMESTAMP, Parameter.SortOrder.DESCENDING_NULLS_LAST)));
		return (all.size() > 0 ? all.get(0) : null);
	}

	public static CustomPayment create(int UserId, int BundleId) {
		List where = CollectionFactory.createList();
		where.add(new Parameter(USER_ID, UserId));
		delete(TABLE_NAME, where);

		return create(TABLE_NAME, ROWMAP, ID, getSetParameters(UserId, BundleId));
	}

	public static CustomPayment completePayment(Object id, long timestamp) {
		return updateById(id, new Parameter(TIMESTAMP, timestamp));
	}

	public static CustomPayment addPayment(int id, String paymentId) {
		return updateById(id, new Parameter(PAYMENT_ID, paymentId));
	}
	public static CustomPayment updateById(Object id, Parameter... params) {
		List where = CollectionFactory.createList();
		where.add(new Parameter(ID, id));
		List<CustomPayment> updated = update(TABLE_NAME, ROWMAP, where, CollectionFactory.createList(params));
		return unique(updated);
	}

	public static CustomPayment addSubscription(int id, String subscriptionId) {
		return updateById(id, new Parameter(SUBSCRIPTION_ID, subscriptionId));
	}

	private static List getSetParameters(int UserId, int BundleId) {
		List setParameters = CollectionFactory.createList();

		setParameters.add(new Parameter(USER_ID, UserId));
		setParameters.add(new Parameter(BUNDLE_ID, BundleId));

		return setParameters;
	}
}
