package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;
import java.util.List;

/**
 * @author Impressie
 */
public class ProfileTagDao extends Dao {

	public final static String ID = "id", PROFILE_ID = "profile_id", TAG_ID = "tag_id";
	public final static String TABLE_NAME = "user_tag";
	public final static RowMapper TAG_ROWMAP = (rs, i) -> {
		int id = rs.getInt(TAG_ID);
		Tag tag = TagDao.findById(id);

		return tag;
	};
	public final static RowMapper PROFILE_ROWMAP = (rs, i) -> ProfileDao.findById(rs.getInt(PROFILE_ID));

	public static List<Tag> findTags(Object profileId) {
		List params = CollectionFactory.createList();
		params.add(toParameter(PROFILE_ID, profileId));

		List sort = CollectionFactory.createList();
		List group = CollectionFactory.createList();
		group.add(new Parameter(TAG_ID));

		return find(TABLE_NAME, TAG_ROWMAP, params, sort, group);
	}

	public static void create(Object profileId, Object tagId) {
		List params = CollectionFactory.createList();
		params.add(toParameter(PROFILE_ID, profileId));
		params.add(toParameter(TAG_ID, tagId));

		create(TABLE_NAME, TAG_ROWMAP, ID, params);
	}

	public static void delete(Collection tagIdsToDelete) {
		if(0 < tagIdsToDelete.size())
			delete(TABLE_NAME, new Parameter(TAG_ID, tagIdsToDelete, Parameter.Comparator.IN).setType(Parameter.Type.STRING));
	}

	public static Parameter toParameter(String column, Object value) {
		Parameter param = new Parameter(column, value);
		if(PROFILE_ID.equals(column) || TAG_ID.equals(column))
			param.setType(Parameter.Type.INT);

		return param;
	}

	public static List<Profile> findProfilesByTags(List<Tag> tags) {
		List where = CollectionFactory.createList();
		where.add(new Parameter(TAG_ID, tags, Parameter.Comparator.IN));

		List sort = CollectionFactory.createList();
		List group = CollectionFactory.createList();
		group.add(new Parameter(PROFILE_ID));

		return find(TABLE_NAME, PROFILE_ROWMAP, where, sort, group);
	}
}

