package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.User.AccountType;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * @author Impressie
 */
public class AccountTypeDao extends Dao {

	public static final String ID = "id", NAME = "name";
	public static final int DEFAULT_TYPE_ID = 3;

	private final static String TABLE_NAME = "type";

	private final static RowMapper ID_MAPPER = (resultSet, i) -> resultSet.getInt(ID);

	private final static RowMapper ROWMAP = (resultSet, i) -> {
		String name = resultSet.getString(NAME);
		int id = resultSet.getInt(ID);

		return new AccountType(id, name);
	};

	public static AccountType findById(Object id) {
		return (AccountType) unique(find(TABLE_NAME, ROWMAP, ID, id));
	}
	public static String findNameById(int id) {
		return findById(id).getName();
	}
	public static Integer getUserAcoountId() {
		return findIdByName("user");
	}
	public static Integer findIdByName(String name) {
		return (Integer) unique(find(TABLE_NAME, ID_MAPPER, NAME, name));
	}

	public static List getAll() { return find(TABLE_NAME, ROWMAP, CollectionFactory.createList()); }
}
