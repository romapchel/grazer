package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * @author Impressie
 */
public class TagPageDao extends Dao {

	public final static String TABLE_NAME = "tag_page";
	public final static RowMapper ROWMAP = new RowMapper() {
		@Override
		public Tag mapRow(ResultSet resultSet, int i) throws SQLException {
			int tagId = resultSet.getInt(TAG_ID);
			return TagDao.findById(tagId);
		}
	};
	public final static RowMapper PAGEMAP = new RowMapper() {
		@Override
		public Website mapRow(ResultSet resultSet, int i) throws SQLException {
			int pageId = resultSet.getInt(PAGE_ID);
			return WebsiteDao.findById(pageId);
		}
	};
	public final static String ID = "id", PAGE_ID = "page_id", TAG_ID = "tag_id";

	public static List<Tag> findByPageId(Object pageId) {
		Parameter pageIdParameter = new Parameter(PAGE_ID, pageId);
		return find(TABLE_NAME, ROWMAP, pageIdParameter);
	}

	public static List<Website> findByTagIds(String... tagIds) {
		return findByTagIds(CollectionFactory.createList(tagIds));
	}
	public static List<Website> findByTagIds(Collection<String> tagIds) {
		List params = CollectionFactory.createList();
		if(0 < tagIds.size())
			params.add(new Parameter(TAG_ID, tagIds, Parameter.Comparator.IN));

		params.add(new Parameter(PAGE_ID, WebsiteDao.getAll(true), Parameter.Comparator.IN));

		List grouping = CollectionFactory.createList();
		grouping.add(new Parameter(PAGE_ID));

		List sortparameters = CollectionFactory.createList();

		return find(TABLE_NAME, PAGEMAP, params, sortparameters, grouping);
	}

	public static void delete(Object pageId, Object tagId) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(PAGE_ID, pageId));
		params.add(new Parameter(TAG_ID, tagId));

		delete(TABLE_NAME, params);
	}

	public static void create(Object pageId, Object tagId) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(PAGE_ID, pageId).setType(Parameter.Type.INT));
		params.add(new Parameter(TAG_ID, tagId).setType(Parameter.Type.INT));
		if(find(TABLE_NAME, ROWMAP, params).size() != 0)
			return;

		create(TABLE_NAME, ROWMAP, ID, params);
	}

	public static Website updatePageTags(int pageId, Collection tagIdsToAdd, Collection tagIdsToRemove) {
//		if(tagIdsToAdd.size() != 0 || 0 != tagIdsToRemove.size())
//			System.out.println(String.format("# to add: %d, # to remove: %d", tagIdsToAdd.size(), tagIdsToRemove.size()));

		for(Object tagId : tagIdsToAdd) {
			create(pageId, tagId);
		}
		for(Object tagId : tagIdsToRemove) {
			delete(pageId, tagId);
		}

		return WebsiteDao.findById(pageId);
	}
}
