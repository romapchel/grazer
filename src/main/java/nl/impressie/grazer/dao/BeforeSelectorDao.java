package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.BeforeSelector;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
public class BeforeSelectorDao extends Dao {
	public static final String TABLE_NAME = "before";
	public static final String ID = "id", NAME = "name", SELECTOR = "selector", PAGE_ID = "page_id";

	public static final RowMapper<BeforeSelector> ROWMAP = (rs, rowNum) -> {
		int id = rs.getInt(ID);

		String name = rs.getString(NAME);
		String selector = rs.getString(SELECTOR);

		return new BeforeSelector(id, name, selector);
	};

	public static List findByPageId(Object pageId) {
		return find(PAGE_ID, pageId);
	}

	public static List<BeforeSelector> findById(Object id) {
		return find(ID, id);
	}

	private static List<BeforeSelector> find(String column, Object value) {
		return find(TABLE_NAME, ROWMAP, column, value);
	}

	public static BeforeSelector create(Object pageId, String name, String selector) {
		List insertList = CollectionFactory.createList();
		insertList.add(new Parameter(NAME, name));
		insertList.add(new Parameter(SELECTOR, selector));

		return create(pageId, insertList);
	}

	public static BeforeSelector create(Object pageId, List<Parameter> insertList) {
		int idParamIndex = -1;

		for(int index = 0, max = insertList.size(); index < max; index++) {
			Parameter param = insertList.get(index);

			//if id is not set, create a new row, if id is set then it might be an updated, but all were previously deleted so it needs to be recreated with the ID as parameter to keep the same id
			Object val = param.getParamater();
			if(ID.equals(param.getColumn())) {
				if(val.toString().length() == 0)
					idParamIndex = index;
				else
					param.setType(Parameter.Type.INT);

				continue;
			}

			if(val.toString().length() == 0)
				return null;
		}

		insertList.add(new Parameter(PAGE_ID, pageId));

		if(idParamIndex >= 0) {
			List where = CollectionFactory.createList();
			where.add(insertList.get(idParamIndex));

			insertList.remove(idParamIndex);
		}


		return (BeforeSelector) create(TABLE_NAME, ROWMAP, ID, insertList);
	}

	public static void deleteByPageId(Object pageId) {
		delete(TABLE_NAME, new Parameter(PAGE_ID, pageId).setType(Parameter.Type.INT));
	}

	public static Set<String> getColumns() {
		return CollectionFactory.createSet(ID, NAME, SELECTOR, PAGE_ID);
	}

	public static BeforeSelector updateOrCreate(Map values) {
		values = prepValues(values);
		return updateOrCreate(values, TABLE_NAME, ROWMAP, ID, getColumns(), ID);
	}

	protected static Map prepValues(Map<String, Object> values) {
		for(String key: values.keySet()) {
			Object val = values.get(key);
			switch(key) {
				case ID:
				case PAGE_ID:
					val = Integer.valueOf(val.toString());
				default:

			}
			values.put(key, val);
		}
		return values;
	}
}
