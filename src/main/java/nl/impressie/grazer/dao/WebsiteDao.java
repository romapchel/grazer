package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.SeReDe;
import nl.impressie.grazer.model.DB.*;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.UrlUtil;
import nl.impressie.grazer.util.caching.CacheUtil;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class WebsiteDao extends Dao {
	public static final RowMapper<Website> ROWMAP = new RowMapper() {
		@Override
		public Website mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt(ID);

			String name = rs.getString(NAME);
			String url = rs.getString(URL);
			String nieuwszender = rs.getString(NIEUWSZENDER);

			String maxDepth = rs.getString(MAX_DEPTH);
			String lastCrawlDate = rs.getString(LAST_CRAWL_DATE);
			String dateFormat = rs.getString(DATE_FORMAT);
			String moreInfo = rs.getString(MORE_INFO);
			String description = rs.getString(DESCRIPTION);

			boolean disabled = rs.getBoolean(DISABLED);
			boolean healthy = rs.getBoolean(HEALTHY);
			boolean defective = rs.getBoolean(DEFECTIVE);

			SeReDe date = SeReDe.loadSeReDe(rs.getString(DATE));
			SeReDe title = SeReDe.loadSeReDe(rs.getString(TITLE));
			SeReDe content = SeReDe.loadSeReDe(rs.getString(CONTENT));
			SeReDe snippet = SeReDe.loadSeReDe(rs.getString(SNIPPET));
			SeReDe rechtsgebied = SeReDe.loadSeReDe(rs.getString(RECHTSGEBIED));
			SeReDe nieuwsSoort = SeReDe.loadSeReDe(rs.getString(NIEUWS_SOORT));
			SeReDe rubrieken = SeReDe.loadSeReDe(rs.getString(RUBRIEKEN));

			SeReDe crawl = SeReDe.loadSeReDe(rs.getString(CRAWL));
			SeReDe article = SeReDe.loadSeReDe(rs.getString(ARTICLE));

			String searchKey = rs.getString(SEARCH_KEY);
			String searchMethod = rs.getString(SEARCH_METHOD);
			String searchHeader = rs.getString(SEARCH_HEADER);
			String searchData = rs.getString(SEARCH_DATA);
			String jsonUrl = rs.getString(JSON_URL);
			String pageKey = rs.getString(PAGE_KEY);
			PageSearch pageSearch = new PageSearch(searchKey, searchMethod, searchHeader, searchData, jsonUrl, pageKey);

			String cookieUrl = rs.getString(COOKIE_URL);
			String cookieMethod = rs.getString(COOKIE_METHOD);
			String cookieData = rs.getString(COOKIE_DATA);
			String cookieCss = rs.getString(COOKIE_CSS);
			CookieWall cookieWall = new CookieWall(cookieUrl, cookieMethod, cookieData, cookieCss);

			String javascript = rs.getString(JS);
			String jsTestId = rs.getString(JS_TEST_ID);
			JavaScriptObject jsObject = new JavaScriptObject(jsTestId, javascript);

			Website page = new Website(id, name, url, nieuwszender, maxDepth, crawl, article, lastCrawlDate, dateFormat, moreInfo, disabled, healthy, defective,
					date, title, content, snippet, rechtsgebied, nieuwsSoort, rubrieken, pageSearch, jsObject, description, cookieWall);

			return page;
		}
	};
	public static final String TABLE_NAME = "page";
	public static final String ID = "id", NAME = "name", URL = "url", NIEUWSZENDER = "nieuwszender", LAST_CRAWL_DATE = "last_crawl_date", MAX_DEPTH = "max_depth", DATE_FORMAT = "date_format", MORE_INFO = "more_info", DATE = "date",
			TITLE = "title", CONTENT = "content", SNIPPET = "snippet", RECHTSGEBIED = "rechtsgebied", NIEUWS_SOORT = "nieuws_soort", RUBRIEKEN = "rubrieken", CRAWL = "crawl", ARTICLE = "article",
			SEARCH_KEY = "search_key", SEARCH_METHOD = "search_method", SEARCH_HEADER = "search_header", SEARCH_DATA = "search_data", JSON_URL = "json_url", PAGE_KEY = "page_key", JS = "js", JS_TEST_ID = "js_test_id", DISABLED = "disabled", HEALTHY = "healthy", DEFECTIVE = "defective",
			DESCRIPTION = "description", COOKIE_URL = "cookie_url", COOKIE_METHOD = "cookie_method", COOKIE_DATA = "cookie_data", COOKIE_CSS = "cookie_css";

	public final static String NAV = "nav", ALT = "alt", BEFORE = "before", REPLACEMENTS = "replacements", ADDITIONAL_PAGES = "additional_pages";

	public static List<Website> getAll() {
		return getAll(true);
	}

	public static List<Website> getAll(boolean activeOnly) {
		return getAll(activeOnly, CollectionFactory.createList());
	}

	public static List<Website> getAll(boolean activeOnly, List<Parameter> sort) {
		List parameters = CollectionFactory.createList();
		if(activeOnly) {
			parameters.add(new Parameter(DISABLED, false));
			parameters.add(new Parameter(DEFECTIVE, false));
		}

		return find(TABLE_NAME, ROWMAP, parameters, sort);
	}

	public static int count(boolean ignoreInactive) {
		return count(ignoreInactive, false);
	}
	public static int count(boolean ignoreInactive, boolean ignoreBroken) {
		List params = CollectionFactory.createList();
		if(ignoreBroken)
			params.add(new Parameter(DEFECTIVE, false));

		if(ignoreInactive)
			params.add(new Parameter(DISABLED, false));

		return count(TABLE_NAME, params);
	}

	public static Website findById(Object id) {
		return unique(find(TABLE_NAME, ROWMAP, ID, id));
	}

	public static Website update(Object id, List<Parameter> parameters) {
		CacheUtil.resetWebsitesCache();

		Website found = (id == null ? null : findById(id));
		if(null == found)
			return create(TABLE_NAME, ROWMAP, ID, parameters);
		else if(parameters.size() == 0)
			return found;

		List where = CollectionFactory.createList();
		where.add(new Parameter(ID, id));

		List<Website> pages = update(where, parameters);

		if(1 == pages.size())
			return pages.get(0);
		return null;
	}

	public static Website save(Website page) {
		Map values = page.getCurrent();
		if(values.containsKey(WebsiteDao.ID))
			values.remove(WebsiteDao.ID);

		return updateOrCreate(page.getId(), values);
	}

	public static List<Website> setAllStatus(boolean disabled) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(DISABLED, disabled));
		return update(CollectionFactory.createList(), params);
	}

	public static List<Website> update(List<Parameter> where, List<Parameter> updates) {
		CacheUtil.resetWebsitesCache();
		return update(TABLE_NAME, ROWMAP, where, updates);
	}


	public static Website create(Object id, String url) {
		Website byId = findById(id);
		if(null != byId)
			return byId;

		List parameters = CollectionFactory.createList();
		if(id != null)
			parameters.add(new Parameter(ID, id).setType(Parameter.Type.INT));
		else
			id = url;

		url = UrlUtil.fixProtocol(url);
		if(!UrlUtil.isValidUrl(url))
			return null;

		parameters.add(new Parameter(NAME, id));
		parameters.add(new Parameter(URL, url));

		CacheUtil.resetWebsitesCache();

		return create(TABLE_NAME, ROWMAP, ID, parameters);
	}
	public static Website create(String name, String url, String nieuwszender) {
		return create(name, url, nieuwszender, null, new SeReDe(), new SeReDe(), null, null, null, new SeReDe(), new SeReDe(),
				new SeReDe(), new SeReDe(), new SeReDe(), new SeReDe(), new SeReDe(), new PageSearch("", "", "", "", "", ""),
				new JavaScriptObject());
	}
	public static Website create(String name, String url, String nieuwszender, String maxDepth, SeReDe crawl, SeReDe article, String lastCrawlDate,
								 String dateFormat, String moreInfo, SeReDe date, SeReDe title, SeReDe content,
								 SeReDe snippet, SeReDe rechtsgebied, SeReDe nieuwsSoort, SeReDe rubrieken, PageSearch pageSearch, JavaScriptObject javascript) {
		Website page = findByUrl(url);
		if(null != page)
			return page;

		List parameters = CollectionFactory.createList();
		parameters.add(new Parameter(NAME, name));
		parameters.add(new Parameter(URL, url));
		parameters.add(new Parameter(NIEUWSZENDER, nieuwszender));
		parameters.add(new Parameter(MAX_DEPTH, maxDepth));
		parameters.add(new Parameter(LAST_CRAWL_DATE, lastCrawlDate));
		parameters.add(new Parameter(DATE_FORMAT, dateFormat));
		parameters.add(new Parameter(MORE_INFO, moreInfo));

		parameters.add(new Parameter(CRAWL, crawl.toString()));
		parameters.add(new Parameter(ARTICLE, article.toString()));
		parameters.add(new Parameter(DATE, date.toString()));
		parameters.add(new Parameter(TITLE, title.toString()));
		parameters.add(new Parameter(CONTENT, content.toString()));
		parameters.add(new Parameter(SNIPPET, snippet.toString()));

		parameters.add(new Parameter(RECHTSGEBIED, rechtsgebied.toString()));
		parameters.add(new Parameter(NIEUWS_SOORT, nieuwsSoort.toString()));
		parameters.add(new Parameter(RUBRIEKEN, rubrieken.toString()));

		parameters.add(new Parameter(PAGE_KEY, pageSearch.getPageKey()));
		parameters.add(new Parameter(SEARCH_KEY, pageSearch.getKey()));
		parameters.add(new Parameter(JSON_URL, pageSearch.getJsonUrl()));

		parameters.add(new Parameter(JS, javascript.getJavascript()));
		parameters.add(new Parameter(JS_TEST_ID, javascript.getTestId()));

		return update(0, parameters);
	}

	public static int delete(Object id) {
		List<Parameter> params = CollectionFactory.createList();
		params.add(new Parameter(ID, id));

		return delete(params);
	}

	public static int delete(List params) {
		return delete(TABLE_NAME, params);
	}

	public static Website findByUrl(String url) {
		return (Website) unique(find(TABLE_NAME, ROWMAP, URL, url));
	}

	public static Website findByName(String name) {
		Parameter nameParam = new Parameter(NAME, name);
		return unique(find(TABLE_NAME, ROWMAP, nameParam));
	}

	public static List<String> getColumns() {
		return CollectionFactory.createList(
			ID,
			NAME,
			URL,
			NIEUWSZENDER,
			LAST_CRAWL_DATE,
			MAX_DEPTH,
			DATE_FORMAT,
			MORE_INFO,
			DATE,
			TITLE,
			CONTENT,
			SNIPPET,
			RECHTSGEBIED,
			NIEUWS_SOORT,
			RUBRIEKEN,
			CRAWL,
			ARTICLE,
			SEARCH_KEY,
			SEARCH_METHOD,
			SEARCH_HEADER,
			SEARCH_DATA,
			JSON_URL,
			PAGE_KEY,
			JS,
			JS_TEST_ID,
			DISABLED,
			HEALTHY,
			DEFECTIVE,
			DESCRIPTION,
			COOKIE_URL,
			COOKIE_METHOD,
			COOKIE_DATA,
			COOKIE_CSS
		);
	}

	public static Website updateOrCreate(Map values) {
		return updateOrCreate(null, values);
	}
	public static Website updateOrCreate(Object id, Map values) {
		values = prepValues(values);
		Website page;
		if(id == null)
			page = updateOrCreate(values, TABLE_NAME, ROWMAP, ID, getColumns(), ID);
		else
			page = updateOrCreate(CollectionFactory.createMap(new CollectionFactory.MapPair(ID, id)), values, TABLE_NAME, ROWMAP, ID, getColumns(), ID);

		if(values.containsKey(NAV)) {
			NavigationDao.deleteByPageId(page.getId());
			Object found = values.get(NAV);
			if(found instanceof String) {
				Navigation.CSFImport((String)found, page.getId());
			} else {
				throw new NotImplementedException("There is no method defined to instantiate this object");
			}
		}
		if(values.containsKey(BEFORE)) {
			BeforeSelectorDao.deleteByPageId(page.getId());
			Object found = values.get(BEFORE);
			if(found instanceof String) {
				BeforeSelector.CSFImport((String)found, page.getId());
			} else {
				throw new NotImplementedException("There is no method defined to instantiate this object");
			}

		}
		if(values.containsKey(ALT)) {
			AltPageDao.deleteByPageId(page.getId());
			Object found = values.get(ALT);
			if(found instanceof String) {
				AltPage.CSFImport((String)found, page.getId());
			} else {
				throw new NotImplementedException("There is no method defined to instantiate this object");
			}

		}
		return findById(page.getId());
	}

	public static Map getTitles() {
		Map titles = CollectionFactory.createMap();

		titles.put(TITLE, "Titel");
		titles.put(DATE, "Datum");
		titles.put(CONTENT, "Inhoud");

		return titles;
	}

	public static List disableById(String id) {
		return toggleDisabledById(id, true);
	}

	public static List enableById(String id) {
		return toggleDisabledById(id, false);
	}

	public static List toggleDisabledById(String ids, boolean toggleTo) {
		List update = CollectionFactory.createList();
		update.add(new Parameter(WebsiteDao.DISABLED, toggleTo));

		List total = CollectionFactory.createList();
		for(String id : ids.split(",")) {
			id = id.trim();

			List where = CollectionFactory.createList();
			where.add(new Parameter(WebsiteDao.DEFECTIVE, false));
			if(0 < id.length())
				where.add(new Parameter(WebsiteDao.ID, id));

			total.addAll(WebsiteDao.update(where, update));
		}
		return total;
	}

	public static List getAdminSelect() {
		boolean ascending = true;

		List sort = CollectionFactory.createList();
		sort.add(new Parameter(HEALTHY, ascending));
		sort.add(new Parameter(DISABLED, ascending));
		sort.add(new Parameter(NAME, ascending));

		boolean justActive = false;
		return getAll(justActive, sort);
	}

	protected static Map prepValues(Map<String, Object> values) {
		for(String key: values.keySet()) {
			Object val = values.get(key);
			switch(key) {
				case DISABLED:
				case HEALTHY:
				case DEFECTIVE:
					val = Boolean.valueOf(val.toString());
				default:

			}
			values.put(key, val);
		}
		return values;
	}
}
