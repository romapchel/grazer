package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Navigation;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
public class NavigationDao extends Dao {
	public static final RowMapper<Navigation> ROWMAP = new RowMapper<Navigation>() {
		@Override
		public Navigation mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt(ID);
			int pageId = rs.getInt(PAGE_ID);
			String paging = rs.getString(PAGING);
			String after = rs.getString(AFTER);
			String url = rs.getString(URL);
			int jump = rs.getInt(JUMP);
			int statrt = rs.getInt(START);
			int numberOfJumps = rs.getInt(END);

			Navigation nav = new Navigation(id, pageId, url, paging, after, statrt, jump, numberOfJumps);

			return nav;
		}
	};
	public static final String TABLE_NAME = "navigation";
	public final static String ID = "id", PAGE_ID = "page_id", URL = "url", PAGING = "paging", AFTER = "after", JUMP = "jump", START = "start", END = "end";

	public static Navigation findById(Object id) {
		return unique(find(TABLE_NAME, ROWMAP, ID, id));
	}

	public static Navigation findByPageId(Object pageId) {
		return unique(find(TABLE_NAME, ROWMAP, PAGE_ID, pageId));
	}

	public static Navigation update(Object id, List<Parameter> parameters) {
		parameters = setParameterTypes(parameters);
		if(null == findById(id))
			return create(TABLE_NAME, ROWMAP, ID, parameters);
		List where = CollectionFactory.createList();
		where.add(new Parameter(ID, id));

		List<Navigation> pages = update(TABLE_NAME, ROWMAP, where, parameters);

		if(1 == pages.size())
			return pages.get(0);
		return null;
	}

	public static Navigation create(Map values) {
		List params = CollectionFactory.createList();
		for(String key: getColumns()) {
			params.add(new Parameter(key, values.get(key)));
		}
		return create(TABLE_NAME, ROWMAP, ID, params);
	}

	private static List<Parameter> setParameterTypes(List<Parameter> parameters) {
		for(Parameter param : parameters) {
			String column = param.getColumn();
			if(!(URL.equals(column) || PAGING.equals(column) || AFTER.equals(column)))
				param.setType(Parameter.Type.INT);
		}
		return parameters;
	}

	public static int deleteByPageId(Object pageId) {
		List par = CollectionFactory.createList();
		par.add(new Parameter(PAGE_ID, pageId).setType(Parameter.Type.INT));

		return delete(TABLE_NAME, par);
	}

	public static Set<String> getColumns() {
		return CollectionFactory.createSet(ID, PAGE_ID, URL, PAGING, AFTER, JUMP, START, END);
	}

	public static Navigation updateOrCreate(Map values) {
		values = prepValues(values);
		return updateOrCreate(values, TABLE_NAME, ROWMAP, ID, getColumns(), ID);
	}

	protected static Map prepValues(Map<String, Object> values) {
		for(String key: values.keySet()) {
			Object val = values.get(key);
			switch(key) {
				case ID:
				case PAGE_ID:
				case JUMP:
				case START:
				case END:
					try {
						val = Integer.valueOf(val.toString());
					} catch(NumberFormatException e) {}
				default:

			}
			values.put(key, val);
		}
		return values;
	}
}
