package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;
import java.util.List;

/**
 * @author Impressie
 */
public class ProfileWebsiteDao extends Dao {
	public static final String ID = "id", PROFILE_ID = "profile_id", PAGE_ID = "page_id";
	public static final String TABLE_NAME = "user_site";
	public static final RowMapper SITE_ROWMAP = (rs, i) -> {
		int pageId = rs.getInt(PAGE_ID);
		Website page = WebsiteDao.findById(pageId);

		return page;
	};

	public static List<Website> findPagesByProfId(int profId) {
		return find(TABLE_NAME, SITE_ROWMAP, PROFILE_ID, profId);
	}

	public static void create(Object profileId, Object pageId) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(PROFILE_ID, profileId).setType(Parameter.Type.INT));
		params.add(new Parameter(PAGE_ID, pageId).setType(Parameter.Type.INT));

		create(TABLE_NAME, SITE_ROWMAP, ID, params);
	}

	public static void delete(Collection pageIdsToDelete) {
		if(0 < pageIdsToDelete.size())
			delete(TABLE_NAME, new Parameter(PAGE_ID, pageIdsToDelete, Parameter.Comparator.IN));
	}
}
