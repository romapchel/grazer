package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Bundle;
import nl.impressie.grazer.model.CustomPayment;
import nl.impressie.grazer.model.Invoice;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * @author Impressie
 */
public class InvoiceDao extends Dao {
	private static final String TABLE_NAME = "invoice";

	private static final String ID = "id";
	private static final String INVOICE_NUMBER = "invoice_number", PAYMENT_ID = "payment_id", BUNDLE_ID = "bundle_id",
			USER_ID = "user_id", TIMESTAMP = "timestamp", DESCRIPTION = "description", COMPANY = "company", NAME = "name", ADRESS = "adress",
			ADRESS2 = "adress2", SUBTOTAL = "subtotal", VAT = "vat", TOTAL = "total";

	private final static RowMapper<Invoice> ROWMAP = (resultSet, i) -> {
		int id = resultSet.getInt(ID);
		String invoiceNumber = resultSet.getString(INVOICE_NUMBER);
		String description = resultSet.getString(DESCRIPTION);
		String company = resultSet.getString(COMPANY);
		String name = resultSet.getString(NAME);
		String adress = resultSet.getString(ADRESS);
		String adress2 = resultSet.getString(ADRESS2);
		int userId = resultSet.getInt(USER_ID);
		Double subtotal = resultSet.getDouble(SUBTOTAL) / 100;
		Double vat = resultSet.getDouble(VAT) / 100;
		Double total = resultSet.getDouble(TOTAL) / 100;
		long timestamp = resultSet.getLong(TIMESTAMP);
		return new Invoice(id, invoiceNumber, description, company, name, adress, adress2, userId, subtotal, vat, total, timestamp);
	};

	public static Invoice findById(Object id) {
		List<Invoice> items = find(TABLE_NAME, ROWMAP, ID, id);
		return unique(items);
	}

	public static List<Invoice> findForUser(int userId) {
		List where = CollectionFactory.createList(new Parameter(USER_ID, userId));
		List sort = CollectionFactory.createList(new Parameter(TIMESTAMP, Parameter.SortOrder.DESCENDING_NULLS_LAST));
		return find(TABLE_NAME, ROWMAP, where, sort);
	}

	public static Invoice findMostRecentForPayment(Object paymentId) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(PAYMENT_ID, paymentId));
		List sort = CollectionFactory.createList();
		sort.add(new Parameter(TIMESTAMP, Parameter.SortOrder.DESCENDING_NULLS_LAST));
		List<Invoice> items = find(TABLE_NAME, ROWMAP, params, sort);
		if(items.size() > 0)
			return items.get(0);

		return null;
	}

	public static Invoice create(CustomPayment payment) {
		Bundle bundle = payment.getBundle();
		double total = bundle.getPriceAsDouble();
		double subtotal = total / 1.21;
		double vat = total - subtotal;
		CustomUser user = payment.getUser();

		List params = CollectionFactory.createList();
		params.add(new Parameter(PAYMENT_ID, payment.getId()));
		params.add(new Parameter(BUNDLE_ID, bundle.getId()));
		params.add(new Parameter(USER_ID, user.getId()));
		params.add(new Parameter(TIMESTAMP, payment.getTimestamp()));
		params.add(new Parameter(DESCRIPTION, bundle.getDescription()));
		params.add(new Parameter(COMPANY, user.getOrganisation()));
		params.add(new Parameter(NAME, user.getFullName()));

		params.add(new Parameter(ADRESS, user.getStreetAddress()));
		params.add(new Parameter(ADRESS2, user.getCityAddress()));

		params.add(new Parameter(SUBTOTAL, subtotal * 100));
		params.add(new Parameter(VAT, vat * 100));
		params.add(new Parameter(TOTAL, total * 100));

		Invoice created = create(TABLE_NAME, ROWMAP, ID, params);

		if(created != null) {
			List where = CollectionFactory.createList();
			Object id = created.getId();
			where.add(new Parameter(ID, id));;
			List set = CollectionFactory.createList();
			set.add(new Parameter(INVOICE_NUMBER, String.format("%s-%s", DateUtil.convertTimestampToFormat(payment.getTimestamp(), "yyyyMMdd"), String.valueOf(id))));

			List<Invoice> updated = update(TABLE_NAME, ROWMAP, where, set);
			created = unique(updated);
		}

		return created;
	}

	public static List<Invoice> getAllForThisMonth() {
		List where = CollectionFactory.createList();
		where.add(new Parameter(TIMESTAMP, DateUtil.getStartOfMonth(), Parameter.Comparator.BEWLOW_OR_EQUAL_TO_STORED));

		List sort = CollectionFactory.createList();
		sort.add(new Parameter(TIMESTAMP));

		return find(TABLE_NAME, ROWMAP, where, sort);
	}
}
