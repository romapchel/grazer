package nl.impressie.grazer.dao;

import nl.impressie.grazer.hibernate.models.UnknownTagModel;
import nl.impressie.grazer.hibernate.services.UnknownTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Impressie
 */
@Component
public class UnknownTagDao {
	private static UnknownTagDao instance;
	@Autowired
	private UnknownTagService service;

	public UnknownTagDao() {
		if(instance == null)
			instance = this;
	}

	public static List<UnknownTagModel> getAll() {
		return instance.service.getAll();
	}

	public static UnknownTagModel find(String name, String type, String url) {
		return instance.service.get(name, type, url);
	}
	public static UnknownTagModel findById(String id) {
		return instance.service.get(id);
	}

	public static UnknownTagModel ignore(int id) {
		UnknownTagModel model = instance.service.get(new Long(id));
		if(model == null)
			return model;

		model.setIgnored(true);
		return instance.service.save(model);
	}

	public static UnknownTagModel create(String type, String name, String url) {
		if(name.length() == 0 || type.length() == 0)
			return null;

		UnknownTagModel tag = find(name, type, url);
		if(tag != null)
			return tag;

		name = name.substring(0, 1).toUpperCase() + name.substring(1);

		UnknownTagModel model = new UnknownTagModel();
		model.setIgnored(false);
		model.setUrl(url);
		model.setType(type);
		model.setTag(name);

		return instance.service.save(model);
	}
}
