package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.AdditionalSourceLink;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.jdbc.core.RowMapper;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class AdditionalSourceLinkDao extends Dao {

	private static final String TABLE_NAME = "website_additional_url";
	private static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public AdditionalSourceLink mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt(ID);
			int pageId = rs.getInt(PAGE_ID);
			String url = rs.getString(URL);

			AdditionalSourceLink tagReplacement = new AdditionalSourceLink(id, pageId, url);
			return tagReplacement;
		}
	};
	private static Map<String, Parameter.Type> parameter_maps;

	public final static String ID = "id", PAGE_ID = "page_id", URL = "url";

	public static AdditionalSourceLink findById(Object id) {
		return unique(find(toParameter(ID, id)));
	}

	public static List<AdditionalSourceLink> find(Parameter... params) {
		return find(CollectionFactory.createList(params));
	}

	public static List<AdditionalSourceLink> find(List params) {
		List sort = CollectionFactory.createList();

		return find(TABLE_NAME, ROWMAP, params, sort);
	}

	public static List<AdditionalSourceLink> findByPageId(Object pageId) {
		return find(new Parameter(PAGE_ID, pageId));
	}

	public static int delete(Object id) {
		return delete(TABLE_NAME, toParameter(ID, id));
	}

	public static AdditionalSourceLink update(int id, int pageId, String url) {
		if(url.length() == 0) {
			delete(id);
			return null;
		}

		List parameters = toParameters(pageId, url);

		return update(id, parameters);
	}

	private static List<Parameter> toParameters(Integer pageId, String url) {
		List parameters = CollectionFactory.createList();

		if(pageId != null)
			parameters.add(toParameter(PAGE_ID, pageId));

		if(url != null)
			parameters.add(toParameter(URL, url));

		return parameters;
	}

	public static AdditionalSourceLink create(List parameters) {
		return (AdditionalSourceLink)create(TABLE_NAME, ROWMAP, ID, parameters);
	}

	public static List<AdditionalSourceLink> save(HttpServletRequest request, int pageId) {
		List<AdditionalSourceLink> saved = CollectionFactory.createList();
		for(Map<String, String> replacement: getParametersFromRequest(request, Website.Types.ADDITIONAL_SOURCE_LINK)) {
			Integer id = NumberUtils.toInt(safeGetParameter(replacement, ID), 0);
			String url = safeGetParameter(replacement, URL, "");
			AdditionalSourceLink model = update(id, pageId, url);
			if(saved != null)
				saved.add(model);
		}
		return saved;
	}

	public static AdditionalSourceLink update(Object id, List updateParameters) {
		if(null == findById(id)) return create(updateParameters);

		List where = CollectionFactory.createList();
		where.add(toParameter(ID, id));
		return (AdditionalSourceLink)unique(update(TABLE_NAME, ROWMAP, where, updateParameters));
	}

	private static void setUpParameterTypes() {
		parameter_maps = CollectionFactory.createMap();
		parameter_maps.put(ID, Parameter.Type.INT);
		parameter_maps.put(PAGE_ID, Parameter.Type.INT);
		parameter_maps.put(URL, Parameter.Type.STRING);
	}

	public static Parameter setType(Parameter param) {
		if(null == parameter_maps) setUpParameterTypes();

		String column = param.getColumn();

		if(parameter_maps.containsKey(column)) param.setType(parameter_maps.get(column));

		return param;
	}

	private static Parameter toParameter(String column, Object value) {
		Parameter param = new Parameter(column, value);
		return setType(param);
	}

	public static AdditionalSourceLink updateOrCreate(Map values) {
		List params = toParameters(
				Integer.valueOf(safeGetParameter(values, PAGE_ID, "0")),
				safeGetParameter(values, URL, "")
		);

		String id = safeGetParameter(values, ID, "0");
		AdditionalSourceLink current = findById(id);

		if(current == null) {
			if(!(id.equals("0")))
				params.add(toParameter(ID, id));
			return create(params);
		} else {
			return update(id, params);
		}
	}

	public static int removeForPage(Object pageId) {
		return delete(TABLE_NAME, new Parameter(PAGE_ID, pageId));
	}
}
