package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.User.Role;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Impressie
 */
public class RoleDao extends Dao {
	public static final String ID = "id", NAME = "role_name", USERNAME = "username", END_DATE = "end_date";

	public static final String TABLE_NAME = "user_role";
	public static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt(ID);
			String name = rs.getString(NAME);
			String username = rs.getString(USERNAME);
			int end = rs.getInt(END_DATE);

			Role role = new Role(id, name, username, end);
			return role;
		}
	};

	public static Role updateEnd(int Id, int EndDate) {
		List whereParameters = CollectionFactory.createList();
		whereParameters.add(new Parameter(ID, Id));

		List setParameters = CollectionFactory.createList();
		setParameters.add(new Parameter(END_DATE, EndDate));

		List roles = update(TABLE_NAME, ROWMAP, whereParameters, setParameters);
		return (Role) roles.get(0);

	}

	public static Role update(int Id, String Name, String UserName, int EndDate) {
		List whereParameters = CollectionFactory.createList();
		whereParameters.add(new Parameter(ID, Id));

		List setParameters = getSetParameters(Name, UserName, EndDate);

		List roles = update(TABLE_NAME, ROWMAP, whereParameters, setParameters);
		return (Role) roles.get(0);
	}

	public static List getSetParameters(String Name, String UserName, int EndDate) {
		List parameters = CollectionFactory.createList();

		parameters.add(new Parameter(NAME, Name));
		parameters.add(new Parameter(USERNAME, UserName));
		parameters.add(new Parameter(END_DATE, EndDate));

		return parameters;
	}

	public static List findActiveForUser(String Username) {
		return findActiveForUser(Username, "");
	}

	public static List<Role> findActiveForUser(String Username, String Rolename) {
		List parameters = CollectionFactory.createList();
		parameters.add(new Parameter(USERNAME, Username));

		if(Rolename.length() > 0)
			parameters.add(new Parameter(NAME, Rolename));

		List sortParameters = CollectionFactory.createList();
		sortParameters.add(new Parameter(END_DATE, false));

		return find(parameters, sortParameters);
	}

	public static Role findRoleForUser(String Username, String Rolename) {
		List where = CollectionFactory.createList();
		where.add(new Parameter(USERNAME, Username));
		where.add(new Parameter(NAME, Rolename));

		return (Role) unique(find(TABLE_NAME, ROWMAP, where));
	}

	public static List<Role> findAllForUser(String Username) {
		List parameters = CollectionFactory.createList();
		parameters.add(new Parameter(USERNAME, Username));

		List sortParameters = CollectionFactory.createList();
		sortParameters.add(new Parameter(END_DATE, false));

		return find(parameters, sortParameters);
	}

	public static List<Role> find(List parameters, List sortParameters) {
		return (List<Role>) find(TABLE_NAME, ROWMAP, parameters, sortParameters);
	}

	public static Role create(String roleName, String userName, int endDate) {
		Role role = findRoleForUser(userName, roleName);
		if(null != role) {

		}
		return (Role) create(TABLE_NAME, ROWMAP, ID, getSetParameters(roleName, userName, endDate));
	}

}
