package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.User.Role;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class TypeRoleDao extends Dao {

	private final static String TABLE_NAME = "type_role";
	private final static RowMapper ROWMAP = new RowMapper() {
		@Override
		public String mapRow(ResultSet resultSet, int i) throws SQLException {
			return resultSet.getString(ROLE_NAME);
		}
	};

	public static final String ID = "id", TYPE_ID = "type_id", ROLE_NAME = "role_name";

	public static List findByTypeId(Object typeId) {
		return find(TABLE_NAME, ROWMAP, TYPE_ID, typeId);
	}

	public static List findAuthoritieListByTypeId(Object typeId) {
		Map authorityMap = findAuthoritieMapByTypeId(typeId);
		List authourities = CollectionFactory.createList();
		authourities.addAll(authorityMap.values());

		return authourities;
	}

	public static Map findAuthoritieMapByTypeId(Object typeId) {
		Map authorityMap = CollectionFactory.createMap();

		List<String> roles = findByTypeId(typeId);
		for(String role : roles) {
			if(authorityMap.containsKey(role)) continue;

			authorityMap.put(role, new SimpleGrantedAuthority(role));
		}
		if(!authorityMap.containsKey(Role.NONE))
			authorityMap.put(Role.NONE, new SimpleGrantedAuthority(Role.NONE));

		return authorityMap;
	}
}
