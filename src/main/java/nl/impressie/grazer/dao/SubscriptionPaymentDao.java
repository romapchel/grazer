package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.CustomPayment;
import nl.impressie.grazer.model.Invoice;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static nl.impressie.grazer.dao.SubscriptionPaymentDao.Attribute.PAYMENT_ID;
import static nl.impressie.grazer.dao.SubscriptionPaymentDao.Attribute.TIMESTAMP;

/**
 * @author Impressie
 */
public class SubscriptionPaymentDao extends Dao {
	public enum Attribute {
		ID("id"),
		PAYMENT_ID("payment_id", Parameter.Type.INT),
		INVOICE_ID("invoice_id", Parameter.Type.INT),
		TIMESTAMP("timestamp"),
		MOLLIE_PAYMENT_ID("mollie_payment_id");

		private String columnName;
		private String columnTitle;
		private Parameter.Type type;

		Attribute(String columnName) {
			this(columnName, columnName);
		}
		Attribute(String columnName, String columnTitle) {
			this(columnName, columnTitle, null);
		}
		Attribute(String columnName, Parameter.Type type) {
			this(columnName, columnName, type);
		}
		Attribute(String columnName, String columnTitle, Parameter.Type type) {
			this.columnName = columnName;
			this.columnTitle = columnTitle;
			this.type = type;
		}

		public Parameter getParameter(Object value) {
			Parameter parameter = new Parameter(columnName, value);
			if(type != null)
				parameter.setType(type);

			return parameter;
		}

		public Parameter getParameter(Object value, Parameter.Comparator comparator) {
			return getParameter(value).setComparator(comparator);
		}

		public String getColumnTitle() { return columnTitle; }

		public Parameter getSortParameter(Parameter.SortOrder sortOrder) {
			return new Parameter(columnName, sortOrder);
		}
	}

	private static final String TABLE_NAME = "subscription_payment";
	private static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public Object mapRow(ResultSet resultSet, int i) throws SQLException {
			Map map = CollectionFactory.createMap();
			for(Attribute attribute: Attribute.values()) {
				map.put(attribute.columnTitle, resultSet.getObject(attribute.columnName));
			}
			return map;
		}
	};

	public static boolean paymentAlreadyHandled(String mollieId) {
		int occurances = count(TABLE_NAME, Attribute.MOLLIE_PAYMENT_ID.getParameter(mollieId));
		return occurances > 0;
	}

	public static void create(CustomPayment payment, String mollieId, Invoice invoice) {
		List parameters = CollectionFactory.createList();
		parameters.add(Attribute.PAYMENT_ID.getParameter(payment.getId()));
		parameters.add(Attribute.INVOICE_ID.getParameter(invoice.getId()));
		parameters.add(Attribute.MOLLIE_PAYMENT_ID.getParameter(mollieId));
		parameters.add(TIMESTAMP.getParameter(DateUtil.getTimestamp()));

		create(TABLE_NAME, ROWMAP, Attribute.ID.columnName, parameters);
	}

	public static Map findMostRecentForPayment(Object paymentId) {
		List<Parameter> params = CollectionFactory.createList(PAYMENT_ID.getParameter(paymentId));
		List<Parameter> sort = CollectionFactory.createList(TIMESTAMP.getSortParameter(Parameter.SortOrder.DESCENDING_NULLS_LAST));

		List<Map> items = find(TABLE_NAME, ROWMAP, params, sort);
		if(items.size() > 0)
			return items.get(0);

		return null;
	}

	public static List getPayments(CustomPayment payment) {
		return find(TABLE_NAME, ROWMAP, Attribute.PAYMENT_ID.getParameter(payment.getId()));
	}
}
