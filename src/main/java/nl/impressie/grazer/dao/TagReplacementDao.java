package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.TagReplacement;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.jdbc.core.RowMapper;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class TagReplacementDao extends Dao {

	private static final String TABLE_NAME = "website_tag_replacement";
	private static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public TagReplacement mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt(ID);
			int pageId = rs.getInt(PAGE_ID);
			String type = rs.getString(TAG_TYPE);
			String text = rs.getString(REPLACE_TEXT);
			String with = rs.getString(REPLACE_WITH);
			TagReplacement tagReplacement = new TagReplacement(id, pageId, type, text, with);
			return tagReplacement;
		}
	};
	private static Map<String, Parameter.Type> parameter_maps;

	public final static String ID = "id", PAGE_ID = "page_id", TAG_TYPE = "tag_type", REPLACE_TEXT = "replace_text", REPLACE_WITH = "replace_with";

	public static TagReplacement findById(Object id) {
		return unique(find(toParameter(ID, id)));
	}

	public static List<TagReplacement> find(Parameter... params) {
		return find(CollectionFactory.createList(params));
	}

	public static List<TagReplacement> find(List params) {
		List sort = CollectionFactory.createList();

		return find(TABLE_NAME, ROWMAP, params, sort);
	}

	public static List<TagReplacement> findByPageId(Object pageId) {
		return find(new Parameter(PAGE_ID, pageId));
	}

	public static int delete(Object id) {
		return delete(TABLE_NAME, toParameter(ID, id));
	}

	public static TagReplacement update(int id, int pageId, String type, String text, String with) {
		if(text.length() == 0 && with.length() == 0) {
			delete(id);
			return null;
		}

		List parameters = toParameters(pageId, type, text, with);

		return update(id, parameters);
	}

	private static List<Parameter> toParameters(Integer pageId, String type, String text, String with) {
		List parameters = CollectionFactory.createList();

		if(pageId != null)
			parameters.add(toParameter(PAGE_ID, pageId));

		if(type != null)
			parameters.add(toParameter(TAG_TYPE, type));

		if(text != null)
			parameters.add(toParameter(REPLACE_TEXT, text));

		if(with != null)
			parameters.add(toParameter(REPLACE_WITH, with));

		return parameters;
	}

	public static TagReplacement create(List parameters) {
		return (TagReplacement)create(TABLE_NAME, ROWMAP, ID, parameters);
	}

	public static List<TagReplacement> save(HttpServletRequest request, int pageId) {
		List savedReplacements = CollectionFactory.createList();
		for(Map<String, String> replacement: getParametersFromRequest(request, Website.Types.TAG_REPLACEMENT)) {
			Integer id = NumberUtils.toInt(safeGetParameter(replacement, ID), 0);
			String type = safeGetParameter(replacement, TAG_TYPE, "");
			String text = safeGetParameter(replacement, REPLACE_TEXT, "");
			String replacementText = safeGetParameter(replacement, REPLACE_WITH, "");
			TagReplacement saved = update(id, pageId, type, text, replacementText);
			if(saved != null)
				savedReplacements.add(saved);
		}
		return savedReplacements;
	}

	public static TagReplacement update(Object id, List updateParameters) {
		if(null == findById(id)) return create(updateParameters);

		List where = CollectionFactory.createList();
		where.add(toParameter(ID, id));
		return (TagReplacement)unique(update(TABLE_NAME, ROWMAP, where, updateParameters));
	}

	private static void setUpParameterTypes() {
		parameter_maps = CollectionFactory.createMap();
		parameter_maps.put(ID, Parameter.Type.INT);
		parameter_maps.put(PAGE_ID, Parameter.Type.INT);
		parameter_maps.put(TAG_TYPE, Parameter.Type.STRING);
		parameter_maps.put(REPLACE_TEXT, Parameter.Type.STRING);
		parameter_maps.put(REPLACE_WITH, Parameter.Type.STRING);
	}

	public static Parameter setType(Parameter param) {
		if(null == parameter_maps) setUpParameterTypes();

		String column = param.getColumn();

		if(parameter_maps.containsKey(column)) param.setType(parameter_maps.get(column));

		return param;
	}

	private static Parameter toParameter(String column, Object value) {
		Parameter param = new Parameter(column, value);
		return setType(param);
	}

	public static TagReplacement updateOrCreate(Map values) {
		List params = toParameters(
				Integer.valueOf(safeGetParameter(values, PAGE_ID, "0")),
				safeGetParameter(values, TAG_TYPE, ""),
				safeGetParameter(values, REPLACE_TEXT, ""),
				safeGetParameter(values, REPLACE_WITH, "")
		);

		String id = safeGetParameter(values, ID, "0");
		TagReplacement tag = findById(id);

		if(tag == null) {
			if(!(id.equals("0"))) {
				Parameter idParam = new Parameter(ID, id);
				idParam.setType(Parameter.Type.INT);
				params.add(idParam);
			}
			return create(params);
		} else {
			return update(id, params);
		}

	}

	public static int removeForPage(Object pageId) {
		return delete(TABLE_NAME, new Parameter(PAGE_ID, pageId));
	}
}
