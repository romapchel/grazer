package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.NewSearchTerm;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.SearchUtil;
import nl.impressie.grazer.util.XMLUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class NewSearchTermDao extends Dao {
	public static final String ID = "id", USER_ID = "user_id", TERM = "term", TIME = "timestamp", TYPE = "type";

	private static final String TABLE_NAME = "new_search_term";
	private static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public NewSearchTerm mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt(ID);
			int userId = rs.getInt(USER_ID);
			String searchterm = rs.getString(TERM);
			Long made = rs.getLong(TIME);
			String type = rs.getString(TYPE);

			NewSearchTerm term = new NewSearchTerm(id, userId, searchterm, made, type);

			return term;
		}
	};

	public static NewSearchTerm create(int UserId, String Term, String Type) {
		Long made = DateUtil.getTimestamp();

		return (NewSearchTerm) create(TABLE_NAME, ROWMAP, ID, getSetParameters(UserId, Term, made, Type));
	}

	private static List getSetParameters(int UserId, String Term, Long Timestamp, String Type) {
		List setParameters = CollectionFactory.createList();

		if(UserId != 0)
			setParameters.add(new Parameter(USER_ID, UserId));

		setParameters.add(new Parameter(TERM, Term));
		setParameters.add(new Parameter(TIME, Timestamp));
		setParameters.add(new Parameter(TYPE, Type));

		return setParameters;
	}

	public static List<NewSearchTerm> getAllByDate() {
		List whereParams = CollectionFactory.createList();
		List sortParams = CollectionFactory.createList();
		sortParams.add(new Parameter(TIME, Parameter.SortOrder.DESCENDING_NULLS_LAST));

		List<NewSearchTerm> all = find(TABLE_NAME, ROWMAP, whereParams, sortParams);
		Map<String, NewSearchTerm> found = CollectionFactory.createMap();
		List withFrequency = CollectionFactory.createList();
		for(NewSearchTerm searchTerm : all) {
			String term = searchTerm.getTerm();
			if(found.containsKey(term)) {
				found.get(term).increaseFrequency();
				continue;
			}

			found.put(term, searchTerm);
			withFrequency.add(searchTerm);
		}

		return withFrequency;
	}

	public static void addNewTerms(String LongTerm, String Type) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		int userId = 0;
		if(user != null)
			userId = user.getId();
		for(String term : SearchUtil.splitTerms(LongTerm)) {
			if(isNewTerm(term))
				create(userId, term, Type);
		}
	}

	private static boolean isNewTerm(String term) {
		return !XMLUtil.getThesaurusTerms().contains(term);
	}

	public static void deleteOlderThan(Long Timestamp) {
		List<Parameter> params = CollectionFactory.createList();
		params.add(new Parameter(TIME, Timestamp, Parameter.Comparator.ABOVE_STORED));
		delete(TABLE_NAME, params);
	}
}
