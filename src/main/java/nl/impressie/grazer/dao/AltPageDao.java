package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.AltPage;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
public class AltPageDao extends Dao {
	public static final String TABLE_NAME = "altpage";
	public static final String ID = "id", PAGE_ID = "page_id", MOMENT = "moment", URL = "url", METHOD = "method", DATA = "data";
	public static final RowMapper<AltPage> ROWMAP = (rs, rowNum) -> {
		int id = rs.getInt(ID);
		int pageId = rs.getInt(PAGE_ID);
		String moment = rs.getString(MOMENT);
		String url = rs.getString(URL);

		boolean pre = !moment.equals(AltPage.POST);

		String method = rs.getString(METHOD);
		String data = rs.getString(DATA);

		AltPage altPage = new AltPage(id, url, pre, pageId, method, data);

		return altPage;
	};

	public static List<AltPage> update(List where, List update) {
		List list = CollectionFactory.createList();

		if(where == null || 0 == find(where).size()) {
			AltPage newPage = (AltPage) create(TABLE_NAME, ROWMAP, ID, update);
			list.add(newPage);
		} else {
			list = update(TABLE_NAME, ROWMAP, where, update);
		}

		return list;
	}

	public static List<AltPage> findByPageId(Object pageId) {
		return findByPageId(pageId, "");
	}

	public static List<AltPage> findByPageId(Object pageId, String type) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(PAGE_ID, pageId));
		if(null != type && 0 < type.length())
			params.add(new Parameter(MOMENT, type));

		return find(params);
	}

	public static AltPage findById(Object id) {
		Parameter primaryKey = new Parameter(ID, id);
		return (AltPage) unique(find(primaryKey));
	}

	public static List<AltPage> find(Parameter... params) {
		return find(CollectionFactory.createList(params));
	}

	public static List<AltPage> find(List params) {
		return find(TABLE_NAME, ROWMAP, params);
	}

	public static AltPage create(Object pageId, String url, boolean pre) {
		if(url.length() == 0)
			return null;

		String type = (pre) ? AltPage.PRE : AltPage.POST;

		List insertList = CollectionFactory.createList();
		insertList.add(new Parameter(PAGE_ID, pageId));
		insertList.add(new Parameter(URL, url));
		insertList.add(new Parameter(MOMENT, type));

		List<AltPage> list = update(null, insertList);
		if(1 == list.size())
			return list.get(0);
		return null;
	}

	public static void deleteByPageId(Object pageId) {
		Parameter primaryKey = new Parameter(PAGE_ID, pageId);
		delete(TABLE_NAME, primaryKey);
	}

	public static Set<String> getColumns() {
		return CollectionFactory.createSet(ID, PAGE_ID, URL, MOMENT);
	}

	public static AltPage updateOrCreate(Map values) {
		values = prepValues(values);
		return updateOrCreate(values, TABLE_NAME, ROWMAP, ID, getColumns(), ID);
	}

	protected static Map prepValues(Map<String, Object> values) {
		for(String key: values.keySet()) {
			Object val = values.get(key);
			switch(key) {
				case ID:
				case PAGE_ID:
					val = Integer.valueOf(val.toString());
				default:

			}
			values.put(key, val);
		}
		return values;
	}
}
