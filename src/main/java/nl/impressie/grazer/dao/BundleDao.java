package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Bundle;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * @author Impressie
 */
public class BundleDao extends Dao {
	public static final String ID = "id", PRICE = "price", TIMES = "times", INTERVAL_NUMBER = "interval_number", INTERVAL_TYPE = "interval_type", DESCRIPTION = "description", TYPE_ID = "type_id";

	private static final String TABLE_NAME = "bundle";
	private static final RowMapper<Bundle> ROWMAP = (rs, rowNum) -> {
		int id = rs.getInt(ID);
		int price = rs.getInt(PRICE);
		int times = rs.getInt(TIMES);
		int intervalNmbr = rs.getInt(INTERVAL_NUMBER);
		String intervalType = rs.getString(INTERVAL_TYPE);
		String description = rs.getString(DESCRIPTION);
		int type_id = rs.getInt(TYPE_ID);

		Bundle bundle = new Bundle(id, price, times, intervalNmbr, intervalType, description, type_id);

		return bundle;
	};

	public static Bundle create(int price, int times, int intervalNmbr, String intervalType, String description) {
		return create(price, times, intervalNmbr, intervalType, description, AccountTypeDao.getUserAcoountId());
	}
	public static Bundle create(int price, int times, int intervalNmbr, String intervalType, String description, int typeId) {
		List set = createSetList(price, times, intervalNmbr, intervalType, description);
		set.add(new Parameter(TYPE_ID, typeId));

		return (Bundle) create(TABLE_NAME, ROWMAP, ID, set);
	}

	public static List<Bundle> getAll() {
		List where = CollectionFactory.createList();
		where.add(new Parameter(ID, 0, Parameter.Comparator.BELOW_STORED));

		return find(TABLE_NAME, ROWMAP, where);
	}

	public static Bundle findById(Object id) {
		return (Bundle) unique(find(TABLE_NAME, ROWMAP, ID, id));
	}

	private static List createSetList(int price, int times, int intervalNumber, String intervalType, String description) {
		List set = CollectionFactory.createList();
		set.add(new Parameter(PRICE, price));
		set.add(new Parameter(TIMES, times));
		set.add(new Parameter(INTERVAL_NUMBER, intervalNumber));
		set.add(new Parameter(INTERVAL_TYPE, intervalType));
		set.add(new Parameter(DESCRIPTION, description));

		return set;
	}

	public static Bundle update(int id, int price, int times, int intervalNumber, String intervalType, String description) {
		List where = CollectionFactory.createList();
		where.add(new Parameter(ID, id));
		List set = createSetList(price, times, intervalNumber, intervalType, description);

		List<Bundle> bundles = update(TABLE_NAME, ROWMAP, where, set);
		return unique(bundles);
	}

	public static int delete(Object id) {
		return delete(TABLE_NAME, new Parameter(ID, id));
	}
}
