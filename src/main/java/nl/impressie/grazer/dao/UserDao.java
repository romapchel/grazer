package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Verification;
import nl.impressie.grazer.model.KeyValue;
import nl.impressie.grazer.model.Mail.VerificationMail;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.DatabaseUtils.CRUDObject;
import nl.impressie.grazer.util.DatabaseUtils.DBC;
import nl.impressie.grazer.util.DatabaseUtils.DBQueryObject;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class UserDao extends Dao {
	public static final String ID = "id", USERNAME = "username", PASSWORD = "password", ORGANISATION = "organisation", INITIALS = "initials",
			TUSSENVOEGSELS = "tsvg", LASTNAME = "lastname", MAIL = "mail", MOLLIE_ID = "mollie_id", TYPE_ID = "type_id",
			STREET = "street", HOUSENUMBER = "housenumber", ZIP = "zip", CITY = "city",
			ACTIVE_PROFILE_ID = "active_profile_id", ENABLED = "enabled", LAST_LOGIN = "last_login", CURRENT_LOGIN = "current_login";

	private static final String TABLE_NAME = "user";
	private static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public CustomUser mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt(ID);

			String username = rs.getString(USERNAME);
			String password = rs.getString(PASSWORD);
			String mollieId = rs.getString(MOLLIE_ID);

			String organisation = rs.getString(ORGANISATION);
			String initials = rs.getString(INITIALS);
			String tsvg = rs.getString(TUSSENVOEGSELS);
			String lastname = rs.getString(LASTNAME);
			String mail = rs.getString(MAIL);

			String street = rs.getString(STREET);
			String housenumber = rs.getString(HOUSENUMBER);
			String zip = rs.getString(ZIP);
			String city = rs.getString(CITY);

			int activeProfile = rs.getInt(ACTIVE_PROFILE_ID);
			boolean enabled = rs.getBoolean(ENABLED);
			int lastLogin = rs.getInt(LAST_LOGIN);
			int currentLogin = rs.getInt(CURRENT_LOGIN);

			CustomUser user = new CustomUser(id, username, password, mollieId,
					organisation, initials, tsvg, lastname, mail, activeProfile,
					enabled, lastLogin, currentLogin, street, housenumber, zip, city);
			return user;
		}
	};

	public static CustomUser findById(Object id) {
		return find(new Parameter(ID, String.valueOf(id)));
	}

	/**
	 * @param username The username that is linked to the user account that should be retrieved
	 * @return a custom user object
	 */
	public static CustomUser findByUsername(String username) {
		return findByUsername(username, false);
	}
	public static CustomUser findByUsername(String username, boolean enabledOnly) {
		Parameter parameter = new Parameter(USERNAME, username);

		if(enabledOnly)
			return find(new Parameter(ENABLED, true), parameter);

		return find(parameter);
	}

	private static CustomUser find(Parameter... params) {
		return (CustomUser) unique(find(TABLE_NAME, ROWMAP, params));
	}

	public static int delete(Object id) {
		return delete((int)id);
	}
	public static int delete(int id) {
		List primary = CollectionFactory.createList();
		primary.add(new Parameter(ID, id));

		return delete(TABLE_NAME, primary);
	}

	public static int deleteAll() {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.setMode(DBQueryObject.DELETE);
		queryObject.addTable(TABLE_NAME);

		return DBC.getInstance().runQueryObject(queryObject);
	}

	public static CustomUser create(HttpServletRequest request) {
		return create(UserDao.getParamaters(request));
	}

	public static CustomUser create(List<Parameter> parameters) {
		return create(parameters, true);
	}
	public static CustomUser create(List<Parameter> parameters, boolean sendVerificationMail) {
		boolean typeSet = false;
		CustomUser currentUser = AuthenticationUtil.getCurrentUser();
		if(null != currentUser && currentUser.isAdmin()) {
			for(Parameter parameter : parameters) {
				if(TYPE_ID.equals(parameter.getColumn())) {
					typeSet = true;
					break;
				}
			}
		}
		if(!typeSet)
			parameters.add(new Parameter(TYPE_ID, AccountTypeDao.DEFAULT_TYPE_ID));

		parameters.add(new Parameter(ENABLED, false));
		CustomUser user = (CustomUser) create(TABLE_NAME, ROWMAP, ID, parameters);

		if(null != user) {
			Verification ver = VerificationDao.generate(user);
			if(sendVerificationMail) {
				if(ver == null) {
					user.delete();
					return null;
				}
				VerificationMail verificationMail = new VerificationMail(user, ver);
				verificationMail.send();
			} else {
				ver.activateUser();
				ver.delete();
			}
		}

		return user;
	}

	public static CustomUser update(Object id, List<Parameter> parameters) {
		CustomUser user = UserDao.findById(id);
		if(user == null)
			user = create(parameters);

		List where = CollectionFactory.createList();
		where.add(new Parameter(ID, id));

		List updated = update(where, parameters);
		if(1 == updated.size())
			user = (CustomUser) updated.get(0);
		AuthenticationUtil.addToUpdatedUsers(user.getId());
		return user;
	}

	public static List update(List<Parameter> where, List<Parameter> parameters) {
		return update(TABLE_NAME, ROWMAP, where, parameters);
	}

	public static int update(int id, Map newValues) {
		CustomUser OP = UserDao.findById(id);
		if(OP == null)
			return 0;

		List params = CollectionFactory.createList();
		params.add(new KeyValue("id", OP.getId()));

		List updates = CollectionFactory.createList();
		for(Object key : newValues.keySet()) {
			updates.add(new KeyValue(key, newValues.get(key)));
		}
		AuthenticationUtil.addToUpdatedUsers(id);
		return CRUDObject.update(TABLE_NAME, params, updates);
	}

	public static List getAll() {
		return find(TABLE_NAME, ROWMAP, CollectionFactory.createList());
	}

	public static boolean isValidPassword(String password) {
		return password.length() > 0;
	}

	public static boolean setActiveProfileId(int profileId, int userId) {
		//make sure the profile actually exists and belongs to this user or is global
		Profile prof = ProfileDao.findById(profileId);
		int profUserId = prof.getUserId();
		if((prof == null) || (profUserId != 0 && profUserId != userId))
			return false;

		CRUDObject.update(TABLE_NAME, new KeyValue(ID, userId), new KeyValue(ACTIVE_PROFILE_ID, profileId));
		return true;
	}

	public static Map loadAccountValues(String Username, String Organisation, String Initials, String Tsvg, String Lastname,
										String street, String housenumber, String zip, String city, String Mail) {
		Map<String, Object> values = CollectionFactory.createMap();
		values.put(USERNAME, Username);
		values.put(ORGANISATION, Organisation);
		values.put(INITIALS, Initials);
		values.put(TUSSENVOEGSELS, Tsvg);
		values.put(LASTNAME, Lastname);
		values.put(STREET, street);
		values.put(HOUSENUMBER, housenumber);
		values.put(ZIP, zip);
		values.put(CITY, city);
		values.put(MAIL, Mail);

		return values;
	}

	public static Map validateCreateOrUpdate(Map requestParameters, boolean ignoreMissing) {
		boolean update = false;
		boolean checkUnique = true;
		if(requestParameters.containsKey(ID)) {
			CustomUser byId = findById(safeGetParameter(requestParameters, ID));
			update = (byId instanceof CustomUser);
			if(update)
				checkUnique =  !(byId.getUsername().trim().toLowerCase().equals(safeGetParameter(requestParameters, USERNAME).trim().toLowerCase()));
		}
		if(update)
			return validateUpdate(requestParameters, ignoreMissing, checkUnique);
		else
			return validateCreate(requestParameters, ignoreMissing, true);
	}

	public static Map validateUpdate(Map requestParameters, boolean ignoreMissing, boolean checkUnique) {
		Map errors = validateCreate(requestParameters, ignoreMissing, checkUnique);

		validateMissing(ignoreMissing, ID, requestParameters, errors);
		validateNumber(ignoreMissing, ID, requestParameters, errors);

		return errors;
	}

	public static Map validateCreate(Map requestParameters, boolean ignoreMissing) {
		return validateCreate(requestParameters, ignoreMissing, true);
	}

	private static Map validateCreate(Map requestParameters, boolean ignoreMissing, boolean checkUnique) {
		Map errors = CollectionFactory.createMap();

		validateMissing(ignoreMissing, USERNAME, requestParameters, errors);
		validateLength(ignoreMissing, USERNAME, requestParameters, errors, 3, 500);
		if(checkUnique)
			validateUnique(TABLE_NAME, ignoreMissing, USERNAME, requestParameters, errors);

		validateMissing(ignoreMissing, PASSWORD, requestParameters, errors);
		validateLength(ignoreMissing, PASSWORD, requestParameters, errors, 1, 72);
		validateDuplicate(ignoreMissing, PASSWORD, requestParameters, errors);

		validateLength(ignoreMissing, ORGANISATION, requestParameters, errors, 0, 500);
		validateLength(ignoreMissing, INITIALS, requestParameters, errors, 1, 500);
		validateLength(ignoreMissing, TUSSENVOEGSELS, requestParameters, errors, 0, 500);

		validateMissing(ignoreMissing, LASTNAME, requestParameters, errors);

		validateMissing(ignoreMissing, MAIL, requestParameters, errors);

		validateMissing(ignoreMissing, STREET, requestParameters, errors);

		validateMissing(ignoreMissing, HOUSENUMBER, requestParameters, errors);

		validateMissing(ignoreMissing, ZIP, requestParameters, errors);

		validateMissing(ignoreMissing, CITY, requestParameters, errors);

		return errors;
	}

	public static List<Parameter> getParamaters(HttpServletRequest request) {
		return getParamaters(request.getParameterMap());
	}
	public static List<Parameter> getParamaters(Map requestMap) {
		List<Parameter> params = CollectionFactory.createList();

		addParam(params, requestMap, USERNAME, false);
		Parameter pass = addParam(params, requestMap, PASSWORD, false);
		//encrypt password
		if(pass != null)
			pass.encryptParameter(new BCryptPasswordEncoder());

		addParam(params, requestMap, ORGANISATION);
		addParam(params, requestMap, INITIALS);
		addParam(params, requestMap, TUSSENVOEGSELS);
		addParam(params, requestMap, LASTNAME);
		addParam(params, requestMap, STREET);
		addParam(params, requestMap, HOUSENUMBER);
		addParam(params, requestMap, ZIP);
		addParam(params, requestMap, CITY);
		addParam(params, requestMap, MAIL, false);

		return params;
	}
}
