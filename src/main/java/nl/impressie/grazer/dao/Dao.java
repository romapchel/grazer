package nl.impressie.grazer.dao;

import nl.impressie.grazer.controller.Controller;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.JSPMessage;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.DatabaseUtils.DBC;
import nl.impressie.grazer.util.DatabaseUtils.DBQueryObject;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.LogUtil;
import org.springframework.jdbc.core.RowMapper;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
abstract class Dao {

	protected static <T> List<T> find(String TableName, RowMapper<T> RowMapper, String Column, Object value) {
		List<Parameter> params = CollectionFactory.createList();
		params.add(new Parameter(Column, value));

		return find(TableName, RowMapper, params);
	}

	protected static <T> List<T> find(String TableName, RowMapper<T> RowMapper, Parameter... params) {
		return find(TableName, RowMapper, CollectionFactory.createList(params), CollectionFactory.createList());
	}

	protected static <T> List<T> find(String TableName, RowMapper<T> RowMapper, List<Parameter> parameters) {
		return find(TableName, RowMapper, parameters, CollectionFactory.createList());
	}

	protected static <T> List<T> find(String TableName, RowMapper<T> RowMapper, List<Parameter> parameters, List<Parameter> Sortparameters) {
		return find(TableName, RowMapper, parameters, Sortparameters, CollectionFactory.createList());
	}

	protected static <T> List<T> find(String TableName, RowMapper<T> RowMapper, List<Parameter> parameters, List<Parameter> Sortparameters, List<Parameter> groupParameters) {

		DBQueryObject queryObject = createDBQueryObject(TableName, parameters, Sortparameters, groupParameters);
		List results = CollectionFactory.createList();
		try {
			List found = (List) DBC.getInstance().runQueryObjectSearch(queryObject, RowMapper);
			for(Object o : found) {
				if(o != null)
					results.add(o);
			}
		} catch(Exception e) {
			LogUtil.logException(e);
		}

		return results;
	}

	private static DBQueryObject createDBQueryObject(String TableName, List<Parameter> parameters, List<Parameter> Sortparameters, List<Parameter> groupParameters) {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.setMode(DBQueryObject.SELECT);
		queryObject.addTable(TableName);

		for(Parameter param : Sortparameters) {
			queryObject.addOrderBy(param);
		}

		for(Parameter param : parameters) {
			queryObject.addWhereParameter(param);
		}

		for(Parameter param : groupParameters) {
			queryObject.addGroupParameter(param);
		}

		return queryObject;
	}

	protected static int count(String TableName, Parameter... parameters) {
		return count(TableName, CollectionFactory.createList(parameters));
	}
	protected static int count(String TableName, List<Parameter> parameters) {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.setMode(DBQueryObject.COUNT);
		queryObject.addTable(TableName);

		for(Parameter param : parameters) {
			queryObject.addWhereParameter(param);
		}

		return DBC.getInstance().countQueryObjectSearch(queryObject);
	}

	protected static <T> List<T> update(String TableName, RowMapper<T> RowMap, List<Parameter> Whereparameters, List<Parameter> Setparameters) {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.addTable(TableName);
		queryObject.setMode(DBQueryObject.UPDATE);


		for(Parameter paramater : Whereparameters) {
			queryObject.addWhereParameter(paramater);
		}
		for(Parameter paramater : Setparameters) {
			queryObject.addUpdatedValue(paramater);
		}

		DBC.getInstance().runQueryObject(queryObject);

		return find(TableName, RowMap, Whereparameters);
	}

	protected static <T> T create(String TableName, RowMapper<T> RowMap, String IdColumn, List<Parameter> Setparameters) {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.addTable(TableName);
		queryObject.setMode(DBQueryObject.INSERT);

		for(Parameter paramater : Setparameters) {
			queryObject.addUpdatedValue(paramater);
		}

		int id = DBC.getInstance().insert(queryObject);

		return unique(find(TableName, RowMap, IdColumn, id));
	}

	protected static int delete(String TableName, Parameter... parameters) {
		return delete(TableName, CollectionFactory.createList(parameters));
	}

	protected static int delete(String TableName, List<Parameter> Parameters) {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.addTable(TableName);
		queryObject.setMode(DBQueryObject.DELETE);


		for(Parameter paramater : Parameters) {
			queryObject.addWhereParameter(paramater);
		}

		DBC.getInstance().runQueryObject(queryObject);

		return DBC.getInstance().runQueryObject(queryObject);
	}

	protected static <T> T unique(List<T> items) {
		if(items == null || items.size() == 0)
			return null;

		if(1 < items.size())
			LogUtil.logException(new Exception("Multiple results found where only one was expected"));

		return items.get(0);
	}

	protected static Map validateDuplicate(boolean ignoreMissing, String key, Map requestParameters, Map errors) {
		String parameter = safeGetParameter(requestParameters, key, null);
		if((!ignoreMissing) || null != parameter) {
			String repeatKey = String.format("%s-repeat", key);
			String repeat = safeGetParameter(requestParameters, repeatKey);
			if(!parameter.equals(repeat)) {
				errors.put(repeatKey, JSPMessage.ERROR_MATCH);
				errors.put(key, JSPMessage.ERROR_MATCH);
			}
		}

		return errors;
	}

	protected static Map validateLength(boolean ignoreMissing, String key, Map requestParameters, Map errors, int minLength, int maxLength) {
		String parameter = safeGetParameter(requestParameters, key, null);

		if((!ignoreMissing) || null != parameter) {
			int length = parameter.length();
			if(minLength <= length && maxLength >= length)
				return errors;

//			if(minLength > length)
//				errors.put(key, JSPMessage.ERROR_SHORT);
//			else if(maxLength < length)
//				errors.put(key, JSPMessage.ERROR_LONG);

			List l = CollectionFactory.createList();
			if(errors.containsKey(key)) {
				Object ob = errors.get(key);
				if(ob instanceof List)
					l = (List)ob;
				else
					l.add(ob);
			}

			l.add(CollectionFactory.createMap(new CollectionFactory.MapPair(JSPMessage.ERROR, JSPMessage.ERROR_LENGTH),
						new CollectionFactory.MapPair(JSPMessage.PARAMS, CollectionFactory.createList(minLength, maxLength))));

			if(l.size() > 0)
				errors.put(key, l);
		}

		return errors;
	}

	protected static Map validateNumber(boolean ignoreMissing, String key, Map requestParameters, Map errors) {
		String parameter = safeGetParameter(requestParameters, key, null);

		try {
			if((!ignoreMissing) || null != parameter)
				Integer.parseInt(parameter);
		} catch(NumberFormatException e) {
			errors.put(key, "In dit veld moet een getal worden ingevuld");
		}

		return errors;
	}

	protected static Map validateMissing(boolean ignoreMissing, String key, Map requestParameters, Map errors) {
		String parameter = safeGetParameter(requestParameters, key, null);

		if((!ignoreMissing) && (null == parameter || parameter.trim().length() == 0))
			errors.put(key, JSPMessage.ERROR_REQUIRED);

		return errors;
	}

	protected static Map validateUnique(String tableName, boolean ignoreMissing, String key, Map requestParameters, Map errors) {
		String parameter = safeGetParameter(requestParameters, key, null);

		if((!ignoreMissing) || null != parameter) {
			RowMapper map = new RowMapper() {
				@Override
				public String mapRow(ResultSet resultSet, int i) {
					return "true";
				}
			};
			String found = (String) unique(find(tableName, map, key, parameter));
			if(null != found)
				errors.put(key, JSPMessage.ERROR_UNIQUE);
		}

		return errors;
	}

	protected static Parameter addParam(List parameters, Map requestMap, String key) {
		return addParam(parameters, requestMap, key, true);
	}

	protected static Parameter addParam(List parameters, Map requestMap, String key, boolean addZeroLengthValue) {
		String parameter = safeGetParameter(requestMap, key, null);
		Parameter param = null;

		if(null != parameter) {
			if(addZeroLengthValue || 0 < parameter.length()) {
				param = new Parameter(key, parameter);
				parameters.add(param);
			}
		}

		return param;
	}

	public static Map modelListToMap(List list) {
		Map map = CollectionFactory.createMap();
		for(Model model : (List<Model>) list) {
			if(null == model)
				continue;

			map.put(model.getId(), model);
		}
		return map;
	}

	protected static String safeGetParameter(HttpServletRequest request, String key) {
		return safeGetParameter(request.getParameterMap(), key);
	}
	protected static String safeGetParameter(Map requestParameters, String key) {
		return safeGetParameter(requestParameters, key, "");
	}
	protected static String safeGetParameter(Map requestParameters, String key, String defaultValue) {
		return Controller.safeGetParameter(requestParameters, key, defaultValue);
	}

	protected static <T> T updateOrCreate(Map values, String tablename, RowMapper<T> rowMapper, String idColumn, Collection<String> columns, String... primaryKeys) {
		return updateOrCreate(null, values, tablename, rowMapper, idColumn, columns, primaryKeys);
	}
	protected static <T> T updateOrCreate(Map<String, Object> whereValues, Map values, String tablename, RowMapper<T> rowMapper, String idColumn, Collection<String> columns, String... primaryKeys) {
		List where = CollectionFactory.createList();
		if(whereValues == null) {
			for(String key: primaryKeys) {
				where = addParamToListFromMap(where, values, key);
			}
		} else {
			for(String key: whereValues.keySet()) {
				where = addParamToListFromMap(where, whereValues, key);
			}
		}

		List set = CollectionFactory.createList();
		for(String key: columns) {
			set = addParamToListFromMap(set, values, key);
		}

		List<T> items = find(tablename, rowMapper, where);
		T current = unique(items);
		if(current == null) {
			current = (T)create(tablename, rowMapper, idColumn, set);

		} else {
			List<T> update = update(tablename, rowMapper, where, set);
			current = unique(update);

		}
		return current;
	}

	protected static List addParamToListFromMap(List list, Map map, String param) {
		if(map.containsKey(param)) {
			list = addParamToList(list, param, map.get(param));
		}
		return list;
	}

	protected static List addParamToList(List list, String key, Object value) {
		return addParamToList(list, key, value, null);
	}
	protected static List addParamToList(List list, String key, Object value, Parameter.Type type) {
		Parameter parameter = new Parameter(key, value);
		if(null != type)
			parameter = parameter.setType(type);

		list.add(parameter);
		return list;
	}

	protected static Collection<Map<String, String>> getParametersFromRequest(HttpServletRequest request, final String type) {
		return getParametersFromRequest(request, type, "_");
	}
	protected static Collection<Map<String, String>> getParametersFromRequest(HttpServletRequest request, final String type, final String connectiveSymbol) {
		final String prefix = type + connectiveSymbol;
		Map<String, String[]> parameterMap = request.getParameterMap();
		Map<String, Map<String, String>> replacements = CollectionFactory.createMap();
		for(String name: parameterMap.keySet()) {
			if(!name.startsWith(prefix))
				continue;

			int nextConectiveSymbol = name.indexOf(connectiveSymbol, prefix.length());
			String key = name.substring(prefix.length(), nextConectiveSymbol);
			if(!replacements.containsKey(key))
				replacements.put(key, CollectionFactory.createMap());

			int preNameLength = prefix.length() + key.length() + connectiveSymbol.length();

			replacements.get(key).put(name.substring(preNameLength), safeGetParameter(parameterMap, name, ""));
		}
		return replacements.values();
	}
}
