package nl.impressie.grazer.dao;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Verification;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.security.SecureRandom;
import java.util.List;

/**
 * @author Impressie
 */
public class VerificationDao extends Dao {

	public static final String TABLE_NAME = "user_verification";
	public static final String ID = "id", USER_ID = "user_id", VERIFICATION_CODE = "code", TIMESTAMP = "created_on";

	private static final String VALID_PW_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	private static final SecureRandom random = new SecureRandom();

	private static final RowMapper<Verification> ROWMAP = (rs, rowNum) -> {
		int id = rs.getInt(ID);
		int userId = rs.getInt(USER_ID);
		String verificationCode = rs.getString(VERIFICATION_CODE);
		Long createdOn = rs.getLong(TIMESTAMP);

		return new Verification(id, userId, verificationCode, createdOn);
	};

	public static Verification findByUserId(Object userId) {
		return unique(find(TABLE_NAME, ROWMAP, new Parameter(USER_ID, userId)));
	}
	public static Verification findByCode(String code) {
		return unique(find(TABLE_NAME, ROWMAP, new Parameter(VERIFICATION_CODE, code)));
	}

	public static Verification generate(CustomUser user) {
		List params = CollectionFactory.createList();
		params.add(new Parameter(USER_ID, user.getId()));
		params.add(new Parameter(VERIFICATION_CODE, generateCode()));
		params.add(new Parameter(TIMESTAMP, DateUtil.getTimestamp()));

		return create(TABLE_NAME, ROWMAP, ID, params);
	}

	private static String generateCode() {
		StringBuilder code = new StringBuilder();
		for(int i = 0; i < 25; i++) {
			int index = random.nextInt(VALID_PW_CHARS.length());
			code.append(VALID_PW_CHARS.charAt(index));
		}

		return code.toString();
	}

	public static boolean delete(int id) {
		return delete(TABLE_NAME, new Parameter(ID, id)) == 1;
	}
}
