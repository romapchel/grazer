package nl.impressie.grazer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author Impressie
 */
@ControllerAdvice
public class ExceptionHandlerController extends Controller {

	public static final String DEFAULT_ERROR_VIEW = "error";

	@ExceptionHandler(value = {Exception.class, RuntimeException.class})
	public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception e) {
		ModelAndView model = new ModelAndView(DEFAULT_ERROR_VIEW);

		model.addObject("datetime", new Date());
		e.printStackTrace();
		String message = e.toString();
		model.addObject("exception", message);
		model.addObject("url", request.getRequestURL());
		model.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);

		return view(request, model, DEFAULT_ERROR_VIEW);
	}
}