package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.NewSearchTermDao;
import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.dao.TagPageDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.Crawler.ArticleSearch;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Role;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.LogUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class MainController extends nl.impressie.grazer.controller.Controller {

	@GetMapping({"/favicon-16x16.png", "/favicon-32x32.png", "/manifest.json"})
	@ResponseBody
//    public byte[] getImage(HttpServletRequest request, ModelAndView model, @RequestParam(name = "type", defaultValue = "") String date) throws IOException {
	public String getImage(HttpServletRequest request, ModelAndView model, @RequestParam(name = "type", defaultValue = "") String date) {
//        InputStream in = getClass().getResourceAsStream("/com/baeldung/produceimage/image.jpg");
//        return IOUtils.toByteArray(in);
		return "";
	}

	private static Map<String, String> addToMap(Map<String, String> map, String rechtsgebied) {
		map.put(rechtsgebied.toLowerCase(), rechtsgebied);
		return map;
	}

	public static Map<String, String> getValidTags() {
		Map<String, String> map = CollectionFactory.createMap();
		map = addToMap(map, "Bestuursrecht");
		map = addToMap(map, "Bestuursrecht: Ambtenarenrecht");
		map = addToMap(map, "Bestuursrecht: Belastingrecht");
		map = addToMap(map, "Bestuursrecht: Bestuursprocesrecht");
		map = addToMap(map, "Bestuursrecht: Bestuursstrafrecht");
		map = addToMap(map, "Bestuursrecht: Europees Bestuursrecht");
		map = addToMap(map, "Bestuursrecht: Mededingingsrecht");
		map = addToMap(map, "Bestuursrecht: Omgevingsrecht");
		map = addToMap(map, "Bestuursrecht: Socialezekerheidsrecht");
		map = addToMap(map, "Bestuursrecht: Vreemdelingenrecht");
		map = addToMap(map, "Bestuursrecht: Overige Subrechtsgebieden");

		map = addToMap(map, "Civiel Recht");
		map = addToMap(map, "Civiel Recht: Aanbestedingsrecht");
		map = addToMap(map, "Civiel Recht: Arbeidsrecht");
		map = addToMap(map, "Civiel Recht: Burgerlijk Procesrecht");
		map = addToMap(map, "Civiel Recht: Europees Civiel Recht");
		map = addToMap(map, "Civiel Recht: Goederenrecht");
		map = addToMap(map, "Civiel Recht: Insolventierecht");
		map = addToMap(map, "Civiel Recht: Intellectueel-eigendomsrecht");
		map = addToMap(map, "Civiel Recht: Internationaal Privaatrecht");
		map = addToMap(map, "Civiel Recht: Mededingingsrecht");
		map = addToMap(map, "Civiel Recht: Ondernemingsrecht");
		map = addToMap(map, "Civiel Recht: Personen- en Familierecht");
		map = addToMap(map, "Civiel Recht: Verbintenissenrecht");
		map = addToMap(map, "Civiel Recht: Overige Subrechtsgebieden");

		map = addToMap(map, "Internationaal Publiekrecht");
		map = addToMap(map, "Internationaal Publiekrecht: Mensenrechten");
		map = addToMap(map, "Internationaal Publiekrecht: Volkenrecht");
		map = addToMap(map, "Internationaal Publiekrecht: Overige Subrechtsgebieden");

		map = addToMap(map, "Strafrecht");
		map = addToMap(map, "Strafrecht: Europees Strafrecht");
		map = addToMap(map, "Strafrecht: Internationaal Strafrecht");
		map = addToMap(map, "Strafrecht: Materieel Strafrecht");
		map = addToMap(map, "Strafrecht: Penitentiair Strafrecht");
		map = addToMap(map, "Strafrecht: Strafprocesrecht");
		map = addToMap(map, "Strafrecht: Overige Subrechtsgebieden");

		map = addToMap(map, "Tuchtrecht");
		map = addToMap(map, "Tuchtrecht: Accountants");
		map = addToMap(map, "Tuchtrecht: Advocaten");
		map = addToMap(map, "Tuchtrecht: Diergeneeskundigen");
		map = addToMap(map, "Tuchtrecht: Gerechtsdeurwaarders");
		map = addToMap(map, "Tuchtrecht: Gezondheidszorg");
		map = addToMap(map, "Tuchtrecht: Notarissen");
		map = addToMap(map, "Tuchtrecht: Notariaat");
		map = addToMap(map, "Tuchtrecht: Scheepvaart");
		map = addToMap(map, "Tuchtrecht: Overige Subrechtsgebieden");

		return map;
	}

	@RequestMapping("/temp")
	@ResponseBody
	public String bla() throws Exception {
		Map<Object, List<Tag>> listOfInvalidPages = CollectionFactory.createMap();
		Map<Object, Website> pages = CollectionFactory.createMap();
		Map<String, String> valid = MainController.getValidTags();
		Set<Tag> otherInvalidTags = CollectionFactory.createSet();
		for (Tag tag : TagDao.getType(Tag.RECHTSGEBIED)) {
			String name = tag.getName().toLowerCase();
			if (valid.containsKey(name))
				continue;

			List<Website> invalidWebsites = TagPageDao.findByTagIds(tag.getId());
			for (Website page : invalidWebsites) {
				List list = CollectionFactory.createList();
				Object pageId = page.getId();
				if (listOfInvalidPages.containsKey(pageId))
					list = listOfInvalidPages.get(pageId);

				if (!pages.containsKey(pageId))
					pages.put(pageId, page);

				list.add(tag);

				listOfInvalidPages.put(pageId, list);
			}

			if (invalidWebsites.size() == 0)
				otherInvalidTags.add(tag);
		}

		StringBuilder builder = new StringBuilder();

		for (Object pageId : listOfInvalidPages.keySet()) {
			Website page = pages.get(pageId);
			builder.append("<div>");

			builder.append(String.format("<div><span>%s</span> <span>%s</span> <span>%s</span></div>", page.getId(), page.getUrl(), page.getName()));
			builder.append("<div>Heeft de volgende invalide tags:</div>");
			List<Tag> tags = listOfInvalidPages.get(pageId);
			for (Tag tag : tags) {
				builder.append(String.format("<div><span>%s</span></div>", tag.getName()));
			}

			builder.append("</div>");
		}

		builder.append("<div>");
		builder.append("<div><span>Other invalid tags:</span></div>");
		for (Tag tag : otherInvalidTags) {
			builder.append(String.format("<div><span>%s</span></div>", tag.getName()));
		}
		builder.append("</div>");

		builder.append("<div>");
		builder.append("<div><span>Created tags:</span></div>");
		for (String tagName : valid.values()) {
			Tag tag = TagDao.findByName(tagName, Tag.RECHTSGEBIED);
			if (tag != null)
				continue;

			tag = TagDao.create(Tag.RECHTSGEBIED, tagName);

			if (tag instanceof Tag)
				builder.append(String.format("<div><span>%s</span></div>", tag.getName()));

		}
		builder.append("</div>");

		return builder.toString();
	}

    @RequestMapping("/temp/paid")
    public ModelAndView tempPaid(HttpServletRequest request, ModelAndView model) throws Exception {
        return view(request, model, "payment/paid", "");
    }



	@RequestMapping("/about/grazer")
	public ModelAndView index(HttpServletRequest request, ModelAndView model) throws Exception {
		Long start = DateUtil.getTimestamp();
		ArticleSearch legalSearch = new ArticleSearch();
		Map legalSearchResult = legalSearch.search();
		List<Article> legalArticles = (List<Article>) legalSearchResult.get("artikels");

		if (0 < legalArticles.size()) {
			Article legal = legalArticles.get(0);
			model.addObject("legal_article", legal);
		}

		long elapsedTimeInMS = DateUtil.getTimestamp() - start;
		if (elapsedTimeInMS > 5000)
			LogUtil.log("Homepage took: " + DateUtil.elapsedTimeToString(elapsedTimeInMS));

		return view(request, model, "home", "");
	}

	@RequestMapping({"/", "/about/legal"})
	public ModelAndView aboutLegal(HttpServletRequest request, ModelAndView model) {
		ArticleSearch legalSearch = new ArticleSearch();
		Map legalSearchResult = legalSearch.search();
		List<Article> legalArticles = (List<Article>) legalSearchResult.get("artikels");

		if (0 < legalArticles.size()) {
			Article legal = legalArticles.get(0);
			model.addObject("legal_article", legal);
		}

		return view(request, model, "legal/about");
	}

	@RequestMapping("/about")
	public ModelAndView about(HttpServletRequest request, ModelAndView model) {
		return view(request, model, "about");
	}

	@RequestMapping("/faq")
	public ModelAndView faq(HttpServletRequest request, ModelAndView model) {
		return view(request, model, "faq");
	}

	@GetMapping("/download")
	public ResponseEntity<byte[]> downloadFile(HttpServletRequest request, ModelAndView model,
											   @RequestParam(name = "path", defaultValue = "") String path,
											   @RequestParam(name = "name", defaultValue = "download") String name) {
		File file = new File(path);
		if (file.exists())
			return fileToResponse(file, name);

		return new ResponseEntity(HttpStatus.NOT_FOUND);
	}

	@RequestMapping("/pdf/{FILE}")
	public ResponseEntity<byte[]> privacy(@PathVariable("FILE") String file) {
		String name;
		String path;
		switch (file) {
			case "privacy":
				name = "Privacybeleid.pdf";
				path = "WEB-INF/data/pdf/privacybeleid_matching_apps.pdf";
				break;
			case "terms_and_conditions":
			default:
				name = "Algemene voorwaarden.pdf";
				path = "WEB-INF/data/pdf/algemene_voorwaarden_exoneratieclausule_matching_apps.pdf";
				break;
		}
		return downloadResource(path, name);
	}

	@RequestMapping("/sessionexpired")
	public ModelAndView sessionExpired(HttpServletRequest request, ModelAndView model) {
		return view(request, model, "sessionexpired");
	}

	@RequestMapping("/error")
	public ModelAndView error(HttpServletRequest request, ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();

		String url = "/";

		if(user != null) {
			List<GrantedAuthority> authorities = user.getAuthorities();
			if(authorities.size() == 1 && Role.NONE.equals(authorities.get(0).getAuthority()))
				url = PaymentController.PAYWALL_URL;
		}

		return redirect(request, model, url);
	}

	@GetMapping("/terms/new")
	public ModelAndView newTerms(HttpServletRequest request, ModelAndView model) {
		String url = "allNewSearchTerms";
		model.addObject("terms", NewSearchTermDao.getAllByDate());
		model.addObject("until", DateUtil.getDaysAfterToday(-30, "yyyy-MM-dd"));
		return view(request, model, url);
	}

	@PostMapping("/terms/delete")
	public ModelAndView deleteTerms(HttpServletRequest request, ModelAndView model, @RequestParam(name = "until", defaultValue = "") String date) {
		String url = "terms/new";
		if(date != "")
			NewSearchTermDao.deleteOlderThan(DateUtil.convertDateToTimestamp(date, "yyyy-MM-dd"));
		return redirect(request, model, url);
	}
}
