package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.*;
import nl.impressie.grazer.enums.CSFExport;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.hibernate.models.PageModel;
import nl.impressie.grazer.model.Crawler.SeReDe;
import nl.impressie.grazer.model.DB.*;
import nl.impressie.grazer.model.JSPMessage;
import nl.impressie.grazer.model.Report;
import nl.impressie.grazer.threads.ArticleTagUpdater;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.*;
import nl.impressie.grazer.util.crawlerUtils.IndexManager;
import nl.impressie.grazer.util.crawlerUtils.IndexUtil;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.json.XML;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.pmw.tinylog.Level;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @author Impressie
 */
@Controller
@RequestMapping(WebsiteController.WEBSITE)
public class WebsiteController extends nl.impressie.grazer.controller.Controller {

	public static final String WEBSITE = "website";

	private static final String MANAGE_VIEW = "website/manage";

	public static final String EXPORT_FULL = "export_full";
	public static final String EXPORT_PARTIAL = "export_partial";
	public static final String EXPORT_PETER = "export_peter";

	@Value("${indexing.maxdepth}")
	private int globalMaxDepth;
	@Value("${indexing.defaultmode}")
	private String indexMode;

	public static String getIds(HttpServletRequest request) {
		String[] params = {Actions.INDEX, Actions.COPY, Actions.EDIT, Actions.TOGGLE, Actions.DELETE, Actions.DETAIL, Actions.HEALTH, Actions.BREAK, Actions.ENABLE, Actions.DISABLE, Actions.EXPORT, Actions.IMPORT, Actions.EMPTY, Selections.SELECTION, Selections.ALL};

		return getIds(request, params);
	}


	@RequestMapping("/export")
	public ResponseEntity<byte[]> exportPages(HttpServletRequest request,
										  @RequestParam(name = "export_style", defaultValue = "") String exportStyle) {
		CSFExport export = CSFExport.FULL;
		try {
			export = CSFExport.valueOf(exportStyle);
		} catch(IllegalArgumentException e) {}

		List all = WebsiteDao.getAll();
		StringBuilder builder = export.getCSFContent(all);

		File csv = new File("test.csv");
		try (PrintWriter writer = new PrintWriter(csv)) {
			writer.write(builder.toString());
		} catch (FileNotFoundException e) {
			LogUtil.logException(e);
		}

		return fileToResponse(csv, "export.csv");
	}

	private void addFiltersToSession(HttpServletRequest request, Map params) {
		addFilterToSession(request, "db-filter-default", params);
		addFilterToSession(request, "db-filter-disabled-hide", params);
		addFilterToSession(request, "db-filter-name", params);
	}
	private void addFilterToSession(HttpServletRequest request, String key, Map params) {
		if(params.containsKey(key))
			SessionUtil.addToSession(request, key, params.get(key));
	}

	@RequestMapping("/action")
	public <T> T action(ModelAndView model, HttpServletRequest request) {
		Map params = request.getParameterMap();
		String id = getIds(request);

		addFiltersToSession(request, params);

		if(null != id) {
			if(params.containsKey(Actions.COPY))
				return (T)getManagePage(request, model, id, "true");

			else if(params.containsKey(Actions.EDIT))
				return (T)getManagePage(request, model, id, "false");

			else if(params.containsKey(Actions.TOGGLE))
				return (T)toggleSites(model, request, id);

			else if(params.containsKey(Actions.ENABLE))
				return (T)enableSites(model, request, id);

			else if(params.containsKey(Actions.DISABLE))
				return (T)disableSites(model, request, id);

			else if(params.containsKey(Actions.DELETE))
				return (T)deletePage(model, request, id);

			else if(params.containsKey(Actions.BREAK))
				return (T)toggleBrokenStatus(model, request, id);

			else if(params.containsKey(Actions.INDEX)) {
				if(!IndexManager.isBusy()) {
					IndexUtil.startIndexThread(CollectionFactory.createList(WebsiteDao.findById(id)), indexMode, "", true, globalMaxDepth);
				} else {
					System.out.println("indexer is running");
					SessionUtil.addMessageToSession(request, new JSPMessage(JSPMessage.ERROR_CRAWLER_IS_RUNNING, JSPMessage.ERROR));
				}
			}

//            else if(params.containsKey(Actions.DETAIL))
//                return viewHealth(model, request, id, false);

			else if(params.containsKey(Actions.HEALTH) || params.containsKey(Actions.DETAIL))
				return (T)viewOverview(model, request, id);

			else if(params.containsKey(Actions.EMPTY))
				emptypages(id);

			else if(params.containsKey(Actions.EXPORT))
				return (T)downloadPages(id);

			else if(params.containsKey(Actions.DEFAULT)) {

			}
		}

		return (T)adminRedirect(request, model);
	}

	private ModelAndView importPages(String ids, HttpServletRequest request, ModelAndView model) {
		String xml = "";
		JSONObject jsonObject = XML.toJSONObject(xml);
		Website page = new Website(XMLUtil.getGson().fromJson(jsonObject.toString(), PageModel.class));
		WebsiteDao.save(page);

		return redirect(request, model, "");
	}

	private ResponseEntity<byte[]> downloadPages(String ids) {
		List<Website> pages = CollectionFactory.createList();
		if(0 == ids.length()) {
			pages = WebsiteDao.getAll(false);
		} else {
			for(String id : ids.split(",")) {
				Website page = WebsiteDao.findById(id.trim());
				if(page != null)
					pages.add(page);
			}
		}
		List l = CollectionFactory.createList();
		for(Website page: pages) {
			l.add(new PageModel(page));
		}

		Object whatever;
		if(l.size() == 1)
			whatever = l.get(0);
		else
			whatever = l;//this will probably bug because the json will be an array not an object

		File csv = new File("test.xml");
		String source = XMLUtil.getGson().toJson(whatever);
		JSONObject json = new JSONObject(source);
		String xml = XML.toString(json);

		try (PrintWriter writer = new PrintWriter(csv)) {
			writer.write(xml);
		} catch (FileNotFoundException e) {
			LogUtil.logException(e);
		}

		return fileToResponse(csv, "export.xml");
	}
	private void emptypages(String ids) {
		if(0 == ids.length()) {
			for(Website site : WebsiteDao.getAll(false)) {
				site.deleteAllArticles();
			}
		} else {
			for(String id : ids.split(",")) {
				Website page = WebsiteDao.findById(id.trim());
				page.deleteAllArticles();
			}
		}
	}

	private ModelAndView viewOverview(ModelAndView model, HttpServletRequest request, String ids) {
		List<Website> sites = CollectionFactory.createList();
		if(0 < ids.length()) {
			for(String id : ids.split(", ")) {
				sites.add(WebsiteDao.findById(id));
			}
		} else {
			sites = WebsiteDao.getAll(false);
		}

		Report rep = new Report();
		for(Website site : sites) {
			HealthReport hr = site.generateHealthReport(false);
			rep.addHealthReport(hr);
		}

		model.addObject("report", rep);

		return adminView(request, model, "website/report");
	}

	@RequestMapping("new")
	public ModelAndView getNew(ModelAndView model, HttpServletRequest request,
							   @RequestParam(name = "id", defaultValue = "0") String id,
							   @RequestParam(name = "type", defaultValue = Website.Types.ALT_PAGE) String type,
							   @RequestParam(name = "index", defaultValue = "0") int index) {
		String view;
		Object selected;
		switch(type) {
			case Website.Types.BUTTON:
				view = "buttonView";
				selected = Button.findById(id);
				break;
			case Website.Types.NAVIGATION:
				view = "navigationView";
				selected = NavigationDao.findById(id);
				break;
			case Website.Types.BEFORES:
				view = "beforeView";
				selected = BeforeSelectorDao.findById(id);
				break;
			case Website.Types.TAG_REPLACEMENT:
				view = "tagReplacementView";
				selected = TagReplacementDao.findById(id);
				break;
			case Website.Types.ADDITIONAL_SOURCE_LINK:
				view = "additionalSourceLinkView";
				selected = AdditionalSourceLinkDao.findById(id);
				break;
			case Website.Types.ALT_PAGE:
			default:
				view = "altPageView";
				selected = AltPageDao.findById(id);
				break;
		}

		model.addObject("item", selected);
		model.addObject("index", index);

		view = String.format("includes/website/%s", view);

		return view(request, model, view);
	}

	@GetMapping("/disable")
	public ModelAndView disableSites(ModelAndView model, HttpServletRequest request, @RequestParam(name = "id", defaultValue = "0") String id) {
		return toggleSiteDisabled(model, request, id, true);
	}

	@GetMapping("/enable")
	public ModelAndView enableSites(ModelAndView model, HttpServletRequest request, @RequestParam(name = "id", defaultValue = "0") String id) {
		return toggleSiteDisabled(model, request, id, false);
	}

	@GetMapping("/toggle")
	public ModelAndView toggleSites(ModelAndView model, HttpServletRequest request, @RequestParam(name = "id", defaultValue = "0") String id) {
		return toggleSiteDisabled(model, request, id);
	}

	private ModelAndView toggleSiteDisabled(ModelAndView model, HttpServletRequest request, String id) {
		Website page = WebsiteDao.findById(id);
		boolean disabled = true;
		if(null != page)
			disabled = !page.isDisabled();
		else
			id = "0";

		return toggleSiteDisabled(model, request, id, disabled);
	}

	private ModelAndView toggleSiteDisabled(ModelAndView model, HttpServletRequest request, String id, boolean disable) {
		if(disable)
			WebsiteDao.disableById(id);
		else
			WebsiteDao.enableById(id);

		return adminRedirect(request, model);
	}

	@GetMapping("/reset")
	public ModelAndView resetWebsites(ModelAndView model, HttpServletRequest request) {
		for(Website page : WebsiteDao.getAll(false)) {
			page.delete();
		}
		return adminRedirect(request, model);
	}

	private List<String> getExportOrder() {
		List items = CollectionFactory.createList();

		items.add(WebsiteDao.ID);
		items.add(WebsiteDao.NAME);
		items.add(WebsiteDao.URL);
		items.add(WebsiteDao.NIEUWSZENDER);
		items.add(WebsiteDao.LAST_CRAWL_DATE);
		items.add(WebsiteDao.MAX_DEPTH);
		items.add(WebsiteDao.DATE_FORMAT);
		items.add(WebsiteDao.MORE_INFO);
		items.add(WebsiteDao.DATE);
		items.add(WebsiteDao.TITLE);
		items.add(WebsiteDao.CONTENT);
		items.add(WebsiteDao.SNIPPET);
		items.add(WebsiteDao.RECHTSGEBIED);
		items.add(WebsiteDao.NIEUWS_SOORT);
		items.add(WebsiteDao.RUBRIEKEN);
		items.add(WebsiteDao.CRAWL);
		items.add(WebsiteDao.ARTICLE);
		items.add(WebsiteDao.SEARCH_KEY);
		items.add(WebsiteDao.SEARCH_METHOD);
		items.add(WebsiteDao.SEARCH_HEADER);
		items.add(WebsiteDao.SEARCH_DATA);
		items.add(WebsiteDao.JSON_URL);
		items.add(WebsiteDao.PAGE_KEY);
		items.add(WebsiteDao.JS);
		items.add(WebsiteDao.JS_TEST_ID);
		items.add(WebsiteDao.DISABLED);
		items.add(WebsiteDao.HEALTHY);
		items.add(WebsiteDao.DEFECTIVE);
		items.add(WebsiteDao.DESCRIPTION);
		items.add(WebsiteDao.COOKIE_URL);
		items.add(WebsiteDao.COOKIE_METHOD);
		items.add(WebsiteDao.COOKIE_DATA);
		items.add(WebsiteDao.COOKIE_CSS);

		return items;
	}
	private Map<String, String> getPeterExport() {
		Map items = CollectionFactory.createMap();

		items.put(WebsiteDao.RECHTSGEBIED, "Rechtsgebied");
		items.put(WebsiteDao.NIEUWS_SOORT, "Informatiesoort");
		items.put(WebsiteDao.RUBRIEKEN, "Maatschappelijke sector");

		return items;
	}

	@RequestMapping(value = "/import", consumes = {"multipart/form-data"})
	public ModelAndView importSites(ModelAndView model, HttpServletRequest request,
									@RequestParam(name = "import", required = false) MultipartFile importFile,
									@RequestParam(name = "save_changes", defaultValue = "false") boolean saveChanges,
									@RequestParam(name = "save_unchanged", defaultValue = "false") boolean saveUnchanged,
									@RequestParam(name = "exclude_urls", defaultValue = "false") boolean excludeUrls,
									@RequestParam(name = "skip_new", defaultValue = "false") boolean skipNew) {
		if(importFile == null)
			return redirect(request, model, AdminController.ADMIN_WEBSITE_URL);

		List<String> actions = CollectionFactory.createList();
		List<Map> listOfUpdates = CollectionFactory.createList();
		Map<Object, Website> sites = WebsiteDao.modelListToMap(WebsiteDao.getAll());

		String extension = FilenameUtils.getExtension(importFile.getOriginalFilename());
		switch(extension) {
			case "csv":
				listOfUpdates = readCSVFile(importFile);
				break;
			case "xml":
				listOfUpdates = readXMLFile(importFile);
				break;
			default:
				LogUtil.log(String.format("Unknown file extension '%s'", extension));
		}
		List<String> types = CollectionFactory.createList(WebsiteDao.RECHTSGEBIED, WebsiteDao.RUBRIEKEN, WebsiteDao.NIEUWS_SOORT);
		for(Map<String, String> updates: listOfUpdates) {
			String url = updates.containsKey(WebsiteDao.URL) ? updates.get(WebsiteDao.URL) : "";

			for(String type: types) {
				if(updates.containsKey(type)) {
					if(!SeReDe.isLoadableSeReDeString(updates.get(type)))
						updates.put(type, new SeReDe("", "", "", updates.get(type)).toString());
				}
			}

			if(url.length() > 0) {
				if(url.trim().startsWith("n.v.t."))
					continue;

				url = UrlUtil.fixProtocol(url);
				if(!UrlUtil.isValidUrl(url)){
					LogUtil.log(String.format("could not make a URL out of: %s", url), Level.ERROR);
					continue;
				}
				updates.put(WebsiteDao.URL, url);
			}

			String id = updates.get(WebsiteDao.ID);
			Website page = WebsiteDao.findById(id);
			if(null == page) {
				if(updates.containsKey(WebsiteDao.NAME))
					page = WebsiteDao.findByName(updates.get(WebsiteDao.NAME));
			}
			if(null == page) {
				if(url.length() == 0) {
					//LogUtil.log(String.format("No URL entered for new page"), Level.ERROR);
					if(!skipNew) {
						StringBuilder builder = addUpdates(new StringBuilder(), updates);
						actions.add(String.format("Failed to create page %s", builder.toString()));
					}
					continue;
				} else {
					page = WebsiteDao.findByUrl(url);
					if(null == page && url.startsWith("http://")) {
						page = WebsiteDao.findByUrl("https://" + url.substring(7));
					}
				}
			}
			if(null == page) {
				if(skipNew)
					continue;

				page = WebsiteDao.create(id, url);
				actions.add(String.format("Created page id: %s, url: %s", id, url));
			}

			StringBuilder builder = new StringBuilder();
			builder.append(String.format("updated page id: %s, url: %s", page.getId(), page.getUrl().toString()));

			Map<String, Tag> toRemove = page.getTags();

			updates.remove(WebsiteDao.ID);
			if(excludeUrls)
				updates.remove(WebsiteDao.URL);

			page = page.update(updates);
			page = page.loadTagsFromSerede(true);

			sites.remove(page.getId());

			Map<String, Tag> toAdd = page.getTags();

			for(Tag tag: toRemove.values()) {
				builder.append(String.format("\n\tremoved tag: %s(%s)", tag.getName(), tag.getId()));
			}
			for(Tag tag: toAdd.values()) {
				builder.append(String.format("\n\tadded tag: %s(%s)", tag.getName(), tag.getId()));
			}
			builder = addUpdates(builder, updates);
			actions.add(builder.toString());

			ArticleTagUpdater.updateTags(page.getAllArticles(), toRemove, toAdd);

			if(updates.containsKey(WebsiteDao.REPLACEMENTS)) {
				int removed = TagReplacementDao.removeForPage(page.getId());
				actions.add(String.format("Removed %d tag replacements", removed));

				List added = TagReplacement.CSFImport(updates.get(WebsiteDao.REPLACEMENTS), page.getId());
				actions.add(String.format("added %d tag replacements", added.size()));
			}

			if(updates.containsKey(WebsiteDao.ADDITIONAL_PAGES)) {
				int removed = AdditionalSourceLinkDao.removeForPage(page.getId());
				actions.add(String.format("Removed %d additional pages", removed));

				List added = AdditionalSourceLink.CSFImport(updates.get(WebsiteDao.ADDITIONAL_PAGES), page.getId());
				actions.add(String.format("added %d tag replacements", added.size()));
			}
		}

		boolean save = saveChanges || saveUnchanged;
		if(save) {
			File file = new File(String.format("%d.txt", DateUtil.getTimestamp()));
			try (PrintWriter writer = new PrintWriter(file)) {
				if(saveChanges) {
					for(String line: actions) {
						writer.write(String.format("%s\n", line));
					}
				}
				if(saveUnchanged) {
					for(Website page: sites.values()) {
						writer.write(String.format("Page %s %s(%s) was not updated\n", page.getId(), page.getName(), page.getUrl()));
					}
				}
			} catch (FileNotFoundException e) {
				LogUtil.logException(e);
			}
			SessionUtil.addToSession(request, "resultDownload", file.getPath());
		}

		return adminRedirect(request, model);
	}
	private StringBuilder addUpdates(StringBuilder builder, Map<String, String> updates) {
		for(String column: updates.keySet()) {
			builder.append(String.format("\n\tset column '%s' to value '%s'", column, updates.get(column)));
		}
		return builder;
	}

	private List<Map> readXMLFile(MultipartFile importFile) {
		List listOfUpdates = CollectionFactory.createList();

		Set columns = CollectionFactory.createSet(getExportOrder());

		try {
			Document doc = Jsoup.parse(importFile.getInputStream(), "UTF-8", "");
			for(Element site: doc.getElementsByTag("site")) {
				Map siteMap = CollectionFactory.createMap();
				for(Element col: site.getAllElements()) {
					String name = col.tagName();
					if(columns.contains(name)) {
						String value = col.text();
						if(WebsiteDao.RECHTSGEBIED.equals(name) || WebsiteDao.NIEUWS_SOORT.equals(name) || WebsiteDao.RUBRIEKEN.equals(name)) {
							if(!SeReDe.isLoadableSeReDeString(value)) {
								value = new SeReDe("", "", "", value).toString();
							}
						}
						siteMap.put(name, value);
					}
				}
				listOfUpdates.add(siteMap);
			}
		} catch(IOException e) {
			LogUtil.logException(e);
		}
		return listOfUpdates;
	}

	private List<Map> readCSVFile(MultipartFile importFile) {
		List listOfUpdates = CollectionFactory.createList();

		List<String> results = readFile(importFile);

		List<String> columns = CollectionFactory.createList();
		String seperator = "|";
		boolean first = true;
		CSFExport export;
		for(String line: results) {
			if(line.startsWith("\"sep=")) {
				seperator = line.substring(5, 6);
				continue;
			}
			String[] vars = line.split(Pattern.quote(seperator));
			if(first) {
				columns = CollectionFactory.createList();
				for(String var: vars) {
					columns.add(var.trim());
				}
				export = CSFExport.getByImportedColumns(columns);
				if(export != null)
					columns = export.getColumns();

				first = false;
				continue;
			}

			Map<String, String> updates = CollectionFactory.createMap();
			int index = 0;
			for(String column: columns) {
				String value = (vars.length > index ? vars[index].trim() : "");
				updates.put(column, value);
				index++;
			}
			listOfUpdates.add(updates);
		}

		return listOfUpdates;
	}

	@RequestMapping(value = "/upload", consumes = {"multipart/form-data"})
	public ModelAndView viewHealth(ModelAndView model,
								   HttpServletRequest request,
								   @RequestParam(name = "xml", required = false) MultipartFile xml,
								   @RequestParam(name = "update_mode", required = false, defaultValue = "default") String mode
	) {
		try {
			String tempPath = "temp";

			File temp = new File(tempPath);
			xml.transferTo(temp);

			Map<String, String[]> parameterMap = request.getParameterMap();
			String optionKey = "options";
			Map<String, Map> options = CollectionFactory.createMap();
			int startSkip = 1 + optionKey.length();
			for(String key: parameterMap.keySet()) {
				boolean isOption = key.startsWith(optionKey);
				if(isOption) {
					int endNewKey = key.indexOf("]");
					String newKey = key.substring(startSkip, endNewKey);
					String remainder = key.substring(endNewKey + 1);
					Map currentOptions = (options.containsKey(newKey) ? options.get(newKey) : CollectionFactory.createMap());

					String[] value = parameterMap.get(key);
					if("[]".equals(remainder)) {
						for(String param: value) {
							currentOptions.put(param, true);
						}
					} else if(remainder.startsWith("[") && remainder.endsWith("]")) {
						Object v = (value.length == 1 ? value[0] : value);
						currentOptions.put(remainder.substring(1, remainder.length() - 1), v);
					} else {
						Object v = (value.length == 1 ? value[0] : value);
						currentOptions.put(remainder, v);
					}

					options.put(newKey, currentOptions);
				}
			}

			IndexUtil.updateIndex(mode, tempPath, options);

			temp.delete();
		} catch(IOException e) {
			LogUtil.logException(e);
		}

		return adminRedirect(request, model);
	}

	@RequestMapping("/health")
	public ModelAndView viewHealth(ModelAndView model, HttpServletRequest request,
								   @RequestParam(value = "id", defaultValue = "0") String id,
								   @RequestParam(value = "all", defaultValue = "false") boolean checkAll) {
		Website page = WebsiteDao.findById(id);
		if(null != page) {
			model.addObject("selected", page);
			model.addObject("report", page.generateHealthReport(checkAll));
		}

		return adminView(request, model, "healthReports");
	}

	@RequestMapping("/delete")
	public ModelAndView deletePage(
			ModelAndView model, HttpServletRequest request,
			@RequestParam(value = "id", defaultValue = "0") String id) {
		for(String i : id.split(",")) {
			Website page = WebsiteDao.findById(i.trim());
			page.delete();
		}
		return redirect(request, model, AdminController.ADMIN_WEBSITE_URL);
	}

	public ModelAndView toggleBrokenStatus(ModelAndView model, HttpServletRequest request, String id) {
		for(String i : id.split(",")) {
			Website page = WebsiteDao.findById(i.trim());
			List updates = CollectionFactory.createList();
			boolean newDefectiveStauts = !page.isDefective();
			updates.add(new Parameter(WebsiteDao.DEFECTIVE, newDefectiveStauts));
			updates.add(new Parameter(WebsiteDao.DISABLED, newDefectiveStauts));

			WebsiteDao.update(page.getId(), updates);
		}
		return redirect(request, model, AdminController.ADMIN_WEBSITE_URL);
	}

	@PostMapping("/save")
	public ModelAndView saveWebsite(ModelAndView model, HttpServletRequest request) {
		String url = safeGetParameter(request, WebsiteDao.URL);
		if(safeGetParameter(request, WebsiteDao.NAME).length() == 0 || url.length() == 0 || (safeGetParameter(request, WebsiteDao.ID).length() == 0 && WebsiteDao.findByUrl(url) != null)) {
			model.setViewName("redirect_back");
			return model;
		}

		Map<String, Map> found = sortParamaters(request);
		int pageId = savePage(found);

		if(pageId != 0) {
			saveNavigation(found, pageId);
			saveAltPages(found, pageId);
			saveBefores(found, pageId);
			TagReplacementDao.save(request, pageId);
			AdditionalSourceLinkDao.save(request, pageId);
		}

		return redirect(request, model, String.format("%s?id=%s", AdminController.ADMIN_WEBSITE_URL, pageId));
	}

	private int savePage(Map<String, Map> found) {
		Map<String, List> pages = found.get(Website.Types.PAGE);
		int PID = 0;
		for(String id : pages.keySet()) {
			List params = pages.get(id);
			for(List para : ((Map<String, List>) found.get("serede")).values()) {
				params.addAll(para);
			}
			Website page = WebsiteDao.update(id, params);
			if(page != null) {
				page.loadTagsFromSerede();
				PID = (Integer) page.getId();
			}
		}
		return PID;
	}

	private void saveNavigation(Map<String, Map> found, int pageId) {
		Map<String, List> navs = found.get(Website.Types.NAVIGATION);
		NavigationDao.deleteByPageId(pageId);

		if(null == navs)
			return;

		for(String id : navs.keySet()) {
			Object paginationId = 0;

			List updates = CollectionFactory.createList();
			updates.add(new Parameter(NavigationDao.PAGE_ID, pageId));

			List<Parameter> foundParams = navs.get(id);
			boolean add = true;
			for(Parameter param : foundParams) {
				if(param.getColumn().equals(NavigationDao.URL))
					add = (0 < param.getParamater().toString().length());

				if(param.getColumn().equals(NavigationDao.ID))
					paginationId = param.getParamater();
				else
					updates.add(param);

			}
			if(add)
				NavigationDao.update(paginationId, updates);
		}
	}

	private void saveAltPages(Map<String, Map> found, int pageId) {
		Map<String, List> pages = found.get(Website.Types.ALT_PAGE);

		AltPageDao.deleteByPageId(pageId);

		if(null == pages)
			return;

		for(String index : pages.keySet()) {
			List updates = CollectionFactory.createList();
			updates.add(new Parameter(AltPageDao.PAGE_ID, pageId));
			List where = CollectionFactory.createList();

			List<Parameter> foundParams = pages.get(index);
			boolean add = true;
			for(Parameter param : foundParams) {
				if(param.getColumn().equals(AltPageDao.URL))
					add = (0 < param.getParamater().toString().length());

				if(param.getColumn().equals(AltPageDao.ID))
					where.add(param);
				else
					updates.add(param);
			}
			if(add)
				AltPageDao.update(where, updates);
		}
	}

	private void saveBefores(Map<String, Map> found, int pageId) {
		Map<String, List> selectors = found.get(Website.Types.BEFORES);

		BeforeSelectorDao.deleteByPageId(pageId);

		if(null == selectors)
			return;

		for(List selectorParams : selectors.values()) {
			BeforeSelectorDao.create(pageId, selectorParams);
		}
	}

	private Map sortParamaters(HttpServletRequest request) {
		Map<String, Map> found = CollectionFactory.createMap();
		Set<String> paramaterKeys = request.getParameterMap().keySet();
		String page_id_key = "id";
		String page_id = request.getParameter(page_id_key);

		String seperator = "-";

		for(String key : paramaterKeys) {
			if(key.equals(page_id_key))
				continue;
			if(key.startsWith(Website.Types.TAG_REPLACEMENT) || key.startsWith(Website.Types.ADDITIONAL_SOURCE_LINK))
				continue;

			String parameter = request.getParameter(key);
			int con = key.indexOf(seperator);
			String type = Website.Types.PAGE;
			String remain = key;
			if(con != -1) {
				type = remain.substring(0, con);
				remain = remain.substring(con + seperator.length());
			}

			con = remain.indexOf(seperator);
			String id = page_id;
			if(con != -1) {
				id = remain.substring(0, con);
				remain = remain.substring(con + seperator.length());
			}

			Map<String, List> tp = CollectionFactory.createMap();
			if(found.containsKey(type))
				tp = found.get(type);
			else
				found.put(type, tp);

			List d = CollectionFactory.createList();
			boolean isSerede = type.equals("serede");
			Parameter para;
			if(tp.containsKey(id)) {
				if(isSerede)
					continue;
				d = tp.get(id);
				para = new Parameter(remain, parameter);
			} else {
				tp.put(id, d);
				if(isSerede) {
					String selector = request.getParameter(String.format("%1$s%2$s%3$s%2$sselector", type, seperator, id));
					String prop = request.getParameter(String.format("%1$s%2$s%3$s%2$sproperty", type, seperator, id));
					String regex = request.getParameter(String.format("%1$s%2$s%3$s%2$sregex", type, seperator, id));

					String dvk = String.format("%1$s%2$s%3$s%2$sdefault", type, seperator, id);
					String defaultValue = getDefault(request, dvk);

					SeReDe serede = new SeReDe(selector, prop, regex, defaultValue);
					para = new Parameter(id, serede.toString());
				} else {
					para = new Parameter(remain, parameter);
				}
			}
			if(null != para)
				d.add(para);
		}
		return found;
	}

	private String getDefault(HttpServletRequest request, String defaultKey) {
		Map params = request.getParameterMap();
		String arrayKey = String.format("%s[]", defaultKey);
		if(params.containsKey(arrayKey)) {
			StringBuilder builder = new StringBuilder();
			for(String id : (String[]) params.get(arrayKey)) {
				Tag tag = TagDao.findById(id.trim());
				if(null == tag) continue;

				if(0 < builder.length()) builder.append(", ");

				builder.append(tag.getName());
			}
			return builder.toString();
		} else {
			return request.getParameter(defaultKey);
		}
	}

	public static ModelAndView getManagePage(HttpServletRequest request, ModelAndView model, String id, String copy) {
		Website selected = WebsiteDao.findById(id);
		if(null != selected) {
			if(Boolean.valueOf(copy))
				selected.resetUniques();

			model.addObject("selected", selected);
		}
		model.addObject("defaultAction", Actions.EDIT);

//		I don't know what this is, but this breaks the admin page
		String limited = request.getParameter("limit");
		if(limited != null && limited.length() > 0) {
			List zero = CollectionFactory.createList();
			for(Website page: WebsiteDao.getAll(false)) {
				if(page.getAllArticles().size() == 0)
					zero.add(page);
			}
			model.addObject(ITEMS, zero);
		}

		String downloadLink = SessionUtil.getAndRemoveStringFromSession(request, "resultDownload");
		if(downloadLink.length() > 0) {
			model.addObject("downloadLink", downloadLink);
		}

		model.addObject("exportOptions", CSFExport.values());

		return adminView(request, model, MANAGE_VIEW);
	}

	@GetMapping("/healthcheck")
	public ModelAndView checkWebsiteHealth(ModelAndView model, HttpServletRequest request) {
		return adminView(request, model, "healthOverview");
	}

	private static ModelAndView adminRedirect(HttpServletRequest request, ModelAndView model) {
		Map filters = CollectionFactory.createMap();
		Map params = request.getParameterMap();
		addFiltersInMapToFilterMap(filters, params);

		SessionUtil.addAllToSession(request, filters);

		return redirect(request, model, AdminController.ADMIN_WEBSITE_URL);
	}

	private static ModelAndView adminView(HttpServletRequest request, ModelAndView model, String viewName) {
		Map filters = CollectionFactory.createMap();
		Map params = request.getParameterMap();
		addFiltersInMapToFilterMap(filters, params);
		addFiltersInMapToFilterMap(filters, SessionUtil.getSessionAttributesMap(request));

		String[] filterKeys = (String[]) filters.keySet().toArray(new String[0]);
		SessionUtil.removeFromSession(request, filterKeys);

		String id = request.getParameter(WebsiteDao.ID);
		if(null == id)
			id = WebsiteController.getIds(request);

		if(null != id && 0 < id.length()) {
			model.addObject(WebsiteDao.ID, id);
			Website sel = WebsiteDao.findById(id);

			if(null != sel && !model.getModelMap().containsAttribute(SELECTED))
				model.addObject(SELECTED, sel);

			Map idMap = CollectionFactory.createMap();
			for(String i : id.split(",")) {
				int intId = Integer.valueOf(i.trim());
				idMap.put(intId, true);
			}

			if(0 < idMap.size())
				model.addObject(IDMAP, idMap);
		}

		model.addObject(FILTERS, filters);

		if(!model.getModelMap().containsAttribute(ITEMS))
			model.addObject(ITEMS, WebsiteDao.getAdminSelect());

		Map tags = CollectionFactory.createMap();
		tags.put(WebsiteDao.RECHTSGEBIED, TagDao.getType(Tag.RECHTSGEBIED));
		tags.put(WebsiteDao.NIEUWS_SOORT, TagDao.getType(Tag.NIEUWSSOORT));
		tags.put(WebsiteDao.RUBRIEKEN, TagDao.getType(Tag.RUBRIEK));

		model.addObject("allTags", tags);

		return view(request, model, viewName);
	}

	private static Map addFiltersInMapToFilterMap(Map filterMap, Map<String, Object> params) {
		for(String key : params.keySet()) {
			if(key.startsWith("db-filter")) {
				Object value = params.get(key);
				if(value instanceof String[])
					value = ((String[]) value)[0];
				filterMap.put(key, value);
			}
		}
		return filterMap;
	}
}
