package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.model.JSPMessage;
import nl.impressie.grazer.threads.ArticleTagUpdater;
import nl.impressie.grazer.util.ContextProvider;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.XMLUtil;
import nl.impressie.grazer.util.crawlerUtils.IndexManager;
import nl.impressie.grazer.util.crawlerUtils.IndexUtil;
import nl.impressie.grazer.util.jstlUtils.TagUtil;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.zeroturnaround.zip.ZipUtil;

import javax.net.ssl.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(IndexController.INDEX)
public class IndexController extends nl.impressie.grazer.controller.Controller {
	public static final String pathToXmlFile = "/WEB-INF/conf/urls.xml";
	public static final String pathToTestXmlFile = "/WEB-INF/conf/testurls.xml";
	public static final String pathToBadTestXmlFile = "/WEB-INF/conf/badtesturls.xml";

	public static final String INDEX_MODE_TEST_SUCCES = "testSucces";
	public static final String INDEX_MODE_TEST_FAILURE = "testFail";

	public static final String INDEX = "index";

	@Value("${indexing.maxdepth}")
	private int globalMaxDepth;
	@Value("${indexing.defaultmode}")
	private String defaultMode;

	//stops the crawler from having problems with ssl
	static {
		TrustManager[] trustAllCertificates = new TrustManager[]{
				new X509TrustManager() {
					@Override
					public X509Certificate[] getAcceptedIssuers() {
						return null; // Not relevant.
					}

					@Override
					public void checkClientTrusted(X509Certificate[] certs, String authType) {
						// Do nothing. Just allow them all.
					}

					@Override
					public void checkServerTrusted(X509Certificate[] certs, String authType) {
						// Do nothing. Just allow them all.
					}
				}
		};

		HostnameVerifier trustAllHostnames = new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true; // Just allow them all.
			}
		};

		try {
//            this is the line that kept breaking nu.nl
//            System.setProperty("jsse.enableSNIExtension", "false");
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCertificates, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(trustAllHostnames);
		} catch(GeneralSecurityException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	@RequestMapping("/reset")
	public ModelAndView resetIndex(HttpServletRequest request, ModelAndView model) {
		stopIndexing(request, model);

		IndexManager.reset();

		String msg = String.format(JSPMessage.SUCCES_REMOVED_THE_TYPE, "index");
		new JSPMessage(msg, JSPMessage.SUCCES).addToRequest(request);

		return redirect(request, model, AdminController.ADMIN_INDEX_URL);
	}

	public static ModelAndView getManagePage(HttpServletRequest request, ModelAndView model) {
		String viewName = "indexManage";

		model.addObject("running", IndexManager.isBusy());
		model.addObject("articles", Article.getAll());

		return view(request, model, viewName);
	}

	@RequestMapping(value = "/upload", consumes = {"multipart/form-data"})
	public ModelAndView downloadIndex(ModelAndView model, HttpServletRequest request, @RequestParam(name = "index", required = false) MultipartFile index) {
		String viewName = "redirect:/index/manage";

		try {
			String tempPath = "temp.zip";

			File temp = new File(tempPath);
			index.transferTo(temp);

			File indexFolder = new File(IndexUtil.getPath());

			ZipUtil.unpack(temp, indexFolder);

			temp.delete();
			IndexManager.destroy();
		} catch(IOException e) {
			LogUtil.logException(e);
		}

		return view(request, model, viewName);
	}

	@RequestMapping("/download")
	public ResponseEntity<byte[]> downloadIndex(ModelAndView model, HttpServletRequest Request) {
		return downloadIndex();
	}
	private ResponseEntity<byte[]> downloadIndex() { return downloadIndex(true); }
	private ResponseEntity<byte[]> downloadIndex(boolean firstRun) {
		try {
			ZipUtil.pack(new File(IndexUtil.getPath()), new File("temp.zip"));
		} catch(Exception e) {
			if(firstRun) {
				IndexUtil.breakOffIndexing();
				return downloadIndex(false);
			} else {
				LogUtil.logException(e);
			}
		}
		File file = new File("temp.zip");
		String filename = "index.zip";
		return fileToResponse(file, filename);
	}

	@RequestMapping("/stop")
	public ModelAndView stopIndexing(HttpServletRequest request, ModelAndView model) {
		IndexUtil.breakOffIndexing();
		JSPMessage message = new JSPMessage(JSPMessage.SUCCES_CRAWLER_STOPPED, JSPMessage.SUCCES);
		message.addToRequest(request);
		return redirect(request, model, AdminController.ADMIN_INDEX_URL);
	}

	@RequestMapping("/start")
	public ModelAndView makeIndex(
			HttpServletRequest request, ModelAndView model,
			@RequestParam(value = "mode", required = false, defaultValue = "") String mode,
			@RequestParam(value = "md", required = false, defaultValue = "-1") int md
	) {
		if(md == -1)
			md = globalMaxDepth;
		if(mode.length() == 0)
			mode = defaultMode;

		JSPMessage message = makeIndex(mode, md);

		message.addToRequest(request);
		return redirectBack(request, model);
	}

	public static JSPMessage makeIndex(String mode, int md) {
		JSPMessage message = new JSPMessage(JSPMessage.ERROR_CRAWLER_IS_RUNNING, JSPMessage.FAILURE);
		if(!IndexManager.isBusy()) {
			IndexUtil.startIndexThread(mode, ContextProvider.getServletContext().getRealPath("") + pathToXmlFile, false, md);

			message = new JSPMessage(JSPMessage.SUCCES_CRAWLER_STARTED, JSPMessage.SUCCES);
		}
		return message;
	}

	@RequestMapping(value = "/index/ajax/start", method = RequestMethod.POST)
	@ResponseBody
	public String makeIndexAjax(
			@RequestParam(value = "mode", required = false, defaultValue = "default") String mode
	) {
		ServletContext servletContext = ContextProvider.getServletContext();
		String path = servletContext.getRealPath("") + pathToXmlFile;
		if(mode.equals(INDEX_MODE_TEST_SUCCES))
			path = servletContext.getRealPath("") + pathToTestXmlFile;
		else if(mode.equals(INDEX_MODE_TEST_FAILURE))
			path = servletContext.getRealPath("") + pathToBadTestXmlFile;
		IndexManager manager = IndexManager.getInstance();
		String msg = JSPMessage.ERROR_CRAWLER_IS_RUNNING;
		JSPMessage message = new JSPMessage(msg, JSPMessage.FAILURE);

		if(IndexUtil.shouldStopIndexing()) {
			manager.startWriting();
			List<Website> targets = XMLUtil.parseXML(path);
			if(mode.equals(INDEX_MODE_TEST_SUCCES))
				targets = WebsiteDao.getAll();
			int index = 0;
			//if parsing has failed 10 times give up
			while(index < 10 && (targets.size() == 0 || (targets.size() == 1 && targets.get(0) == null))) {
				targets = XMLUtil.parseXML(path);
				index++;
			}
			if(targets.size() == 0 || (targets.size() == 1 && targets.get(0) == null)) {
				msg = String.format(JSPMessage.ERROR_PARSE_FAIL_TYPE, "XML config");
				message = new JSPMessage(msg, JSPMessage.FAILURE);
			} else {
				long startTime = System.nanoTime();
				IndexUtil.indexPages(targets, mode, startTime, true, globalMaxDepth);

				msg = String.format(JSPMessage.SUCCES_FINISHED_TYPE, "indexing");
				message = new JSPMessage(msg, JSPMessage.SUCCES);
			}
			manager.finishWritingAndClose();
		}

		JSONObject data = new JSONObject();
		data.put("message", message.toJson());
		return data.toString();
	}

	@RequestMapping("/retag")
	public ModelAndView retag(HttpServletRequest request, ModelAndView model) {
		if(IndexManager.isBusy()) {
			JSPMessage msg = new JSPMessage(JSPMessage.ERROR_CRAWLER_IS_RUNNING, JSPMessage.ERROR);
			msg.addToRequest(request);
		} else {
			Map<String, Tag> parents = new HashMap();
			for(TagUtil.TagFamily family: TagUtil.getTagFamilies(TagDao.getType(Tag.RECHTSGEBIED))) {
				Tag parent = family.getParent();
				for(Tag child: family.getChildren()) {
					parents.put(child.getId(), parent);
				}
			}

			for(Website page: WebsiteDao.getAll(false)) {
				List<Article> articles = page.getAllArticles();
				if(articles.size() == 0)
					continue;

				Article firstArt = articles.get(0);

				List<Tag> toRemove = new ArrayList();
				Map toAdd = new HashMap();

				boolean addRecht = page.getRechtsgebied().getSelector().length() == 0;
				boolean addRubriek = page.getRubrieken().getSelector().length() == 0;
				boolean addType = page.getNieuwssoort().getSelector().length() == 0;

				if(!(addRecht || addRubriek || addType))
					continue;

				if(addRecht)
					toRemove.addAll(firstArt.getTags(Tag.RECHTSGEBIED));

				if(addRubriek)
					toRemove.addAll(firstArt.getTags(Tag.RUBRIEK));

				if(addType)
					toRemove.addAll(firstArt.getTags(Tag.NIEUWSSOORT));

				for(Tag tag: page.getTags().values()) {
					switch(tag.getType()) {
						case Tag.RECHTSGEBIED:
							if(addRecht)
								break;
						case Tag.RUBRIEK:
							if(addRubriek)
								break;
						case Tag.NIEUWSSOORT:
							if(addType)
								break;
						default:
							continue;
					}
					toAdd.put(tag.getId(), tag);
					if(parents.containsKey(tag.getId())) {
						Tag parent = parents.get(tag.getId());
						if(!toAdd.containsKey(parent.getId()))
							toAdd.put(parent.getId(), parent);
					}
				}

				Map removeMap = new HashMap();
				for(Tag tag: toRemove) {
					removeMap.put(tag.getId(), tag);
				}

				if(toAdd.size() > 0 || toRemove.size() > 0) {
					System.out.println("Retagging " + articles.size() + " articles removing " + removeMap.size() + " tags and adding " + toAdd.size() + " tags");
					ArticleTagUpdater.updateTags(articles, removeMap, toAdd);
				}
			}
		}

		return redirectBack(request, model);
	}

	@RequestMapping("/reset/hard")
	public ModelAndView hardReset(HttpServletRequest request, ModelAndView model) {
		IndexManager.reset();
		IndexManager.getInstance().hardReset();
		return redirectBack(request, model);
	}
}