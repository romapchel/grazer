package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.*;
import nl.impressie.grazer.exception.PaymentException;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Bundle;
import nl.impressie.grazer.model.CustomPayment;
import nl.impressie.grazer.model.Invoice;
import nl.impressie.grazer.model.Mail.InvoiceMail;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.LinkUtil;
import nl.impressie.grazer.util.LogUtil;
import nl.stil4m.mollie.Client;
import nl.stil4m.mollie.ClientBuilder;
import nl.stil4m.mollie.ResponseOrError;
import nl.stil4m.mollie.concepts.CustomerPayments;
import nl.stil4m.mollie.concepts.Subscriptions;
import nl.stil4m.mollie.domain.*;
import nl.stil4m.mollie.domain.customerpayments.FirstRecurringPayment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class PaymentController extends nl.impressie.grazer.controller.Controller {

	public static final String BUNDLE = "bundle";

	public static final String PAID_URL = "mollie/paid";
	public static final String PAYWALL_URL = "mollie/paywall";
	private static final String WEBHOOK_URL = "/mollie/webhook";

	public static final String PAID_VIEW = "/payment/paid";
	public static final String PAYWALL_VIEW = "/mollie/paywall";

	private static final String MOLLIE_RECURRING_TYPE_FIRST = "first";

	@Value("${mollie.key}")
	private String mollie_api_key;
	@Value("${mollie.test.key}")
	private String mollie_test_api_key;

	@Value("${server.testmode}")
	private boolean testMode;

	private String getMollieId(CustomUser user) {
		String mollie_customer_id = user.getMollieId();

		if(null == mollie_customer_id)
			mollie_customer_id = user.createMollieId(getClient());

		return mollie_customer_id;
	}

	private String createPayment(CustomPayment customPayment) throws IOException, PaymentException {
		CustomUser user = customPayment.getUser();
		Bundle bundle = customPayment.getBundle();

		String redirectUrl = String.format(LinkUtil.generateAbsoluteUrl("%s?id=%d"), PAID_URL, customPayment.getId());

		Optional webhook = getWebHook();

		String mollie_customer_id = getMollieId(user);

		CustomerPayments customerPayments = getClient().customerPayments(mollie_customer_id);

		CreatePayment createPayment = bundle.getCreatePayment(redirectUrl, webhook);
		CustomerPayment customerPayment = new FirstRecurringPayment(createPayment);

		ResponseOrError<Payment> result = customerPayments.create(customerPayment);
		Payment payment = result.getData();
		if(result.getSuccess()) {
			Links links = payment.getLinks();
			customPayment.addPayment(payment.getId());
			return links.getPaymentUrl();
		} else {
			throw PaymentException.getException(result);
		}

	}

	private void startSubscription(CustomPayment customPayment) {
		try {
			Optional webhook = getWebHook();
			CreateSubscription createSubscription = customPayment.getBundle().getCreateSubscription(webhook);

			String customer_mollie_id = customPayment.getUser().getMollieId();
			Subscriptions subscriptions = getClient().subscriptions(customer_mollie_id);
			ResponseOrError<Subscription> result = subscriptions.create(createSubscription);
			if(result.getSuccess())
				customPayment.addSubscription(result.getData().getId());
			else
				throw PaymentException.getException(result);
		} catch(PaymentException | IOException e) {
			LogUtil.logException(e);
		}
	}

	@GetMapping("/mollie/subscribe")
	public ModelAndView purchaseSubscription(HttpServletRequest request, ModelAndView model,
											 @RequestParam(name = "bundle", defaultValue = "1") int bundleId) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		String url = purchaseSubscription(user, bundleId, true);

		return redirect(request, model, url);
	}

	private String purchaseSubscription(CustomUser user, int bundleId, boolean firstRun) {
		String url = PAYWALL_URL;
		try {
			CustomPayment active = user.getActiveSubscription(getClient());
			if(active != null)
				url = String.format("%s?id=%d", PAID_URL, active.getId());

			CustomPayment customPayment = PaymentDao.create(user.getId(), bundleId);

			url = createPayment(customPayment);
		} catch(PaymentException | IOException e) {
			LogUtil.logException(e);
		}

		return url;
	}

	private CustomPayment getCustomPayment(Payment payment) {
		CustomPayment cp = PaymentDao.getByInitialPayment(payment.getId());

		Optional<String> subscriptionId = payment.getSubscriptionId();
		boolean isSubscription = subscriptionId.isPresent();
		if(isSubscription) {
			String subscription_payment_id = subscriptionId.get();
			cp = PaymentDao.getBySubscription(subscription_payment_id);
		}

		return cp;
	}

	@RequestMapping(WEBHOOK_URL)@ResponseBody
	public void mollieWebhook(HttpServletRequest request, ModelAndView model,
								@RequestParam(name = "id", defaultValue = "") String id) throws IOException {
		if(id.trim().length() == 0)
			return;

		ResponseOrError<Payment> response = getClient().payments().get(id);
		if(response.getSuccess()) {
			Payment payment = response.getData();

			CustomPayment cp = getCustomPayment(payment);
			if(cp == null)
				return;

			if(cp.isPaid(getClient())) {
				if(!SubscriptionPaymentDao.paymentAlreadyHandled(id))
					completePayment(cp, id);

			} else if(cp.isRefunded(getClient())) {
				cp.getUser().rescindBundle(cp.getBundle());
			}
		}
	}

	@GetMapping(PAID_URL)
	public ModelAndView paymentComplete(HttpServletRequest request, ModelAndView model,
										@RequestParam(name = "id", defaultValue = "") int id) {
		CustomPayment customPayment = PaymentDao.getById(id);
		boolean succesful = false;
		boolean alreadyCompleted = false;
		if(null != customPayment) {
			alreadyCompleted = SubscriptionPaymentDao.paymentAlreadyHandled(customPayment.getInitialPaymentId());
			succesful = (testMode && !alreadyCompleted ? completePayment(customPayment, customPayment.getInitialPaymentId()) : customPayment.isPaid(getClient()));

			CustomUser currentUser = AuthenticationUtil.getCurrentUser();
			boolean paymentIsForCurrentUser = (null != currentUser && currentUser.getId() == customPayment.getUser().getId());
			if(paymentIsForCurrentUser) {
				Invoice mostRecentInvoice = customPayment.getMostRecentInvoice();
				if(mostRecentInvoice != null)
					model.addObject("invoiceId", mostRecentInvoice.getId());
			}
		}

		model.addObject("alreadyCompleted", alreadyCompleted);
		model.addObject("succesful", succesful);
		model.addObject("failed", !succesful);

		return view(request, model, PAID_VIEW);
	}

	private boolean completePayment(CustomPayment customPayment, String mollieId) {
		Client client = getClient();
		if(customPayment.isPaid(client)) {
			Payment payment = customPayment.getPayment(client);
			Optional<String> subscriptionId = payment.getSubscriptionId();

			if(subscriptionId.isPresent())
				customPayment = PaymentDao.getBySubscription(subscriptionId.get());
			else if(customPayment.getBundle().getTimes() > 1 && customPayment.getSubscriptionPaymentId() == null)
				startSubscription(customPayment);

			if(extendRole(customPayment)) {
				customPayment.completePayment(DateUtil.getTimestamp());

				Invoice invoice = InvoiceDao.create(customPayment);
				if(null != invoice) {
					new InvoiceMail(customPayment.getUser(), invoice).send();
					invoice.deletePdf();
					SubscriptionPaymentDao.create(customPayment, mollieId, invoice);
					return true;
				}
			}
		}

		return false;
	}

	@GetMapping("/mollie/cancel")
	public ModelAndView cancelSubscription(HttpServletRequest request, ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();

		CustomPayment customPayment = user.getPayment();

		try {
			Subscriptions subs = getClient().subscriptions(user.getMollieId());
			ResponseOrError<Subscription> cancel = subs.cancel(customPayment.getSubscriptionPaymentId());
			if(cancel.getSuccess()) {
				System.out.println("succes");
			} else {
				throw PaymentException.getException(cancel);
			}
		} catch(IOException | PaymentException e) {
			e.printStackTrace();
		}

		return redirect(request, model, PAYWALL_VIEW);
	}

	private boolean extendRole(CustomPayment payment) {
		Bundle bundle = payment.getBundle();
		CustomUser user = payment.getUser();
		if(user == null)
			return false;

		user.updateAccountType(bundle);

		return (int)user.getAccountType().getId() == bundle.getTypeId();
	}

	@GetMapping(PAYWALL_URL)
	public ModelAndView paywall(HttpServletRequest request, ModelAndView model) {
		CustomPayment active = AuthenticationUtil.getCurrentUser().getActiveSubscription(getClient());
		if(null != active)
			return redirect(request, model, AccountController.USER_SUBSCRIPTION_URL);

		model.addObject("bundles", BundleDao.getAll());
		String view = "payment/paywall";

		return view(request, model, view);
	}

	public static ModelAndView postManagePage(HttpServletRequest request, ModelAndView model) {
		String description = safeGetParameter(request.getParameterMap(), BundleDao.DESCRIPTION, "");
		String viewName = makeRedirect(AdminController.ADMIN_BUNDLE_URL);
		if(description.length() > 0) {
			Bundle bundle = BundleDao.create(0, 0, 1, "m", description);
			if(null != bundle)
				viewName = makeRedirect(String.format("%s/%d", AdminController.ADMIN_BUNDLE_URL, bundle.getId()));
		}

		return view(request, model, viewName);
	}

	public static ModelAndView getManagePage(HttpServletRequest request, ModelAndView model) {
		String view = "payment/manage";

		model.addObject("bundles", BundleDao.getAll());

		return view(request, model, view);
	}
	public static ModelAndView postBundleEdit(HttpServletRequest request, ModelAndView model, String id) {
		boolean limitRepition = Boolean.parseBoolean(safeGetParameter(request, "limited-repitition", "false"));
		String action = safeGetParameter(request, Selections.ACTION, "");

		String view = AdminController.ADMIN_BUNDLE_URL;
		switch(action) {
			case Actions.DELETE:
				Bundle bundle = BundleDao.findById(id);
				if(null != bundle)
					bundle.delete();
				break;
			case Actions.CREATE:
			default:
				view = save(request, id, limitRepition);
		}
		return redirect(request, model, view);
	}

	private static String save(HttpServletRequest request, String id, boolean limitRepition) {
		Map parameters = request.getParameterMap();
		int times = -1;
		if(limitRepition) {
			times = 1;
			if(parameters.containsKey(BundleDao.TIMES)) {
				try { times = Integer.valueOf(request.getParameter(BundleDao.TIMES)); } catch(NumberFormatException e) {}
			}
		}
		int intervalNumber = 1;
		if(parameters.containsKey(BundleDao.INTERVAL_NUMBER)) {
			try { intervalNumber = Integer.valueOf(request.getParameter(BundleDao.INTERVAL_NUMBER)); } catch(NumberFormatException e) {}
		}
		int price = 1;
		if(parameters.containsKey(BundleDao.PRICE)) {
			try { price = Integer.valueOf(request.getParameter(BundleDao.PRICE)); } catch(NumberFormatException e) {}
		}
		String intervalType = (parameters.containsKey(BundleDao.INTERVAL_TYPE) ? request.getParameter(BundleDao.INTERVAL_TYPE) : "m");
		String beschrijving = (parameters.containsKey(BundleDao.DESCRIPTION) ? request.getParameter(BundleDao.DESCRIPTION) : "m");

		Bundle bundle = BundleDao.findById(id);
		if(bundle == null)
			bundle = BundleDao.create(price, times, intervalNumber, intervalType, beschrijving);
		else
			bundle = bundle.update(price, times, intervalNumber, intervalType, beschrijving);

		return String.format("%s/%d", AdminController.ADMIN_BUNDLE_URL, bundle.getId());
	}

	public static ModelAndView getBundleEdit(HttpServletRequest request, ModelAndView model, String id) {
		Bundle bundle = BundleDao.findById(id);
		String view = "payment/edit";

		if(bundle == null) {
			view = makeRedirect(AdminController.ADMIN_BUNDLE_URL);
		} else {
			Map repetitions = CollectionFactory.createMap();
			repetitions.put("d", "dag(en)");
			repetitions.put("m", "maand(en)");
			repetitions.put("y", "jaar");
			model.addObject("repetitions", repetitions);

			model.addObject("bundle", bundle);
		}

		return view(request, model, view);
	}

	@PostMapping("/mollie/bundle/create")
	public ModelAndView createBundle(HttpServletRequest request, ModelAndView model,
			 @RequestParam(name = "price", defaultValue = "1") int price,
			 @RequestParam(name = "times", defaultValue = "1") int times,
			 @RequestParam(name = "intervalNmbr", defaultValue = "1") int intervalNmbr,
			 @RequestParam(name = "intervalType", defaultValue = "d") String intervalType,
			 @RequestParam(name = "description", defaultValue = "") String description) {
		BundleDao.create(price, times, intervalNmbr, intervalType, description, AccountTypeDao.DEFAULT_TYPE_ID);

		return redirect(request, model, PAYWALL_URL);
	}

	public Client getClient() {
		String key = (testMode ? mollie_test_api_key : mollie_api_key);
		return getClient(key);
	}
	public static Client getClient(String mollie_api_key) {
		if(mollie_api_key == null)
			return null;

		return new ClientBuilder()
				.withApiKey(mollie_api_key)
				.build();
	}

	public Optional getWebHook() {
		Optional webhook = Optional.empty();
		if(!testMode) {
			String webhookUrl = LinkUtil.generateAbsoluteUrl(WEBHOOK_URL);
			webhook = Optional.of(webhookUrl);
		} else {
//            webhook = Optional.of("http://lexicons.be/grazer/webhook");
		}

		return webhook;
	}

	@RequestMapping("/mollie/invoice")
	public ModelAndView viewInvoices(ModelAndView model, HttpServletRequest request
	) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		List invoices = CollectionFactory.createList();
		if(null != user)
			invoices = InvoiceDao.findForUser(user.getId());

		model.addObject("invoices", invoices);

		return view(request, model, "user/invoices");
	}

	@RequestMapping("/mollie/invoice/download")
	public ResponseEntity<byte[]> downloadIndex(ModelAndView model, HttpServletRequest request,
									 @RequestParam(name = PaymentDao.ID, defaultValue = "0") int id
	) {
		Invoice invoice = InvoiceDao.findById(id);
		ResponseEntity<byte[]> response;
		if(invoice != null) {
			response = fileToResponse(invoice.getInvoice(), "invoice.pdf");
			invoice.deletePdf();
		} else {
			response = new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		return response;
	}
}
