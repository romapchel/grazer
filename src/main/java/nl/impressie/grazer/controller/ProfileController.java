package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.*;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.hibernate.services.ProfileService;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.JSPMessage;
import nl.impressie.grazer.model.Page;
import nl.impressie.grazer.model.Pagination;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.model.User.Role;
import nl.impressie.grazer.util.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static nl.impressie.grazer.controller.Controller.Actions.*;
import static nl.impressie.grazer.controller.Controller.Selections.ACTION;
import static nl.impressie.grazer.controller.Controller.Selections.PAGE;

/**
 * @author Impressie
 */
@RequestMapping("/profile")
@Controller
public class ProfileController extends nl.impressie.grazer.controller.Controller {

	private static Pagination pagination;
	private static Integer MAX_PROFILES = 255;

	@GetMapping("/create")
	public ModelAndView getCreateView(ModelAndView model, HttpServletRequest request) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		if(user.getProfiles().size() >= MAX_PROFILES) {
			SessionUtil.addMessageToSession(request, new JSPMessage("messages.error.profiles.max_exceeded", JSPMessage.ERROR));
			return redirect(request, model, "profile");
		}

		return view(request, model, "profile/create");
	}
	@PostMapping("/create")
	public ModelAndView create(ModelAndView model, HttpServletRequest request,
							   @RequestParam(value = ProfileDao.PROFILE_NAME, defaultValue = "") String name
	) {
		CustomUser user = AuthenticationUtil.getCurrentUser();

		String[] rechtsgebieden = request.getParameterValues(Tag.RECHTSGEBIED + "[]");
		if(rechtsgebieden == null)
			rechtsgebieden = new String[0];
		Profile profile = null;

		Map errors = ProfileDao.validateCreate(request.getParameterMap(), false, false);
		if(user.getProfiles().size() >= MAX_PROFILES)
			errors.put("max_profiles", "messages.error.profiles.max_exceeded");

		if(0 == errors.size())
			profile = ProfileDao.create(name, user.getId());
		else
			model.addObject("errors", errors);


		String view = "profile/create";
		if(null == profile) {
			Map previous = CollectionFactory.createMap();
			previous.put(ProfileDao.PROFILE_NAME, name);
			Map prevRecht = CollectionFactory.createMap();
			for(String recht: rechtsgebieden) {
				prevRecht.put(recht, true);
			}
			previous.put(Tag.RECHTSGEBIED, prevRecht);

			model.addObject("previous", previous);

		} else {
			if(null != rechtsgebieden && 0 < rechtsgebieden.length)
				profile.updateTagsByType(Tag.RECHTSGEBIED, rechtsgebieden);

			model.addObject("profile", profile);
			view = makeRedirect("profile/create/" + profile.getId());
		}

		// set new profile currentActive
		if(profile != null)
			AuthenticationUtil.getCurrentUser().setActiveProfileId(profile.getId());

		// redirect direct search - edit profile
		if (request.getParameter("submit").equals("opslaan-zoek"))
			view = makeRedirect("search");

		if (request.getParameter("submit").equals("opslaan-verfijn"))
			view = makeRedirect("profile/show?action_edit=true&selection_page=3&id=" + profile.getId());

		return view(request, model, view);
	}

	@GetMapping("/create/{ID}")
	public ModelAndView createdLanding(ModelAndView model, HttpServletRequest request,@PathVariable("ID") String id) {
		Profile profile = ProfileDao.findById(id);
		int authenticatedUserId = AuthenticationUtil.getCurrentUser().getId();
		int profileOwnerUserId = profile.getUserId();
		if(profile != null && profileOwnerUserId == authenticatedUserId)
			model.addObject("profile", profile);

		return view(request, model, "profile/landing_created");
	}

	@RequestMapping("")
	public ModelAndView profileLanding(ModelAndView model, HttpServletRequest request) {
		String id = request.getParameter(String.format("%s-%s", ProfileDao.TABLE_NAME, ProfileDao.ID));
		if(null != id) {
			AuthenticationUtil.getCurrentUser().setActiveProfileId(id);
			model.addObject("amount_profiles", ProfileDao.findByUserId(AuthenticationUtil.getCurrentUser().getId()).size());
		}
		model.addObject("show_globals", 0 < ProfileDao.findByUserId(0).size());

		return view(request, model, "profile/landing");
	}

	@RequestMapping("/show")
	public ModelAndView settings(HttpServletRequest request, ModelAndView model) {
		String pg = request.getParameter(PAGE);
		if(null == pg)
			pg = "1";

		Profile profile = null;

		CustomUser user = AuthenticationUtil.getCurrentUser();

		if(user != null) {
			String profId = request.getParameter(ProfileDao.ID);
			String editParameter = request.getParameter(Actions.EDIT);
			boolean edit = String.valueOf(true).equals(editParameter);

			Profile prof = (profId == null ? null : ProfileDao.findById(profId));
			if(null == prof && null == user.getActiveProfile()) {
				List<Profile> profs = user.getProfiles();
				if(0 < profs.size())
					prof = profs.get(0);
			}

			if(null != prof)
				user.setActiveProfileId(prof.getId());

			List<Profile> profiles = user.getProfiles();
			if(user.hasRole(Role.GLOBAL_EDIT))
				profiles.addAll(ProfileDao.findByUserId(0));

			model.addObject("profiles", profiles);
			model.addObject("edit", edit);
			model.addObject("listOfProfiles", user.getProfiles());

			profile = user.getActiveProfile();

			if(null != profile) {
				model.addObject("active", profile);
			}
		}

		Pagination pagination = getPagination();
		if(pagination.getPage(pg).getContentType().equals(ProfileDao.PROFILE_SITE)) {
			if(null != profile) {
				Map tags = profile.getTags();

				List sites = (0 < tags.size() ? TagPageDao.findByTagIds(tags.keySet()) : WebsiteDao.getAll(true));

				model.addObject("allSites", WebsiteDao.modelListToMap(sites));
			}
		} else {
			model.addObject("allTags", TagDao.getAllTagsAsMap());
		}

		model.addObject("pagination", pagination);
		model.addObject("global_pagination", getPagination());
		model.addObject("current_page", pg);


		return view(request, model, "profile/view");
	}

	@RequestMapping("/save")
	public ModelAndView saveGetRequest(HttpServletRequest request, ModelAndView model,
									   @RequestParam(value = Selections.PAGE, defaultValue = "1", required = false) int page) {

		CustomUser user = AuthenticationUtil.getCurrentUser();
		Profile prof = user.getActiveProfile();
		model.addObject("listOfProfiles", user.getProfiles());

		boolean userCanEditGlobal = user.hasRole(Role.GLOBAL_EDIT);
		boolean profileBelongsToUser = prof.getUserId() == user.getId();

		boolean update = profileBelongsToUser || (prof.isGlobal() && userCanEditGlobal);

		if(update)
			prof = updateProfile(request, page, prof);

		String global = request.getParameter("global");
		if(userCanEditGlobal)
			prof = prof.updateGlobal("true".equals(global));

		String viewName = "/search";
		int nextPage = nextPage(request, page);
		if(nextPage > 0)
			viewName = String.format("/profile/show?%s=%s&%s=%d", Actions.EDIT, String.valueOf(true), Selections.PAGE, nextPage);

		return redirect(request, model, viewName);
	}

	private Profile updateProfile(HttpServletRequest request, int pagenumber, Profile prof) {
		Page page = getPagination().getPage(pagenumber);
		String type = page.getContentType();
		Map params = request.getParameterMap();
		String[] items = (String[]) params.get("items[]");
		if(items == null)
			items = new String[0];

		String name = request.getParameter(ProfileDao.PROFILE_NAME);
		if(null != name)
			prof.updateName(name);

		switch(type) {
			case ProfileDao.PROFILE_TAG:
				prof = prof.updateTagsByType(page.getType(), items);
				break;
			case ProfileDao.PROFILE_SITE:
				prof = prof.updateSites(items);
				break;
			case ProfileDao.PROFILE_SEARCH:
				String title = request.getParameter(ProfileDao.TITLE_SEARCH);
				String content = request.getParameter(ProfileDao.CONTENT_SEARCH);
				prof = prof.updateSearch(title, content);
				break;
			case ProfileDao.MAIL_OPTION:
				String option = request.getParameter(ProfileDao.MAIL_OPTION);
				prof = prof.updateMail(option);
		}

		return prof;
	}

	@RequestMapping("/action")
	public ModelAndView action(HttpServletRequest request, ModelAndView model) {


		String action = request.getParameter(ACTION);
		String id = request.getParameter(ProfileDao.ID);


	if(null != action) {
			if(CREATE.equals(action)) {
				Profile newProf = ProfileDao.create(request);
				if(null != newProf)
					id = newProf.getId().toString();
			} else if(DELETE.equals(action)) {
				ProfileDao.delete(id);
			}
		}

		String url = "/profile/show";
		boolean edit = false;
		if(null == id && null == action) {
			id = request.getParameter(Actions.EDIT);
			if(id != null)
				edit = true;
			else
				id = request.getParameter(Actions.VIEW);
		}

		if(null != id)
			url = String.format("%s?%s=%s", url, ProfileDao.ID, id);

		if(edit)
			url = String.format("%s&%s=%s", url, Actions.EDIT, String.valueOf(edit), "weekly_mail", request.getParameter("weekly_mail"));

		return redirect(request, model, url);
	}

	@RequestMapping(value = "/import", consumes = {"multipart/form-data"})
	public ModelAndView importProfile(ModelAndView model, HttpServletRequest request, @RequestParam(name = "xml", required = false) MultipartFile xml) {
		String url = "profile/show";
		try {
			String tempPath = String.format("temp_profile%d", DateUtil.getTimestamp());

			File temp = new File(tempPath);
			xml.transferTo(temp);

			Profile prof = XMLUtil.parseProfile(tempPath);

			if(prof != null)
				url = String.format("%s?id=%s", url, prof.getId().toString());

			temp.delete();
		} catch(IOException e) {
			LogUtil.logException(e);
		}


		return redirect(request, model, url);
	}

	@RequestMapping("/export")
	public ModelAndView export(HttpServletRequest request, ModelAndView model) {
		String id = request.getParameter(ProfileDao.ID);
		String url = "profile/export";
		Profile profile = ProfileDao.findById(id);
		String date = DateUtil.convertTimestampToFormat(DateUtil.getTimestamp(), "yyyy-MM-dd HH:mm:ss");

		if(null == profile)
			LogUtil.log(String.format("No profile found with id: %s", id));

		model.addObject("profile", profile);
		model.addObject("date", date);

		model.setViewName(url);

		return model;
	}

	private Pagination getPagination() {
		if(null == pagination) {
			pagination = new Pagination();
			Page page;
			int pagenumber = 1;

			page = new Page(pagenumber);
			page.setType(Tag.RECHTSGEBIED);
			page.setContentType(ProfileDao.PROFILE_TAG);
			page.setShowChildren(false);
			pagination.addPage(page);
			pagenumber++;

			page = new Page(pagenumber);
			page.setType(Tag.RECHTSGEBIED);
			page.setShowParents(false);
			page.setContentType(ProfileDao.PROFILE_TAG);
			pagination.addPage(page);
			pagenumber++;

			page = new Page(pagenumber);
			page.setType(Tag.RUBRIEK);
			page.setContentType(ProfileDao.PROFILE_TAG);
			pagination.addPage(page);
			pagenumber++;

			page = new Page(pagenumber);
			page.setType(Tag.NIEUWSSOORT);
			page.setContentType(ProfileDao.PROFILE_TAG);
			pagination.addPage(page);
			pagenumber++;


//
//			page = new Page(5);
//			page.setContentType(ProfileDao.PROFILE_SEARCH);
//			pagination.addPage(page);
		}

		return pagination;
	}

	private int nextPage(HttpServletRequest request, int page) {
		String action = request.getParameter(ACTION);
		if(NEXT_PAGE.equals(action))
			page++;
		else if(PREV_PAGE.equals(action))
			page--;

		if(page < 1)
			page = 1;
		else if(page > getPagination().size())
			page = -1;

		return page;
	}

	@RequestMapping("/global")
	public ModelAndView globalProfiles(ModelAndView model, HttpServletRequest request) {
		String profId = request.getParameter(ProfileDao.ID);
		String action = request.getParameter(Selections.ACTION);

		Profile prof = ProfileDao.findById(profId);
		CustomUser user = AuthenticationUtil.getCurrentUser();

		String url = "profile/addGlobal";

		if(prof != null && 0 == prof.getUserId()) {
			if(Actions.VIEW.equals(action)) {
				model.addObject("selected_profile", prof);
			} else if(Actions.COPY.equals(action)) {
				Profile newProf = ProfileDao.copyProfile(prof, user);
				url = String.format("redirect:/profile/show?id=%s", newProf.getId().toString());
			}
		}

		List<Profile> globalProfiles = ProfileDao.findByUserId(0);
		globalProfiles.sort(Comparator.comparing(o -> o.getName().toLowerCase()));
		model.addObject("global_profiles", globalProfiles);
		model.addObject("pagination", getPagination());

		return view(request, model, url);
	}
}
