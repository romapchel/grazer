package nl.impressie.grazer.controller;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.hibernate.models.UnknownTagModel;
import nl.impressie.grazer.hibernate.services.UnknownTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Impressie
 */
@Controller
@RequestMapping(UnknownTagController.UNKNOWN_TAG)
public class UnknownTagController extends nl.impressie.grazer.controller.Controller {
	private static final String UNKNOWN_TAG_VIEW = "tag/unknown";

	public static final String UNKNOWN_TAG = TagController.TAG + "/unknown";

	@Autowired
	private UnknownTagService unknownTagService;

	private String getIds(HttpServletRequest request) {
		String[] params = {Actions.EDIT, Actions.TOGGLE, Actions.DELETE, Actions.ENABLE, Actions.DISABLE, Actions.MERGE, Selections.SELECTION, Selections.ALL};

		return getIds(request, params);
	}

	@RequestMapping("/action")
	public ModelAndView action(ModelAndView model, HttpServletRequest request) {
		Map params = request.getParameterMap();
		String id = getIds(request);


		return prepRedirect(model, request);
	}


	public ModelAndView getManagePage(HttpServletRequest request, ModelAndView model, String id) {
		return prepManagementPage(model, request, id);
	}

	public ModelAndView prepRedirect(ModelAndView model, HttpServletRequest request) {
		return redirect(request, model, AdminController.ADMIN_TAG_URL);
	}

	public ModelAndView prepManagementPage(ModelAndView model, HttpServletRequest request, String id) {
		Map ids = CollectionFactory.createMap();
		for(String i : id.split(",")) {
			UnknownTagModel t = unknownTagService.get(i);

			if(null != t) ids.put(t.getId(), t);
		}
		model.addObject("selected_tags", ids);

		model.addObject("tags", unknownTagService.getAll());

		return view(request, model, UNKNOWN_TAG_VIEW);
	}

}
