package nl.impressie.grazer.controller;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.MailUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Impressie
 */
@Controller
public class ContactController extends nl.impressie.grazer.controller.Controller {
	@GetMapping("/contact")
	public ModelAndView getContactPage(HttpServletRequest request, ModelAndView model) {
		model = adduserMail(model);
		return view(request, model, "contactView");
	}

	@PostMapping("/contact")
	public ModelAndView sendContactRequest(HttpServletRequest request, ModelAndView model,
										   @RequestParam(name = "mail", defaultValue = "") String mail,
										   @RequestParam(name = "subject", defaultValue = "") String subject,
										   @RequestParam(name = "message", defaultValue = "") String message) {

		Map<String, String> errors = CollectionFactory.createMap();

		if(0 == mail.length())
			errors.put("mail", "Dit veld is verplicht");

		if(0 == message.length())
			errors.put("message", "Dit veld is verplicht");

		if(0 == subject.length())
			errors.put("subject", "Dit veld is verplicht");

		String url = "redirect:/suggestion";
		if(0 == errors.size()) {
			MailUtil.sendMail(subject, message);
			url = "redirect:/";
		} else {
			Map prev = CollectionFactory.createMap();
			prev.put("mail", mail);
			prev.put("subject", subject);
			prev.put("message", message);

			model.addObject("previous", prev);
			model.addObject("errors", errors);
		}

		return view(request, model, url);
	}

	@GetMapping("/suggestion")
	public ModelAndView getsuggestionPage(HttpServletRequest request, ModelAndView model) {
		model = adduserMail(model);
		return view(request, model, "suggestionView");
	}

	private ModelAndView adduserMail(ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		Map prev = CollectionFactory.createMap();
		if(null != user)
			prev.put("mail", user.getMail());

		model.addObject("previous", prev);

		return model;
	}

	@PostMapping("/suggestion")
	public ModelAndView sendSuggestion(HttpServletRequest request, ModelAndView model,
									   @RequestParam(name = "mail", defaultValue = "") String mail,
									   @RequestParam(name = "suggestion-type", defaultValue = "") String type,
									   @RequestParam(name = "site", defaultValue = "") String site,
									   @RequestParam(name = "message", defaultValue = "") String message) {
		Map<String, String> errors = CollectionFactory.createMap();

		boolean siteSuggestion = "site".equals(type.toLowerCase());
		if(siteSuggestion && 0 == site.length())
			errors.put("site", "Als u een nieuwe site wilt suggesteren moet u het addres van de site hier invullen");

		if(0 == mail.length())
			errors.put("mail", "Dit veld is verplicht");

		if(0 == message.length())
			errors.put("message", "Dit veld is verplicht");

		String subject = String.format("%s suggestion", type);
		if(siteSuggestion)
			subject = String.format("%s: %s", subject, site);

		String url = "redirect:/suggestion";
		if(0 == errors.size()) {
			MailUtil.sendMail(subject, message);
			url = "redirect:/";
		} else {
			Map prev = CollectionFactory.createMap();
			prev.put("mail", mail);
			prev.put("site", site);
			prev.put("message", message);
			prev.put("suggestion-type", type);

			model.addObject("previous", prev);
			model.addObject("errors", errors);
		}

		return view(request, model, url);
	}
}
