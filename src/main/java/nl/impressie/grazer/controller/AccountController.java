package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.AccountTypeDao;
import nl.impressie.grazer.dao.RoleDao;
import nl.impressie.grazer.dao.UserDao;
import nl.impressie.grazer.dao.VerificationDao;
import nl.impressie.grazer.exception.PaymentException;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.CustomPayment;
import nl.impressie.grazer.model.DB.Verification;
import nl.impressie.grazer.model.JSPMessage;
import nl.impressie.grazer.model.Mail.VerificationMail;
import nl.impressie.grazer.model.User.AccountType;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.spring.beans.Captcha;
import nl.impressie.grazer.util.*;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.stil4m.mollie.Client;
import nl.stil4m.mollie.ResponseOrError;
import nl.stil4m.mollie.concepts.Subscriptions;
import nl.stil4m.mollie.domain.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
@Controller
public class AccountController extends nl.impressie.grazer.controller.Controller {
	@Value("${server.testmode}")
	private boolean testMode;

	@Value("${debug.account.id: 1}")
	private int debugAccountId;

	@Value("${mollie.key}")
	private String mollie_api_key;
	@Value("${mollie.test.key}")
	private String mollie_test_api_key;

	@Autowired
	private MessageSource messageSource;

	private static final String AFTER_LOGIN = "search";
	private static final String LOGIN_URL = "login";

	public static final String USER = "account";

	public static final String USER_SUBSCRIPTION_URL = "user/subscription";
	public static final String USER_SUBSCRIPTION_CANCEL_URL = "user/subscription/cancel";

	@GetMapping("/user/landing")
	public ModelAndView dashboard(HttpServletRequest request, ModelAndView model) {
		String viewName = PaymentController.PAID_VIEW;
		model.addObject("alreadyCompleted", true);
		return view(request, model, viewName);
	}

	@GetMapping("/verify/resend")
	public ModelAndView resendVerification(HttpServletRequest request, ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		String url = "/";
		if(!user.isActive()) {
			url = "registered";
			Verification verification = user.getVerification();
			if(verification != null)
				verification.delete();

			verification = VerificationDao.generate(user);
			new VerificationMail(user, verification).send();
		}

		return redirect(request, model, url);
	}

	@GetMapping("/verify")
	public ModelAndView verifyUser(HttpServletRequest request, ModelAndView model, @RequestParam(value = "code", defaultValue = "-1") String code) {

		Verification verification = VerificationDao.findByCode(code);
		boolean succes = false;
		if(verification != null) {
			succes = verification.activateUser();
			if(succes) {
				CustomUser user = verification.getUser();
				AuthenticationUtil.setLoggedInUser(user);
				user.login();
				verification.delete();
			}
		}
		model.addObject("succes", succes);

		return view(request, model, "user/verified");
	}

	@GetMapping("/logout")
	public String logoutDo(HttpServletRequest request) {
		SecurityContextHolder.clearContext();
		HttpSession session = request.getSession(false);
		if(session != null) {
			session.invalidate();
		}
		for(Cookie cookie : request.getCookies()) {
			cookie.setMaxAge(0);
		}

		return "logout";
	}

	@GetMapping(LOGIN_URL)
	public ModelAndView getLogin(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "service", required = false) String service,
			@RequestParam(value = "logout", required = false) String logout,
			HttpServletRequest request, ModelAndView model) {

		String view = "login";

		if(AuthenticationUtil.getCurrentUser() == null && testMode && debugAccountId != 0)
			AuthenticationUtil.setLoggedInUser(UserDao.findById(debugAccountId));

		CustomUser user = AuthenticationUtil.getCurrentUser();
		if(user != null) {
			new JSPMessage(JSPMessage.ERROR_ALREADY_LOGGED_IN, JSPMessage.FAILURE).addToRequest(request);
			return redirect(request, model, AFTER_LOGIN);
		}

		JSPMessage message = null;
		String username = SessionUtil.getAndRemoveStringFromSession(request, "username");
		if(error != null) {
			message = new JSPMessage(JSPMessage.ERROR_INCORRECT_LOGIN, JSPMessage.FAILURE);
			CustomUser temp = UserDao.findByUsername(username, false);

			if(temp != null) {
				if(!temp.isEnabled()) {

				}
			}
		} else if(service != null) {
			message = new JSPMessage(JSPMessage.ERROR_DB_CONNECTION, JSPMessage.FAILURE);
		} else if(logout != null) {
			username = null;
			message = new JSPMessage(JSPMessage.SUCCES_LOGOUT, JSPMessage.SUCCES);
		}

		if(username != null && username.length() > 0)
			model.addObject("username", username);

		if(message != null)
			model.addObject("message", message);

		return view(request, model, view);
	}

	@RequestMapping("/accounts/show")
	public ModelAndView showAllAccounts(HttpServletRequest request, ModelAndView model) {
		List<CustomUser> users = UserDao.getAll();

		model.addObject("users", users);

		return view(request, model, "accountView");
	}

	@RequestMapping("/account/delete")
	public ModelAndView deleteAccount(HttpServletRequest request, ModelAndView model, @RequestParam(value = "id", required = false) String id) {
		CustomUser user = UserDao.findById(id);
		JSPMessage message = new JSPMessage(JSPMessage.ERROR_REMOVE_USER, JSPMessage.FAILURE);

		if(user != null) {
			String name = user.getUsername();
			user.delete();
			String msg = JSPMessage.SUCCES_REMOVE_USER;
			message = new JSPMessage(msg, JSPMessage.SUCCES);
		}

		message.addToRequest(request);

		return redirect(request, model, AdminController.ADMIN_USER_URL);
	}

	@RequestMapping("/accounts/save")
	public ModelAndView saveAccount(
			HttpServletRequest request, ModelAndView model,
			@RequestParam(value = UserDao.ID, required = false) int userId,
			@RequestParam(value = "role", required = false) String role,
			@RequestParam(value = "roleTime", required = false) String time,
			@RequestParam(value = "profile", required = false) String profID) {
		int untill = -1;
		try {
			untill = DateUtil.getDaysAfterTodayAsInt(Integer.parseInt(time));
		} catch(NumberFormatException e) {}

		CustomUser user = UserDao.findById(userId);
		JSPMessage message;
		if(user == null) {
			String msg = String.format(JSPMessage.ERROR_NOT_FOUND_TYPE, "User");
			message = new JSPMessage(msg, JSPMessage.FAILURE, false);
		} else {

			String msg = String.format(JSPMessage.SUCCES_SAVED_THE_SPECIFIC, "User", user.getUsername());
			message = new JSPMessage(msg, JSPMessage.SUCCES, false);
			if(user.getProfiles().size() > 0) {
				try {
					user.setActiveProfileId(profID);
				} catch(NumberFormatException e) {
					msg = String.format(JSPMessage.ERROR_INVALID, "Profile");
					message = new JSPMessage(msg, JSPMessage.FAILURE, false);
				}
			}
		}
		message.addToRequest(request);
		return redirect(request, model, "accounts/show");
	}

	@RequestMapping("/accounts/new")
	public ModelAndView newAccount(
			HttpServletRequest request, ModelAndView model,
			@RequestParam(value = UserDao.USERNAME, required = false) String username,
			@RequestParam(value = UserDao.PASSWORD, required = false) String pass,
			@RequestParam(value = "role", required = false) String role,
			@RequestParam(value = "roleTime", required = false) String timeString) {
		int time;
		try {
			time = DateUtil.getDaysAfterTodayAsInt(Integer.parseInt(timeString));

		} catch(NumberFormatException e) {
			String msg = String.format(JSPMessage.ERROR_INVALID, "days the role is valid");
			new JSPMessage(msg, JSPMessage.FAILURE).addToRequest(request);
			return redirect(request, model, "accounts/show");
		}

		createUserAndRole(username, pass, role, time).addToRequest(request);

		return redirect(request, model, "accounts/show");
	}

	private JSPMessage createUserAndRole(String username, String pass, String role, int time) {
		CustomUser user = UserDao.findByUsername(username);

		if(user != null) {
			String msg = String.format(JSPMessage.ERROR_ALREADY_EXISTS_SPECIFIC, "an account", "username", username);
			JSPMessage message = new JSPMessage(msg, JSPMessage.FAILURE);
			return message;
		}

//        CustomUser.create(username, pass, role, time);

//        String msg = String.format(JSPMessage.SUCCES_SAVED_THE_SPECIFIC, "User", username);
//        JSPMessage message = new JSPMessage(msg, JSPMessage.SUCCES);
		JSPMessage message = new JSPMessage("Currently not implemented, please contact your administrator", JSPMessage.FAILURE);
		return message;
	}

	@GetMapping("/user/edit")
	public ModelAndView editAccountGet(HttpServletRequest request, ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		model.addObject("uid", user.getId());
		model.addObject("current", user.loadAccountValues());

		model = addActiveDataToModel(model, user);

		return view(request, model, "user/editView");
	}

	private ModelAndView addActiveDataToModel(ModelAndView model, CustomUser user) {
		List active = user.getActiveSubscriptions(PaymentController.getClient(getMollieApiKey()));
		if(active.size() > 0)
			model.addObject("active", active);

		model.addObject("accountType", user.getAccountType());

		model.addObject("invoices", user.getInvoices());

		return model;
	}

	@PostMapping("/user/edit")
	public ModelAndView editAccountPost(
			@RequestParam(value = UserDao.ID, required = false, defaultValue = "") String id,
			@RequestParam(value = "old" + UserDao.PASSWORD, required = false) String oldpass,
			@RequestParam(value = UserDao.PASSWORD, required = false) String pass,
			@RequestParam(value = UserDao.PASSWORD + "-repeat", required = false) String passRepeat,
			@RequestParam(value = UserDao.ORGANISATION, required = false) String organisation,
			@RequestParam(value = UserDao.INITIALS, required = false) String initials,
			@RequestParam(value = UserDao.TUSSENVOEGSELS, required = false) String tsvg,
			@RequestParam(value = UserDao.LASTNAME, required = false) String lastname,
			@RequestParam(value = UserDao.STREET, required = false) String street,
			@RequestParam(value = UserDao.HOUSENUMBER, required = false) String housnumber,
			@RequestParam(value = UserDao.ZIP, required = false) String zip,
			@RequestParam(value = UserDao.CITY, required = false) String city,
			@RequestParam(value = UserDao.MAIL, required = false) String mail,
			HttpServletRequest request, ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		model = addActiveDataToModel(model, user);

		if(!passwordEncoder.matches(oldpass, user.getPassword())) {
			Map errors = CollectionFactory.createMap(
					new CollectionFactory.MapPair("oldpassword", "error.tooltip.current")
			);
			model.addObject("errors", errors);
			return view(request, model, "user/editView");
		}
		Map paramMap = request.getParameterMap();
		if(safeGetParameter(paramMap, UserDao.PASSWORD, "").length() == 0) {
			paramMap.remove(UserDao.PASSWORD);
			paramMap.remove(String.format("%s-repeat", UserDao.PASSWORD));
		}
		id = String.valueOf(user.getId());

		ErrorsOrResult result = saveAccount(paramMap, id, true, false);


		String url = "user/editView";
		Map errors = validateAccount("", pass, passRepeat, organisation, initials, tsvg, lastname, mail);
		errors.remove("users");
		errors.remove("username");
		if(pass.length() == 0 && passRepeat.length() == 0) {
			errors.remove("password");
			errors.remove("password-repeat");
		}

		model.addObject("uid", user.getId());
		model.addObject("current", UserDao.loadAccountValues(user.getUsername(), organisation, initials, tsvg, lastname,
				street, housnumber, zip, city, mail));

		JSPMessage message = new JSPMessage("error.form.invalid", JSPMessage.FAILURE);
		if(!id.equals("" + user.getId())) {
			url = "redirect:/user/edit";

		} else if(!passwordEncoder.matches(oldpass, user.getPassword())) {
			errors.put("oldpassword", "error.tooltip.current");

		} else if(errors.size() == 0) {
			message = new JSPMessage(String.format(JSPMessage.SUCCES_SAVED_YOUR_TYPE, "account wijzigingen"), JSPMessage.SUCCES);
			url = "redirect:/";
			List params = CollectionFactory.createList();
			params.add(new Parameter(UserDao.ORGANISATION, organisation));
			params.add(new Parameter(UserDao.INITIALS, initials));
			params.add(new Parameter(UserDao.TUSSENVOEGSELS, tsvg));
			params.add(new Parameter(UserDao.LASTNAME, lastname));
			params.add(new Parameter(UserDao.MAIL, mail));
			params.add(new Parameter(UserDao.PASSWORD, passwordEncoder.encode(pass)));
			UserDao.update(user.getId(), params);

		}
		model.addObject("errors", errors);

		message.addToRequest(request);

		return view(request, model, url);
	}

	private String getMollieApiKey() {
		return (testMode ? mollie_test_api_key : mollie_api_key);
	}

	@GetMapping(USER_SUBSCRIPTION_URL)
	public ModelAndView getSubscriptionView(HttpServletRequest request, ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();

		model = addActiveDataToModel(model, user);

		return view(request, model, "user/subscription");
	}
	@GetMapping(USER_SUBSCRIPTION_CANCEL_URL)
	public ModelAndView cancelSubscription(HttpServletRequest request, ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		CustomPayment active = user.getActiveSubscription(PaymentController.getClient(getMollieApiKey()));
		if(active != null) {
			String subId = active.getSubscriptionPaymentId();
			cancelSubscriptionById(subId, user.getMollieId());
		}
		return redirect(request, model, USER_SUBSCRIPTION_URL);
	}
	@GetMapping(USER_SUBSCRIPTION_CANCEL_URL + "/{subId}")
	public ModelAndView cancelSubscriptionById(HttpServletRequest request, ModelAndView model, @PathVariable("subId") String subId) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		if(user != null) {
			cancelSubscriptionById(subId, user.getMollieId());
		}
		return redirect(request, model, USER_SUBSCRIPTION_URL);
	}
	private boolean cancelSubscriptionById(String subId, String mollieId) {
		Client client = PaymentController.getClient(getMollieApiKey());
		Subscriptions subs = client.subscriptions(mollieId);
		try {
			ResponseOrError<Subscription> ROE = subs.cancel(subId);

			if(!ROE.getSuccess())
				throw PaymentException.getException(ROE);

			return true;
		} catch(IOException | PaymentException e) {
			LogUtil.logException(e);
		}
		return false;
	}

	@GetMapping("/register")
	public ModelAndView getRegisterView(HttpServletRequest request, ModelAndView model) {
		if(null != AuthenticationUtil.getCurrentUser())
			return redirect(request, model, "search");

		return view(request, model, "registerView");
	}

	@PostMapping("/register")
	public ModelAndView postRegisterView(HttpServletRequest request, ModelAndView model) {
//		if(Captcha.verifyCapthcha(request)) {
			ErrorsOrResult<CustomUser> result = saveAccount(request, false, true);

			if(!result.isSuccesful()) {
				model.addObject("errors", result.getErrors());

				Map parameterMap = result.getParameterMap();
				parameterMap.remove(UserDao.PASSWORD);
				model.addObject("previous", parameterMap);

				return view(request, model, "registerView");
			}

			AuthenticationUtil.setLoggedInUser(result.getResult());
//		} else {
//			JSPMessage message = new JSPMessage(JSPMessage.ERROR_CAPTCHA_INVALID, JSPMessage.ERROR);
//			message.addToRequest(request);
//			return view(request, model, "registerView");
//		}

		return redirect(request, model, "registered");
	}
	@GetMapping("/registered")
	public ModelAndView getRegisterLandingView(HttpServletRequest request, ModelAndView model) {
		return view(request, model, "registerLanding");
	}

	private Map validateAccount(String username, String pass, String passRepeat, String organisation, String initials, String tsvg, String lastname, String mail) {
		Map errors = CollectionFactory.createMap();

		String error = getEmptyOrShortError(username, 3, "De gebruikersnaam");

		if(error.length() == 0 && accountExists(username))
			error += "<p>Deze gebruikersnaam bestaat al</p>";
		if(error.length() > 0)
			errors.put("username", error);

		error = getEmptyOrShortError(pass, 5, "Het wachtwoord");

		if(!pass.equals(passRepeat)) {
			String unequal = "error.tooltip.match";
			errors.put("password", error);
			errors.put("password-repeat", error);
		}

//        error = getEmptyError(organisation);
//        if(error.length() > 0)
//            errors.put("organisation", error);
//
//        error = getEmptyError(initials);
//        if(error.length() > 0)
//            errors.put("initials", error);

//        error = getEmptyError(tsvg);
//        if(error.length() > 0)
//            errors.put("tsvg", error);

		error = getEmptyError(lastname);
		if(error.length() > 0)
			errors.put("lastname", error);

		error = getEmptyError(mail);
		if(error.length() > 0)
			errors.put("mail", error);

		return errors;
	}

	private String getEmptyError(String field) {
		return getEmptyOrShortError(field, 0, "");
	}

	private String getEmptyOrShortError(String field, int minLength, String name) {
		String error = "";
		int fieldLength = field.length();

		if(fieldLength == 0)
			error = "error.tooltip.required";
		else if(fieldLength < minLength)
			error = "error.tooltip.minimum";

		return error;
	}

	@PostMapping("/account/exists")
	@ResponseBody
	public boolean accountExists(@RequestParam(value = "username", required = false) String username) {
		return UserDao.findByUsername(username) != null;
	}

	public static ModelAndView getManagePage(HttpServletRequest request, ModelAndView model, String id) {
		return prepareAccountEdit(model, request, id, false);
	}

	public static ModelAndView postManagePage(HttpServletRequest request, ModelAndView model) {
		String id = request.getParameter(UserDao.ID);
		String url = String.format("%s?id=%s", AdminController.ADMIN_USER_URL, id);
		Map parameterMap = CollectionFactory.clone(request.getParameterMap());

		if(parameterMap.containsKey("password") && request.getParameter("password").length() == 0) {
			parameterMap.remove("password");
			parameterMap.remove("password-repeat");
		}

		ErrorsOrResult<CustomUser> result = saveAccount(parameterMap, id, true, false);
		if(!result.isSuccesful()) {
			model.addObject("errors", result.getErrors());
			model.addObject("previous", result.getParameterMap());
			return prepareAccountEdit(model, request, id, true);
		}
		return redirect(request, model, url);
	}

	private static ErrorsOrResult<CustomUser> saveAccount(HttpServletRequest request, boolean ignoreMissing, boolean sendVerification) {
		return saveAccount(request.getParameterMap(), "-1", ignoreMissing, sendVerification, true);
	}
	private static ErrorsOrResult<CustomUser> saveAccount(Map requestMap, String id, boolean ignoreMissing, boolean sendVerification) {
		return saveAccount(requestMap, id, ignoreMissing, sendVerification, false);
	}
	private static ErrorsOrResult<CustomUser> saveAccount(Map requestMap, String id, boolean ignoreMissing, boolean sendVerification, boolean verifyCaptcha) {
		ErrorsOrResult<CustomUser> result = new ErrorsOrResult();

		List<Parameter> paramaters = UserDao.getParamaters(requestMap);
		result.addParameters(paramaters);

		Map errors = UserDao.validateCreateOrUpdate(requestMap, ignoreMissing);

		if(verifyCaptcha && !Captcha.verifyCapthcha(requestMap))
			errors.put(Captcha.PARAM_NAME, JSPMessage.ERROR_CAPTCHA_INVALID);

		result.addErrors(errors);

		if(0 == errors.size()) {
			CustomUser updated;
			if("-1".equals(id)) {
				updated = UserDao.create(paramaters, sendVerification);
			} else {
				updated = UserDao.update(id, paramaters);
			}

			CustomUser currentUser = AuthenticationUtil.getCurrentUser();
			if(null != updated && null != currentUser && currentUser.isAdmin()) {
				AccountType type = AccountTypeDao.findById(safeGetParameter(requestMap, UserDao.TYPE_ID));
				if(type != null) {
					int typeId = (int)type.getId();
					int days;
					boolean daysIsEndDate = false;
					if(typeId == AccountTypeDao.getUserAcoountId()) {
						if(requestMap.containsKey(RoleDao.END_DATE)) {
							days = DateUtil.getDifferenceBetweenDateAndToday(DateUtil.converStringToInt(safeGetParameter(requestMap, RoleDao.END_DATE), "yyyy-MM-dd"));
							//daysIsEndDate = true;
						} else {
							days = 0;
						}
					} else {
						days = -1;
					}

					updated = updated.updateAccountType(typeId, days, daysIsEndDate);
				}
			}

			result.addResult(updated);
		}

		return result;
	}

	public static ModelAndView prepareAccountEdit(ModelAndView model, HttpServletRequest request, String id, boolean requestOverrides) {
		CustomUser user = UserDao.findById(id);
		Map current = CollectionFactory.createMap();
		if(null != user) {
			model.addObject("selected", user);
			current = user.getCurrent();
		}

		if(requestOverrides) {
			for(Parameter par : UserDao.getParamaters(request)) {
				String value = par.getParamater().toString();
				if(null != value && 0 != value.length()) {
					String col = par.getColumn();
					if(current.containsKey(col)) {
						current.put(col, value);
					}
				}
			}
		}

		model.addObject("current", current);

		model.addObject("items", UserDao.getAll());
		model.addObject("types", AccountTypeDao.getAll());
		return view(request, model, "accountManage");
	}

}
