package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.ProfileDao;
import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.dao.UserDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.Crawler.ArticleSearch;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.Email;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.crawlerUtils.IndexManager;
import org.apache.lucene.facet.FacetResult;
import org.apache.lucene.facet.Facets;
import org.apache.lucene.facet.LabelAndValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Impressie
 */
@Controller
@RequestMapping("/search")
public class SearchController extends nl.impressie.grazer.controller.Controller {

	@Value("${server.loadtest.api.key}")
	private String loadTestApiKey;



	@RequestMapping("/loadTest")@ResponseBody
	public Object loadTest(HttpServletRequest request, ModelAndView model) {
		if(loadTestApiKey.equals(request.getParameter("api"))) {
			long start = DateUtil.getTimestamp();
			model = search(request, model);

			long elapsedTimeInMS = DateUtil.getTimestamp() - start;

			if(elapsedTimeInMS > 5000)
				LogUtil.log("loadTest took: " + DateUtil.elapsedTimeToString(elapsedTimeInMS));

			return model.getModelMap().containsKey("currentHits") && (int)model.getModelMap().get("currentHits") > 0;
		}
		return redirect(request, model, "404");
	}

	@RequestMapping("/feed")
	public ModelAndView feed(HttpServletRequest request, ModelAndView model) {
		CustomUser user = AuthenticationUtil.getCurrentUser();
		String redirect = "search?feed=true";

		if(null != user && user.getProfiles().size() == 0)
			redirect = "user/landing";

		return redirect(request, model, redirect);
	}





	@RequestMapping("")
	public ModelAndView search(HttpServletRequest request, ModelAndView model) {
		String profIdKey = String.format("%s-%s", ProfileDao.TABLE_NAME, ProfileDao.ID);
		String profId = request.getParameter(profIdKey);

		CustomUser user = AuthenticationUtil.getCurrentUser();
		String action = request.getParameter(Selections.ACTION);
		boolean reset = Actions.RESET.equals(action);

		if(null != profId)
			reset = reset || user.setActiveProfileId(profId);

		Boolean feed = Boolean.valueOf(request.getParameter("feed"));

		if(reset) {
			String view = "search";
			if(feed)
				view = "search/feed";

			return redirect(request, model, view);
		}


		ArticleSearch search = ArticleSearch.createSearch(request);
		Map results = search.search();

		model.addObject("facetMap", getFacets(results));

		Map parameterMap = request.getParameterMap();
		if(feed && parameterMap.size() <= 3 && 0 == (Integer) results.get(IndexManager.TOTAL_HITS)) {
			return redirect(request, model, "search");
		}

		model.addAllObjects(results);

		String newSearchKey = "new_search";
		boolean isNewSearch = Actions.CREATE.equals(action);
		Boolean wasNewSearch = Boolean.valueOf(request.getParameter(newSearchKey));

		model.addObject(newSearchKey, isNewSearch || wasNewSearch);

		model.addObject("search", search);
		model.addObject("rubrieken", TagDao.getType(Tag.RUBRIEK));
		model.addObject("rechtsgebieden", TagDao.getType(Tag.RECHTSGEBIED));
		model.addObject("soorten", TagDao.getType(Tag.NIEUWSSOORT));

		Profile profile = (user == null ? null : user.getActiveProfile());
		if(null != profile)
			model.addObject("profile", profile);

		return view(request, model, "searchView");
	}

	private Map getFacets(Map searchResults) {
		Facets facets = (Facets) searchResults.get(IndexManager.FACET_FIELD);
		List<FacetResult> allDims = CollectionFactory.createList();
		try {
			if(null != facets)
				allDims = facets.getAllDims(Integer.MAX_VALUE);
		} catch(IOException e) {
			e.printStackTrace();
		}

		Map allFacets = CollectionFactory.createMap();
		for(FacetResult tags : allDims) {
			String type = tags.dim;
//            String type = dim.substring(0, dim.length() - IndexManager.FACET_FIELD.length());

			Map tagFacets = CollectionFactory.createMap();
			allFacets.put(type, tagFacets);
			for(LabelAndValue lbv : tags.labelValues) {
				tagFacets.put(lbv.label, lbv.value);
			}
		}
		return allFacets;
	}
}
