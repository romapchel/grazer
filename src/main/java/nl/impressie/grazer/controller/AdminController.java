package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.InvoiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Impressie
 */
@org.springframework.stereotype.Controller
@RequestMapping(AdminController.ADMIN)
public class AdminController extends Controller {

	public static final String ADMIN = "admin";

	public static final String ADMIN_WEBSITE_URL = String.format("%s/%s", ADMIN, WebsiteController.WEBSITE);
	public static final String ADMIN_USER_URL = String.format("%s/%s", ADMIN, AccountController.USER);
	public static final String ADMIN_TAG_URL = String.format("%s/%s", ADMIN, TagController.TAG);
	public static final String ADMIN_BUNDLE_URL = String.format("%s/%s", ADMIN, PaymentController.BUNDLE);
	public static final String ADMIN_INDEX_URL = String.format("%s/%s", ADMIN, IndexController.INDEX);
	public static final String ADMIN_SUBSCRIPTION_VIEW_URL = String.format("%s/%s", ADMIN, AdminController.ADMIN_SUBSCRIPTIONS);

	public static final String ADMIN_UNKNOWN_TAG_URL = String.format("%s/%s", ADMIN, UnknownTagController.UNKNOWN_TAG);

	private static final String INDEX_VIEW = "admin/index";
	private static final String ADMIN_SUBSCRIPTIONS_VIEW = "admin/subscriptions";
	private static final String ADMIN_SUBSCRIPTIONS = "subscriptions";

	@Autowired
	private UnknownTagController unknownTagController;

	@GetMapping("")
	public ModelAndView index(HttpServletRequest request, ModelAndView model) {
		return view(request, model, INDEX_VIEW);
	}


	@GetMapping(WebsiteController.WEBSITE)
	public ModelAndView getWebsiteManage(HttpServletRequest request, ModelAndView model,
								@RequestParam(value = "id", defaultValue = "") String id,
								@RequestParam(value = "copy", defaultValue = "false") String copy) {

		return WebsiteController.getManagePage(request, model, id, copy);
	}

	@GetMapping(AdminController.ADMIN_SUBSCRIPTIONS)
	public ModelAndView getUserManage(HttpServletRequest request, ModelAndView model) {
		List invoices = InvoiceDao.getAllForThisMonth();

		model.addObject("invoices", invoices);

		return view(request, model, ADMIN_SUBSCRIPTIONS_VIEW);
	}

	@GetMapping(AccountController.USER)
	public ModelAndView getUserManage(HttpServletRequest request, ModelAndView model,
							 @RequestParam(value = "id", defaultValue = "-1") String id) {
		return AccountController.getManagePage(request, model, id);
	}
	@PostMapping(AccountController.USER)
	public ModelAndView postUserManage(HttpServletRequest request, ModelAndView model) {
		return AccountController.postManagePage(request, model);
	}

	@GetMapping(TagController.TAG)
	public ModelAndView getTagManage(HttpServletRequest request, ModelAndView model,
							 @RequestParam(value = "id", defaultValue = "") String id) {
		return TagController.getManagePage(request, model, id);
	}

	@GetMapping(UnknownTagController.UNKNOWN_TAG)
	public ModelAndView getUnknownTagManager(HttpServletRequest request, ModelAndView model,
							 @RequestParam(value = "id", defaultValue = "") String id) {
		return unknownTagController.getManagePage(request, model, id);
	}

	@GetMapping(IndexController.INDEX)
	public ModelAndView getIndexManage(HttpServletRequest request, ModelAndView model) {
		return IndexController.getManagePage(request, model);
	}

	@GetMapping(PaymentController.BUNDLE)
	public ModelAndView getBundleManage(HttpServletRequest request, ModelAndView model) {
		return PaymentController.getManagePage(request, model);
	}
	@PostMapping(PaymentController.BUNDLE)
	public ModelAndView postBundleManage(HttpServletRequest request, ModelAndView model) {
		return PaymentController.postManagePage(request, model);
	}
	@GetMapping(PaymentController.BUNDLE + "/{ID}")
	public ModelAndView getBundleEdit(HttpServletRequest request, ModelAndView model,
									  @PathVariable("ID") String id) {
		return PaymentController.getBundleEdit(request, model, id);
	}
	@PostMapping(PaymentController.BUNDLE + "/{ID}")
	public ModelAndView postBundleEdit(HttpServletRequest request, ModelAndView model,
									  @PathVariable("ID") String id) {
		return PaymentController.postBundleEdit(request, model, id);
	}
}
