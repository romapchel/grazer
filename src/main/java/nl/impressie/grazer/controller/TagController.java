package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.ProfileTagDao;
import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.dao.TagPageDao;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.exception.CSVFormatException;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.LogUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Impressie
 */
@Controller
@RequestMapping(TagController.TAG)
public class TagController extends nl.impressie.grazer.controller.Controller {
	private static final String TAG_MANAGE_VIEW = "tag/manage";

	public static final String TAG = "tag";

	public static String getIds(HttpServletRequest request) {
		String[] params = {Actions.EDIT, Actions.TOGGLE, Actions.DELETE, Actions.ENABLE, Actions.DISABLE, Actions.MERGE, Selections.SELECTION, Selections.ALL};

		return getIds(request, params);
	}

	@RequestMapping(value = "/import", consumes = {"multipart/form-data"})
	public ModelAndView importSites(ModelAndView model, HttpServletRequest request,
									@RequestParam(name = "csv", required = false) MultipartFile importFile,
									@RequestParam(name = "type", defaultValue = "") String type) {
		if(importFile != null) {
			try {
				List<Map> listOfUpdates = readCSVFile(importFile, type);

				Tag diverse = TagDao.findByName("Diverse rechtsgebieden", Tag.RECHTSGEBIED);
				String[] tagIds = new String[2];
				tagIds[0] = (diverse == null ? "0" : diverse.getId());

				Map<Object, Map> trueUpdates = CollectionFactory.createMap();
				String add = "add";
				String remove = "remove";
				for(Map<String, Tag> update: listOfUpdates) {
					Tag current = update.get("current");
					String currentId = (current == null ? "0" : current.getId());
					tagIds[1] = currentId;
					List<Website> websitesToUpdate = TagPageDao.findByTagIds(tagIds);

					for(Website page: websitesToUpdate) {
						Map<String, Tag> toAdd = (trueUpdates.containsKey(page.getId()) ? (Map)trueUpdates.get(page.getId()).get(add) : CollectionFactory.createMap());
						toAdd.put(update.get("parent").getId(), update.get("parent"));
						if(update.containsKey("child"))
							toAdd.put(update.get("child").getId(), update.get("child"));

						Map<String, Tag> toExclude = (trueUpdates.containsKey(page.getId()) ? (Map)trueUpdates.get(page.getId()).get(remove) : CollectionFactory.createMap());
						if(!toAdd.containsKey(currentId) && !"0".equals(currentId))
							toExclude.put(currentId, current);

						Map trueUpdate = CollectionFactory.createMap();
						trueUpdate.put(add, toAdd);
						trueUpdate.put(remove, toExclude);
						trueUpdates.put(page.getId(), trueUpdate);
					}
				}
				for(Object pageId: trueUpdates.keySet()) {
					Website page = WebsiteDao.findById(pageId);
					Map<String, Tag> toExclude = (Map<String, Tag>)trueUpdates.get(pageId).get(remove);
					Map<String, Tag> toAdd = (Map<String, Tag>)trueUpdates.get(pageId).get(add);
					if((int)pageId == 8668) {
						for(Tag tag: toAdd.values()) {
							System.out.println(tag.getName());
						}
					}
					page.changeTags(toExclude, toAdd, true);
				}

			} catch(CSVFormatException e) {
				LogUtil.logException(e);
			}
		}
		return redirect(request, model, AdminController.ADMIN_TAG_URL);
	}
	private List<Map> readCSVFile(MultipartFile importFile, String type) throws CSVFormatException {
		List listOfUpdates = CollectionFactory.createList();

		List<String> results = readFile(importFile);

		String seperator = ";";

		for(String line: results) {
			Map update = CollectionFactory.createMap();
			if(line.startsWith("\"sep=")) {
				seperator = line.substring(5, 6);
				continue;
			}
			String[] vars = line.split(Pattern.quote(seperator));
			if(vars.length < 2)
				throw new CSVFormatException(String.format("[%s] is not valid content for tagupdates", line));

			//Tag current = TagDao.create(type, vars[0]);
			Tag current = TagDao.findByName(vars[0], type);

			update.put("current", current);

			String[] parentTagnames = vars[1].split(",");
			String[] childTagnames = (vars.length > 2 ? vars[2].split(",") : new String[0]);

			if(vars.length > 2 && parentTagnames.length != childTagnames.length)
				throw new CSVFormatException(String.format("[%s] and [%s] do not contain the same number of tagnames", vars[1], vars[2]));

			int length = parentTagnames.length;
			for(int i = 0; i < length; i++) {
				Tag parent = TagDao.create(type, parentTagnames[i].trim());
				update.put("parent", parent);
				if(vars.length > 2) {
					String childTagname = childTagnames[i].trim();
					if(!childTagname.toLowerCase().equals("geen subrechtsgebied"))
						update.put("child", TagDao.create(type, String.format("%s: %s", parent.getName(), childTagname)));
				}

			}

			listOfUpdates.add(update);
		}

		return listOfUpdates;
	}

	@RequestMapping("/action")
	public ModelAndView action(ModelAndView model, HttpServletRequest request) {
		Map params = request.getParameterMap();
		String id = getIds(request);

		if(params.containsKey(Actions.EDIT)) return getManagePage(request, model, id);
		if(params.containsKey(Actions.DELETE)) deletePages(id);
		if(params.containsKey(Actions.MERGE)) return merge(model, request, id);

		return prepRedirect(model, request);
	}

	public ModelAndView merge(ModelAndView model, HttpServletRequest request, String ids) {
		String tagId = mergeTags(ids);
		if(null == tagId) return prepRedirect(model, request);

		return prepManagementPage(model, request, tagId);
	}

	public String mergeTags(String ids) {
		List<Tag> tags = CollectionFactory.createList();
		Map tagMap = CollectionFactory.createMap();
		for(String id : ids.split(",")) {
			Tag tag = TagDao.findById(id);
			tags.add(tag);
			tagMap.put(tag.getId(), tag);
		}

		if(0 == tags.size())
			return null;

		String name = "merged tag " + DateUtil.getTimestamp();
		String tagType = tags.get(0).getType();
		Tag newTag = TagDao.create(tagType, name);

		List<Article> articles = TagDao.getArticlesForTags(tags);
		for(Article art : articles) {
			if(art.hasTag(newTag))  //skip ones with the new tag so it doesn't waste time redoing the tags again for duplicates
				continue;

			art.removeTags(tagMap);
			art.addTag(newTag);
		}

		List<Profile> profiles = ProfileTagDao.findProfilesByTags(tags);
		for(Profile profile : profiles) {
			if(profile.hasTag(newTag))  //skip ones with the new tag so it doesn't waste time redoing the tags again for duplicates
				continue;
			Map<Object, Tag> currentTags = profile.getTagsByType(tagType);
			List<String> newTags = CollectionFactory.createList();
			for(Tag cur : currentTags.values()) {
				if(!tagMap.containsKey(cur.getId()))
					newTags.add(cur.getId());
			}
			newTags.add(newTag.getId());

			profile.updateTagsByType(tagType, newTags);
		}

		TagDao.deleteTags(tags);

		return newTag.getId();
	}

	private void deletePages(String ids) {
		List toDelete = CollectionFactory.createList();
		for(String id : ids.split(",")) {
			Tag t = TagDao.findById(id.trim());
			toDelete.add(t);
		}
		TagDao.deleteTags(toDelete);
	}

	@RequestMapping("/save")
	public ModelAndView save(ModelAndView model, HttpServletRequest request) {
		TagDao.save(request);

		return prepRedirect(model, request);
	}


	public static ModelAndView getManagePage(HttpServletRequest request, ModelAndView model, String id) {
		return prepManagementPage(model, request, id);
	}

	public ModelAndView prepRedirect(ModelAndView model, HttpServletRequest request) {
		return redirect(request, model, AdminController.ADMIN_TAG_URL);
	}

	public static ModelAndView prepManagementPage(ModelAndView model, HttpServletRequest request, String id) {
		Map ids = CollectionFactory.createMap();
		for(String i : id.split(",")) {
			Tag t = TagDao.findById(i);

			if(null != t) ids.put(t.getId(), t);
		}
		Map<Object, Integer> tagsUseByPages = CollectionFactory.createMap();
		for(Website page: WebsiteDao.getAll()) {
			for(Tag tag: page.getTags().values()) {
				String tagId = tag.getId();
				int used = tagsUseByPages.containsKey(tagId) ? tagsUseByPages.get(tagId) : 0;
				used++;
				tagsUseByPages.put(tagId, used);
			}
		}

		model.addObject("tagsUseByPages", tagsUseByPages);
		model.addObject("id_map", ids);

		model.addObject("item_map", TagDao.getAllTagsAsMap());

		if(request.getParameterMap().containsKey("tab"))
			model.addObject("tab", request.getParameter("tab"));

		return view(request, model, TAG_MANAGE_VIEW);
	}

}
