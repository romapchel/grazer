package nl.impressie.grazer.controller;

import nl.impressie.grazer.dao.ProfileDao;
import nl.impressie.grazer.dao.UserDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.Crawler.ArticleSearch;
import nl.impressie.grazer.model.Email;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.util.AuthenticationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

@Controller
@RequestMapping("/mailing")
public class MailingController {

    public static List<Article> getArticles(Profile profile) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        ArticleSearch search = new ArticleSearch(profile);
        //profile.updateTagsByType()
        // Using getDeclareMethod() method
        Method m = ArticleSearch.class
                .getDeclaredMethod("setGoUntillItemNumber", int.class);

        // Using setAccessible() method
        m.setAccessible(true);

        // Using invoke() method
        m.invoke(search, 20);

        Map result =  search.search();
        return (List<Article> )result.get("artikels");
    }



    public static List<Email> sortForEmailing(String option) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        List<CustomUser> list = UserDao.getAll();

        Map<CustomUser, List<Profile>> map = CollectionFactory.createMap();

        List<Email> emailList = CollectionFactory.createList();

        for (CustomUser users : list) {

            map.put(users, users.getProfiles());

        }
        for (CustomUser user : map.keySet()) {
            for (Profile profile : map.get(user)) {
                if (!Objects.equals(profile.getMailOption(),"Don`t receive") && profile.getMailOption().equals(option)){
                    System.out.println("added " + user.getUsername() + " Profile " + profile.getName() + " with " + profile.getMailOption() + " option " + " will send to " + user.getMail());
                    emailList.add(new Email(user.getMail(), user.getName(), getArticles(profile), user.getUsername() + " " + user.getLastname()));


                }
                System.out.println("didn`t send to " + user.getUsername() + " : " + profile.getName() + " --> " + profile.getMailOption());
            }
        }
        return emailList;
    }


    @GetMapping("")
    public ModelAndView main(){
        CustomUser user = AuthenticationUtil.getCurrentUser();

        ModelAndView modelAndView = new ModelAndView("mailing");
        System.out.println(user.getProfiles());

        List<String> options = CollectionFactory.createList();
        options.add("Daily");
        options.add("Every 3 days");
        options.add("Weekly");
        List<Profile> listOfProfiles = user.getProfiles();

        listOfProfiles.removeIf(profile -> profile.getName().equals("Alles"));
        listOfProfiles.removeIf(profile -> profile.getName().equals("allesniks"));

        modelAndView.addObject("listOfProfiles",listOfProfiles);
        modelAndView.addObject("optionsList", options);
        return modelAndView;
    }

    @GetMapping("/save")
    public String save(HttpServletRequest request){
        Map map = request.getParameterMap();

        for (Object key: map.keySet())
        {
            String keyStr = (String)key;
            String[] value = (String[])map.get(keyStr);
            System.out.println("Key" + (String)key + "   :   " + Arrays.toString(value));
            Profile profile = ProfileDao.findById(key);
            profile.updateMail(value[0]);

        }



        return "redirect:/mailing";
    }

}
