package nl.impressie.grazer.controller;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.JSPMessage;
import nl.impressie.grazer.model.MenuButton;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.*;
import org.joda.time.DateTime;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class Controller {
	private static final String DEFAULT_SUB_PAGE = "legal";
	private static final String LAST_PATH = "last_path";

	protected static final String SELECTED = "selected";
	protected static final String ITEMS = "items";
	protected static final String FILTERS = "filters";
	protected static final String IDMAP = "id_map";

	public class Actions {
		public final static String CREATE = "action_create", COPY = "action_copy", EDIT = "action_edit", TOGGLE = "action_toggle", DELETE = "action_delete", HEALTH = "action_health", DOWNLOAD = "action_download",
				BREAK = "action_break", DETAIL = "action_detail", ENABLE = "action_enable", DISABLE = "action_disable", EMPTY = "action_empty", MERGE = "action_merge", INDEX = "action_index",
				NEXT_PAGE = "action_next_page", PREV_PAGE = "action_prev_page", EXPORT = "action_export", IMPORT = "action_import", RESET = "action_reset",
				VIEW = "action_view", DEFAULT = "action_default", CHANGE = "action_change";
	}

	public class Selections {
		public final static String ALL = "selection_all", NONE = "selection_none", SELECTION = "selection_selection", ACTION = "selection_action", PAGE = "selection_page";
	}

	public static String getIds(HttpServletRequest request, String... actions) {

		String[] selections = {TagController.Selections.SELECTION};
		String all = TagController.Selections.ALL;

		return getIds(request, actions, selections, all);
	}
	public static String getIds(HttpServletRequest request, String[] params, String[] selections, String all) {
		String ids = null;
		for(String param : params) {
			ids = request.getParameter(param);
			if(null != ids)
				break;
		}

		for(String selection : selections) {
			if(selection.equals(ids)) {
				String key = String.format("%s[]", ids);
				Object o = request.getParameterMap().get(key);
				if(o instanceof String[]) {
					StringBuilder builder = new StringBuilder();
					for(String id : (String[]) o) {
						if(builder.length() > 0)
							builder.append(", ");
						builder.append(id);
					}
					ids = builder.toString();
				} else {
					ids = request.getParameter(key);
				}
			}
		}
		if(all.equals(ids))
			ids = "";

		return ids;
	}

	public static String safeGetParameter(HttpServletRequest request, String key) {
		return safeGetParameter(request.getParameterMap(), key);
	}
	public static String safeGetParameter(Map requestParameters, String key) {
		return safeGetParameter(requestParameters, key, "");
	}
	public static String safeGetParameter(HttpServletRequest request, String key, String defaultValue) {
		return safeGetParameter(request.getParameterMap(), key, defaultValue);
	}
	public static String safeGetParameter(Map requestParameters, String key, String defaultValue) {
		String param = null;
		if(requestParameters.containsKey(key)) {
			Object o = requestParameters.get(key);
			if(o instanceof String[]) {
				String[] array  = (String[]) o;
				if(array.length == 1)
					param = array[0];
			} else if (o instanceof String) {
				param = (String)o;
			} else if(o instanceof Integer) {
				param = String.valueOf((int)o);
			}
		}

		if(param == null)
			param = defaultValue;

		return param;
	}

	public static String makeRedirect(String viewmname) {
		if(!viewmname.startsWith("/") && !viewmname.startsWith("http"))
			viewmname = "/" + viewmname;

		return String.format("redirect:%s", viewmname);
	}

	protected static ModelAndView redirectBack(HttpServletRequest request, ModelAndView model) {
		String url = SessionUtil.getAndRemoveStringFromSession(request, LAST_PATH);
		if(url == null || url.length() == 0)
			url = "/search";

		return redirect(request, model, url);
	}
	protected static ModelAndView redirect(HttpServletRequest request, ModelAndView model, String url) {
		if(!url.startsWith("redirect:"))
			url = makeRedirect(url);

		return view(request, model, url);
	}

	protected static ModelAndView view(HttpServletRequest request, ModelAndView model, String viewName) {
		return view(request, model, viewName, DEFAULT_SUB_PAGE);
	}
	protected static ModelAndView view(HttpServletRequest request, ModelAndView model, String viewName, String subPage) {
		SessionUtil.addToSession(request, LAST_PATH, request.getAttribute("org.springframework.web.servlet.HandlerMapping.pathWithinHandlerMapping"));

		model.addObject("sub_page", subPage);

		Map modelmap = model.getModelMap();
		if(modelmap.containsKey("errors")) {
			Map<String, Object> errorMap = (Map<String, Object>)modelmap.get("errors");
			for(String key: errorMap.keySet()) {
				Object errors = errorMap.get(key);
				List items = CollectionFactory.createList();
				if(errors instanceof String) {
					items.add(CollectionFactory.createMap(new CollectionFactory.MapPair("error", errors)));

				} else if(errors instanceof Map) {
					items.add(errors);

				} else if(errors instanceof Collection) {
					for(Object error : (Collection) errors) {
						if(error instanceof String)
							items.add(CollectionFactory.createMap(new CollectionFactory.MapPair("error", error)));
						else
							items.add(error);
					}

				}
				errorMap.put(key, items);
			}
			model.addObject("errors", errorMap);
		}

		CustomUser user = AuthenticationUtil.getCurrentUser();
		if(user != null && !user.isActive()) {
			if(!"registerLanding".equals(viewName) && !"user/verified".equals(viewName)) {
				viewName = makeRedirect("registered");
			}
		}
		model.setViewName(viewName);
		model.addObject("page", viewName.replace("/", "."));
		model.addObject("user", user);
		model.addObject("buttons", getMenuButtons());
		model.addObject("js", SessionUtil.isJSEnabled(request));
		model.addObject("cookie-js", new Cookie(SessionUtil.JS_COOKIE_NAME, "false"));

		LogUtil.log("Navigating to %s at %s", viewName, DateTime.now().toString());

		if(request != null && !viewName.startsWith("redirect:")) {
			String msg = SessionUtil.getAndRemoveStringFromSession(request, JSPMessage.CONTENT_PARAMETER);
			if(msg == null || msg.length() == 0)
				msg = request.getParameter(JSPMessage.CONTENT_PARAMETER);
			msg = UrlUtil.decodeSpaces(msg);

			String type = SessionUtil.getAndRemoveStringFromSession(request, JSPMessage.TYPE_PARAMETER);
			if(type == null || type.length() == 0)
				type = request.getParameter(JSPMessage.TYPE_PARAMETER);

			if(msg != null && msg.length() > 0) {
				JSPMessage message = new JSPMessage(msg, false);
				if(type != null && type.length() > 0)
					message = new JSPMessage(msg, type, false);
				model.addObject("message", message);
			}
		}
		return model;
	}

	protected static List<MenuButton> getMenuButtons() {
		List buttons = CollectionFactory.createList();

		CustomUser user = AuthenticationUtil.getCurrentUser();
		buttons.add(new MenuButton("/", "h", "Hoofdpagina"));
		if(user != null) {
			buttons.add(new MenuButton("/profile", "p", "Profiel(en)"));
			buttons.add(new MenuButton("/search", "p", "Zoeken"));
			buttons.add(new MenuButton("/user/edit", "i", "Instellingen"));
//			buttons.add(new MenuButton(AccountController.USER_SUBSCRIPTION_URL, "s", "subscriptions"));
			buttons.add(new MenuButton("/mailing", "i", "E-mailattendering(en)"));
			if(user.isAdmin()) {
				buttons.add(new MenuButton(AdminController.ADMIN, "a", "Administratie"));
				buttons.add(new MenuButton("/index/start", "b", "Begin indexeren"));
				buttons.add(new MenuButton("/index/stop", "s", "Stop indexeren"));
			}
		}
		buttons.add(new MenuButton("/search/feed", "n", "Nieuwe berichten"));
		buttons.add(new MenuButton("/suggestion", "s", "Suggesties"));

		buttons.add(new MenuButton("/about", "o", "Over ons"));
		buttons.add(new MenuButton("/about/grazer", "o", "Over Grazer"));
		buttons.add(new MenuButton("/contact", "c", "Contact"));

		return buttons;
	}

	protected static ResponseEntity<byte[]> downloadResource(String path, String filename) {
		try {
			File file = ContextProvider.getApplicationContext().getResource(path).getFile();
			return fileToResponse(file, filename);
		} catch(IOException e) {
			LogUtil.logException(e);
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	protected static ResponseEntity<byte[]> fileToResponse(File file, String downloadName) {

		HttpHeaders headers = new HttpHeaders();
		byte[] contents = new byte[0];
		try {
			contents = Files.readAllBytes(file.toPath());
		} catch(IOException e) {
			LogUtil.logException(e);
		}
		headers.setContentDispositionFormData(downloadName, downloadName);
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		String extension = downloadName.substring(downloadName.lastIndexOf(".") + 1);
		switch(extension) {
			case "csv":
				headers.setContentType(new MediaType("text", "csv"));
				break;
			case "pdf":
				headers.setContentType(MediaType.APPLICATION_PDF);
				break;
			case "txt":
				headers.setContentType(MediaType.TEXT_PLAIN);
				break;
			case "zip":
				headers.setContentType(MediaType.MULTIPART_FORM_DATA);
				break;
			case "xml":
				headers.setContentType(MediaType.TEXT_XML);
				break;
			default:
				LogUtil.log(String.format("Unknown extension [%s]", extension));
				break;
		}
		ResponseEntity responseEntity = new ResponseEntity(contents, headers, HttpStatus.OK);
		return responseEntity;
	}

	protected static List<String> readFile(MultipartFile importFile) {
		BufferedReader br;
		List<String> results = CollectionFactory.createList();
		try {
			String line;
			InputStream is = importFile.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				results.add(line);
			}
			is.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		return results;
	}
}
