package nl.impressie.grazer.util;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.JSPMessage;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.Map;

/**
 * @author Impressie
 */
public class SessionUtil {
	public static final String JS_COOKIE_NAME = "javascript";

	public static void addToSession(HttpServletRequest Request, String Key, Object Value) {
		HttpSession session = Request.getSession();
		session.setAttribute(Key, Value);
	}

	public static String getAndRemoveStringFromSession(HttpServletRequest Request, String Key) {
		Object o = getAndRemoveFromSession(Request, Key);
		String s = "";
		if(o != null)
			s = String.valueOf(o);
		return s;
	}

	public static void removeFromSession(HttpServletRequest request, String... keys) {
		for(String key : keys) {
			getAndRemoveFromSession(request, key);
		}
	}

	public static Object getAndRemoveFromSession(HttpServletRequest Request, String Key) {
		HttpSession session = Request.getSession();
		Object object = session.getAttribute(Key);
		session.removeAttribute(Key);
		return object;
	}

	public static Map getSessionAttributesMap(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Map sessionMap = CollectionFactory.createMap();
		Enumeration e = session.getAttributeNames();
		while(e.hasMoreElements()) {
			String param = (String) e.nextElement();
			sessionMap.put(param, session.getAttribute(param));
		}
		return sessionMap;
	}

	public static Boolean isJSEnabled(HttpServletRequest request) {
		Cookie cookie = WebUtils.getCookie(request, JS_COOKIE_NAME);
		boolean jsEnabled = false;
		if(null != cookie)
			jsEnabled = Boolean.valueOf(cookie.getValue());
		return jsEnabled;
	}

	public static void addAllToSession(HttpServletRequest request, Map map) {
		for(Object key : map.keySet()) {
			addToSession(request, key.toString(), map.get(key));
		}
	}

	public static void addMessageToSession(HttpServletRequest request, JSPMessage jspMessage) {
		addToSession(request, JSPMessage.CONTENT_PARAMETER, jspMessage.getContent());
		addToSession(request, JSPMessage.TYPE_PARAMETER, jspMessage.getType());
	}
}
