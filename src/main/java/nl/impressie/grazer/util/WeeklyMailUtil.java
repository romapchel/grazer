package nl.impressie.grazer.util;


import nl.impressie.grazer.model.Crawler.Article;
import org.apache.commons.io.IOUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class WeeklyMailUtil {



    public void sendMail(String to, List<Article> listOnNews, String profileName, String username) {

        // Sender's email ID needs to be mentioned
        String from = "notpchelintsevroma@gmail.com";
        SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy");
        Date date = new Date();

        // Assuming you are sending email from through gmails smtp
        String host = "smtp.gmail.com";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.debug", false);
        // Get the Session object.// and pass username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("notpchelintsevroma@gmail.com", "ocuaimcvwldwarwy");

            }

        });

        // Used to debug SMTP issues
        session.setDebug(true);

        try {

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("E-mailattendering van "+ formatter.format(date) + " op basis van uw profiel " + profileName);

            // Now set the actual message
            message.setContent("<!DOCTYPE html>\n" +
                    "<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" lang=\"en\">\n" +
                    "\n" +
                    "<head>\n" +
                    "<title></title>\n" +
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                    "<!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]-->\n" +
                    "<!--[if !mso]><!-->\n" +
                    "<link href=\"https://fonts.googleapis.com/css?family=Bitter\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    "<link href=\"https://fonts.googleapis.com/css?family=Cabin\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    "<link href=\"https://fonts.googleapis.com/css?family=Droid+Serif\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    "<link href=\"https://fonts.googleapis.com/css?family=Permanent+Marker\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    "<link href=\"https://fonts.googleapis.com/css?family=Oxygen\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    "<link href=\"https://fonts.googleapis.com/css?family=Oswald\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    "<link href=\"https://fonts.googleapis.com/css?family=Merriweather\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    "<!--<![endif]-->\n" +
                    "<style>\n" +
                    "* {\n" +
                    "box-sizing: border-box;\n" +
                    "}\n" +
                    "\n" +
                    "body {\n" +
                    "margin: 0;\n" +
                    "padding: 0;\n" +
                    "}\n" +
                    "\n" +
                    "a[x-apple-data-detectors] {\n" +
                    "color: inherit !important;\n" +
                    "text-decoration: inherit !important;\n" +
                    "}\n" +
                    "\n" +
                    "#MessageViewBody a {\n" +
                    "color: inherit;\n" +
                    "text-decoration: none;\n" +
                    "}\n" +
                    "\n" +
                    "p {\n" +
                    "line-height: inherit\n" +
                    "}\n" +
                    "\n" +
                    ".desktop_hide,\n" +
                    ".desktop_hide table {\n" +
                    "mso-hide: all;\n" +
                    "display: none;\n" +
                    "max-height: 0px;\n" +
                    "overflow: hidden;\n" +
                    "}\n" +
                    "\n" +
                    "@media (max-width:660px) {\n" +
                    ".desktop_hide table.icons-inner {\n" +
                    "display: inline-block !important;\n" +
                    "}\n" +
                    "\n" +
                    ".icons-inner {\n" +
                    "text-align: center;\n" +
                    "}\n" +
                    "\n" +
                    ".icons-inner td {\n" +
                    "margin: 0 auto;\n" +
                    "}\n" +
                    "\n" +
                    ".row-content {\n" +
                    "width: 100% !important;\n" +
                    "}\n" +
                    "\n" +
                    ".mobile_hide {\n" +
                    "display: none;\n" +
                    "}\n" +
                    "\n" +
                    ".stack .column {\n" +
                    "width: 100%;\n" +
                    "display: block;\n" +
                    "}\n" +
                    "\n" +
                    ".mobile_hide {\n" +
                    "min-height: 0;\n" +
                    "max-height: 0;\n" +
                    "max-width: 0;\n" +
                    "overflow: hidden;\n" +
                    "font-size: 0px;\n" +
                    "}\n" +
                    "\n" +
                    ".desktop_hide,\n" +
                    ".desktop_hide table {\n" +
                    "display: table !important;\n" +
                    "max-height: none !important;\n" +
                    "}\n" +
                    "}\n" +
                    "</style>\n" +
                    "</head>\n" +
                    "\n" +
                    "<body style=\"background-color: #ebebeb; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;\">\n" +
                    "<table class=\"nl-container\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ebebeb;\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td>\n" +
                    "<table class=\"row row-1\" align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td>\n" +
                    "<table class=\"row-content stack\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 640px;\" width=\"640\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td class=\"column column-1\" width=\"100%\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;\">\n" +
                    "<table class=\"image_block block-1\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tr>\n" +
                    "<td class=\"pad\" style=\"padding-bottom:20px;padding-top:20px;width:100%;padding-right:0px;padding-left:0px;\">\n" +
                    "<div class=\"alignment\" align=\"center\" style=\"line-height:10px\"><img src=\"https://grazer.news/img/logos/legal-logo.svg\n\" style=\"display: block; height: auto; border: 0; width: 189px; max-width: 100%;\" width=\"189\" alt=\"Logo\" title=\"Logo\"></div>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    " </td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "<table class=\"row row-2\" align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td>\n" +
                    "<table class=\"row-content stack\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 640px;\" width=\"640\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td class=\"column column-1\" width=\"100%\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;\">\n" +
                    "<table class=\"text_block block-2\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;\">\n" +
                    "<tr>\n" +
                    "<td class=\"pad\" style=\"padding-top:40px;\">\n" +
                    "<div style=\"font-family: Arial, sans-serif\">\n" +
                    "<div class=\"txtTinyMce-wrapper\" style=\"font-size: 12px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #555555; line-height: 1.2;\">\n" +
                    "<p style=\"margin: 0; font-size: 14px; text-align: center;\"><span style=\"font-size:30px;color:#000000;\">Grazer® Legal NL nieuwsbericht voor " + username + "</span></p>\n" +
                    "<p style=\"margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 14.399999999999999px;\">&nbsp;</p>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "   \n" +
                    "   \n" +
                    "   \n" + generateEmail(listOnNews) +
                    "   \n" +
                    "   \n" +
                    "<table class=\"row row-4\" align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td>\n" +
                    "<table class=\"row-content stack\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; border-radius: 2px; width: 640px;\" width=\"640\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td class=\"column column-1\" width=\"100%\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 30px; padding-bottom: 40px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;\">\n" +
                    "<table class=\"button_block block-1\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tr>\n" +
                    "<td class=\"pad\" style=\"text-align:center;\">\n" +
                    "<div class=\"alignment\" align=\"center\">\n" +
                    "<!--[if mso]><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"https://www.grazer.news\" style=\"height:42px;width:447px;v-text-anchor:middle;\" arcsize=\"10%\" stroke=\"false\" fillcolor=\"#2a7eaf\"><w:anchorlock/><v:textbox inset=\"0px,0px,0px,0px\"><center style=\"color:#ffffff; font-family:Arial, sans-serif; font-size:16px\"><![endif]--><a href=\"https://www.grazer.news\" target=\"_blank\" style=\"text-decoration:none;display:inline-block;color:#ffffff;background-color:#2a7eaf;border-radius:4px;width:auto;border-top:1px solid #2a7eaf;font-weight:400;border-right:1px solid #2a7eaf;border-bottom:1px solid #2a7eaf;border-left:1px solid #2a7eaf;padding-top:5px;padding-bottom:5px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;\"><span style=\"padding-left: auto;padding-right: auto;font-size:16px;display:inline-block;letter-spacing:normal;\"><span dir=\"ltr\" style=\"word-break: break-word; line-height: 32px;\">Ga naar de website van Grazer ® Legal NL voor het volledige nieuwsoverzicht</span></span></a>\n" +
                    "<!--[if mso]></center></v:textbox></v:roundrect><![endif]-->\n" +
                    "</div>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "<table class=\"row row-5\" align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td>\n" +
                    "<table class=\"row-content stack\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 640px;\" width=\"640\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td class=\"column column-1\" width=\"100%\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;\">\n" +
                    "<table class=\"text_block block-2\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;\">\n" +
                    "<tr>\n" +
                    "<td class=\"pad\" style=\"padding-bottom:50px;padding-left:10px;padding-right:10px;padding-top:50px;\">\n" +
                    "<div style=\"font-family: sans-serif\">\n" +
                    "<div class=\"txtTinyMce-wrapper\" style=\"font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #555555; line-height: 1.2; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;\">\n" +
                    "<p style=\"margin: 0; font-size: 14px; text-align: center;\">© 2022 Grazer® Legal NL</p>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "<table class=\"row row-6\" align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td>\n" +
                    "<table class=\"row-content stack\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 640px;\" width=\"640\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<td class=\"column column-1\" width=\"100%\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;\">\n" +
                    "<table class=\"icons_block block-1\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tr>\n" +
                    "<td class=\"pad\" style=\"vertical-align: middle; color: #9d9d9d; font-family: inherit; font-size: 15px; padding-bottom: 5px; padding-top: 5px; text-align: center;\">\n" +
                    "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" +
                    "<tr>\n" +
                    "<td class=\"alignment\" style=\"vertical-align: middle; text-align: center;\">\n" +
                    "<!--[if vml]><table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"display:inline-block;padding-left:0px;padding-right:0px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;\"><![endif]-->\n" +
                    "<!--[if !vml]><!-->\n" +
                    "<table class=\"icons-inner\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; display: inline-block; margin-right: -4px; padding-left: 0px; padding-right: 0px;\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\">\n" +
                    "<!--<![endif]-->\n" +
                    "<tr>\n" +
                    "</tr>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>\n" +
                    "</tbody>\n" +
                    "</table><!-- End -->\n" +
                    "</body>\n" +
                    "\n" +
                    "</html>", "text/html");

            System.out.println("sending...");
            // Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

        public String generateEmail(List<Article> list) {
        StringBuilder s = new StringBuilder("");
        for (Article article : list)

            s.append("<table class=\"row row-3\" align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n" + "<tbody>\n" + "<tr>\n" + "<td>\n" + "<table class=\"row-content stack\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 640px;\" width=\"640\">\n" + "<tbody>\n" + "<tr>\n" + "<td class=\"column column-1\" width=\"100%\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;\">\n" + "<table class=\"text_block block-1\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;\">\n" + "<tr>\n" + "<td class=\"pad\" style=\"padding-bottom:15px;padding-left:30px;padding-right:30px;padding-top:60px;\">\n" + "<div style=\"font-family: sans-serif\">\n" + "<div class=\"txtTinyMce-wrapper\" style=\"font-size: 12px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #555555; line-height: 1.2;\">\n" + "<p style=\"margin: 0; font-size: 16px; text-align: center;\"><em><strong>").append(article.getTitle()).append("</strong></em></p>\n").append("</div>\n").append("</div>\n").append("</td>\n").append("</tr>\n").append("</table>\n").append("<table class=\"text_block block-2\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;\">\n").append("<tr>\n").append("<td class=\"pad\" style=\"padding-bottom:25px;padding-left:60px;padding-right:60px;padding-top:25px;\">\n").append("<div style=\"font-family: sans-serif\">\n").append("<div class=\"txtTinyMce-wrapper\" style=\"font-size: 12px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #555555; line-height: 1.2;\">\n").append("<p style=\"margin: 0; font-size: 14px; text-align: center;\">").append(article.getSnippet()).append("</p>\n").append("</div>\n").append("</div>\n").append("</td>\n").append("</tr>\n").append("</table>\n").append("<table class=\"button_block block-3\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"mso-table-lspace: 0pt; mso-table-rspace: 0pt;\">\n").append("<tr>\n").append("<td class=\"pad\" style=\"padding-bottom:40px;padding-left:30px;padding-right:30px;padding-top:30px;text-align:center;\">\n").append("<div class=\"alignment\" align=\"center\">\n").append("<!--[if mso]><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\" ").append(article.getUrl()).append(" \" style=\"height:42px;width:290px;v-text-anchor:middle;\" arcsize=\"10%\" stroke=\"false\" fillcolor=\"#3AAEE0\"><w:anchorlock/><v:textbox inset=\"0px,0px,0px,0px\"><center style=\"color:#ffffff; font-family:Arial, sans-serif; font-size:16px\"><![endif]--><a href=\"" + article.getUrl() + "\" target=\"_blank\" style=\"text-decoration:none;display:block;color:#ffffff;background-color:#3AAEE0;border-radius:4px;width:50%; width:calc(50% - 2px);border-top:1px solid #3AAEE0;font-weight:400;border-right:1px solid #3AAEE0;border-bottom:1px solid #3AAEE0;border-left:1px solid #3AAEE0;padding-top:5px;padding-bottom:5px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;\"><span style=\"padding-left:5px;padding-right:5px;font-size:16px;display:inline-block;letter-spacing:normal;\"><span dir=\"ltr\" style=\"margin: 0; word-break: break-word; line-height: 32px;\">OPEN</span></span></a>\n").append("<!--[if mso]></center></v:textbox></v:roundrect><![endif]-->\n").append("</div>\n").append("</td>\n").append("</tr>\n").append("</table>\n").append("</td>\n").append("</tr>\n").append("</tbody>\n").append("</table>\n").append("</td>\n").append("</tr>\n").append("</tbody>\n").append("</table>");

        return String.valueOf(s);

    }

}
//ocuaimcvwldwarwy