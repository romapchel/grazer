package nl.impressie.grazer.util;

import nl.impressie.grazer.factories.CollectionFactory;

import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class MapUtil {
	public static void addToMapList(Map map, Object key, Object listItem) {
		List list = CollectionFactory.createList();

		if(map.containsKey(key))
			list = (List) map.get(key);
		else
			map.put(key, list);

		list.add(listItem);
	}

	public static void increaseMapValue(Map map, Object key) {
		int value = 0;
		if(map.containsKey(key)) {
			try {
				Object val = map.get(key);
				if(val instanceof Integer)
					value = (int) val;
				else
					value = Integer.parseInt(val.toString());
			} catch(Exception e) {
				LogUtil.logException(e);
			}
			map.remove(key);
		}
		value++;
		map.put(key, value);
	}
}
