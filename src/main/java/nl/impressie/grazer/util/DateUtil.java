package nl.impressie.grazer.util;

import nl.impressie.grazer.exception.DateFormatException;
import nl.impressie.grazer.model.Bundle;
import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;

import static org.joda.time.Days.daysBetween;

/**
 * @author Impressie
 */
public class DateUtil {
	public static int getDaysAfterTodayAsInt(int daysAfterToday) {
		return Integer.valueOf(getDaysAfterToday(daysAfterToday, "yyyyMMdd"));
	}

	public static Long getTodayAsTimestamp() {
		return getDaysAfterTodayAsTimestamp(0);
	}

	public static Long getDaysAfterTodayAsTimestamp(int daysAfterToday) {
		return convertDateToTimestamp(getDaysAfterToday(daysAfterToday, "yyyyMMdd"), "yyyyMMdd");
	}

	public static Long getStartOfMonth() {
		LocalDate localDate = new LocalDate();
		localDate = localDate.dayOfMonth().withMinimumValue();
		String dateformat = "yyyyMMdd";
		DateTimeFormatter dtf = getFormatter(dateformat);
		return convertDateToTimestamp(dtf.print(localDate), dateformat);
	}

	public static String getDaysAfterToday(int daysAfterToday, String format) {
		LocalDate localDate = new LocalDate();
		localDate = localDate.plusDays(daysAfterToday);
		DateTimeFormatter dtfOut = getFormatter(format);
		return dtfOut.print(localDate);
	}

	public static int getDaysAfterAsInt(int startDate, int daysAfter) {
		return Integer.valueOf(getDaysAfter(startDate, daysAfter, "yyyyMMdd"));
	}

	public static String getDaysAfter(int startDate, int daysAfter, String format) {
		DateTimeFormatter dtfIn = getFormatter("yyyyMMdd");
		LocalDate localDate = LocalDate.parse(String.valueOf(startDate), dtfIn);
		localDate = localDate.plusDays(daysAfter);
		DateTimeFormatter dtfOut = getFormatter(format);
		return dtfOut.print(localDate);
	}

	public static int getCurrentDateAsInt() {
		return getDaysAfterTodayAsInt(0);
	}

	public static Long getTimestamp() {
		return new DateTime().getMillis();
	}

	public static int getDifferenceBetweenDateAndToday(int date) {
		return getDifferenceBetweenDateAndToday(date, "yyyyMMdd");
	}

	public static int getDifferenceBetweenDateAndToday(int date, String format) {
		int daysAfterToday;
		try {
			LocalDate start = new LocalDate();
			DateTimeFormatter formatter = getFormatter(format);
			LocalDate end = formatter.parseLocalDate(String.valueOf(date));
			daysAfterToday = daysBetween(start, end).getDays();
		} catch(IllegalFieldValueException e) {
			daysAfterToday = -1;
		}
		return daysAfterToday;
	}

	public static String convertIntToFormat(int Date, String NewFormat) {
		return convertStringToFormat(String.valueOf(Date), "yyyyMMdd", NewFormat);
	}

	public static String convertStringToFormat(String Date, String CurrentFormat, String NewFormat) {
		if("0" == Date)
			return Date;

		DateTimeFormatter formatter = getFormatter(CurrentFormat);
		DateTime dt = DateTime.parse(Date, formatter);
		return dt.toString(NewFormat);
	}

	public static String convertDateToFormat(Date date, String dateFormat) {
		DateTimeFormatter dtf = getFormatter(dateFormat);
		DateTime dateTime = new DateTime(date);
		return dtf.print(dateTime);
	}
	public static Long convertDateToTimestamp(String Date, String Dateformat) {
		DateTimeFormatter dtf = getFormatter(Dateformat);
		DateTime dt = DateTime.parse(Date, dtf);
		return dt.getMillis();
	}

	public static String convertTimestampToFormat(Long Made, String DateTimeFormat) {
		DateTime timestamp = new DateTime(Made);
		return timestamp.toString(DateTimeFormat);
	}

	public static String deleteOrdinal(String date) {
		Matcher match = RegexUtil.getCaseInsensitiveMatcher("([0-9]+)(st|nd|rd|th)", date);
		while(match.find()) {
			date = date.replaceAll(Matcher.quoteReplacement(match.group(0)), match.group(1));
		}

		return date;
	}

	/**
	 * recieves a String containing the date, in various formats, returns a string yyyyMMdd
	 *
	 * @param DateString    The String containing the date
	 * @param DateExtractor The regex string to extract the date
	 * @param DateFormat    The format of the date in the string
	 * @return Returns a String with the date in format yyyyMMdd, or "0" if no date was found
	 */
	public static String translateDate(String DateString, String DateExtractor, String DateFormat) throws DateFormatException {
		return translateDate(DateString, DateExtractor, DateFormat, true);
	}

	public static String translateDate(String DateString, String DateExtractor, String DateFormat, boolean changeFormatOnFailure) throws DateFormatException {
		String original = DateString;
		if(0 == DateString.length() || DateFormat.length() == 0)
			DateString = "0";

		//remove some meaningless crap, needed for parool.nl
		DateString = DateString.replaceAll("\\sCET\\s", " ");
		DateString = DateString.replaceAll(" ", " ");

		if(DateExtractor.length() > 0) {
			Matcher m = RegexUtil.getCaseInsensitiveMatcher(DateExtractor, DateString);
			if(m.find())
				DateString = m.group(0);
			else
				DateString = "0";
		}

		DateString = DateString.toLowerCase().trim();

		DateString = DateString.replaceAll("'", "");
		DateString = DateUtil.deleteOrdinal(DateString);

		//Dutch months
		DateString = DateString.replaceAll("januari", "01");
		DateString = DateString.replaceAll("februari", "02");
		DateString = DateString.replaceAll("maart", "03");
		DateString = DateString.replaceAll("april", "04");
		DateString = DateString.replaceAll("mei", "05");
		DateString = DateString.replaceAll("juni", "06");
		DateString = DateString.replaceAll("juli", "07");
		DateString = DateString.replaceAll("augustus", "08");
		DateString = DateString.replaceAll("september", "09");
		DateString = DateString.replaceAll("oktober", "10");
		DateString = DateString.replaceAll("november", "11");
		DateString = DateString.replaceAll("december", "12");

		//English months
		DateString = DateString.replaceAll("january", "01");
		DateString = DateString.replaceAll("february", "02");
		DateString = DateString.replaceAll("march", "03");
		DateString = DateString.replaceAll("april", "04");
		DateString = DateString.replaceAll("may", "05");
		DateString = DateString.replaceAll("june", "06");
		DateString = DateString.replaceAll("july", "07");
		DateString = DateString.replaceAll("august", "08");
		DateString = DateString.replaceAll("september", "09");
		DateString = DateString.replaceAll("october", "10");
		DateString = DateString.replaceAll("november", "11");
		DateString = DateString.replaceAll("december", "12");


		DateString = DateString.replaceAll("january", "01");

		//Found, unsure of context
		DateString = DateString.replaceAll("januar", "01");
		DateString = DateString.replaceAll("okt", "10");
		DateString = DateString.replaceAll("sept", "09");
		DateString = DateString.replaceAll("mrt", "03");

		//English short
		DateString = DateString.replaceAll("jan", "01");
		DateString = DateString.replaceAll("feb", "02");
		DateString = DateString.replaceAll("mar", "03");
		DateString = DateString.replaceAll("apr", "04");
		DateString = DateString.replaceAll("may", "05");
		DateString = DateString.replaceAll("jun", "06");
		DateString = DateString.replaceAll("jul", "07");
		DateString = DateString.replaceAll("aug", "08");
		DateString = DateString.replaceAll("sep", "09");
		DateString = DateString.replaceAll("oct", "10");
		DateString = DateString.replaceAll("nov", "11");
		DateString = DateString.replaceAll("dec", "12");

//        DateString = DateString.replaceAll("^(?=\\d[^\\d])", "0");
//        DateString = DateString.replaceAll("(?<!\\d)(?=\\d[^\\d])", "0");

		DateFormat = DateFormat.replaceAll("`T`", "`t`");
		try {
			DateString = DateUtil.convertStringToFormat(DateString, DateFormat, "yyyyMMdd");
		} catch(IllegalArgumentException e) {
			if(changeFormatOnFailure) {
				String splitter = "";
				if(DateString.contains("-") && !DateFormat.contains("-"))
					splitter = "-";
				else if(DateString.contains("/") && !DateFormat.contains("/"))
					splitter = "/";
				else if(DateString.contains(" ") && !DateFormat.contains(" "))
					splitter = " ";

				if(splitter.length() > 0)
					DateString = translateDate(DateString, DateExtractor, DateFormat.replaceAll("[\\s/\\-]", splitter), false);
				else
					throw new DateFormatException(DateString, DateExtractor, DateFormat, original);
			} else {
				throw new DateFormatException(DateString, DateExtractor, DateFormat, original);
			}
		}

		return DateString;
	}

	private static DateTimeFormatter getFormatter(String format) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(format);

		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		formatter = formatter.withDefaultYear(currentYear);

		return formatter;
	}

	public static String elapsedTimeToString(long elapsedTimeInMS) {
		int hours = (int)elapsedTimeInMS / 1000 / 60 / 60;
		elapsedTimeInMS = elapsedTimeInMS - (hours * 1000 * 60 * 60);
		int minutes = (int)elapsedTimeInMS / 1000 / 60;
		elapsedTimeInMS = elapsedTimeInMS - (minutes * 1000 * 60);
		int seconds = (int)elapsedTimeInMS / 1000;
		elapsedTimeInMS = elapsedTimeInMS - (seconds * 1000);
		StringBuilder builder = new StringBuilder();

		if(hours > 0)
			builder.append(hours + ":");
		if(minutes > 0)
			builder.append(minutes + ":");

		builder.append(seconds + "." + elapsedTimeInMS);
		return builder.toString();
	}

	public static int converStringToInt(String date, String format) {
		DateTimeFormatter formatter = getFormatter(format);
		DateTimeFormatter output = getFormatter("yyyyMMdd");
		DateTime dt = formatter.parseDateTime(date);

		return Integer.valueOf(output.print(dt));
	}

	public static int calculateDate(int startDate, Bundle bundle, boolean add) {
		String format = "yyyyMMdd";
		LocalDate start = (startDate == 0 ? new LocalDate() : new LocalDate(convertDateToTimestamp(String.valueOf(startDate), format)));
		Date date = calculateDate(start, bundle.getIntervalType(), bundle.getIntervalNmbr(), add);
		return Integer.valueOf(convertDateToFormat(date, format));
	}
	public static Date calculateDate(String intervalType, int intervalNmbr) {
		return calculateDate(new LocalDate(), intervalType, intervalNmbr, true);
	}
	public static Date calculateDate(LocalDate start, String intervalType, int intervalNmbr, boolean add) {
		switch(intervalType.toLowerCase().trim()) {
			case "y":
				start = (add ? start.plusYears(intervalNmbr) : start.minusYears(intervalNmbr));
				break;
			case "m":
				start = (add ? start.plusMonths(intervalNmbr) : start.minusMonths(intervalNmbr));
				break;
			case "d":
			default:
				start = (add ? start.plusDays(intervalNmbr) : start.minusDays(intervalNmbr));
				break;
		}
		return start.toDate();
	}
}
