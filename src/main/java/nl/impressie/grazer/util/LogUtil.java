package nl.impressie.grazer.util;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Mail.ExceptionMail;
import nl.impressie.grazer.model.Mail.HTMLMail;
import nl.impressie.grazer.util.crawlerUtils.IndexUtil;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;
import org.pmw.tinylog.labelers.TimestampLabeler;
import org.pmw.tinylog.policies.SizePolicy;
import org.pmw.tinylog.policies.StartupPolicy;
import org.pmw.tinylog.policies.WeeklyPolicy;
import org.pmw.tinylog.writers.RollingFileWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.List;

/**
 * @author Impressie
 */
@Component
public class LogUtil {
	private static LogUtil instance;
	private static Level level = Level.TRACE;
	private static final String logFile = "logs/nieuwslezer.log";

	@Value("${server.testmode}")
	private boolean testMode;

	public static boolean DEBUG;

	private LogUtil() {
		Configurator
				.defaultConfig()
				.formatPattern(             //How the log messages are written
						"{date:yyyy-MM-dd HH:mm} {level}: {message}"
				)
				.writer(                   //How the log files are written
						new RollingFileWriter(
								logFile,                    //File name
								52,                         //Number of backup files
								new TimestampLabeler(),     //Naming metod, adds timestamp to name
								new StartupPolicy(),        //Starts new log file on startup
								new WeeklyPolicy(),         //Start new log file every week
								new SizePolicy(10 * 1024)   //Start new log file when size limit is reached
						)
				)
				.level(level)
				.activate();
//		LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(java.util.logging.Level.OFF);

	}

	@PostConstruct
	private void init() {
		DEBUG = testMode;
	}

	public static LogUtil instantiate() {
		if(instance == null)
			instance = new LogUtil();

		return instance;
	}
	private static LogUtil getInstance() {
		if(instance == null)
			new Exception("LogUtil was not succesfully initialised by spring").printStackTrace();

		return instance;
	}

	public static void setLevel(Level level) {
		LogUtil.level = level;
	}

	public static void log(String message, Object... arguments) {
		if(arguments.length > 0) {
			message = String.format(message, arguments);
		}

		log(message, level);
	}

	public static void log(String message, Level level) {
		getInstance();
		System.out.println(String.format("[%s} was logged at level %s", message, level.name()));

		if(level.equals(Level.ERROR))
			Logger.error(message);
		else if(level.equals(Level.WARNING))
			Logger.warn(message);
		else if(level.equals(Level.INFO))
			Logger.info(message);
		else if(level.equals(Level.DEBUG))
			Logger.debug(message);
		else if(level.equals(Level.TRACE))
			Logger.trace(message);
		else if(!level.equals(Level.OFF)) {
			String error = "The logger level was not recognised";
			System.err.println(error);
			Logger.error(error);
		}
	}

	public static void logError(Error exception) {
		if(DEBUG)
			exception.printStackTrace();
		HTMLMail mail = new ExceptionMail();
		StackTraceElement[] stacktrace = exception.getStackTrace();

		Throwable cause = exception.getCause();
		if(cause != null) {
			String line = cause.toString();
			mail.addLine(line);
		}

		String message = String.format("%s: %s", exception.getClass().getName(), exception.getMessage());
		mail.addLine(message);

		for(StackTraceElement el : stacktrace) {
			mail.addLine(el.toString());
		}
		mail.send();
	}

	public static String exceptionToLog(Exception exception) {
		if(DEBUG)
			exception.printStackTrace();

		StringBuilder builder = new StringBuilder();

		for(String line: exceptionToStrings(exception)) {
			builder.append(line);
			builder.append(System.lineSeparator());
		}

		return builder.toString();
	}
	public static Collection<String> exceptionToStrings(Exception exception) {
		List<String> strings = CollectionFactory.createList(getExceptionTitle(exception));

		StackTraceElement[] stacktrace = exception.getStackTrace();
		for(StackTraceElement el : stacktrace) {
			strings.add(el.toString());
		}

		return strings;
	}

	public static void logException(Exception exception) {
		logException("", exception);
	}

	public static void logException(String message, Exception exception) {
		exception.printStackTrace();
		if(DEBUG) {
			System.out.println("debug log");
			if(message.length() > 0)
				log(message);

			exceptionToLog(exception);

		} else if(!DEBUG && IndexUtil.getStartTime() != 0) {
			IndexUtil.addToMail(message, exception);
		} else {

			//ExceptionMail mail = new ExceptionMail();
			//mail.addException(message, exception);
            //mail.send();
		}
	}

	public static void logExceptionToFile(Exception exception) {
		exception.printStackTrace();

		StringBuilder builder = new StringBuilder();
		for(String el : exceptionToStrings(exception)) {
			builder.append(el);
		}

		log(builder.toString());
	}
	public static String getExceptionTitle(Exception exception) {
		return String.format("%s: %s", exception.getClass().getName(), exception.getMessage());
	}

	public static void logClass(Class<?> klas) {
		String location = klas.getProtectionDomain().getCodeSource().getLocation().toString();
		System.out.println(location);
	}

	public static void logClassPath() {
		ClassLoader cl = ClassLoader.getSystemClassLoader();

		URL[] urls = ((URLClassLoader) cl).getURLs();

		for(URL url : urls) {
			LogUtil.log(url.getFile());
		}
		LogUtil.log(System.getProperty("java.class.path"));
	}

	public static void logJavaVersion() {
		LogUtil.log(System.getProperty("java.version"));
	}
}
