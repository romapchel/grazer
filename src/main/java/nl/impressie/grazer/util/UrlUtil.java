package nl.impressie.grazer.util;

import org.mozilla.universalchardet.UniversalDetector;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author Impressie
 */
public class UrlUtil {
	public static String encode(String string) {
		try {
			string = string.replaceAll("\\s", "%20");
			string = URLEncoder.encode(string, "UTF-8");
		} catch(UnsupportedEncodingException e) {
		}

		return string;
	}

	public static String encodeTrimWhitespace(String string) {
		string = trimWhitespace(string);

		return encode(string);
	}

	public static String trimWhitespace(String string) {
		string = string.replace("\n", " ");
		string = string.replaceAll("\\s+", " ");
		string = string.trim();

		return string;
	}

	public static String decodeSpaces(String string) {
		if(string == null || string.length() == 0)
			return string;

		string = string.replace("%20", " ");
		return string;
	}

	public static String getEncoding(byte[] bytes) {
		UniversalDetector detector = new UniversalDetector(null);
		detector.handleData(bytes, 0, bytes.length);
		detector.dataEnd();
		String encoding = detector.getDetectedCharset();
		detector.reset();
		if(encoding == null)
			encoding = "UTF-8";
		return encoding;
	}

	public static String fixProtocol(String url) {
		if(url.startsWith("www."))
			url = "http://" + url;

		return url;
	}

	public static boolean isValidUrl(String url) {
		try {
			new URL(url);
		} catch(MalformedURLException e) {
			LogUtil.logException(e);
			return false;
		}
		return true;
	}
}
