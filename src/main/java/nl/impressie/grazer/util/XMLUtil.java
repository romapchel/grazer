package nl.impressie.grazer.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import nl.impressie.grazer.dao.AltPageDao;
import nl.impressie.grazer.dao.BeforeSelectorDao;
import nl.impressie.grazer.dao.ProfileDao;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.SeReDe;
import nl.impressie.grazer.model.DB.*;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class XMLUtil {
	private static List<String> thesaurus;

	public static List<Website> parseXML(String path) {
		return parseXML(path, false, false, false);
	}

	public static List getTags(String path){
		List items = CollectionFactory.createList();
		Document doc = null;

		try {
			File fXmlFile = new File(path);
			doc = Jsoup.parse(fXmlFile, "UTF-8");
		} catch(IOException e) {
			LogUtil.logException(e);
		}

		if(doc != null) {
			for(Element site : doc.select("site")) {
				Map page = CollectionFactory.createMap();
				page.put(WebsiteDao.URL, stringExtract(site, WebsiteDao.URL));
				page.put(WebsiteDao.NAME, stringExtract(site, WebsiteDao.NAME));
				page.put(WebsiteDao.RECHTSGEBIED, stringExtract(site, Tag.RECHTSGEBIED));
				page.put(WebsiteDao.NIEUWS_SOORT, stringExtract(site, Tag.NIEUWSSOORT));
				page.put(WebsiteDao.RUBRIEKEN, stringExtract(site, Tag.RUBRIEK));
				page.put(WebsiteDao.NIEUWSZENDER, stringExtract(site, WebsiteDao.NIEUWSZENDER));
				page.put(WebsiteDao.DESCRIPTION, stringExtract(site, WebsiteDao.DESCRIPTION));
				items.add(page);
			}
		}
		return items;
	}

	public static List<Website> parseXML(String path, boolean override, boolean ignoreSelectors, boolean disableOnUpgrade) {
		List<Website> targets = CollectionFactory.createList();

		File fXmlFile = new File(path);
		Document doc = null;
		try {
			doc = Jsoup.parse(fXmlFile, "UTF-8");
		} catch(IOException e) {
		}

		if(doc == null)
			return CollectionFactory.createList();

		StringBuilder ids = new StringBuilder();

		for(Element site : doc.select("site")) {
			String url = stringExtract(site, "url");
			String name = stringExtract(site, "siteName");
			String nieuwszender = stringExtract(site, "nieuwszender");

			Website current = WebsiteDao.findByUrl(url);
			if(current != null && !override)
				continue;

			if(ignoreSelectors) {
				List<Parameter> params = CollectionFactory.createList();

				String rechtsgebied = stringExtract(site, Tag.RECHTSGEBIED);
				String nieuwssoort = stringExtract(site, Tag.NIEUWSSOORT);
				String rubrieken = stringExtract(site, Tag.RUBRIEK);

				if(current == null) {
					current = WebsiteDao.create(name, url, nieuwszender);
				} else {
					params.add(new Parameter(WebsiteDao.NIEUWSZENDER, nieuwszender));
					params.add(new Parameter(WebsiteDao.NAME, name));
				}
				if(current == null)
					continue;

				SeReDe serede = current.getRechtsgebied();
				params.add(new Parameter(WebsiteDao.RECHTSGEBIED, new SeReDe(serede.getSelector(), serede.getProperty(), serede.getRegex(), rechtsgebied).toString()));
				serede = current.getNieuwssoort();
				params.add(new Parameter(WebsiteDao.NIEUWS_SOORT, new SeReDe(serede.getSelector(), serede.getProperty(), serede.getRegex(), nieuwssoort).toString()));
				serede = current.getRubrieken();
				params.add(new Parameter(WebsiteDao.RUBRIEKEN, new SeReDe(serede.getSelector(), serede.getProperty(), serede.getRegex(), rubrieken).toString()));

				current = WebsiteDao.update(current.getId(), params);
			} else {
				String depth = String.valueOf(intExtract(site, "depth", 0)); //get it as an int so the defaultvalue if given if it's not a correct number, then turn it to a String
				String dateFormat = stringExtract(site, "dateformat");
				String moreInfo = stringExtract(site, "meerInformatie");

				SeReDe crawl = seredeExtract(site, WebsiteDao.CRAWL);
				SeReDe article = seredeExtract(site, WebsiteDao.ARTICLE);
				SeReDe date = seredeExtract(site, WebsiteDao.DATE);
				SeReDe snippet = seredeExtract(site, WebsiteDao.SNIPPET);
				SeReDe title = seredeExtract(site, "titles");
				SeReDe content = seredeExtract(site, WebsiteDao.CONTENT);
				SeReDe rechtsgebied = seredeExtract(site, Tag.RECHTSGEBIED);
				SeReDe nieuwssoort = seredeExtract(site, Tag.NIEUWSSOORT);
				SeReDe rubrieken = seredeExtract(site, Tag.RUBRIEK);

				PageSearch search = searchExtract(site);
				JavaScriptObject jsObject = extractJSObject(site);

				String lastCrawlDate = null;
				if(current != null && override)
					current.delete();

				current = WebsiteDao.create(name, url, nieuwszender, depth, crawl, article, lastCrawlDate, dateFormat, moreInfo,
						date, title, content, snippet, rechtsgebied, nieuwssoort, rubrieken, search, jsObject);

				if(current == null)
					continue;

				Object pageId = current.getId();

				createBefores(pageId, site);
				createAltPages(pageId, site);
				createNavigation(pageId, site);
				createButtons(pageId, site);
			}

			if(ids.length() > 0)
				ids.append(", ");
			ids.append(current.getId());

			targets.add(current);
		}

		for(Website page : targets) {
			page.loadTagsFromSerede();
		}

		if(disableOnUpgrade && ids.length() > 0)
			WebsiteDao.disableById(ids.toString());

		return targets;
	}

	public static void createButtons(Object pageId, Element site) {
		for(Element button : site.select("button")) {
			String selector = stringExtract(button, "selector");
			String testId = stringExtract(button, "testid");
			int clicks = intExtract(button, "clicks", 0);
			int order = intExtract(button, "order", 0);
			boolean indexAfterClick = Boolean.parseBoolean(stringExtract(button, "indexAfterEachClick"));

			Button.create(pageId, selector, clicks, order, indexAfterClick, testId);
		}
	}

	public static void createNavigation(Object pageId, Element site) {
		Element nav = getChildElementWithTag(site, "nav");

		String navurl = stringExtract(nav, "url");
		String paging = stringExtract(nav, "paging");
		String addon = stringExtract(nav, "after");
		int start = intExtract(nav, "start", 0);
		int jump = intExtract(nav, "jump", 1);
		int numberOfJumps = intExtract(nav, "end", 0);

		Navigation.create(pageId, navurl, paging, addon, start, jump, numberOfJumps);
	}

	public static void createBefores(Object websiteId, Element site) {
		Elements befores = site.select("before");
		if(befores == null)
			return;

		for(Element before : befores) {
			String name = stringExtract(before, "name");
			String selector = stringExtract(before, "selector");
			BeforeSelectorDao.create(websiteId, name, selector);
		}
	}

	public static void createAltPages(Object websiteId, Element site) {
		Element pages = getChildElementWithTag(site, "altPages");
		if(pages == null)
			return;

		boolean pre = true;
		for(Element page : pages.select("pre")) {
			AltPageDao.create(websiteId, page.ownText(), pre);
		}
		pre = false;
		for(Element page : pages.select("post")) {
			AltPageDao.create(websiteId, page.ownText(), pre);
		}
	}

	public static JavaScriptObject extractJSObject(Element site) {
		Element javascriptElement = getChildElementWithTag(site, "javascript");
		String script = stringExtract(javascriptElement, "javascript");
		String testID = stringExtract(javascriptElement, "testId");

		return new JavaScriptObject(testID, script);
	}

	public static PageSearch searchExtract(Element site) {
		Element searchElement = getChildElementWithTag(site, "search");
		String key = stringExtract(searchElement, "key");

		Element json = getChildElementWithTag(searchElement, "json");
		String searchUrl = stringExtract(json, "url");
		String pageKey = stringExtract(json, "page");
		String method = stringExtract(json, "method");
		String header = stringExtract(json, "header");
		String data = stringExtract(json, "data");

		return new PageSearch(key, method, header, data, searchUrl, pageKey);
	}

	public static Element getChildElementWithTag(Element element, String tagname) {
		if(element == null)
			return null;

		return element.select(":root > " + tagname).first();
	}

	public static String stringExtract(Element element, String tagName) {
		if(element == null)
			return "";

		Element tag = element;
		if(null != tagName)
			tag = element.select(tagName).first();

		if(tag == null)
			return "";
		return tag.ownText().replaceAll("\n ", "").trim();
	}

	public static int intExtract(Element element, String tagname, int defaultValue) {
		String string = stringExtract(element, tagname);

		int integer = defaultValue;
		try {
			integer = Integer.parseInt(string);
		} catch(NumberFormatException e) {
		}

		return integer;
	}

	public static SeReDe seredeExtract(Element site, String tagName) {
		Element seredeElement = site.select(tagName).first();
		if(null == seredeElement)
			return new SeReDe();

		String selector = stringExtract(seredeElement, "selector");

		int size = seredeElement.getAllElements().size();
		if(1 == size)
			selector = stringExtract(seredeElement, null);

		String property = stringExtract(seredeElement, "property");

		String regex = stringExtract(seredeElement, "regex");

		String defaultvalue = stringExtract(seredeElement, "default");

		return new SeReDe(selector, property, regex, defaultvalue);
	}

	public static Element createElement(String tagname, String content) {
		Element element = new Element(tagname);
		element.html(content);
		return element;
	}

	public static void saveAddElement(Element root, Element child) {
		if(root == null || child == null)
			return;
		root.appendChild(child);
	}

	public static Element createAltPagesElement(ArrayList<AltPage> altPages) {
		if(altPages.size() < 1)
			return null;

		Element altPageContainer = new Element("altPages");
		for(AltPage page : altPages) {
			XMLUtil.saveAddElement(altPageContainer, page.toXml());
		}

		return altPageContainer;
	}

	public static List<String> getThesaurusTerms() {
		if(thesaurus == null)
			thesaurus = createThesaurus();
		return thesaurus;
	}

	private static List<String> createThesaurus() {
		List<String> terms = CollectionFactory.createList();
		try {
			Document doc = Jsoup.parse(ContextProvider.getApplicationContext().getResource("/WEB-INF/conf/justitiethesaurus.xml").getFile(), "UTF-8");
			Elements descriptors = doc.select("descriptor");
			for(Element descriptor : descriptors) {
				terms.add(stringExtract(descriptor, "descriptor-term"));
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		return terms;
	}

	public static Profile parseProfile(String path) {
		File fXmlFile = new File(path);
		Document doc = null;
		try {
			doc = Jsoup.parse(fXmlFile, "UTF-8");
		} catch(IOException e) {
		}

		if(doc == null)
			return null;

		CustomUser user = AuthenticationUtil.getCurrentUser();

		String profileName = stringExtract(doc, "profile > name");

		List tagIds = CollectionFactory.createList();
		List siteIds = CollectionFactory.createList();

		for(Element tag : doc.select("tag")) {
			String id = stringExtract(tag, "id");
			if(null != id && 0 < id.length())
				tagIds.add(id);

		}
		for(Element site : doc.select("site")) {
			String id = stringExtract(site, "id");
			if(null != id && 0 < id.length())
				siteIds.add(id);
		}

		Profile profile = ProfileDao.create(profileName, user.getId());
		profile = profile.updateTags(tagIds).updateSites(siteIds);

		String titleSearch = doc.select("titlesearch").text();
		String contentSearch = doc.select("contentsearch").text();

		profile = profile.updateSearch(titleSearch.trim(), contentSearch.trim());

		return profile;
	}

	public static Gson getGson() { return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create(); }
}
