package nl.impressie.grazer.util;

import org.simplejavamail.api.email.EmailPopulatingBuilder;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.MailerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static nl.impressie.grazer.util.LogUtil.DEBUG;

/**
 * @author Impressie
 */
@Component
public class  MailUtil {
	private static MailUtil instance;

	@Value("${mail.sender.default}")
	private String defaultSender;
	@Value("${mail.sender.update}")
	private String updateSender;
	@Value("${mail.sender.exception}")
	private String exceptionSender;
	@Value("${mail.sender.verification}")
	private String verificationSender;

	@Value("${mail.receiver.default}")
	private String defaultReceiver;

	@Value("${mail.username}")
	private String username;
	@Value("${mail.password}")
	private String password;
	@Value("${mail.host}")
	private String host;
	@Value("${mail.port}")
	private int port;

	@Value("${mail.javamailproperties.mail.smtp.auth}")
	private String auth;
	@Value("${mail.javamailproperties.mail.smtp.starttls.enable}")
	private String starttls;

	@Value("${mail.smtp.dkim.privatekey}")
	private String dkimPrivateKeyLocation;
	private File dkimPrivateKey;
	@Value("${mail.smtp.dkim.signingdomain}")
	private String dkimDomain;
	@Value("${mail.smtp.dkim.selector}")
	private String dkimSelector;

	private JavaMailSender mailSender;
	private Mailer simpleMailer;

	public MailUtil() { instance = this; }

	private void prepareMailsender() {
		try {
			dkimPrivateKey = new ClassPathResource(dkimPrivateKeyLocation).getFile();
		} catch(IOException e) {
			e.printStackTrace();
		}
		JavaMailSenderImpl newSender = new JavaMailSenderImpl();

		newSender.setHost(host);
		newSender.setPort(port);

		if(Boolean.valueOf(auth)) {
			newSender.setUsername(username);
			newSender.setPassword(password);
			Properties javaMailProperties = new Properties();
			javaMailProperties.setProperty("mail.smtp.auth", auth);
			javaMailProperties.setProperty("mail.smtp.starttls.enable", starttls);

			newSender.setJavaMailProperties(javaMailProperties);
		}
		mailSender = newSender;

		simpleMailer = MailerBuilder
				.withSMTPServer(host, port, username, password)
				.buildMailer();
	}

	private void signAndSendMail(EmailPopulatingBuilder builder) {
		if(dkimPrivateKey != null)
			builder.signWithDomainKey(dkimPrivateKey, dkimDomain, dkimSelector);

		simpleMailer.sendMail(builder.buildEmail());
	}

	private static MailUtil getInstance() {
		if(instance.mailSender == null)
			instance.prepareMailsender();

		return instance;
	}

	public static String getDefaultReceiver() { return getInstance().defaultReceiver; }

	public static String getUpdateSender() { return getInstance().updateSender; }
	public static String getExceptionSender() { return getInstance().exceptionSender; }
	public static String getVerificationSender() { return getInstance().verificationSender; }

	public static void sendMail(String subject, String message) {
		sendMail(getInstance().defaultReceiver, subject, message);
	}
	public static void sendMail(String to, String subject, String message) {
		sendMail(getInstance().defaultSender, to, subject, message);
	}
	public static void sendMail(String from, String to, String subject, String message) {
		sendMail(from, to, subject, message, new File[0]);
	}
	public static void sendMail(String from, String to, String subject, String message, File... files) {
		sendMail(from, to, subject, message, files, new HashMap());
	}
	public static void sendMail(String from, String to, String subject, String message, File[] files, Map<String, File> inlineFiles) {
		if(DEBUG) {
			LogUtil.log("Mailing is currently disabled");
			LogUtil.log(message);
			return;
		}

		MimeMessage mimeMail = getInstance().mailSender.createMimeMessage();

		try {
			EmailPopulatingBuilder builder = EmailBuilder.startingBlank()
					.from(null, from)
					.to(null, to)
					.withSubject(subject)
					.withHTMLText(message);
			if(inlineFiles != null) {
				for(String id: inlineFiles.keySet()) {
					File file = inlineFiles.get(id);
					builder.withAttachment(id, new FileDataSource(file));
				}
			}
			if(null != files) {
				for(File file: files) {
					builder.withEmbeddedImage(file.getName(), new FileDataSource(file));
				}
			}
			getInstance().signAndSendMail(builder);


			MimeMessageHelper helper = new MimeMessageHelper(mimeMail, MimeMessageHelper.MULTIPART_MODE_RELATED);

			helper.setFrom(from);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(message, true);

			for(String id: inlineFiles.keySet()) {
				File file = inlineFiles.get(id);
				helper.addInline(id, file);
			}

			if(null != files) {
				for(File file: files) {
					helper.addAttachment(file.getName(), file);
				}
			}

			getInstance().mailSender.send(mimeMail);
		} catch(MessagingException e) {
			LogUtil.logExceptionToFile(e);
			LogUtil.log("mailing failed because of: " + e.getMessage());
		} catch(Exception e) {
			LogUtil.log("mailing failed because of: " + e.getMessage());
			throw e;
		}
	}
}
