package nl.impressie.grazer.util;

import nl.impressie.grazer.factories.CollectionFactory;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * @author Impressie
 */
public interface EncodeUtil {
	String ENCODING_SYMBOL = "$";

	static String encode(Map<String, Object> values) {
		StringBuilder builder = new StringBuilder();
		for(String key: values.keySet()) {
			builder.append(encode(values.get(key).toString(), key));
		}
		return builder.toString();
	}
	static String encode(String toEncode, String encoder) {
		if(0 == toEncode.length())
			return "";

		return String.format("[%1$s]%2$s[/%1$s]", toEncoding(encoder), toEncode);
	}

	static String toEncoding(String encoding) {
		return String.format("%1$s%2$s%1$s", ENCODING_SYMBOL, encoding);
	}

	static Map<String, String> load(String text, Collection<String> keys) {
		Map map = CollectionFactory.createMap();
		for(String key: keys) {
			map.put(key, load(text, key));
		}
		return map;
	}

	static String load(String text, String encoding) {
		encoding = toEncoding(encoding);
		encoding = encoding.replace("$", "\\$");
		String found = "";
		String pattern = String.format("(?<=\\[%1$s\\]).*?(?=\\[/%1$s\\])", encoding);
		Matcher match = RegexUtil.getDotAllMatcher(pattern, text);

		if(match.find()) {
			found = match.group(0);
		}
		return found;
	}
}
