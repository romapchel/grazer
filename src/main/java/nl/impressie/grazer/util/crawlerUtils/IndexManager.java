package nl.impressie.grazer.util.crawlerUtils;

import nl.impressie.grazer.exception.IndexManagerException;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.util.LogUtil;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.facet.Facets;
import org.apache.lucene.facet.FacetsCollector;
import org.apache.lucene.facet.FacetsConfig;
import org.apache.lucene.facet.taxonomy.FacetLabel;
import org.apache.lucene.facet.taxonomy.FastTaxonomyFacetCounts;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.facet.taxonomy.TaxonomyWriter;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyReader;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyWriter;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.*;
import org.apache.lucene.store.AlreadyClosedException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.NoSuchFileException;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class IndexManager {
	private static IndexManager indexManager;

	private static IndexSearcher searcher;
	private static IndexReader reader;
	private static IndexWriter writer;

	private static TaxonomyWriter taxoWriter;
	private static TaxonomyReader taxoReader;

	private static FacetsConfig facetsConfig;

	private String path;

	private boolean writing, searching;
	private boolean updateSearcher;

	private static FSDirectory directory;
	private static FSDirectory taxoDir;

	public static final String TOTAL_HITS = "totalHits";
	public static final String FACET_FIELD = "facets";

	private static final String INDEX_PATH = "WEB-INF/data/index";

	private void prepareIndex(String path) {
		File file = new File(path);
		if(!file.exists())
			file.mkdirs();
		if(file.listFiles().length < 1) {
			try {
				deleteLockFile();
				IndexWriter tempWriter = openWriter(FSDirectory.open(file.toPath()));
				tempWriter.close();
			} catch(IOException e) {
				LogUtil.logException(e);
			}
		}
	}

	private IndexManager(String path) {
		try {
			prepareIndex(path);

			this.path = path;

			if(null == directory) {
				File file = new File(path);
				file.mkdirs();
				directory = FSDirectory.open(file.toPath());
				unlockIndex(directory);
			}
			if(null == taxoDir) {
				String taxoPath = String.format("%s/taxo", path);
				File file = new File(taxoPath);
				file.mkdirs();
				taxoDir = FSDirectory.open(file.toPath());
				unlockIndex(taxoDir);
			}
		} catch(IOException e) {
			LogUtil.logException(e);
		}
		writing = false;
		searching = false;
	}
	public static IndexManager getInstance() {
		if(indexManager == null)
			indexManager = new IndexManager(getPathString());

		return indexManager;
	}
	private static String getPathString() {
		String location = IndexManager.class.getProtectionDomain().getCodeSource().getLocation().getFile();
		int end = location.indexOf("WEB-INF");

		String webinf = "";
		if(end > 0)
			webinf = location.substring(0, end);

		String path = webinf + INDEX_PATH;

		return path;
	}

	private static void unlockIndex(Directory directory) {
		try {
			String writeLockName = IndexWriter.WRITE_LOCK_NAME;
			directory.deleteFile(writeLockName);
		} catch(NoSuchFileException e) {
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public String getPath() {
		return path;
	}

	private void closeSearcher() {
		try {
			if(searcher != null)
				searcher = null;
			if(reader != null) {
				reader.close();
				reader = null;
			}
			updateSearcher = false;
		} catch(IOException e) {
			LogUtil.logException(e);
		}
	}

	private void closeWriter() {
		try {
			commit(false);
			if(writer != null) {
				writer.close();
				writer = null;
			}
			if(taxoWriter != null) {
				taxoWriter.close();
				taxoWriter = null;
			}
		} catch(IOException e) {
			LogUtil.logException(e);
		}
	}

	private void updateSearcher() {
		if(updateSearcher)
			closeSearcher();
	}

	private IndexReader getReader() {
		getSearcher();
		return reader;
	}

	private IndexSearcher getSearcher() {
		updateSearcher();
		if(searcher == null) {
			try {
				reader = DirectoryReader.open(directory);
				searcher = new IndexSearcher(reader);
			} catch(IOException e) {
				LogUtil.logException(e);
			}
		}
		return searcher;
	}

	private IndexWriter openWriter(FSDirectory directory) {
		return openWriter(directory, false);
	}

	private IndexWriter openWriter(FSDirectory directory, boolean deleteOldIndex) {
		return openWriter(directory, deleteOldIndex, 1);
	}

	private IndexWriter openWriter(FSDirectory directory, boolean deleteOldIndex, int run) {
		try {
			IndexWriterConfig config = new IndexWriterConfig(getAnalyzer());
			if(deleteOldIndex)
				config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
			else
				config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
			config.setRAMBufferSizeMB(1024);
			return new IndexWriter(directory, config);
		} catch(LockObtainFailedException e) {
			deleteLockFile();
			return retryOpenWriter(directory, deleteOldIndex, run, e);
		} catch(AccessDeniedException e) {
			return retryOpenWriter(directory, deleteOldIndex, run, e);
		} catch(IOException e) {
			LogUtil.logException(e);
			destroy();
		}
		return null;
	}

	private void deleteLockFile() {
	}

	private IndexWriter retryOpenWriter(FSDirectory directory, boolean deleteOldIndex, int run, Exception e) {
		if(run == 1) {
			destroy();
			return getInstance().openWriter(directory, deleteOldIndex, run+1);
		} else if(run == 2) {
			unlockIndex(directory);
			destroy();
			return getInstance().openWriter(directory, deleteOldIndex, run+1);
		} else {
			//currently failing on this makes nothing happen, this can cause the mailbox to be spammed full of error messages
			LogUtil.logException(new IndexManagerException("The application was recently killed while writing to the index and didn't manage to clear the lock, please delete the write.lock file in the index folder and restart the application"));
			LogUtil.logException(e);
		}
		return null;
	}

	private TaxonomyWriter openTaxoWriter(FSDirectory directory) {
		return openTaxoWriter(directory, true);
	}

	private TaxonomyWriter openTaxoWriter(FSDirectory directory, boolean retry) {
		return openTaxoWriter(directory, retry, true);
	}
	private TaxonomyWriter openTaxoWriter(FSDirectory directory, boolean retry, boolean append) {
		try {
			IndexWriterConfig.OpenMode mode = (append ? IndexWriterConfig.OpenMode.CREATE_OR_APPEND : IndexWriterConfig.OpenMode.CREATE);
			DirectoryTaxonomyWriter directoryTaxonomyWriter = new DirectoryTaxonomyWriter(directory, mode);
			int size = directoryTaxonomyWriter.getSize();
			if(size == 1) {
				directoryTaxonomyWriter.addCategory(new FacetLabel("indexMustExist", "create"));
				directoryTaxonomyWriter.commit();
				directoryTaxonomyWriter.close();
				directoryTaxonomyWriter = new DirectoryTaxonomyWriter(directory);
			}
			return directoryTaxonomyWriter;
		} catch(LockObtainFailedException e) {
			if(retry) {
				destroy();
				return getInstance().openTaxoWriter(directory, false);
			}
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void hardReset() {
		closeWriter();
		writer = openWriter(directory, true);
		closeWriter();
	}

	private IndexWriter getWriter() {
		if(writer == null) {
			if(directory == null) {
				destroy();
				writer = getInstance().getWriter();
			} else
				writer = openWriter(directory);
		}

		return writer;
	}

	private TaxonomyWriter getTaxoWriter() {
		if(taxoWriter == null) {
			if(taxoDir == null) {
				destroy();
				taxoWriter = getInstance().getTaxoWriter();
			} else {
				taxoWriter = openTaxoWriter(taxoDir);
			}
		}
		return taxoWriter;
	}

	private TaxonomyReader getTaxoReader() {
		try {
			if(taxoWriter == null)
				getTaxoWriter();

			taxoReader = new DirectoryTaxonomyReader(taxoDir);
		} catch(IOException e) {
			e.printStackTrace();
		}

		return taxoReader;
	}

	public static String getTagFacetField(String tagName) {
		FacetsConfig conf = IndexManager.getFacetsConfig();
		FacetsConfig.DimConfig dimConfig = conf.getDimConfig(tagName);
		return dimConfig.indexFieldName;
	}

	public static FacetsConfig getFacetsConfig() {
		if(facetsConfig == null) {
			facetsConfig = new FacetsConfig();

			String facetField = String.format("%s%s", Tag.RUBRIEK, FACET_FIELD);
			facetsConfig.setIndexFieldName(Tag.RUBRIEK, facetField);
			facetsConfig.setMultiValued(facetField, true);
			facetsConfig.setRequireDimCount(facetField, false);

			facetField = String.format("%s%s", Tag.RECHTSGEBIED, FACET_FIELD);
			facetsConfig.setIndexFieldName(Tag.RECHTSGEBIED, facetField);
			facetsConfig.setMultiValued(facetField, true);
			facetsConfig.setRequireDimCount(facetField, false);

			facetField = String.format("%s%s", Tag.NIEUWSSOORT, FACET_FIELD);
			facetsConfig.setIndexFieldName(Tag.NIEUWSSOORT, facetField);
			facetsConfig.setMultiValued(facetField, true);
			facetsConfig.setRequireDimCount(facetField, false);
		}
		return facetsConfig;
	}

	//search documents
	public Map search(Query query) {
		return search(query, 0, getReader().maxDoc(), null);
	}

	public Map search(Query query, int limit) {
		return search(query, 0, limit, null);
	}

	public Map search(Query query, Sort sort) {
		return search(query, 0, getReader().maxDoc(), sort);
	}

	public Map search(Query query, int limit, Sort sort) {
		return search(query, 0, limit, sort);
	}

	public Map search(Query query, int offset, int limit, Sort sort) {
		return search(query, offset, limit, sort, true);
	}
	public Map search(Query query, int offset, int limit, Sort sort, boolean firstTry) {
		if(limit <= 0)
			limit = 1;

		TopDocs tops;
		Map results = CollectionFactory.createMap();

		try {
			if(sort == null)
				sort = Article.getDefaultSorter();

			FacetsCollector fc = new FacetsCollector();
			tops = FacetsCollector.search(getSearcher(), query, limit, sort, fc);

		} catch(AlreadyClosedException e) {
			destroy();
			return getInstance().search(query, offset, limit, sort);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
			System.err.println("IllegalArgumentException");
			tops = null;
		} catch(IOException e) {
			System.err.println("IOException");
			tops = null;
			LogUtil.logException(e);
		}

		if(null == tops)
			tops = new TopDocs(new TotalHits(0, TotalHits.Relation.EQUAL_TO), new ScoreDoc[0]);

		ScoreDoc[] hits = tops.scoreDocs;

		TotalHits totalHits = tops.totalHits;
		results.put(TOTAL_HITS, Math.toIntExact(totalHits.value));

		List<Document> documents = CollectionFactory.createList();

		if(limit > hits.length)
			limit = hits.length;

		int index = -1;
		for(ScoreDoc hit : hits) {
			index++;
			if(index < offset)
				continue;
			if(index > limit)
				break;

			try {
				Document doc = getSearcher().doc(hit.doc);
				documents.add(doc);
			} catch(AlreadyClosedException e) {
				if(firstTry) {
					closeSearcher();
					return search(query, offset, limit, sort, false);
				}
				LogUtil.logException(e);
			} catch(IOException e) {
				LogUtil.logException(e);
			}
		}

		results.put("documents", documents);
		return results;
	}

	public Facets countFacets(Query query) throws IOException {
		FacetsConfig facetsConfig = getFacetsConfig();

		FacetsCollector facetsCollector = new FacetsCollector();
		int limit = Integer.MAX_VALUE;
		FacetsCollector.search(getSearcher(), query, limit, facetsCollector);

		TaxonomyReader taxoReader = getTaxoReader();
		try {
			Facets facets = new FastTaxonomyFacetCounts(taxoReader, facetsConfig, facetsCollector);

			if(facets.getAllDims(limit).size() > 0)
				return facets;
		} catch(AlreadyClosedException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int deleteDocuments(Query query) throws IOException {
		return deleteDocuments(query, true);
	}
	private static int deleteDocuments(Query query, boolean firstTry) throws IOException {
		int hits = (int)getInstance().search(query).get(TOTAL_HITS);
		try {
			getInstance().getWriter().deleteDocuments(query);
			commit();
		} catch(AlreadyClosedException e) {
			if(firstTry) {
				getInstance().closeWriter();
				return deleteDocuments(query, false);
			}

			throw e;
		}
		return hits;
	}

	private static void commit() {
		commit(true);
	}
	private static void commit(boolean commitIfNull) {
		try {
			if(writer != null || commitIfNull)
				getInstance().getWriter().commit();

			if(taxoWriter != null || commitIfNull)
				getInstance().getTaxoWriter().commit();

		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	//add document
	public void addDocument(Document luceneDoc) throws IndexManagerException, IOException {
		if(isWriting() || IndexUtil.isStopping()) {
			getWriter().addDocument(luceneDoc);
			commit();
			updateSearcher = true;
		} else
			throw new IndexManagerException("the method startWriting() wasn't called");
	}

	public boolean startWriting() {
		if(!writing) {
			writing = true;
			return true;
		}
		return false;
	}

	public void finishWriting() {
		if(writing) {
			writing = false;
			if(getWriter() != null)
				commit();
		}
	}

	public void finishWritingAndClose() {
		finishWriting();
		destroy();
	}

	public void startSearching() {
		searching = true;
	}

	public void finishSearching() {
		searching = false;
	}

	public boolean isWriting() {
		return writing;
	}

	public boolean isSearching() {
		return searching;
	}

	public static void destroy() {
		if(indexManager != null) {
			indexManager.closeSearcher();
			indexManager.closeWriter();
			indexManager = null;
		}
	}

	public static boolean isBusy() {
		return indexManager != null && (indexManager.isSearching() || indexManager.isWriting());
	}

	public boolean needsUpdate() {
		return updateSearcher;
	}

	public static Analyzer getAnalyzer() {
		return new CaseInsensitiveWhitespaceAnalyzer();
	}

	public Document buildLuceneFacets(Document doc) {
		try {
			doc = getFacetsConfig().build(getTaxoWriter(), doc);
		} catch(IOException e) {
			e.printStackTrace();
		}
		return doc;
	}

	public static void reset() {
		System.out.println("resetting");
		getInstance().finishWritingAndClose();
		int i = 0;
		//wait up to 5 seconds for the indexing to stop
		while(getInstance().needsUpdate() && i < 10) {
			try {
				Thread.sleep(500);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			i++;
		}
		Article.deleteAll();
		getInstance().openTaxoWriter(directory, true, false);
		getInstance().finishWritingAndClose();
	}
}
