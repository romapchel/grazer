package nl.impressie.grazer.util.crawlerUtils;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;

/**
 * @author Impressie
 */
public final class CaseInsensitiveWhitespaceAnalyzer extends Analyzer {
//    lucene 3.6
//    @Override
//    public final TokenStream tokenStream(String fieldName, Reader reader)
//    {
//        //todo: remove punctuation from the analyser
//        Tokenizer tokenizer = new WhitespaceTokenizer(Version.LUCENE_36, reader);
//        TokenStream filter = new LowerCaseFilter(Version.LUCENE_36, tokenizer);
//        return filter;
//    }
//    Lucene 4.0
//    @Override
//    protected TokenStreamComponents createComponents(String field, Reader reader)
//    {
//        Tokenizer tokenizer = new WhitespaceTokenizer(Version.LUCENE_40, reader);
//        TokenStream filter = new LowerCaseFilter(Version.LUCENE_40, tokenizer);
//        return new TokenStreamComponents(tokenizer, filter);
//    }

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {
		Tokenizer source = new WhitespaceTokenizer();
		TokenStream filter = new LowerCaseFilter(source);
		return new TokenStreamComponents(source, filter);
	}
}