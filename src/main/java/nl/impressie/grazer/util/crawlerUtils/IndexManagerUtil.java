package nl.impressie.grazer.util.crawlerUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author Impressie
 */
public class IndexManagerUtil implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		IndexManager.getInstance();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		IndexManager.getInstance().finishWritingAndClose();
	}
}
