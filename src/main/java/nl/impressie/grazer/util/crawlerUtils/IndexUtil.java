package nl.impressie.grazer.util.crawlerUtils;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptJobManager;
import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.dao.UnknownTagDao;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.exception.DateFormatException;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.Crawler.SeReDe;
import nl.impressie.grazer.model.DB.*;
import nl.impressie.grazer.model.KeyValue;
import nl.impressie.grazer.model.LastIndex;
import nl.impressie.grazer.model.Mail.UpdateMail;
import nl.impressie.grazer.threads.IndexTask;
import nl.impressie.grazer.util.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.pmw.tinylog.Level;
import org.springframework.util.StringUtils;

import javax.net.ssl.SSLHandshakeException;
import java.io.*;
import java.math.BigDecimal;
import java.net.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;

/**
 * @author Impressie
 */
public class IndexUtil {
	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36";//"Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";
	public static WebClient jsClient;
	private static boolean stopping = false;
	private static long startTime;
	private static int newDateForPage;
	private static Map visitedURLs;
	private static Map cookieMap;
	private static boolean updateOnly;

	public static int GLOBAL_MAX_DEPTH = 0;

	public static UpdateMail mail;
	private static Map mistakeMap;

	public static long getStartTime() {
		return startTime;
	}

	public static void setStartTime(long StartTime) {
		startTime = StartTime;
	}

	public static boolean isStopping() {
		return stopping;
	}

	public static void stopStopping() {
		stopping = false;
	}

	public static void removeMail() {
		mail = null;
	}

	public static void sendMail() {
		if(mail != null) {
			mail.send();
			removeMail();
		}
	}

	public static void addToMail(String name, int before, int after) {
		if(mail == null)
			mail = new UpdateMail();
		mail.addSite(name, before, after);
	}

	public static void addToMail(String message, Exception exception) {
		if(mail == null)
			mail = new UpdateMail();
		mail.addException(message, exception);
	}

	public static boolean isNew(String url, Map visitedURLs) {
		if(visitedURLs == null)
			return true;

		//Todo: add check that compares the depth the website was found at, if the depth was higher than the depth of the current find it should be replaced, maybe?
		return !visitedURLs.containsKey(url);
	}

	public static String getRedirectedUrl(String url) {
		Connection.Response res = getJsoupResponse(url, Connection.Method.GET, CollectionFactory.createMap(), 0);

		if(res != null)
			return res.url().toString();

		return url;
	}

	public static Document getJsoupDoc(String url) {
		return getJsoupDoc(url, Connection.Method.GET, CollectionFactory.createMap());
	}

	public static Document getJsoupDoc(String url, Connection.Method method, Map data) {
		return getJsoupDoc(url, method, data, 0);
	}

	public static Document getJsoupDoc(String url, Connection.Method method, Map data, int runs) {
		Document doc = null;
		Connection.Response res = getJsoupResponse(url, method, data, runs);
		if(null != res) {
			String contentType = res.contentType();
			if(!(contentType.startsWith("text") || contentType.contains("json") || contentType.contains("rss") || contentType.endsWith("xml"))) {
				if(!(contentType.startsWith("image") || contentType.endsWith("pdf") || contentType.endsWith("pdf;charset=UTF-8") || contentType.endsWith("x-icon")))
					LogUtil.log(String.format("Unhandled contentype: %s", contentType));
				return null;
			}
			try {
				byte[] bytes = res.bodyAsBytes();
				String encoding = UrlUtil.getEncoding(bytes);

				if(encoding.equals("UTF-8"))
					doc = res.parse();
				else
					doc = Jsoup.parse(new String(bytes, encoding));

				updateCookies(res);

				Thread.sleep(100);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return doc;

	}

	public static void updateCookies(Connection.Response res) {
		String host = res.url().getHost();
		Map cookies = CollectionFactory.createMap();

		if(cookieMap == null)
			cookieMap = CollectionFactory.createMap();

		if(cookieMap.containsKey(host)) {
			cookies = (Map) cookieMap.get(host);
			cookieMap.remove(host);
		}
		cookies.putAll(res.cookies());
		cookieMap.put(host, cookies);
	}

	public static Connection.Response getJsoupResponse(String url, Connection.Method method, Map data, int runs) {
		return getJsoupResponse(url, method, data, CollectionFactory.createMap(), runs);
	}

	public static Connection.Response getJsoupResponse(String url, Connection.Method method, Map data, Map headers, int runs) {
		int timeoutInSeconds = 300;
		int maxRuns = 5;

		try {
			if(!url.contains("www.tweedekamer.nl"))
				url = java.net.URLDecoder.decode(url, "UTF-8");

			if(!url.startsWith("https://www.linklaters.com"))
				url = url.replaceAll("\\s", "+");

			if(url.contains("www.acm.nl"))
				url = url.replace("=so:", "=so%3A");
		} catch(UnsupportedEncodingException e) {
			LogUtil.logException(e);
		}

		if(data.size() == 0 && Connection.Method.POST.equals(method)) {
			Map map = parseData(url);
			data = ((Map) map.get("data"));
			url = (String) map.get("url");
		}

		try {
			URL tempUrl = new URL(url);
			String host = tempUrl.getHost();

			String altHost = host;
			String worldWideWebPrefix = "www.";
			if(host.startsWith(worldWideWebPrefix))
				altHost = host.substring(worldWideWebPrefix.length());

			Map cookies = CollectionFactory.createMap();
			if(cookieMap == null)
				cookieMap = CollectionFactory.createMap();

			if(cookieMap.containsKey(host))
				cookies = (Map) cookieMap.get(host);
			else if(cookieMap.containsKey(altHost))
				cookies = (Map) cookieMap.get(altHost);

			Connection con = Jsoup.connect(url)
					.timeout(timeoutInSeconds * 1000)
					.ignoreContentType(true)
					.followRedirects(true)
					.method(method)
					.validateTLSCertificates(false);

			if(data.size() > 0)
				con.data(data);

			if(cookies.size() > 0)
				con.cookies(cookies);

			if(headers.size() > 0)
				con.headers(headers);

			if(!headers.containsKey("referrer"))
				con.referrer("http://www.google.com");

			if(!headers.containsKey("User-Agent"))
				con.userAgent(USER_AGENT);

			return con.execute();

		} catch(SocketTimeoutException e) {
			if(runs < maxRuns)
				return getJsoupResponse(url, method, data, headers, ++runs);
			LogUtil.logException(e);
		} catch(HttpStatusException e) {
			int statusCode = e.getStatusCode();
			try {
				if(statusCode == 429 || statusCode == 403)
					Thread.sleep(1000);
			} catch(InterruptedException e1) {
				LogUtil.logException(e);
			}

			if(statusCode != 404) {
				if(runs < maxRuns)
					return getJsoupResponse(url, method, data, headers, ++runs);
			}
			LogUtil.log(String.format("httperror (%d) at: %s", statusCode, url));
		} catch(UnknownHostException | MalformedURLException e) {
			if(runs < maxRuns && !url.startsWith("http"))
				return getJsoupResponse("http://" + url, method, data, headers, ++runs);
			else
				LogUtil.logException(String.format("Unknown host in url [%s]", url), e);

		} catch(SSLHandshakeException | ConnectException e) {
			LogUtil.logException(e);
		} catch(IOException e) {
			System.out.println(url);
			LogUtil.logException(e);
		} catch(Exception e) {
			LogUtil.logException(e);
		}

		return null;
	}

	public static Map parseData(String url) {
		String trueUrl = url;

		Map data = CollectionFactory.createMap();
		int questionMark = url.indexOf("?");

		if(questionMark > -1) {
			trueUrl = url.substring(0, questionMark);
			String fullDataString = url.substring(questionMark + 1);
			for(String dataString : fullDataString.split("&")) {
				String[] parts = dataString.split("=");
				if(parts.length == 2)
					data.put(parts[0], parts[1]);

			}
		}

		Map returnData = CollectionFactory.createMap();
		returnData.put("data", data);
		returnData.put("url", trueUrl);
		return returnData;
	}

	public static String regexSearch(String patternString, String text) {
		Matcher matcher = RegexUtil.getCaseInsensitiveMatcher(patternString, text);

		String rechtsgebiedenString = "";
		while(matcher != null && matcher.find()) {
			if(rechtsgebiedenString.length() > 0) {
				rechtsgebiedenString += ", ";
			}
			rechtsgebiedenString += matcher.group(0);
		}
		return rechtsgebiedenString;
	}

	public static String StringListToString(List<String> strings, String seperator) {
		StringBuilder sb = new StringBuilder();
		for(String string : strings) {
			if(sb.toString().length() > 0)
				sb.append(seperator);
			sb.append(string);
		}
		return sb.toString();
	}

	public static Article indexMetaData(Article Art, Website page, Elements HTMLDoc) {
		String selector = page.getSnippet().getSelector();
		String prop = page.getSnippet().getProperty();

		//System.out.println("Snippet selector: " + selector);
		if(selector.length() > 0) {
			Elements snippetElements = HTMLDoc.select(selector);
			String snippet;
			snippet = StringListToString(getTextContent(snippetElements, prop), " ");
			snippet = snippet.replaceAll("\\s+", " ");

			//System.out.println("Created Snippet: " + snippet);

			if(snippet.length() > Article.SNIPPETMIN) {
				if(snippet.length() < Article.SNIPPETMAX)
					Art.setSnippet(snippet);
				else
					Art.setSnippet(snippet.substring(0, Article.SNIPPETMAX));

			} else {
				Art.makeSnippet();
			}
		}

//		SeReDeTags(page.getRechtsgebied(), Tag.RECHTSGEBIED, Art, HTMLDoc, page.getReplacements());
		SeReDeTags(page.getNieuwssoort(), Tag.NIEUWSSOORT, Art, HTMLDoc);
		SeReDeTags(page.getRubrieken(), Tag.RUBRIEK, Art, HTMLDoc);

		Collection<Tag> tempTags = page.getTempTags();
		for(Tag temptag : tempTags) {
			Art.addTag(temptag);
		}

		Art.update();

		return Art;
	}

	public static void SeReDeTags(SeReDe serede, String tagType, Article article, Elements HTMLDoc) {
		SeReDeTags(serede, tagType, article, HTMLDoc, CollectionFactory.createMap());
	}
	public static void SeReDeTags(SeReDe serede, String tagType, Article article, Elements HTMLDoc, Map<String, String> translations) {
		String selector = serede.getSelector();
		String prop = serede.getProperty();
		String regex = serede.getRegex();
		if(selector.length() > 0) {
			//remove any current tag, mostly there for seperated info page, this breaks the possibility of adding tags from the link page though, which I believe is more common and harder to prevent, so I'm removing this
			//article.removeAllTags(tagType);

			String tagsString;
			Elements elements = HTMLDoc.select(selector);
			if(regex.length() > 0) {
				Matcher matcher = RegexUtil.getCaseInsensitiveMatcher(regex, elements.text());

				StringBuilder tagsStringBuilder = new StringBuilder();
				while(matcher != null && matcher.find()) {
					if(tagsStringBuilder.length() > 0) {
						tagsStringBuilder.append(Article.SEPERATOR);
					}
					tagsStringBuilder.append(matcher.group(0));
				}
				tagsString = tagsStringBuilder.toString();
			} else {
				tagsString = StringListToString(getTextContent(elements, prop), ", ");
			}
			tagStringToAddedTags(article, tagType, tagsString, translations);
		}
		if(article.getTags(tagType).size() == 0) {
			//Add tags using the regex
			String tagsString = regexSearch(regex, HTMLDoc.text());
			tagStringToAddedTags(article, tagType, tagsString, translations);
		}
		for(String tagName : serede.getDefaultValue().split(Article.SEPERATOR)) {
			Tag tag = TagDao.findByName(tagName.trim(), tagType);
			if(tag != null)
				article.addTag(tag);

		}
	}

	public static void tagStringToAddedTags(Article article, String tagType, String tagString, Map<String, String> translations) {
		List<Tag> tags = CollectionFactory.createList();
		for(String tagName : tagString.split(",")) {
			String compareTag = tagName.toLowerCase().trim();
			if(translations.containsKey(compareTag))
				tagName = translations.get(compareTag);

			int minimumTagCharacterLength = 2;
			int maximumTagWordCount = 10;
			if(tagName.length() > minimumTagCharacterLength) {
				String[] wordsInTag = tagName.split(" ");
				if(wordsInTag.length > maximumTagWordCount) {
					TagDao.deleteTags(tags);
					tags = CollectionFactory.createList();
					break;
				}
//				Tag tag = TagDao.create(tagType, tagName);
				Tag tag = TagDao.findByName(tagName, tagType);
				if(tag == null)
					UnknownTagDao.create(tagType, tagName, article.getWebsiteUrl());
				else
					tags.add(tag);
			}
		}
		if(tags.size() > 0)
			article.addTags(tags);
	}

	public static String fixURL(String url, URL parentUrl) {
		url = fixStupidMistakes(url);

		if("#".equals(url))
			return null;

		if(url.startsWith("ibp.jsp?"))
			url = "/" + url;

		if(url.startsWith("<"))
			return null;

		String protocol = parentUrl.getProtocol();
		String host = parentUrl.getHost();

		String path = parentUrl.getPath();
		int lastSlash = path.lastIndexOf("/");
		String parentPath = "";
		if(lastSlash != -1)
			parentPath = path.substring(0, lastSlash);

		if(lastSlash != -1 && !(url.startsWith("?") || url.startsWith("/") || url.startsWith("http") || url.startsWith("wwww")))
			url = String.format("%s/%s", parentPath, url);

		if(url.startsWith("?"))
			url = String.format("%s%s", parentUrl.getPath(), url);

		//Add the domain to relativa links
		if(url.matches("^\\/[a-zA-Z0-9].*"))
			url = String.format("%s://%s%s", protocol, host, url);

		while(url.matches("^..\\/[a-zA-Z0-9].*")) {
			url = url.substring(2);
			if(host.endsWith("/"))
				host = host.substring(0, host.length() - 1);
			int ind = host.lastIndexOf("/");
			if(-1 < ind)
				host = host.substring(0, ind);
		}

		if(url.matches("^.\\/][a-zA-Z0-9].*"))
			url = String.format("%s://%s%s", protocol, host, url.substring(1));

		String redirectedUrl = getRedirectedUrl(url);

		int redirectUrlParameters = redirectedUrl.indexOf("?");
		if(redirectUrlParameters != -1) {
			String redirectedUrlWithoutParams = redirectedUrl.substring(0, redirectUrlParameters);
			if(url.equals(redirectedUrlWithoutParams))
				redirectedUrl = url;
		}

		https:
//kvdl.nl/mens-maatschappij/over-kennedy-van-der-laan/

		return redirectedUrl;
	}

	public static List addAllNew(List addThese, List toThisList) {
		if(toThisList == null)
			toThisList = CollectionFactory.createList();

		for(Object object : addThese) {
			if(!toThisList.contains(object))
				toThisList.add(object);
		}
		return toThisList;
	}

	public static List evaluateJsonObject(List items, Object jsonObject, String currentKey, String key, boolean searchAll) {
		if(currentKey.toLowerCase().equals(key.toLowerCase())) {
			if(!(jsonObject instanceof JSONArray)) {
				items.add(jsonObject.toString());
				return items;
			}
		} else if(!searchAll) {
			return items;
		}

		if(jsonObject instanceof String) {
			String jsonString = (String) jsonObject;

			if(jsonString.startsWith("{"))
				jsonObject = new JSONObject(jsonString);
			else if(jsonString.startsWith("["))
				jsonObject = new JSONArray(jsonString);
		}

		if(jsonObject instanceof JSONObject) {
			JSONObject newObject = (JSONObject) jsonObject;
			List foundItems = findJson(newObject, key, searchAll);
			if(foundItems.size() > 0) {
				addAllNew(foundItems, items);
			}
		} else if(jsonObject instanceof JSONArray) {
			JSONArray array = (JSONArray) jsonObject;
			for(int index = 0, max = array.length(); index < max; index++) {
				if(!IndexManager.getInstance().isWriting())
					break;
				Object arrayObject = array.get(index);
				items = evaluateJsonObject(items, arrayObject, currentKey, key, searchAll);
			}
		}
		return items;
	}

	public static List findJson(JSONObject object, String key, boolean searchAll) {
		List items = CollectionFactory.createList();
		Iterator<?> keys = object.keys();
		while(keys.hasNext() && IndexManager.getInstance().isWriting()) {
			String currentKey = (String) keys.next();
			Object currentItem = object.get(currentKey);
			List found = evaluateJsonObject(items, currentItem, currentKey, key, searchAll);
			addAllNew(found, items);
		}
		return items;
	}

	public static Set getUrlsFromJson(String url, Website page) {
		Set<String> tempUrls = CollectionFactory.createSet();

		PageSearch search = page.getPageSearch();
		Connection.Method method = search.getMethod();
		Map headers = CollectionFactory.createMap();
		Map data = CollectionFactory.createMap();
		if(search != null) {
			method = search.getMethod();
			headers = search.getHeaders();
			data = search.getData();
		}

		Connection.Response res = getJsoupResponse(url, method, data, headers, 0);
		if(res == null)
			return tempUrls;

		if(page.needsReload(res)) {
			res = getJsoupResponse(url, method, data, headers, 0);
			if(res == null)
				return tempUrls;
		}

		String jsonString = res.body();
		if(!jsonString.startsWith("{"))
			return tempUrls;//incase it turns out the page does not return json

		if(jsonString.length() > 2) {
			SeReDe crawl = page.getCrawl();
			String crawlSelector = crawl.getSelector();

			List<String> jsonStrings = CollectionFactory.createList();
			jsonStrings.add(jsonString);
			String[] keys = crawlSelector.split(">");
			boolean searchAll = keys.length == 1;

			for(String sk : keys) {
				if(jsonStrings.size() == 0)
					break;
				List<String> found = CollectionFactory.createList();
				for(String json : jsonStrings) {
					JSONObject jo = new JSONObject(json);
					found.addAll(findJson(jo, sk.trim(), searchAll));
				}
				jsonStrings = found;
			}
			List<String> urls = jsonStrings;
//                List<String> urls = findJson(jsonObject, crawlSelector);

			JSONObject jsonObject = new JSONObject(jsonString);
			if(urls.size() == 0)
				urls = findJson(jsonObject, "url", true);

			URL parentUrl = null;
			try {
				parentUrl = new URL(url);
			} catch(MalformedURLException e) {
				e.printStackTrace();
				return tempUrls;
			}

			for(String urlString : urls) {
				boolean added = false;
				urlString = crawl.runRegex(urlString);

				for(String tempUrlString : urlString.split(SeReDe.SEPERATOR)) {
					if(tempUrlString.length() == 0)
						continue;
					try {
						tempUrlString = fixURL(tempUrlString, parentUrl);
						new URL(tempUrlString);
						tempUrls.add(tempUrlString);
						added = true;
					} catch(MalformedURLException e) {
						System.out.println(String.format("error at: [%s]", tempUrlString));
						e.printStackTrace();
						continue;
					}
				}
				if(!added && urlString.length() > 0)
					tempUrls.add(urlString);
			}
		}
		return tempUrls;
	}

	public static HtmlPage clickButton(WebClient webClient, HtmlPage HtmlUnitPage, Button HtmlButton) {
		// check if an element matching the testId exists
		try {
			String testId = HtmlButton.getTestId();
			if(testId.length() > 0)
				HtmlUnitPage.getHtmlElementById(testId);

		} catch(ElementNotFoundException ex) {
			return null;
		}

		//if the button exists use JS to click on it
		String jsCode = String.format("var button = document.querySelector('%1$s'); button.click(); window.setTimeout(function(console.log('bla');), 1000);", HtmlButton.getSelector());
		jsCode = String.format("var button = document.querySelector('%1$s'); if(button != null) { button.click(); window.setTimeout(function(console.log('bla');), 10); }", HtmlButton.getSelector());
		ScriptResult result = HtmlUnitPage.executeJavaScript(jsCode);

		HtmlPage page = (HtmlPage)webClient.getCurrentWindow().getEnclosedPage();
		page = waitForJs(page);
		return page;
	}

	//max timeout of 10 seconds per page seems like it would be plenty
	private static HtmlPage waitForJs(HtmlPage page) {
		return waitForJs(page, 10);
	}

	private static HtmlPage waitForJs(HtmlPage page, int maximumWaitInSeconds) {

		JavaScriptJobManager manager = page.getEnclosingWindow().getJobManager();
		final int waitTimePerLoop = 1000;
		int timeSpent = 0;
		final int maxTime = maximumWaitInSeconds * 1000;
		final boolean maximumWaitDisabled = 0 == maximumWaitInSeconds;

		while(manager.getJobCount() > 0 && (maximumWaitDisabled || timeSpent < maxTime)) {
			try {
				Thread.sleep(waitTimePerLoop);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			timeSpent += waitTimePerLoop;
		}

		return page;
	}

	public static double nanoToMinutes(double nanoSeconds) {
		double timePassedInMinutes = nanoSeconds / 60000000000.0;
		BigDecimal roundable = new BigDecimal(timePassedInMinutes);
		BigDecimal rounded = roundable.setScale(2, BigDecimal.ROUND_HALF_UP);
		return rounded.doubleValue();
	}

	public static WebClient setupClient() {
		Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);

//		WebClient webClient = new WebClient(BrowserVersion.FIREFOX_52);
		WebClient webClient = new WebClient(BrowserVersion.FIREFOX_78);
		webClient.setAjaxController(new NicelyResynchronizingAjaxController());

		WebClientOptions options = webClient.getOptions();
		options.setThrowExceptionOnScriptError(false);
		options.setPrintContentOnFailingStatusCode(false);
		options.setUseInsecureSSL(true);

		return webClient;
	}

	public static Document loadAndRunJS(String url, JavaScriptObject js) {
		jsClient = setupClient();
		Document doc = null;
		try {
			WebRequest request = new WebRequest(new URL(url));
			HtmlPage page = jsClient.getPage(request);
			String testId = js.getTestId();
			boolean runJS = false;
			if(testId.length() > 0) {
				try {
					WebAssert.assertElementPresent(page, testId);
					runJS = true;
				} catch(AssertionError e) {
				}
			}
			if(runJS) {
				ScriptResult result = page.executeJavaScript(js.getJavascript());
				page = (HtmlPage) jsClient.getCurrentWindow().getEnclosedPage();
				page = waitForJs(page);
			}
			doc = Jsoup.parse(page.asXml());
		} catch(FailingHttpStatusCodeException e) {
			if(e.getStatusCode() != 404)
				e.printStackTrace();
		} catch(Error e) {
			//if(e instanceof NoClassDefFoundError)
				LogUtil.logException(new Exception(String.format("something went wrong with ", url), e));
			LogUtil.logError(e);
		} catch(Exception e) {
			LogUtil.logException(new Exception(String.format("something went wrong with ", url), e));
			//LogUtil.logException(e);
		} finally {
			jsClient.close();
		}
		return doc;
	}

	public static HtmlPage urlToJSPage(WebClient webClient, String url) {
		try {
			HtmlPage HTMLUnitWebsite = webClient.getPage(url);
			webClient.waitForBackgroundJavaScriptStartingBefore(10000);
			try {
				Thread.sleep(5000);
			} catch(InterruptedException e) {
				LogUtil.logException(e);
			}
			return HTMLUnitWebsite;
		} catch(MalformedURLException e) {
			LogUtil.logException(e);
		} catch(IOException e) {
			LogUtil.logException(e);
		} catch(NoSuchFieldError e) {
		}

		return null;
	}

	public static List<String> getTextContent(Element elements, String property) {
		return getTextContent(new Elements(elements), property);
	}

	public static List<String> getTextContent(Elements elements, String property) {
		List contents = CollectionFactory.createList();
		for(Element element : elements) {
			String text = "";
			if(element.tagName().equals("meta"))
				text = element.attr("content");
			else if(element.tagName().equals("time") && element.hasAttr("datetime"))
				text = element.attr("datetime");

			if(0 < property.length()) {
				for(String prop : property.split(",")) {
					if(0 == text.length())
						text = element.attr(prop.trim());
				}
			}

			if(text.length() <= 0)
				text = element.text();

			//replace nbsp with a normal space
			text = text.replaceAll(" ", " ").trim();

			if(text.length() > 0)
				contents.add(text);
		}
		// System.out.println("ContentsDocument: " + contents);
		return contents;
	}

	public static Map populatedVisitedSites(Website target) {
		Map visitedURLs = CollectionFactory.createMap();
		List<Article> articles = target.getAllArticles();
		for(Article article : articles) {
			visitedURLs.put(article.getUrl(), article.getDepth());
		}

		return visitedURLs;
	}

	public static void updateDate(String Url, String date) {
		//todo:change so it works first time looking, even if the url is not present yet
		Website website = WebsiteDao.findByUrl(Url);

		if(website == null)
			return;

		Map map = CollectionFactory.createMap();
		map.put(WebsiteDao.LAST_CRAWL_DATE, date);

		website.update(map);
	}

	public static boolean shouldStopIndexing() {
		return ((!IndexManager.getInstance().isWriting()) || IndexUtil.isStopping());
	}

	public static Thread startIndexThread(String mode, String path, boolean updateOnly, int globalMaxDepth) {
		List<Website> targets = WebsiteDao.getAll();
		return startIndexThread(targets, mode, path, updateOnly, globalMaxDepth);
	}
	public static Thread startIndexThread(List<Website> targets, String mode, String path, boolean updateOnly, int globalMaxDepth) {
		IndexManager.getInstance().startWriting();

		Thread indexThread = new Thread(new IndexTask(targets, mode, updateOnly, globalMaxDepth));

		IndexUtil.setStartTime(System.nanoTime());
		IndexUtil.setGlobalMaxDepth(globalMaxDepth);

		indexThread.start();

		return indexThread;
	}

	public static Map getVisitedURLs() {
		return visitedURLs;
	}

	public static int getNewDateForPage() {
		return newDateForPage;
	}

	public static void indexPages(List<Website> targets, String mode, Long startTime, boolean updateOnly, int globalMaxDepth) {
		IndexUtil.updateOnly = updateOnly;
		IndexUtil.GLOBAL_MAX_DEPTH = globalMaxDepth;
		IndexManager manager = IndexManager.getInstance();
		for(Website target : targets) {
			LogUtil.log(String.format("indexing: %s (%s)", target.getName(), target.getUrl().toString()), Level.TRACE);
			visitedURLs = null;
			cookieMap = null;
			if(target == null)
				continue;

			boolean keepGoing = true;

			try {
				URL url = target.getUrl();
				RobotUtil.load(String.format("%s://%s", url.getProtocol(), url.getHost()));

				newDateForPage = DateUtil.getCurrentDateAsInt();

				keepGoing = target.index();
			} catch(Exception e) {
				LogUtil.logException(e);
			}

			if(!keepGoing)
				break;
		}
		if(manager.isWriting())
			manager.finishWriting();
		System.out.println("All the targets have been indexed");

		if(mode.equals("default"))
			IndexUtil.sendMail();
		IndexUtil.stopStopping();
		if(getStartTime() != 0) {
			double elapsedMinutes = getElapsedMinutes();
			System.out.println("This process took: " + elapsedMinutes + " minutes");
			setStartTime((long) 0);
		}

		IndexManager.destroy();
	}

	public static Double getElapsedMinutes() {
		long elapsedTime = System.nanoTime() - startTime;
		double elapsedMinutes = IndexUtil.nanoToMinutes(elapsedTime);
		return elapsedMinutes;
	}

	public static Elements getElements(Elements doc, String selector) {
		return getElements(doc, null, null, selector);
	}

	public static Elements getElements(Elements doc, HashMap<String, Element> before, String key, String selector) {
		Elements elements = new Elements();
		if(null != before && before.containsKey(key)) {
			elements.add(before.get(key));
		} else if(selector.length() > 0) {
			Elements els = null;
			String prevSelector = ":prev(";
			while(selector.contains(prevSelector)) {
				int startPrevSelector = selector.indexOf(prevSelector);
				int endPrevSelector = selector.indexOf(")", startPrevSelector);

				String temp = selector.substring(startPrevSelector, endPrevSelector + 1);
				while(StringUtils.countOccurrencesOf(temp, "(") > StringUtils.countOccurrencesOf(temp, ")")) {
					int newEndPrevSelector = selector.indexOf(")", endPrevSelector);
					if(newEndPrevSelector == -1)
						break;

					endPrevSelector = newEndPrevSelector;
					temp = selector.substring(startPrevSelector, endPrevSelector + 1);
				}

				String b = selector.substring(0, startPrevSelector);
				String p = selector.substring(startPrevSelector + prevSelector.length(), endPrevSelector);
				selector = selector.substring(endPrevSelector + 1);

				if(els == null)
					els = doc.select(b);
				else
					els = els.select(b);

				els = els.prev(p);
			}

			if(selector.length() > 0) {
				if(null == els)
					els = doc;

				try {
					if(els.size() == 1 && els.get(0) == null) {
						System.out.println("only null elements");
					} else {
						if(!els.isEmpty())
							elements = els.select(selector);
					}
				} catch(IllegalArgumentException e) {
					e.printStackTrace();
				}
			} else if(els != null) {
				elements = els;
			}
		}

		return elements;
	}

	public static Elements customSelect(Elements element, String selector) {
		if(!selector.contains(":text("))
			return element.select(selector);

		for(String select : selector.split("[\\s>+~]+")) {
			if(select.trim().length() == 0)
				continue;

			if(!select.contains(":text")) {
				element = element.select(select);
				continue;
			}

			int textIndex = select.indexOf(":text");

			String preSelector = select.substring(0, textIndex);
			String text = select.substring(textIndex + ":text(".length(), select.length() - 1);

			if(preSelector.trim().length() > 0 && text.trim().length() > 0) {
				List matchingElements = CollectionFactory.createList();
				for(Element el : element.select(preSelector)) {
					if(el.text().contains(text)) {
						matchingElements.add(el);
					}
				}
				element = new Elements(matchingElements);
			} else {
				try {
					throw new Exception(String.format("'%s' is not a valid selector", selector));
				} catch(Exception e) {
					LogUtil.logException(e);
				}
			}


//			String prevSelector = ":prev(";
//			String textSelector = ":text(";
//
//			int previous = select.indexOf(prevSelector);
//			int text = select.indexOf(textSelector);
//
//			int first = text;
//			if(previous != -1 && previous < text)
//				first = previous;
//
//			if(first == -1) {
//				element = element.select(select);
//				continue;
//			}
//
//			String custom = select.substring(first);
//			String normal = select.substring(0, first);
//			if(custom.startsWith(prevSelector))
//				element = element.select(normal).prev(custom.substring(prevSelector.length(), select.length() - 1));
//
//			if(previous > first) ;

		}
		return element;
	}

	public static List<Article> createArticles(List<Map> articleMaps, URL url, Website page) {
		String urlString = url.toString();
		List articles = CollectionFactory.createList();

		int articleIndex = 0;
		for(Map articleMap : articleMaps) {
			String currentUrl = urlString;
			if(articleIndex > 0)
				currentUrl = String.format("%s#%d", currentUrl, articleIndex);

			articleIndex++;

			String content = (String) articleMap.get(WebsiteDao.CONTENT);
			// System.out.println("ArticleContents: " + content);
			if(content == null || 0 == content.trim().length())
				continue;

			String title = (String) articleMap.get(WebsiteDao.TITLE);
			String date = (String) articleMap.get(WebsiteDao.DATE);
			String depth = (String) articleMap.get(WebsiteDao.MAX_DEPTH);

			Article article = Article.create(title, content, date, currentUrl, depth, page);

			String tag = (String) articleMap.get(WebsiteDao.RECHTSGEBIED);
			if(tag != null && tag.length() > 0) {
				for(String tagName: tag.split(",")) {
					Tag tagObject = TagDao.findByName(tagName, Tag.RECHTSGEBIED);

					if(tagObject == null)
						tagObject = TagDao.getChildTag(tagName);

					if(tagObject != null) {
						article.addTag(tagObject);
						// System.out.println("Tagobject: " + tagObject.toString());
					}
				}

				if(article.getTags(Tag.RECHTSGEBIED).size() == 0) {
					System.out.println(tag);
					article.delete();
					continue;
				}
			}

			Elements html = (Elements) articleMap.get("html");

			article = IndexUtil.indexMetaData(article, page, html);
			String moreInfo = page.getMoreInfo();
			if(moreInfo.length() > 0) {
				String moreInfoUrl = html.select(moreInfo).select("a[href]").attr("abs:href");
				moreInfoUrl = IndexUtil.fixURL(moreInfoUrl, url);
				if(moreInfoUrl.length() > 0) {
					html = IndexUtil.getJsoupDoc(moreInfoUrl).getAllElements();
					article = IndexUtil.indexMetaData(article, page, html);
				}
			}

			articles.add(article);
		}

		return articles;
	}

	public static Boolean indexContents(URL Url, int CurrentDepth, Website page, Element HTMLDoc, Map before) {
		return indexContents(Url, CurrentDepth, page, HTMLDoc, false, before);
	}

	public static Boolean indexContents(URL url, int currentDepth, Website page, Element HTMLDoc, boolean javascriptSite, Map before) {
		if(visitedURLs == null)
			visitedURLs = IndexUtil.populatedVisitedSites(page);

		if(HTMLDoc == null)
			return true;

		//add the URL to avoid duplicate content
		if(IndexUtil.isNew(url.toString(), visitedURLs))
			visitedURLs.put(url.toString(), String.valueOf(currentDepth));
		else if(!javascriptSite) //If the site utilises Javascript the URL doesn't always change even when the conten does
			return false;

		Elements linkElements = getLinkElements(page, HTMLDoc);

		List<URL> links = getLinks(page, HTMLDoc, currentDepth, url);

		List articleMaps = getArticles(url, HTMLDoc, page, before);

		if(url.equals("https://uitspraken.rechtspraak.nl/inziendocument?id=ECLI:NL:RBDHA:2020:14421&showbutton=true"))
			System.out.println("");

		List<Article> articles = createArticles(articleMaps, url, page);

		if(0 == articles.size() && 0 == linkElements.size())
			System.out.println(String.format("No content or links found on: %s", url.toString()));

		Map<String, List> beforeSelectors = CollectionFactory.createMap();
		for(BeforeSelector bs : page.getBeforeSelectors()) {
			bs.addFoundElementsToMap(beforeSelectors, HTMLDoc);
		}

//		if(links.size() > 0)
//			System.out.println(String.format("found %d links at %s", links.size(), url));

		int index = 0;
		for(URL link : links) {
			Map beforeLink = CollectionFactory.createMap();
			for(String key : beforeSelectors.keySet()) {
				List value = beforeSelectors.get(key);

				if(value.size() > index)
					beforeLink.put(key, new Elements((Element) value.get(index)));
				else if(value.size() == 1)
					beforeLink.put(key, new Elements((Element) value.get(0)));
			}
			index++;
			int newDepth = currentDepth + 1;

			if(!indexUrl(link, newDepth, page, beforeLink, Connection.Method.GET, CollectionFactory.createMap()))
				return false;
		}

		return true;
	}

	public static List getArticles(URL url, Element document, Website page, Map<String, Elements> before) {
		if(null == document)
			return CollectionFactory.createList();
		return getArticles(url, new Elements(document), page, before);
	}

	public static List getArticles(URL url, Elements document, Website page, Map<String, Elements> before) {
		List articles = CollectionFactory.createList();

		SeReDe articleSerede = page.getArticle();
		Elements articleElements = articleSerede.getElements(document, before.get(WebsiteDao.ARTICLE));

		List<Map> seredeList = page.getSeredeList();

		for(Element art : articleElements) {
			Map article = CollectionFactory.createMap();
			Elements artileElements = new Elements();
			if(null != art)
				artileElements = new Elements(art);

			article.put("html", artileElements);
			article.put("url", url);
			boolean addArticle = true;
			for(Map seredeMap : seredeList) {
				String title = (String) seredeMap.get("title");
				List<KeyValue> replacements = (List) seredeMap.get("replacements");
				SeReDe serede = (SeReDe) seredeMap.get("serede");

				String content = "";
				try {
					Elements elementsFromPreviousPage = before.get(title);
					content = serede.getContent(artileElements, elementsFromPreviousPage);
					// fix: get last element for rechtsgebied in case of ;
					if (title.equals("rechtsgebied")) {
						String[] rechtsgebiedArray = content.split(";");
						content = rechtsgebiedArray[rechtsgebiedArray.length - 1];
						content = content.trim();
					}
				} catch(Exception e) {
					e.printStackTrace();
				}

				Map<String, String> replaced = CollectionFactory.createMap();

				for(KeyValue<String, String> replacement : replacements) {
					String replace = replacement.getKey();
					String with = replacement.getValue();
					if(RegexUtil.getCaseInsensitiveMatcher(replace, content).find())
						replaced.put(replace, with);
				}
				if(replaced.size() > 0 && Tag.isValidtagType(title)) {
					StringBuilder replacedContent = new StringBuilder();
					for(String split: content.split(",")) {
						split = split.trim();
						if(replaced.containsKey(split))
							continue;

						Tag byName = TagDao.findByName(split, title);
						if(byName != null) {
							if(replacedContent.length() > 0)
								replacedContent.append(", ");

							replacedContent.append(byName.getName());
						}
					}
					for(String replace: replaced.values()) {
						if(replacedContent.length() > 0)
							replacedContent.append(", ");

						for(String tagName: replace.split(",")) {
							TagDao.create(title, tagName);
						}
						replacedContent.append(replace);
					}
					content = replacedContent.toString();
				}
				if(replacements.size() > 0 && content.length() == 0 && Tag.isValidtagType(title)) {
					addArticle = false;
					break;
				}
				try {
					if(title.equals(WebsiteDao.DATE)) {
						if(0 == content.length())
							content = "0";
						else {
							String regex = serede.getRegex();
							content = DateUtil.translateDate(content, regex, page.getDateFormat());
						}

						if(!content.equals("0"))
							content = DateUtil.convertDateToTimestamp(content, "yyyyMMdd").toString();
					}
				} catch(DateFormatException e) {
					LogUtil.logException(e);
					content = "0";
				}
				article.put(title, content);
			}
			if(addArticle)
				articles.add(article);
		}

		return articles;
	}

	public static Elements getLinkElements(Website page, Element HTMLDoc) {
		SeReDe crawl = page.getCrawl();
		String selector = crawl.getSelector();
		Elements links = new Elements();

		if(selector.length() > 0 && null != HTMLDoc) {
			Elements anchorLinks = new Elements();
			try {
				links = HTMLDoc.select(selector);
				anchorLinks = links.select("a[href]");
			} catch(IllegalArgumentException e) {
				e.printStackTrace();
			}
			if(anchorLinks.size() > 0)
				links.addAll(anchorLinks);
		}
		return links;
	}

	public static List getLinks(Website page, Element HTMLDoc, int currentDepth, URL url) {
		int maxDepth = page.getUsableMaxDepth();
		Set<String> linkUrls = CollectionFactory.createSet();
		Map<String, List> befores = CollectionFactory.createMap();

		if("https://uitspraken.rechtspraak.nl".equals(url.toString())) {
			int depth = currentDepth;
			final int infiniteLoopPrevention = 1000;
			int oldSize;
			boolean newUrlsFound, notInfiniteLoop;
			do {
				oldSize = linkUrls.size();
				linkUrls.addAll(rechtsprakenJuris(depth * 10));
				depth++;

				newUrlsFound = linkUrls.size() != oldSize;
				notInfiniteLoop = depth < infiniteLoopPrevention;
			} while(depth < maxDepth || (maxDepth == -1 && newUrlsFound && notInfiniteLoop));

		} else {
			SeReDe crawl = page.getCrawl();

			befores = page.getBeforeMap(HTMLDoc);

			Elements links = crawl.getElements(HTMLDoc, (Elements) befores.get(WebsiteDao.CRAWL));

			for(Element link : links) {
				//Don't index the page if max depth has been reached
				//if max-depth == -1, ignore it
				if(maxDepth == -1 || currentDepth < maxDepth) {
					String prop = crawl.getProperty();
					String linkUrl = "";
					for(String p : prop.split(",")) {
						p = p.trim();
						if(0 != p.length())
							linkUrl = link.attr(p);

						if(0 < linkUrl.length())
							break;
					}

					if(0 == linkUrl.length())
						linkUrl = link.attr("abs:href");
					if(linkUrl.length() == 0)
						linkUrl = link.attr("href");
					if(linkUrl.length() == 0)
						linkUrl = link.html();

					String reg = crawl.getRegex();
					if(0 != reg.length()) {
						Matcher matcher = RegexUtil.getMatcher(reg, linkUrl);
						if(matcher.find())
							linkUrl = matcher.group(0);
					}

					linkUrl = IndexUtil.fixURL(linkUrl, url);

					if(null != linkUrl)
						linkUrls.add(linkUrl);
				}
			}

			if(linkUrls.size() == 0)
				linkUrls = IndexUtil.getUrlsFromJson(url.toString(), page);
		}

		int index = 0;
		List testedUrls = CollectionFactory.createList();
		for(String linkUrl : linkUrls) {
			Map beforeLink = CollectionFactory.createMap();
			for(String key : befores.keySet()) {
				List value = befores.get(key);

				if(value.size() > index)
					beforeLink.put(key, new Elements((Element) value.get(index)));//todo: clean this mess up, constantly converting from Element objects to Elements can't be the cleanest way
			}
			index++;

			if(linkUrl.toLowerCase().startsWith("mailto"))
				continue;
			if(linkUrl.length() == 0)
				continue;
			if(!RobotUtil.isAllowed(linkUrl) && false) //gonna skip this because it has to be discussed
				continue;
			//don't index zip files
			if(linkUrl.endsWith(".zip"))
				continue;
			//don't index pdf files
			if(linkUrl.endsWith(".pdf"))
				continue;
			//don't index office files
			if(linkUrl.endsWith(".docx") || linkUrl.endsWith(".xlsx"))
				continue;
			//don't index javascript files
			if(linkUrl.endsWith(".js") || linkUrl.toLowerCase().startsWith("javascript:"))
				continue;
			//don't index css files
			if(linkUrl.endsWith(".css"))
				continue;
			if(!IndexUtil.isNew(linkUrl, visitedURLs))
				continue;

			try {
				linkUrl = fixURL(linkUrl, url);
				URL newUrl = new URL(linkUrl);
				testedUrls.add(newUrl);
			} catch(MalformedURLException e) {
				String message = String.format("MalformedURLException on %s: '%s' is not a valid URL", url.toString(), linkUrl);
				System.out.println(message);
			}
		}
		return testedUrls;
	}

	private static String fixStupidMistakes(String linkUrl) {
		Map<String, Integer> mistakeMap = getStupidMistakes();
		for(String stupidMistake : mistakeMap.keySet()) {
			if(linkUrl.startsWith(stupidMistake))
				linkUrl = linkUrl.substring(mistakeMap.get(stupidMistake));
		}

		return linkUrl;
	}

	private static Map getStupidMistakes() {
		if(mistakeMap == null) {
			mistakeMap = CollectionFactory.createMap();

			String[] stupidMistakesFoundOnWebsites = {"page/", "nieuws/"};
			String[] protocols = {"http://", "https://"};

			for(String stupidMistake : stupidMistakesFoundOnWebsites) {
				for(String protocol : protocols) {
					String mistake = String.format("%s%s", protocol, stupidMistake);
					if(!mistakeMap.containsKey(mistake))
						mistakeMap.put(mistake, protocol.length());
				}
			}
		}

		return mistakeMap;
	}

	public static Boolean indexUrl(URL url, int currentDepth, Website page) {
		return indexUrl(url, currentDepth, page, Connection.Method.GET, CollectionFactory.createMap());
	}

	public static Boolean indexUrl(URL url, int currentDepth, Website page, Connection.Method method, Map data) {
		return indexUrl(url, currentDepth, page, CollectionFactory.createMap(), method, data);
	}

	public static Boolean indexUrl(URL Url, int CurrentDepth, Website page, Map before, Connection.Method method, Map data) {
		if(!IndexManager.isBusy()) {
			breakOffIndexing();
			return false;
		}

		Document HTMLDoc = getDocument(Url, page, method, data);
		if(page.needsReload(HTMLDoc))
			HTMLDoc = getDocument(Url, page, method, data);

		indexContents(Url, CurrentDepth, page, HTMLDoc, before);

		Boolean cancelled = true;

		List<Button> buttons = page.getButtons();
		if(buttons.size() == 0)
			return true;

		WebClient jsClient = IndexUtil.setupClient();
		HtmlPage currentPage = IndexUtil.urlToJSPage(jsClient, Url.toString());
		int index = 0;
		for(Button button : buttons) {
			int i = 0;
			int m = button.getClicks();
			boolean indexafterEach = button.getIndexAfterEachClick();
			//if a button is pressed more than 1.000 times i'm pretty sure something is wrong
			while((i < m || (m == -1 && i < 1000)) && IndexManager.isBusy()) {
				i++;
				if(!(IndexManager.isBusy() && cancelled)) {
					System.out.println("The indexing has been stopped");
					return false;
				}
				String oldXml = currentPage.asXml();

				HtmlPage newPage = IndexUtil.clickButton(jsClient, currentPage, button);
				if(newPage == null)
					break;

				currentPage = newPage;

				String newXml = currentPage.asXml();
				if(oldXml.equals(newXml)) {
					break;
				}
				if(indexafterEach) {
					Document newdoc = Jsoup.parse(newXml);
					cancelled = indexContents(Url, CurrentDepth, page, newdoc, true, before);
				} else {
					HTMLDoc = Jsoup.parse(newXml);
				}
			}
			boolean now = true;
			if(buttons.size() > (index + 1)) {
				if(buttons.get(index + 1).getOrder() == button.getOrder())
					now = false;
			}
			if(!button.getIndexAfterEachClick() && HTMLDoc != null && now)
				indexContents(Url, CurrentDepth, page, HTMLDoc, true, before);
			index++;
		}
		return true;
	}

	private static Document getDocument(URL Url, Website page, Connection.Method method, Map data) {
		Document HTMLDoc;
		if(page.hasJavascript())
			HTMLDoc = IndexUtil.loadAndRunJS(Url.toString(), page.getJavascriptObject());
		else
			HTMLDoc = IndexUtil.getJsoupDoc(Url.toString(), method, data);
		return HTMLDoc;
	}

	public static void breakOffIndexing() {
		breakOffIndexing(null);
	}

	public static void breakOffIndexing(Thread indexThread) {
		LastIndex.indexrun();
		stopping = true;
		startTime = 0;
		IndexManager.getInstance().finishWritingAndClose();

		System.out.println("The indexing has been stopped");
		try {
			if(indexThread != null)
				indexThread.join();
		} catch(InterruptedException e) {
			LogUtil.logException(e);
		}
	}

	public static void setGlobalMaxDepth(int globalMaxDepth) {
		IndexUtil.GLOBAL_MAX_DEPTH = globalMaxDepth;
	}

	public static String getPath() { return IndexManager.getInstance().getPath(); }

	public static void updateIndex(String mode, String tempPath, Map options) {
		Map usedOptions = (options.containsKey(mode) ? (Map)options.get(mode) : CollectionFactory.createMap());
		switch(mode) {
			case "update":
				boolean ignoreExisting = usedOptions.containsKey("ignore_existing");
				boolean ignoreSelectors = usedOptions.containsKey("ignore_selectors");
				boolean disableUpdated = usedOptions.containsKey("disable_updated");
				XMLUtil.parseXML(tempPath, (!ignoreExisting), ignoreSelectors, disableUpdated);
				break;
			case "tags":
			default:
				if(options.size() == 0 && options.containsKey("tags"))
					usedOptions = (Map)options.get("tags");

				u(tempPath, usedOptions);
		}

	}

	private static void u(String tempPath, Map options) {
		List<Map> updates = XMLUtil.getTags(tempPath);
		Set updated = CollectionFactory.createSet();
		for(Map<String, String> update: updates) {
			String url = update.get(WebsiteDao.URL);
			String name = update.get(WebsiteDao.NAME);

			Website page = WebsiteDao.findByUrl(url);
			if(page == null)
				page = WebsiteDao.findByName(name);

			if(page != null) {
				updated.add(page.getId());
				if(8731 == (Integer)page.getId())
					System.out.println();

				Map<String, Tag> toRemove = page.getTags();

				Map<String, Tag> newTags = CollectionFactory.createMap();
				newTags.putAll(TagDao.createFromString(update.get(WebsiteDao.RECHTSGEBIED), Tag.RECHTSGEBIED));
				newTags.putAll(TagDao.createFromString(update.get(WebsiteDao.NIEUWS_SOORT), Tag.NIEUWSSOORT));
				newTags.putAll(TagDao.createFromString(update.get(WebsiteDao.RUBRIEKEN), Tag.RUBRIEK));

				Map toAdd = CollectionFactory.clone(newTags);

				for(String id: toRemove.keySet()) {
					toAdd.remove(id);
				}
				for(String id: newTags.keySet()) {
					toRemove.remove(id);
				}

				if(toRemove.size() > 0 || toAdd.size() > 0) {
					page.resetTags(newTags, true);
					for(Article art : page.getAllArticles()) {
						art.removeTags(toRemove);
						art.addTags(toAdd.values());
						art.update();
					}
				}

				Map updateParams = CollectionFactory.createMap();

				if(options.containsKey("include_zender"))
					updateParams.put(WebsiteDao.NIEUWSZENDER, update.get(WebsiteDao.NIEUWSZENDER));

				if(options.containsKey("include_description"))
					updateParams.put(WebsiteDao.DESCRIPTION, update.get(WebsiteDao.DESCRIPTION));

				if(updateParams.size() > 0)
					page.update(updateParams);
			}
		}

		for(Website page: WebsiteDao.getAll()) {
			if(!updated.contains(page.getId()))
				LogUtil.log(String.format("Page %s(%s) was not updated", page.getName(), page.getUrl()), Level.ERROR);
		}
	}



	public static List<String> rechtsprakenJuris(int page) {
		List urls = CollectionFactory.createList();
		try {
			URL obj = new URL("https://uitspraken.rechtspraak.nl/api/zoek");
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setReadTimeout(5000);

			Map<String, String> headers = CollectionFactory.createMap(
					new CollectionFactory.MapPair("Host", "uitspraken.rechtspraak.nl"),
					new CollectionFactory.MapPair("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0"),
					new CollectionFactory.MapPair("Accept", "application/json, text/javascript, */*; q=0.01"),
					new CollectionFactory.MapPair("Accept-Language", "nl,en-US;q=0.7,en;q=0.3"),
					new CollectionFactory.MapPair("Accept-Encoding", "gzip, deflate, br"),
					new CollectionFactory.MapPair("Referer", "https://uitspraken.rechtspraak.nl/"),
					new CollectionFactory.MapPair("Content-Type", "application/json"),
					new CollectionFactory.MapPair("X-Requested-With", "XMLHttpRequest"),
					new CollectionFactory.MapPair("Content-Length", "382"),
					new CollectionFactory.MapPair("DNT", "1"),
					new CollectionFactory.MapPair("Connection", "keep-alive"),
					new CollectionFactory.MapPair("Pragma", "no-cache"),
					new CollectionFactory.MapPair("Cache-Control", "no-cache")
			);
			for(String key : headers.keySet()) {
				con.addRequestProperty(key, headers.get(key));
			}

			con.setDoOutput(true);

			OutputStreamWriter w = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
			String json = String.format("{\"StartRow\":%d,\"PageSize\":10,\"ShouldReturnHighlights\":true,\"ShouldCountFacets\":true,\"SortOrder\":\"PublicatieDatumDesc\",\"SearchTerms\":[],\"Contentsoorten\":[],\"Rechtsgebieden\":[],\"Instanties\":[],\"DatumPublicatie\":[{\"NodeType\":4,\"Identifier\":\"DitJaar\",\"level\":1}],\"DatumUitspraak\":[],\"Advanced\":{\"PublicatieStatus\":\"AlleenGepubliceerd\"},\"CorrelationId\":\"cee167bf03b14d6da26e756b2a7d05e2\"}", page);
			w.write(json);
			w.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer html = new StringBuffer();

			while((inputLine = in.readLine()) != null) {
				html.append(inputLine);
			}

			in.close();
			con.disconnect();

			List results = new JSONObject(html.toString()).getJSONArray("Results").toList();

			for(Map resultaat : (List<Map>) results) {
				urls.add(resultaat.get("InterneUrl"));
				System.out.println(resultaat.get("InterneUrl"));
			}
		} catch(IOException e) {
			LogUtil.logException(e);
		}

		return urls;
	}
}
