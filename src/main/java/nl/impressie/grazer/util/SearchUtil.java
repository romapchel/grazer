package nl.impressie.grazer.util;

import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.caching.CacheUtil;
import nl.impressie.grazer.util.crawlerUtils.IndexManager;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.facet.DrillDownQuery;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;

import java.util.List;

/**
 * @author Impressie
 */
public class SearchUtil {
	public static List<String> splitTerms(String terms) {
		List<String> termlist = RegexUtil.splitExceptBetween(terms, " ", "\"([^\"]*)\"");
		return termlist;
	}

	public static BooleanQuery.Builder addSearchTermQuery(BooleanQuery.Builder builder, String field, String termString) {
		BooleanQuery.Builder bq = new BooleanQuery.Builder();
		String[] terms = splitTerms(termString).toArray(new String[0]);
		boolean foundTerm = false;
		for(String term : terms) {
			if(term.length() == 0)
				continue;

			term = term.toLowerCase();

			Query q;
			if(term.contains(" ")) {
				BooleanQuery.Builder qb = new BooleanQuery.Builder();
				for(String subterm : term.split(" ")) {
					subterm = termToWildcardTerm(subterm);
					qb.add(new WildcardQuery(new Term(field, subterm)), BooleanClause.Occur.MUST);
				}
				q = qb.build();
			} else {
				q = new WildcardQuery(new Term(field, termToWildcardTerm(term)));
			}

			foundTerm = true;
			bq.add(q, BooleanClause.Occur.MUST);
		}
		if(foundTerm)
			return builder.add(bq.build(), BooleanClause.Occur.MUST);

		return builder;
	}

	public static BooleanQuery.Builder addNewSplitTermQuery(BooleanQuery.Builder builder, String field, String termString, String seperator, BooleanClause.Occur occurence) {
		BooleanQuery.Builder bq = new BooleanQuery.Builder();
		String[] terms = termString.split(seperator);
		boolean foundTerm = false;
		for(String term : terms) {
			if(term.length() == 0)
				continue;

			foundTerm = true;
			bq.add(new TermQuery(new Term(field, term)), BooleanClause.Occur.SHOULD);
		}
		if(foundTerm)
			return builder.add(bq.build(), occurence);

		return builder;
	}

	public static BooleanQuery.Builder addNewSplitWebsiteQuery(BooleanQuery.Builder builder, String field, String termString, String seperator, BooleanClause.Occur occurence) {
		BooleanQuery.Builder bq = new BooleanQuery.Builder();
		String[] terms = termString.split(seperator);
		boolean foundTerm = false;
		for(String term : terms) {
			if(term.length() == 0)
				continue;

			Website site = WebsiteDao.findById(term);
			if(null != site)
				term = site.getUrl().toString().toLowerCase();

			foundTerm = true;
			bq.add(new TermQuery(new Term(field, term)), BooleanClause.Occur.SHOULD);
		}
		if(foundTerm)
			return builder.add(bq.build(), occurence);

		return builder;
	}

	public static BooleanQuery.Builder addContentSearchQuery(BooleanQuery.Builder builder, String term, String... fields) {
		MultiFieldQueryParser qp = new MultiFieldQueryParser(fields, IndexManager.getAnalyzer());
		qp.setAllowLeadingWildcard(true);
		qp.setDefaultOperator(QueryParser.Operator.AND);

		try {
			if(0 < term.length()) {
				term = termToWildcardTerms(term);
				Query query = qp.parse(term);
				builder.add(query, BooleanClause.Occur.MUST);
			}
		} catch(ParseException e) {
			LogUtil.logException(e);
		}
		return builder;
	}

	private static String termToWildcardTerms(String term) {
		String finalSearch = "";
		int lastBit = 0;

		String workingTerm = term;
		while(workingTerm.indexOf("\"", lastBit) != -1) {
			int start = workingTerm.indexOf("\"");
			int end = workingTerm.indexOf("\"", start + 1);

			if(end == -1)
				break;

			if(start > lastBit)
				finalSearch += termToWildcardTerm(workingTerm.substring(lastBit, start));

			finalSearch += workingTerm.substring(start, end + 1);

			lastBit = end + 1;
		}

		if(lastBit < (workingTerm.length() - 1))
			finalSearch += termToWildcardTerm(workingTerm.substring(lastBit));

		return finalSearch;
	}
	private static String termToWildcardTerm(String term) {
		StringBuilder builder = new StringBuilder();
		if(term == null || term.length() == 0)
			return term;

		String wildCard = "*";
		if(!term.startsWith(wildCard))
			builder.append(wildCard);

		builder.append(term);

		if(!term.endsWith(wildCard))
			builder.append(wildCard);

		return builder.toString();
	}

	public static BooleanQuery.Builder addLongRangeOrZeroQuery(BooleanQuery.Builder builder, String field, Long startRange, Long endRange) {
		BooleanQuery.Builder partBuilder = new BooleanQuery.Builder();
		partBuilder.add(addLongRangeQuery(new BooleanQuery.Builder(), field, startRange, endRange).build(), BooleanClause.Occur.SHOULD);
		partBuilder.add(LongPoint.newExactQuery(field, 0), BooleanClause.Occur.SHOULD);

		builder.add(partBuilder.build(), BooleanClause.Occur.MUST);

		return builder;
	}

	public static BooleanQuery.Builder addLongRangeQuery(BooleanQuery.Builder builder, String field, Long startRange, Long endRange) {
		Query query = LongPoint.newRangeQuery(field, startRange, endRange);

		builder.add(query, BooleanClause.Occur.MUST);

		return builder;
	}


	public static BooleanQuery.Builder addNewSplitTermTagQuery(BooleanQuery.Builder builder, String field, String termString, String seperator) {
		DrillDownQuery query = new DrillDownQuery(IndexManager.getFacetsConfig());
		String[] terms = termString.split(seperator);
		field = IndexManager.getTagFacetField(field);
		boolean foundTerm = false;
		for(String term : terms) {
			if(term.length() == 0)
				continue;

			foundTerm = true;
			query.add(field, term);
		}
		if(foundTerm)
			return builder.add(query, BooleanClause.Occur.MUST);

		return builder;
	}

	public static BooleanQuery.Builder addWebsiteSearchQuery(BooleanQuery.Builder builder, String defaultSeperator, boolean ignoreDisabledPages) {
		return CacheUtil.addWebsiteSearchQuery(builder, defaultSeperator, ignoreDisabledPages);
	}

	public static BooleanQuery.Builder addtagExclusion(BooleanQuery.Builder builder, Tag tag) {
		builder.add(new TermQuery(new Term(tag.getType(), tag.getId())), BooleanClause.Occur.MUST_NOT);

		return builder;
	}
}
