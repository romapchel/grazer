package nl.impressie.grazer.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.servlet.ServletContext;

/**
 * @author imrabti
 * https://crazygui.wordpress.com/2011/09/28/get-to-spring-applicationcontext-from-everywhere-the-clean-way/
 */
public class ContextProvider implements ApplicationContextAware {
	private static ContextProvider instance;

	private ApplicationContext applicationContext;

	@Autowired
	private ServletContext servletContext;

	public ContextProvider() {
		instance = this;
	}

	public static ApplicationContext getApplicationContext() {
		return getInstance().applicationContext;
	}

	public static ServletContext getServletContext() {
		return getInstance().servletContext;
	}

	@Override
	public void setApplicationContext(@NotNull ApplicationContext ctx) {
		getInstance().applicationContext = ctx;
	}

	private static ContextProvider getInstance() {
		return instance;
	}
}
