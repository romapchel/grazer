package nl.impressie.grazer.util.DatabaseUtils;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.ContextProvider;
import nl.impressie.grazer.util.LogUtil;
import org.springframework.dao.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class DBC {
	private static nl.impressie.grazer.util.DatabaseUtils.DBC DBC;

	private Connection connection;

	private DBC() {
		connection = getConnection();
	}

	private static RowMapper mapAll = (resultSet, i) -> {
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		Map schema = CollectionFactory.createMap();

		for(int index = 1; index <= columns; index++) {
			schema.put(md.getColumnName(index), resultSet.getObject(index));
		}

		return schema;
	};

	private Connection getConnection() {
		try {
			if(connection == null || connection.isClosed())
				connection = getNewConnection();
		} catch(SQLException e) {
			connection = null;
			return getConnection();
		}
		return connection;
	}

	private Connection getNewConnection() {
		//Todo: store the datasource and reopen/close connection as needed
		DataSource ds = (DataSource) ContextProvider.getApplicationContext().getBean("dataSource");

		Connection con = null;
		try {
			con = ds.getConnection();
		} catch(SQLException e) {
			LogUtil.logException(e);
			//throw new IllegalStateException("Cannot connect the database!", e);
		}

		return con;
	}

	public Object getFirst(String sql, Object[] params, RowMapper rowMapper) {
		Object found = null;
		try {
			found = getJdbcTemplate().queryForObject(sql, params, rowMapper);
		} catch(EmptyResultDataAccessException e) {
		}
		return found;
	}

	private Object search(String sql, Object[] params, RowMapper rowMapper) {
		return search(sql, params, rowMapper, true);
	}

	private int count(DBQueryObject dbqo) {
		String sql = dbqo.getSQL();
		Object[] paramaters = dbqo.getParamters();

		return count(sql, paramaters, true);
	}

	private int count(String sql, Object[] params, boolean firstTry) {
		try {
			return getJdbcTemplate().queryForObject(sql, params, Integer.class);
		} catch(RecoverableDataAccessException | TransientDataAccessResourceException | DataAccessResourceFailureException e) {
			if(firstTry)
				return count(sql, params, false);
			e.printStackTrace();
			return -1;
		}
	}

	private List<Object> search(String sql, Object[] params, RowMapper rowMapper, boolean firstTry) {
		List<Object> found;
		try {
			found = getJdbcTemplate().query(sql, params, rowMapper);
		} catch(EmptyResultDataAccessException e) {
			found = CollectionFactory.createList();
		} catch(RecoverableDataAccessException | TransientDataAccessResourceException | DataAccessResourceFailureException e) {
			if(firstTry)
				found = search(sql, params, rowMapper, false);
			else
				found = CollectionFactory.createList();
		} catch(Exception e) {
			LogUtil.logException(e);
			return null;
		}
		if(found.size() == 1 && found.get(0) == null)
			found = CollectionFactory.createList();

		return found;
	}


	public Map getSchemas() {
		List<Map> all = getJdbcTemplate().query("SELECT * FROM pg_catalog.pg_tables", mapAll);
		Map<String, List> schemas = CollectionFactory.createMap();
		for(Map table: all) {
			String schemeName = table.get("schemaname").toString();
			List tableList = (schemas.containsKey(schemeName) ? schemas.get(schemeName) : CollectionFactory.createList());
			tableList.add(table);
			schemas.put(schemeName, tableList);
		}
		return schemas;
	}

	private JdbcTemplate getJdbcTemplate() {
		JdbcTemplate template = new JdbcTemplate(new SingleConnectionDataSource(getConnection(), true));
		return template;
	}

	public int update(String sql, Object[] params) {
		JdbcTemplate template = getJdbcTemplate();
		int affectedRows = 0;
		try {
			affectedRows = template.update(sql, params);
		} catch(TransientDataAccessResourceException e) {
			String message = Arrays.toString(params);
			LogUtil.logException(message, e);
		}
		return affectedRows;
	}

	public int insert(DBQueryObject queryObject) {
		int id = 0;
		String sql = queryObject.getSQL();
		try {
			PreparedStatement stmt = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int index = 1;
			for(Object parameter : queryObject.getParamters()) {
				stmt.setObject(index, parameter);
				index++;
			}

			Object a = stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next())
				id = rs.getInt(1);

		} catch(SQLException e) {
			String message = String.format("Exception occured when trying to execute the SQL statement: [%1$s]", sql);
			LogUtil.logException(message, e);
		}

		return id;
	}

	public int runQueryObject(DBQueryObject queryObject) {
		if(queryObject.isMode(DBQueryObject.INSERT))
			return insert(queryObject);

		return update(queryObject.getSQL(), queryObject.getParamters());
	}

	public Object runQueryObjectSearch(DBQueryObject queryObject, RowMapper rowMapper) {
		if(queryObject.isMode(DBQueryObject.SELECT))
			return search(queryObject.getSQL(), queryObject.getParamters(), rowMapper);

		return runQueryObject(queryObject);
	}

	public int countQueryObjectSearch(DBQueryObject queryObject) {
		if(queryObject.isMode(DBQueryObject.COUNT))
			return count(queryObject);

		LogUtil.logException(new Exception(String.format("Something went wrong while counting")));
		return -1;
	}

	public static nl.impressie.grazer.util.DatabaseUtils.DBC getInstance() {
		if(DBC == null)
			DBC = new nl.impressie.grazer.util.DatabaseUtils.DBC();
		return DBC;
	}

	public List test() {
		return getJdbcTemplate().query("SELECT * FROM public.\"page\"", mapAll);
	}
}
