package nl.impressie.grazer.util.DatabaseUtils;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.KeyValue;
import nl.impressie.grazer.util.LogUtil;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

/**
 * @author Impressie
 */
public class CRUDObject {
	public static List getAll(String tableName, RowMapper rowMapper) {
		return select(tableName, rowMapper, CollectionFactory.createList());
	}

	/**
	 * @param tableName the name of the table the data will be stored in
	 * @param params    the keys are the column names and the values are the values that will be saved
	 * @return returns the id given to the new object in the database
	 */
	public static int create(String tableName, List<KeyValue> params) {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.setMode(DBQueryObject.INSERT);
		queryObject.addTable(tableName);
		for(KeyValue<String, Object> param : params) {
			queryObject.addUpdatedValue(param.getKey(), param.getValue());
		}

		return DBC.getInstance().runQueryObject(queryObject);
	}

	public static List select(String tableName, RowMapper rowMapper, String column, String value) {
		return select(tableName, rowMapper, new KeyValue<String, String>(column, value));
	}

	public static List select(String tableName, RowMapper rowMapper, KeyValue parameter) {
		List params = CollectionFactory.createList();
		params.add(parameter);

		return select(tableName, rowMapper, params);
	}

	public static List select(String tableName, RowMapper rowMapper, List<KeyValue> parameters) {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.addTable(tableName);
		for(KeyValue<String, Object> param : parameters) {
			queryObject.addWhereParameter(param.getKey(), param.getValue());
		}

		List items = CollectionFactory.createList();
		try {
			return (List) DBC.getInstance().runQueryObjectSearch(queryObject, rowMapper);
		} catch(ClassCastException e) {
			LogUtil.logException(e);
		}

		if(items.size() > 0)
			return items;
		return null;
	}

	public static int update(String tableName, String paramCol, String paramVal, String updateCol, String updateVal) {
		return update(tableName, new KeyValue(paramCol, paramVal), new KeyValue(updateCol, updateVal));
	}

	public static int update(String tableName, KeyValue param, KeyValue update) {
		List<KeyValue> params = CollectionFactory.createList();
		List<KeyValue> updates = CollectionFactory.createList();
		params.add(param);
		updates.add(update);

		return update(tableName, params, updates);
	}

	public static int update(String tableName, List<KeyValue> params, List<KeyValue> updates) {
		DBQueryObject QO = new DBQueryObject(DBQueryObject.UPDATE);
		QO.addTable(tableName);
		boolean hasUpdates = false;
		for(KeyValue<String, Object> param : params) {
			QO.addWhereParameter(param.getKey(), param.getValue());
		}
		for(KeyValue<String, Object> updatekv : updates) {
			if(updatekv.getValue() != null)
				hasUpdates = true;
			QO.addUpdatedValue(updatekv.getKey(), updatekv.getValue());
		}
		if(!hasUpdates)
			return 0;

		return DBC.getInstance().runQueryObject(QO);
	}

	public static int delete(String tableName, String column, String value) {
		return delete(tableName, new KeyValue(column, value));
	}

	public static int delete(String tableName, KeyValue param) {
		List params = CollectionFactory.createList();
		params.add(param);

		return delete(tableName, params);
	}

	public static int delete(String tableName, List<KeyValue> params) {
		DBQueryObject queryObject = new DBQueryObject();
		queryObject.setMode(DBQueryObject.DELETE);
		queryObject.addTable(tableName);
		for(KeyValue<String, Object> param : params) {
			queryObject.addWhereParameter(param.getKey(), param.getValue());
		}

		return DBC.getInstance().runQueryObject(queryObject);
	}
}
