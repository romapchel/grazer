package nl.impressie.grazer.util.DatabaseUtils;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.LogUtil;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collection;
import java.util.List;

/**
 * @author Impressie
 */
public class Parameter {
	private String column;
	private Comparator comparator;
	private Object value, ogValue;
	private Type type;
	private Seperator seperator = Seperator.AND;
	private boolean startsSub = false, endsSub = false;
	private boolean typeWasSet = false;
	private SortOrder sortOrder;

	public enum Seperator {
		AND("AND"),
		OR("OR");

		private final String text;

		Seperator(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
	}
	public enum SortOrder {
		ASCENDING("ASC"),
		DESCENDING("DESC"),
		DESCENDING_NULLS_LAST("DESC NULLS LAST");

		private final String text;

		SortOrder(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
	}
	public enum Type {
		STRING("string"),
		INT("int"),
		BOOLEAN("bool"),
		NONE("no");

		private final String text;

		Type(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	public enum Comparator {
		LIKE("like"),
		IN("in"),
		EQUALS("="),
		NOT_EQUALS("!!="),
		BELOW_STORED(">"),
		ABOVE_STORED("<"),
		ABOVE_OR_EQUAL_TO_STORED(ABOVE_STORED.toString() + EQUALS.toString()),
		BEWLOW_OR_EQUAL_TO_STORED(BELOW_STORED.toString() + EQUALS.toString());

		private final String text;

		Comparator(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	public Parameter setSeperator(Seperator seperator) {
		this.seperator = seperator;
		return this;
	}
	public Parameter setStartsSub(boolean startsSub) {
		this.startsSub = startsSub;
		return this;
	}
	public Parameter setEndsSub(boolean endsSub) {
		this.endsSub = endsSub;
		return this;
	}
	public Seperator getSeperator() {
		return seperator;
	}
	public boolean getStartsSub() {
		return startsSub;
	}
	public boolean getEndsSub() {
		return endsSub;
	}

	public Parameter(String column, SortOrder sortOrder) {
		this(column, "", Comparator.EQUALS, sortOrder);
	}
	public Parameter(String column) {
		this(column, "", Comparator.EQUALS, SortOrder.ASCENDING);
	}

	public Parameter(String column, Object value) {
		this(column, value, Comparator.EQUALS, SortOrder.ASCENDING);
	}

	public Parameter(String Column, Object Value, Comparator Comparator) {
		this(Column, Value, Comparator, SortOrder.ASCENDING);
	}

	public Parameter(String Column, Object Value, Comparator Comparator, SortOrder sortOrder) {
		column = Column;

		value = Value;
		if(value instanceof String)
			value = ((String) value).trim();

		ogValue = value;

		comparator = Comparator;
		this.sortOrder = sortOrder;

		generateType(value);
	}

	public Parameter setComparator(Comparator comparator) {
		this.comparator = comparator;
		return this;
	}

	private static Boolean getBooleanValue(Object value) {
		if(null == value) {
			return true;
		}
		return Boolean.valueOf(value.toString());
	}

	public Parameter setType(Type type) {
		this.type = type;
		typeWasSet = true;
		value = ogValue;
		convertValueType();
		return this;
	}

	private void convertValueType() {
		try {
			if(value instanceof Object[])
				value = CollectionFactory.createList(value);

			if(value instanceof Collection) {
				List newList = CollectionFactory.createList();
				for(Object item : (Collection) value) {
					newList.add(convertObject(item));
				}
				value = newList;
			} else {
				value = convertObject(value);
			}
		} catch(Exception e) {
			LogUtil.log("something went wrong trying to convert the value of the parameter");
			LogUtil.logException(e);
		}
	}

	private Object convertObject(Object value) {
		if(value instanceof Model)
			value = ((Model) value).getId();

		if(Type.STRING.equals(type) && !(value instanceof String)) {
			value = String.valueOf(value);
		} else if(Type.BOOLEAN.equals(type) && !(value instanceof Boolean)) {
			value = Boolean.valueOf(value.toString());
		} else if(Type.INT.equals(type) && !(value instanceof Integer)) {
			try {
				value = Integer.valueOf(value.toString());
			} catch(NumberFormatException e) {
				value = 0;
			}
		}

		return value;
	}

	public StringBuilder appendType(StringBuilder builder) {
		switch(type) {
			case INT:
				builder.append("::int");
				break;
			case BOOLEAN:
				builder.append("::boolean");
				break;
			case NONE:
				break;
			case STRING:
			default:
				builder.append("::varchar");
				break;
		}
		return builder;
	}

	public String toWhere(boolean lowercase) {
		StringBuilder builder = new StringBuilder();

		if(!typeWasSet)
			generateType(value);

		//lowercase only works for strings
		lowercase = (lowercase && Type.STRING.equals(type) && !Comparator.IN.equals(comparator));

		if(lowercase)
			builder.append("LOWER(");
		builder.append("\"");
		builder.append(column);
		builder.append("\"");

		appendType(builder);

		if(lowercase)
			builder.append(")");
		builder.append(String.format(" %s ", comparator));

		if(Comparator.EQUALS.equals(comparator) && value instanceof Collection)
			comparator = Comparator.IN;

		boolean in = Comparator.IN.equals(comparator);
		if(in)
			builder.append("(");

		if(value instanceof Collection) {
			Collection l = (Collection) value;
			for(int i = 0; i < l.size(); i++) {
				if(0 != i)
					builder.append(",");

				appendVar(builder, lowercase);
			}
		} else {
			appendVar(builder, lowercase);
		}

		if(in)
			builder.append(")");

		return builder.toString();
	}

	private StringBuilder appendVar(StringBuilder builder, boolean lowercase) {
		if(lowercase) {
			builder.append("LOWER(?)");
		} else {
			builder.append("?");
		}
		return builder;
	}

	private void generateType(Object value) {
		if(typeWasSet)
			return;

		if(value instanceof String) {
			setType(Type.STRING);
		} else if(value instanceof Integer) {
			setType(Type.INT);
		} else if(value instanceof Boolean) {
			setType(Type.BOOLEAN);
		} else if(value instanceof Object[]) {
			this.value = CollectionFactory.createList(value);
			generateType(this.value);
		} else if(value instanceof Collection) {
			Collection col = (Collection) value;
			for(Object ob : col) {
				if(1 == col.size())
					this.value = ob;

				generateType(ob);
				break;
			}
		} else {
			setType(Type.NONE);
		}

		typeWasSet = false;
	}

	public String toSet() {
		return String.format("\"%1$s\" = ?", column);
	}

	public String toInsert() {
		return String.format("\"%1$s\"", column);
	}

	public String toSort() {
		return String.format("\"%1$s\" %2$s", column, sortOrder.text);
	}

	public String toGroup() {
		return column;
	}

	public Object getParamater() {
		return value;
	}

	public String getColumn() {
		return column;
	}

	public List addParameter(List parameterList) {
		if(value instanceof Collection)
			parameterList.addAll((Collection) value);
		else
			parameterList.add(value);

		return parameterList;
	}

	public boolean isValid() {
		return column != null && comparator != null && value != null;
	}

	@Override
	public String toString() {
		return String.format("Column: %s, Value: %s", column, value);
	}

	public void encryptParameter(PasswordEncoder passwordEncoder) {
		String val = value.toString();
		value = passwordEncoder.encode(val);
	}
}
