package nl.impressie.grazer.util.DatabaseUtils;

import nl.impressie.grazer.factories.CollectionFactory;

import java.util.List;

/**
 * @author Impressie
 */
public class DBQueryObject {
	public final static int SELECT = 0, UPDATE = 1, DELETE = 2, INSERT = 3, COUNT = 4;

	private final static int numberOfModes = 5;

	private List<String> tables = CollectionFactory.createList();
	private List<String> selectRows = CollectionFactory.createList();
	private List<Parameter> orderparameters = CollectionFactory.createList();
	private List<Parameter> parameters = CollectionFactory.createList();
	private List<Parameter> updatedValues = CollectionFactory.createList();
	private List<Parameter> groupParameters = CollectionFactory.createList();
	private int mode;

	public DBQueryObject() {
		this(-1);
	}

	public DBQueryObject(int mode) {
		this.mode = DBQueryObject.SELECT;
		setMode(mode);
	}

	public void addOrderBy(Parameter OrderParamater) {
		if(orderparameters == null)
			orderparameters = CollectionFactory.createList();

		if(OrderParamater.isValid()) {
			orderparameters.add(OrderParamater);
		}
	}

	private boolean isValidMode(int newMode) {
		return (newMode < numberOfModes && newMode > 0);
	}

	public void setMode(int newMode) {
		if(isValidMode(newMode)) {
			mode = newMode;
		}
	}

	public boolean isMode(int queryMode) {
		return queryMode == mode;
	}

	public void addWhereParameter(String Column, Object Value) {
		addWhereParameter(Column, Value, Parameter.Comparator.EQUALS);
	}

	public void addWhereParameter(String Column, Object Value, Parameter.Comparator Comparator) {
		addWhereParameter(new Parameter(Column, Value, Comparator));
	}

	public void addWhereParameter(Parameter Paramater) {
		if(mode == INSERT) {
			addUpdatedValue(Paramater);
			return;
		}
		if(parameters == null)
			parameters = CollectionFactory.createList();
		if(Paramater.isValid()) {
			parameters.add(Paramater);
		}
	}

	public void addTableColumn(String column) {
		if(selectRows == null)
			selectRows = CollectionFactory.createList();
		selectRows.add(column);
	}

	public void addUpdatedValue(String Column, Object Value) {
		addUpdatedValue(new Parameter(Column, Value, Parameter.Comparator.EQUALS));
	}

	public void addUpdatedValue(Parameter UpdateValue) {
		if(mode == DELETE || mode == SELECT || mode == COUNT) {
			addWhereParameter(UpdateValue);
			return;
		}

		if(updatedValues == null)
			updatedValues = CollectionFactory.createList();

		if(UpdateValue.isValid())
			updatedValues.add(UpdateValue);
	}

	public void addTable(String table) {
		if(tables == null)
			tables = CollectionFactory.createList();
		tables.add(table);
	}

	private void addColumnToStringBuilder(StringBuilder builder, String column) {
		if(0 < builder.length())
			builder.append(", ");
		builder.append(escape(column));
	}

	public String getSQL() {
		StringBuilder SQL = new StringBuilder();

		switch(mode) {
			case COUNT:
			case SELECT:
				SQL.append("SELECT");
				break;
			case UPDATE:
				SQL.append("UPDATE");
				break;
			case DELETE:
				SQL.append("DELETE FROM");
				break;
			case INSERT:
				SQL.append("INSERT INTO");
				break;
			default:
				return "";
		}

		if(mode == SELECT || mode == COUNT) {
			SQL.append(" ");
			if(mode == COUNT) SQL.append("COUNT(");

			StringBuilder columns = new StringBuilder();
			for(String selectRow : selectRows) {
				addColumnToStringBuilder(columns, selectRow);
			}
			for(Parameter group : groupParameters) {
				addColumnToStringBuilder(columns, group.getColumn());
			}
			if(0 == columns.length())
				columns.append("*");

			SQL.append(columns);

			if(mode == COUNT) SQL.append(")");
			SQL.append(" FROM");
		}

		addFrom(SQL);

		if(SQL.length() <= 0)
			return "";

		if(!isEmpty(updatedValues)) {
			SQL.append(" ");
			if(mode == UPDATE) {
				SQL.append("SET ");
				int index = 0;
				for(Parameter KV : updatedValues) {
					if(index > 0)
						SQL.append(", ");
					SQL.append(KV.toSet());
					index++;
				}
			} else if(mode == INSERT) {
				SQL.append("(");
				int index = 0;
				for(Parameter KV : updatedValues) {
					if(index > 0)
						SQL.append(", ");
					SQL.append(KV.toInsert());
					index++;
				}
				SQL.append(")");

				SQL.append("VALUES(");
				for(int i = 0; i < index; i++) {
					if(i > 0)
						SQL.append(", ");
					SQL.append("?");
				}
				SQL.append(")");
			}
		}

		addWhere(SQL);

		if(mode == SELECT) {
			StringBuilder builder = new StringBuilder();
			for(Parameter param : orderparameters) {
				if(builder.length() > 0)
					builder.append(", ");
				builder.append(param.toSort());
			}
			if(builder.length() > 0)
				SQL.append(String.format(" ORDER BY %s", builder));

			builder = new StringBuilder();
			for(Parameter param : groupParameters) {
				if(builder.length() > 0)
					builder.append(", ");
				builder.append(param.toGroup());
			}
			if(builder.length() > 0)
				SQL.append(String.format(" GROUP BY %s", builder));
		}

		return SQL.toString();
	}

	private StringBuilder addWhere(StringBuilder SQL) {
		if(mode != INSERT && !isEmpty(parameters)) {
			SQL.append(" WHERE ");
			int index = 0;
			for(Parameter param : parameters) {
				if(index > 0)
					SQL.append(String.format(" %s ", param.getSeperator()));

				if(param.getStartsSub())
					SQL.append("(");

				SQL.append(param.toWhere(true));

				if(param.getEndsSub())
					SQL.append(")");

				index++;
			}
		}

		return SQL;
	}

	private StringBuilder addFrom(StringBuilder SQL) {
		if(!isEmpty(tables)) {
			SQL.append(" ");
			for(int i = 0, m = tables.size(); i < m; i++) {
				if(i > 0)
					SQL.append(", ");
				SQL.append(escape(tables.get(i)));
			}
		} else {
			return new StringBuilder();
		}

		return SQL;
	}

	public Object[] getParamters() {
		List parameterList = CollectionFactory.createList();

		List<Parameter> allParams = CollectionFactory.createList();
		if(!isEmpty(updatedValues)) allParams.addAll(updatedValues);
		if(!isEmpty(parameters)) allParams.addAll(parameters);

		for(Parameter param : allParams) {
			param.addParameter(parameterList);
		}

		return parameterList.toArray();
	}

	private String escape(String tableRow) {
		String escapeString = "\"%1$s\"";

		String escaped = String.format(escapeString, tableRow);

		return escaped;
	}

	private boolean isEmpty(List list) {
		return list == null || list.size() <= 0;
	}

	@Override
	public String toString() {
		return getSQL();
	}

	public DBQueryObject addGroupParameter(Parameter groupParameter) {
		groupParameters.add(groupParameter);

		return this;
	}
}
