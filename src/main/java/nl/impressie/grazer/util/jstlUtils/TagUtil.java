package nl.impressie.grazer.util.jstlUtils;

import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.util.LogUtil;

import java.util.*;

/**
 * @author Impressie
 */
public class TagUtil {
	public static Map<Integer, TagFamily> getTagFamiliesByParentId(Collection<Tag> tags) {
		Map map = CollectionFactory.createMap();
		for(TagFamily family: getTagFamilies(tags)) {
			map.put(family.getParent().getId(), family);
		}
		return map;
	}
	public static List<TagFamily> getTagFamilies(Collection<Tag> tags) {
		Map<String, TagFamily> tagFamilies = new HashMap();
		Map<String, List<Tag>> orphans = new HashMap();
		for(Tag tag: tags) {
			String name = tag.getName();
			if(!containsParentTag(name)) {
				TagFamily family = new TagFamily(tag);
				String lowerName = name.toLowerCase();
				tagFamilies.put(lowerName, family);
				if(orphans.containsKey(lowerName)) {
					for(Tag orphan: orphans.get(lowerName)) {
						family.addChildTag(orphan);
					}
					orphans.remove(lowerName);
				}
				continue;
			}
			String parentName = getParentTag(name);

			if(!tagFamilies.containsKey(parentName)) {
				List orphanList = (orphans.containsKey(parentName) ? orphans.get(parentName) : CollectionFactory.createList());
				orphanList.add(tag);
				orphans.put(parentName, orphanList);
				continue;
			}

			tagFamilies.get(parentName).addChildTag(tag);
		}
		if(orphans.size() > 0) {
			Set<String> keysToRemove = new HashSet();
			for(String parentName: orphans.keySet()) {
				List<Tag> orphanList = orphans.get(parentName);
				Tag parentTag = TagDao.findByName(parentName, orphanList.get(0).getType());
				TagFamily family = (parentTag == null ? null : new TagFamily(parentTag));
				for(Tag orphan: orphanList) {
					if(family == null)
						LogUtil.logException(new Exception(String.format("Failed to find tagfamily for tagname %s (family tag name: %s)", orphan.getName(), parentName)));
					else
						family.addChildTag(orphan);
				}
				if(family != null) {
					keysToRemove.add(parentName);
					tagFamilies.put(parentName, family);
				}
			}
			for(String key: keysToRemove) {
				orphans.remove(key);
			}
		}
		return new ArrayList(tagFamilies.values());
	}

	public static boolean containsParentTag(String tagName) {
		return tagName.contains(":");
	}
	public static String getParentTag(String tagName) {
		return tagName.substring(0, tagName.indexOf(":")).toLowerCase();
	}

	public static class TagFamily {
		private Tag parent;
		private int removeFromChildTagLength = 0;
		private Map<String, Tag> children = CollectionFactory.createMap();
		public TagFamily(Tag parent) {
			removeFromChildTagLength = parent.getName().length() + 1;
			this.parent = parent;
		}
		public TagFamily addChildTag(Tag tag) {
			tag.setName(tag.getName().substring(removeFromChildTagLength).trim());
			children.put(tag.getId(), tag);
			return this;
		}
		public boolean containsChild(Tag tag) {
			return children.containsKey(tag.getId());
		}
		public Collection<Tag> getChildren() { return children.values(); }
		public Tag getParent() { return parent; }

		@Override
		public String toString() {
			return parent.toString();
		}
	}
}
