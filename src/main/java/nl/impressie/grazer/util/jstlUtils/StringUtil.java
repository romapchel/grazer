package nl.impressie.grazer.util.jstlUtils;

/**
 * @author Impressie
 */
public class StringUtil {
	public static String removeCharacters(String string, int front, int back) {
		string = string.substring(front, string.length() - back);
		return string;
	}

	public static String escapeJs(String string) {
		String s = string.replaceAll("(\\')", "\\\\$1");
		return s;
	}
}
