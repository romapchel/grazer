package nl.impressie.grazer.util.jstlUtils;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.RegexUtil;

import java.util.Collection;
import java.util.List;

/**
 * @author Impressie
 */
public class ListUtil {
	public static boolean contains(Object list, Object o) {
		Collection col = null;
		if(list instanceof Collection)
			col = (Collection) list;
		else if(list instanceof Object[])
			col = CollectionFactory.createList((Object[]) list);

		boolean contains = false;
		if(col != null)
			contains = col.contains(o);
		return contains;
	}

	public static List combine(Object firstList, Object secondList) {
		List newList = CollectionFactory.createList();
		List list1 = null, list2 = null;

		if(firstList instanceof Object[])
			list1 = CollectionFactory.createList(firstList);
		else if(firstList instanceof List)
			list1 = (List) firstList;
		else if(firstList instanceof String)
			list1 = ArrayStringToList((String) firstList);

		if(secondList instanceof Object[])
			list2 = CollectionFactory.createList(secondList);
		else if(secondList instanceof List)
			list2 = (List) secondList;
		else if(secondList instanceof String)
			list2 = ArrayStringToList((String) secondList);

		if(list1 == null || list2 == null)
			return newList;
		int l2Max = list2.size();
		int index = 0;
		for(Object item : list1) {
			List entry = CollectionFactory.createList();
			entry.add(item);
			if(index < l2Max)
				entry.add(list2.get(index));
			else
				entry.add("");
			newList.add(entry);
			index++;
		}
		while(index < l2Max) {
			List entry = CollectionFactory.createList();
			entry.add("");
			entry.add(list2.get(index));
			newList.add(entry);
			index++;
		}
		return newList;
	}

	public static List ArrayStringToList(String string) {
		if(string.startsWith("\"") || string.startsWith("["))
			string = string.substring(1);
		if(string.endsWith("\"") || string.endsWith("]"))
			string = string.substring(0, string.length() - 1);
		List returns = CollectionFactory.createList();
		if(string.length() > 0)
			returns = RegexUtil.splitExceptBetween(string, ",", "\\[[^\\]]*\\]");
		return returns;
	}

	public static Collection safeForeach(Object object) {
		if(object == null) {
			return CollectionFactory.createList();
		} else if(object instanceof String) {
			return CollectionFactory.createList(object);
		} else if (object instanceof Collection) {
			return (Collection) object;
		} else {
			throw new UnsupportedOperationException("Unknown object type in ListUtil.safeforeach");
		}
	}
}
