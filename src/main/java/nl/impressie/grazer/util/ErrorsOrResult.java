package nl.impressie.grazer.util;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;

import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class ErrorsOrResult<T> {
	private T result;
	private Map errors;
	private List<Parameter> parameters;

	public ErrorsOrResult() {
		this(CollectionFactory.createMap());
	}
	public ErrorsOrResult(Map errors) {
		this(null, errors);
	}
	public ErrorsOrResult(T result) {
		this(result, CollectionFactory.createMap());
	}
	public ErrorsOrResult(T result, Map errors) {
		this.errors = errors;
		this.result = result;
		this.parameters = CollectionFactory.createList();
	}

	public boolean isSuccesful() {
		if(result == null || errors.size() != 0)
			return false;

		return true;
	}

	public ErrorsOrResult addErrors(Map newErrors) {
		errors.putAll(newErrors);

		return this;
	}

	public ErrorsOrResult addParameters(List<Parameter> paramaters) {
		this.parameters.addAll(paramaters);
		return this;
	}

	public ErrorsOrResult addResult(T result) {
		this.result = result;

		return this;
	}

	public List<Parameter> getParameters() { return parameters; }
	public Map<String, Object> getParameterMap() {
		Map map = CollectionFactory.createMap();
		for(Parameter param: parameters) {
			map.put(param.getColumn(), param.getParamater());
		}
		return map;
	}
	public Map getErrors() { return errors; }
	public T getResult() {
		return result;
	}
}
