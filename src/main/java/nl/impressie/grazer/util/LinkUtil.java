package nl.impressie.grazer.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Impressie
 */
@Component
public class LinkUtil {
	private static LinkUtil instance;

	@Value("${server.url}")
	private String server_url;

	public LinkUtil() {
		instance = this;
	}

	public static String generateAbsoluteUrl(String relativeUrl) {
		LinkUtil util = getInstance();
		return util.generateAbsoluteUrlPrivate(relativeUrl);
	}

	private String generateAbsoluteUrlPrivate(String relativeUrl) {
		if(!relativeUrl.startsWith("http") && !relativeUrl.startsWith("/") && !relativeUrl.startsWith("?"))
			relativeUrl = "/" + relativeUrl;

		return String.format("%s%s", server_url, relativeUrl);
	}

	private static LinkUtil getInstance() {
		return instance;
	}
}
