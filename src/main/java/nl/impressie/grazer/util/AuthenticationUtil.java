package nl.impressie.grazer.util;

import nl.impressie.grazer.dao.UserDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.User.CustomUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class AuthenticationUtil {
	public static Map updatedUsers;

	public static CustomUser getCurrentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return getUserFromAuthentication(authentication);
	}

	public static CustomUser getUserFromAuthentication(Authentication authentication) {
		Object principal = null;
		if(authentication != null)
			principal = authentication.getPrincipal();
		//if user is not logged in principal is a string
		if(principal instanceof CustomUser) {
			CustomUser user = (CustomUser) principal;

			if(user.needsToBeUpdated(getUpdatedUsers()))
				setLoggedInUser(user);

			return user;
		}
		return null;
	}

	public static Map getUpdatedUsers() {
		if(updatedUsers == null)
			updatedUsers = CollectionFactory.createMap();
		return updatedUsers;
	}

	public static void setLoggedInUser(CustomUser user) {
		if(user == null)
			return;

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		user = UserDao.findById(user.getId());
		List<GrantedAuthority> updatedAuthorities = user.getAuthorities();
		Authentication newAuth = new UsernamePasswordAuthenticationToken(user, authentication.getCredentials(), updatedAuthorities);
		SecurityContextHolder.getContext().setAuthentication(newAuth);
	}

	public static void addToUpdatedUsers(Object id) {
		if(id instanceof CustomUser)
			id = ((CustomUser) id).getId();
		
		Map map = getUpdatedUsers();
		map.put(id, DateUtil.getTimestamp());
	}
}
