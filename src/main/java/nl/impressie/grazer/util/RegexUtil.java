package nl.impressie.grazer.util;

import nl.impressie.grazer.factories.CollectionFactory;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Impressie
 */
public class RegexUtil {
	public static Matcher getMatcher(String regex, String text) {
		return getmatcher(regex, 0, text);
	}

	public static Matcher getCaseInsensitiveMatcher(String regex, String text) {
		return getmatcher(regex, Pattern.CASE_INSENSITIVE, text);
	}

	public static Matcher getDotAllMatcher(String regex, String text) {
		return getmatcher(regex, Pattern.DOTALL, text);
	}

	private static Matcher getmatcher(String regex, int flags, String text) {
		if(null == regex)
			regex = "";
		if(null == text)
			text = "";

		Matcher matcher = Pattern.compile(regex, flags).matcher(text);

		return matcher;
	}

	public static String[] splitOnNotPrecededByBackslash(String string, String splitter) {
		return string.split(String.format("(?<!\\\\)%s", splitter));
	}

	public static List splitExceptBetween(String string, String seperator, String exception) {
		List<String> items = CollectionFactory.createList();
		if(null == string)
			return items;

		Matcher matcher = getMatcher(exception, string);
		int lastEnd = 0;
		while(matcher.find()) {
			int thisStart = matcher.start();
			int thisEnd = matcher.end();
			if(thisStart > lastEnd) {
				for(String subTerm : string.substring(lastEnd, thisStart).split(seperator)) {
					subTerm = subTerm.trim();
					if(subTerm.length() > 0)
						items.add(subTerm);
				}
			}
			String subTerm = string.substring(thisStart + 1, thisEnd - 1).trim();
			if(subTerm.length() > 0)
				items.add(subTerm);
			lastEnd = thisEnd;
		}

		for(String subTerm : string.substring(lastEnd).split(seperator)) {
			subTerm = subTerm.trim();
			if(subTerm.length() > 0)
				items.add(subTerm);
		}
		return items;
	}
}
