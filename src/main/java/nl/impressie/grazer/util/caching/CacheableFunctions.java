package nl.impressie.grazer.util.caching;

import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.SearchUtil;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;

/**
 * @author Impressie
 */
@EnableCaching
@Component
public class CacheableFunctions {
    private final static String WEBSITES_QUERY_CACHE = "websites";

    @Cacheable(value = WEBSITES_QUERY_CACHE, key = "#key")
    public BooleanQuery.Builder addWebsiteSearchQuery(String key, BooleanQuery.Builder builder, String defaultSeperator, boolean ignoreDisabledPages) {
        StringBuilder activePageIds = new StringBuilder();
        int totalNumberOfSites = WebsiteDao.count(false, false);
        int numberOfActiveSites = WebsiteDao.count(true, true);
        if(totalNumberOfSites > numberOfActiveSites) {
            for(Website web : WebsiteDao.getAll()) {
                if(0 < activePageIds.length())
                    activePageIds.append(defaultSeperator);

                activePageIds.append(web.getId());
            }
        }
        if(ignoreDisabledPages && 0 < activePageIds.length())
            builder = SearchUtil.addNewSplitWebsiteQuery(builder, Article.WEBSITE_URL_SEARCH, activePageIds.toString(), defaultSeperator, BooleanClause.Occur.MUST);

        return builder;
    }

    @CacheEvict(value = WEBSITES_QUERY_CACHE, allEntries = true)
    public void resetWebsitesCache() { }

}