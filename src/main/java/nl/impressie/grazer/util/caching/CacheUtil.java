package nl.impressie.grazer.util.caching;

import org.apache.lucene.search.BooleanQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Impressie
 */
@Component
public class CacheUtil {
	@Autowired
	private CacheableFunctions cacheableFunctions;

	private static CacheUtil instance;

	private CacheUtil() { }
	public static CacheUtil instantiate() {
		if(instance == null)
			instance = new CacheUtil();
		return instance;
	}

	private BooleanQuery.Builder addWebsiteSearchQueryPrivate(BooleanQuery.Builder builder, String defaultSeperator, boolean ignoreDisabledPages) {
		String key = builder.build().toString() + defaultSeperator + ignoreDisabledPages;
		return cacheableFunctions.addWebsiteSearchQuery(key, builder, defaultSeperator, ignoreDisabledPages);
	}
	private void resetWebsitesCacheprivate () {
		cacheableFunctions.resetWebsitesCache();
	}
	public static BooleanQuery.Builder addWebsiteSearchQuery(BooleanQuery.Builder builder, String defaultSeperator, boolean ignoreDisabledPages) {
		return instance.addWebsiteSearchQueryPrivate(builder, defaultSeperator, ignoreDisabledPages);
	}
	public static void resetWebsitesCache () {
		instance.resetWebsitesCacheprivate();
	}
}
