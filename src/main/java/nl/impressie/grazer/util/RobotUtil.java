package nl.impressie.grazer.util;

import nl.impressie.grazer.model.Robot;

/**
 * @author Impressie
 */
public class RobotUtil {
	private static Robot robot;

	public static void load(String url) {
		robot = new Robot(url);
	}

	public static boolean isAllowed(String url) {
		if(robot != null)
			return robot.check(url);
		return true;
	}
}
