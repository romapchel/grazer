package nl.impressie.grazer.hibernate.repositories;

import nl.impressie.grazer.hibernate.models.AdditionalSourceLinkModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Impressie
 */
//@Repository
public interface AdditionalSourceLinkRepository extends JpaRepository<AdditionalSourceLinkModel, Long> {
}
