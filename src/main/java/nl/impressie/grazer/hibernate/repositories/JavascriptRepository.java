package nl.impressie.grazer.hibernate.repositories;

import nl.impressie.grazer.hibernate.models.JavascriptModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Impressie
 */
@Repository
public interface JavascriptRepository extends JpaRepository<JavascriptModel, Long> {
}
