package nl.impressie.grazer.hibernate.repositories;

import nl.impressie.grazer.hibernate.models.UnknownTagModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Impressie
 */
@Repository
public interface UnknownTagRepository extends JpaRepository<UnknownTagModel, Long> {
}
