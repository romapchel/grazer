package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.model.DB.JavaScriptObject;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_javascript")
@Table(name = "hibernate_javascript")
@Getter
@Setter
public class JavascriptModel {
	public JavascriptModel() {}
	public JavascriptModel(JavaScriptObject object) {
		setJavascriptCode(object.getJavascript());
		setTestId(object.getTestId());
		setJavascript(object.hasJavascript());
	}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	private String javascriptCode;

	@Expose
	private String testId;

	@Expose
	private Boolean javascript;

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		boolean matches = false;
		if(object instanceof JavascriptModel) {
			matches = ((JavascriptModel)object).javascript == this.javascript;

			if(matches)
				matches = ((JavascriptModel)object).testId.equals(this.testId);

			if(matches)
				matches = ((JavascriptModel)object).javascriptCode.equals(this.javascriptCode);
		}

		return matches;
	}
}
