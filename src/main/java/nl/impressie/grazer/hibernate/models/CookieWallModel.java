package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.model.DB.CookieWall;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_cookie_wall")
@Table(name = "hibernate_cookie_wall")
@Getter
@Setter
public class CookieWallModel {
	public CookieWallModel() {}
	public CookieWallModel(CookieWall cookieWall) {
		setUrl(cookieWall.getUrl());
		setCss(cookieWall.getCssString());
		setData(cookieWall.getDataString());
		setMethod(cookieWall.getMethodString());
	}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	private String url;

	@Expose
	private String method, data, css;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "page_id")
	private PageModel page;

//	public Connection.Method getMethod() { return AltPage.parseMethodString(method); }
//
//	public Map getData() { return AltPage.dataStringToMap(data); }
//
//	public SeReDe getCss() { return SeReDe.loadSeReDe(css); }

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		boolean matches = false;
		if(object instanceof CookieWallModel) {
			CookieWallModel model = (CookieWallModel)object;

			matches = model.css.equals(css);
			if(matches) matches = model.data.equals(data);
			if(matches) matches = model.method.equals(method);
			if(matches) matches = model.url.equals(url);
		}
		return matches;
	}
}
