package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.dao.TypeRoleDao;
import nl.impressie.grazer.factories.CollectionFactory;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Map;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_user_account_type")
@Table(name = "hibernate_user_account_type")
@Getter
@Setter
public class AccountTypeModel {
	@Expose
	private String name;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Transient
	private Map<String, GrantedAuthority> roles = CollectionFactory.createMap();

	public AccountTypeModel() {}

	public void setId(Long id) {
		this.id = id;
		roles = TypeRoleDao.findAuthoritieMapByTypeId(id);
	}

	public boolean hasRole(String rolename) {
		return roles.containsKey(rolename);
	}

	public Map<String, GrantedAuthority> getAuthorities() { return roles; }
	public void setAuthorities(Map roles) { this.roles = roles; }
}
