package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Impressie
 */
@Entity(name = "unknown_tag")
@Table(name = "unknown_tag")
@Getter
@Setter
public class UnknownTagModel {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;
	@Expose
	private String type;
	@Expose
	private String tag;
	@Expose
	private String url;
	@Expose
	private Boolean ignored;

	public UnknownTagModel() { }
}
