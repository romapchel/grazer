package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.model.DB.AdditionalSourceLink;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_additional_source_link")
@Table(name = "hibernate_additional_source_link")
@Getter
@Setter
public class AdditionalSourceLinkModel {

	public AdditionalSourceLinkModel() {}
	public AdditionalSourceLinkModel(AdditionalSourceLink source) {
		setId(Long.valueOf(source.getId().toString()));
		setUrl(source.getUrl());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	private String url;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "page_id")
	private PageModel page;

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof AdditionalSourceLinkModel)
			return ((AdditionalSourceLinkModel)object).id == id;

		return false;
	}
}
