package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.model.DB.TagReplacement;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_tag_replacement")
@Table(name = "hibernate_tag_replacement")
@Getter
@Setter
public class TagReplacementModel {
	public TagReplacementModel() {}
	public TagReplacementModel(TagReplacement replacement) {
		setId(Long.valueOf(replacement.getId().toString()));
		setReplaceText(replacement.getReplaceText());
		setReplaceWith(replacement.getReplaceWith());
		setTagType(replacement.getTagType());
	}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	private String tagType, replaceText, replaceWith;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "page_id")
	private PageModel page;

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof TagReplacementModel)
			return ((TagReplacementModel)object).id == id;

		return false;
	}
}
