package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.model.DB.PageSearch;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_page_search")
@Table(name = "hibernate_page_search")
@Getter
@Setter
public class PageSearchModel {
	public PageSearchModel() {}
	public PageSearchModel(PageSearch pageSearch) {
		this.setData(pageSearch.getDataString());
		this.setHeaders(pageSearch.getHeaderString());
		this.setJsonPageKey(pageSearch.getPageKey());
		this.setJsonUrl(pageSearch.getJsonUrl());
		this.setKey(pageSearch.getKey());
		this.setMethod(pageSearch.getMethodString());
	}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	private String key;

	@Expose
	private String jsonUrl, jsonPageKey;

	@Expose
	private String method;

	@Expose
	private String headers, data;

//	public Connection.Method getMethod() { return AltPage.parseMethodString(method); }
//
//	public Map getHeader() { return AltPage.dataStringToMap(headers); }
//	public Map getData() { return AltPage.dataStringToMap(data); }

//	public void setHeaders(Map headers) { this.headers = AltPage.MapToDataString(headers); }
//	public void setData(Map data) { this.data = AltPage.MapToDataString(data); }
//
//	public void setMethod(Connection.Method method) { this.method = AltPage.MethodToString(method); }

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof PageSearchModel) {
			boolean matches = true;

			if(matches) matches = ((PageSearchModel) object).key.equals(key);
			if(matches) matches = ((PageSearchModel) object).jsonUrl.equals(jsonUrl);
			if(matches) matches = ((PageSearchModel) object).jsonPageKey.equals(jsonPageKey);
			if(matches) matches = ((PageSearchModel) object).method.equals(method);
			if(matches) matches = ((PageSearchModel) object).headers.equals(headers);
			if(matches) matches = ((PageSearchModel) object).data.equals(data);

			return matches;
		}

		return false;
	}
}
