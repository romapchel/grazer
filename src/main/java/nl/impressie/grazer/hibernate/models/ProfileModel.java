package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_profile")
@Table(name = "hibernate_profile")
@Getter
@Setter
public class ProfileModel {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	private String name;

	@Expose
	private String contentSearch, titleSearch;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserModel user;

	@Expose
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "hibernate_profile_tags",
			joinColumns = { @JoinColumn(name = "profile_id") },
			inverseJoinColumns = { @JoinColumn(name = "tag_id") }
	)
	private Set<TagModel> tags;

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof ProfileModel)
			return ((ProfileModel)object).id == id;

		return false;
	}
}
