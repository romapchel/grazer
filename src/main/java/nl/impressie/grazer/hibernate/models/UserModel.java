package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.util.XMLUtil;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_user")
@Table(name = "hibernate_user")
@Getter
@Setter
public class UserModel {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "active_profile_id")
	private ProfileModel activeProfile;

	@Expose
	private String username;
	@Expose
	private String mollieId;
	@Expose
	@ColumnDefault("False")
	private Boolean enabled;

	private String password;

	@Expose
	private Integer lastLogin, currentLogin;

	@Expose
	private String organisation;
	@Expose
	private String initials, tussenvoegsel, lastname;
	@Expose
	private String mail;
	@Expose
	private String street, housenumber, zip, city;

	@Expose
	private Long created;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hibernate_user_account_type_id")
	private AccountTypeModel type;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "user_id")
	@Expose
	private Set<ProfileModel> profiles;

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof UserModel)
			return ((UserModel)object).id == id;

		return false;
	}
}
