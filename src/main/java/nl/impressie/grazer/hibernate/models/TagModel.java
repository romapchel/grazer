package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_tag")
@Table(name = "hibernate_tag")
@Getter
@Setter
public class TagModel {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	private String name, description, code;

	@Expose
	private String type;

	@Expose
	private Boolean disabled;

	@ManyToMany(mappedBy = "tags")
	private Set<ProfileModel> profiles;

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof TagModel)
			return ((TagModel)object).id == id;

		return false;
	}
}
