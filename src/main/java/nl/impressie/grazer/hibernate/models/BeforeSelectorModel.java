package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.model.DB.BeforeSelector;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_before_selector")
@Table(name = "hibernate_before_selector")
@Getter
@Setter
public class BeforeSelectorModel {
	public BeforeSelectorModel() {}
	public BeforeSelectorModel(BeforeSelector source) {
		setId(Long.valueOf(source.getId().toString()));
		setType(source.getName());
		setSelector(source.getSelector());
	}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;

	@Expose
	private String type, selector;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "page_id")
	private PageModel page;

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof BeforeSelectorModel)
			return ((BeforeSelectorModel)object).id == id;

		return false;
	}
}
