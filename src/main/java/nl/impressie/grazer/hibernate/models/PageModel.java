package nl.impressie.grazer.hibernate.models;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.model.DB.AdditionalSourceLink;
import nl.impressie.grazer.model.DB.BeforeSelector;
import nl.impressie.grazer.model.DB.TagReplacement;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.XMLUtil;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
@Entity(name = "hibernate_page")
@Table(name = "hibernate_page")
@Getter
@Setter
public class PageModel {
	public PageModel() {}
	public PageModel(Website page) {
		Map current = page.getCurrent();

		setId(Long.valueOf(page.getId().toString()));

		setName(page.getName());
		setUrl(page.getUrl().toString());
		setDescription(page.getDescription());
		setNieuwszender(page.getNieuwszender());

		String depthString = current.get(WebsiteDao.MAX_DEPTH).toString();
		if(depthString.length() > 0)
			setMaxDepth(Integer.valueOf(depthString));

		setDateFormat(page.getDateFormat());
		setMoreInfo(page.getMoreInfo());

		setDisabled(page.isDisabled());
		setDefective(page.isDefective());
		setHealthy(page.isHealthy());

		setJavascript(new JavascriptModel(page.getJavascriptObject()));

		setPageSearch(new PageSearchModel(page.getPageSearch()));

		setCookieWall(new CookieWallModel(page.getCookieWall()));

		Set set = new HashSet();
		for(TagReplacement bs: page.getReplacements()) {
			set.add(new TagReplacementModel(bs));
		}
		setReplacements(set);

		set = new HashSet();
		for(AdditionalSourceLink bs: page.getAddtionalLinkSources()) {
			set.add(new AdditionalSourceLinkModel(bs));
		}
		setAddtionalLinks(set);

		set = new HashSet();
		for(BeforeSelector bs: page.getBeforeSelectors()) {
			set.add(new BeforeSelectorModel(bs));
		}
		setBeforeSelectors(set);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Expose
	private Long id;
	@Expose
	private String name, url, description, nieuwszender;

	@Expose
	private Integer maxDepth;
	@Expose
	private Long lastCrawlDate;
	@Expose
	private String dateFormat;
	@Expose
	private String moreInfo;

	@Expose
	private Boolean disabled, defective, healthy;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "javascript_id")
	private JavascriptModel javascript;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "page_search_id")
	private PageSearchModel pageSearch;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cookie_wall_id")
	private CookieWallModel cookieWall;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "page_id")
	@Expose
	private Set<TagReplacementModel> replacements;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "page_id")
	@Expose
	private Set<AdditionalSourceLinkModel> addtionalLinks;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "page_id")
	@Expose
	private Set<BeforeSelectorModel> beforeSelectors;

	@Expose
	private String date, title, crawl, article, content, snippet, rechtsgebied, nieuwsSoort, rubrieken;

//	public SeReDe getDate() { return SeReDe.loadSeReDe(date); }
//	public SeReDe getTitle() { return SeReDe.loadSeReDe(title); }
//	public SeReDe getCrawl() { return SeReDe.loadSeReDe(crawl); }
//	public SeReDe getArticle() { return SeReDe.loadSeReDe(article); }
//	public SeReDe getContent() { return SeReDe.loadSeReDe(content); }
//	public SeReDe getSnippet() { return SeReDe.loadSeReDe(snippet); }
//	public SeReDe getRechtsgebied() { return SeReDe.loadSeReDe(rechtsgebied); }
//	public SeReDe getNieuwssoort() { return SeReDe.loadSeReDe(nieuwsSoort); }
//	public SeReDe getRubrieken() { return SeReDe.loadSeReDe(rubrieken); }

	@Override
	public String toString() {
		return XMLUtil.getGson().toJson(this);
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof PageModel)
			return ((PageModel)object).id == id;

		return false;
	}
}
