package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.AccountTypeModel;
import nl.impressie.grazer.hibernate.repositories.AccountTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class AccountTypeService {
	@Autowired
	private AccountTypeRepository accountTypeRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public AccountTypeModel save(AccountTypeModel tag) {
		tag = entityManager.merge(tag);
		return tag;
	}

	public AccountTypeModel get(String id) {
		return get(Long.valueOf(id));
	}
	public AccountTypeModel get(Long id) {
		Optional<AccountTypeModel> client = accountTypeRepository.findById(id);
		if(client.isPresent())
			return client.get();

		return null;
	}

	public List<AccountTypeModel> getAll() {
		return accountTypeRepository.findAll();
	}
}