package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.TagReplacementModel;
import nl.impressie.grazer.hibernate.repositories.TagReplacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class TagReplacementService {
	@Autowired
	private TagReplacementRepository repository;

	public List<TagReplacementModel> list() {
		return repository.findAll();
	}

	public TagReplacementModel get(Long id) {
		Optional<TagReplacementModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}