package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.TagModel;
import nl.impressie.grazer.hibernate.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class TagService {
	@Autowired
	private TagRepository repository;

	public List<TagModel> list() {
		return repository.findAll();
	}

	public TagModel getByName(String name, String type) {
		String noSubGroup = ": Geen subrechtsgebied";
		if(name.toLowerCase().endsWith(noSubGroup.toLowerCase()))
			name = name.substring(0, name.length() - noSubGroup.length()).trim();

		TagModel like = new TagModel();
		like.setName(name);
		like.setType(type);

		return findLike(like);
	}

	public List<TagModel> getByType(String type) {
		TagModel like = new TagModel();
		like.setType(type);

		return repository.findAll(Example.of(like));
	}

	private TagModel findLike(TagModel like) {
		Optional<TagModel> found = repository.findOne(Example.of(like));

		if(found.isPresent())
			return found.get();

		return null;
	}

	public TagModel get(Long id) {
		Optional<TagModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}