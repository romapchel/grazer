package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.BeforeSelectorModel;
import nl.impressie.grazer.hibernate.repositories.BeforeSelectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class BeforeSelectorService {
	@Autowired
	private BeforeSelectorRepository repository;

	public List<BeforeSelectorModel> list() {
		return repository.findAll();
	}

	public BeforeSelectorModel get(Long id) {
		Optional<BeforeSelectorModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}