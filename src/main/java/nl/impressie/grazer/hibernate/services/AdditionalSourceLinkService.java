package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.AdditionalSourceLinkModel;
import nl.impressie.grazer.hibernate.repositories.AdditionalSourceLinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class AdditionalSourceLinkService {
	@Autowired
	private AdditionalSourceLinkRepository repository;

	public List<AdditionalSourceLinkModel> list() {
		return repository.findAll();
	}

	public AdditionalSourceLinkModel get(Long id) {
		Optional<AdditionalSourceLinkModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}