package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.PageSearchModel;
import nl.impressie.grazer.hibernate.repositories.PageSearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class PageSearchService {
	@Autowired
	private PageSearchRepository repository;

	public List<PageSearchModel> list() {
		return repository.findAll();
	}

	public PageSearchModel get(Long id) {
		Optional<PageSearchModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}