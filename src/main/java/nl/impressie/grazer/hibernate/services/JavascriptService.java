package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.JavascriptModel;
import nl.impressie.grazer.hibernate.repositories.JavascriptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class JavascriptService {
	@Autowired
	private JavascriptRepository repository;

	public List<JavascriptModel> list() {
		return repository.findAll();
	}

	public JavascriptModel get(Long id) {
		Optional<JavascriptModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}