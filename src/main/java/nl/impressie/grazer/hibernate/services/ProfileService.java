package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.ProfileModel;
import nl.impressie.grazer.hibernate.repositories.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class ProfileService {
	@Autowired
	private ProfileRepository repository;

	public List<ProfileModel> list() {
		return repository.findAll();
	}

	public ProfileModel get(Long id) {
		Optional<ProfileModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}