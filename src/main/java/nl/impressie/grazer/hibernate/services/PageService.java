package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.PageModel;
import nl.impressie.grazer.hibernate.repositories.PageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class PageService {
	@Autowired
	private PageRepository repository;

	public List<PageModel> list() {
		return repository.findAll();
	}

	public PageModel get(Long id) {
		Optional<PageModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}