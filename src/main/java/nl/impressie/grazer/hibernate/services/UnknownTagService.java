package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.UnknownTagModel;
import nl.impressie.grazer.hibernate.repositories.UnknownTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class UnknownTagService {
	@Autowired
	private UnknownTagRepository unknownTagRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public UnknownTagModel save(UnknownTagModel tag) {
		tag = entityManager.merge(tag);
		return tag;
	}

	public UnknownTagModel get(String id) {
		try {
			Long longId = Long.valueOf(id);
			return get(longId);
		} catch(NumberFormatException e) {
			return null;
		}
	}
	public UnknownTagModel get(Long id) {
		Optional<UnknownTagModel> client = unknownTagRepository.findById(id);
		if(client.isPresent())
			return client.get();

		return null;
	}

	public List<UnknownTagModel> getAll() {
		return unknownTagRepository.findAll();
	}

	public UnknownTagModel get(String tag, String type, String url) {
		UnknownTagModel like = new UnknownTagModel();
		like.setTag(tag);
		like.setType(type);
		like.setUrl(url);

		Optional<UnknownTagModel> found = unknownTagRepository.findOne(Example.of(like));

		if(found.isPresent())
			return found.get();

		return null;
	}
}