package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.hibernate.models.CookieWallModel;
import nl.impressie.grazer.hibernate.repositories.CookieWallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
public class CookieWallService {
	@Autowired
	private CookieWallRepository repository;

	public List<CookieWallModel> list() {
		return repository.findAll();
	}

	public CookieWallModel get(Long id) {
		Optional<CookieWallModel> optional = repository.findById(id);
		if(optional.isPresent())
			return optional.get();

		return null;
	}
}