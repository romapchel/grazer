package nl.impressie.grazer.hibernate.services;

import nl.impressie.grazer.enums.Tables;
import nl.impressie.grazer.hibernate.models.UserModel;
import nl.impressie.grazer.hibernate.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author Impressie
 */
@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository repository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public UserModel save(UserModel user) {
		user = entityManager.merge(user);
		return user;
	}

	private final String username = "username", mail = "mail";

	public UserModel findByUsername(String username) {
		return getByCaseInsensitive(this.username, username);
	}
	public UserModel getByEmail(String email) { return getByCaseInsensitive(this.mail, email); }


	public void delete(UserModel user) {
		entityManager.remove(user);
	}

	public List<UserModel> listAll() {
		return  this.repository.findAll();
	}


	protected UserModel getByCaseInsensitive(String parameter, String parameterValue) {
		final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		final CriteriaQuery<UserModel> query = criteriaBuilder.createQuery(UserModel.class);
		final Root<UserModel> root = query.from(UserModel.class);
		query.where(
				criteriaBuilder.equal(criteriaBuilder.lower(root.get(parameter)), parameterValue.toLowerCase())
		);
		query.select(root);
		try {
			return entityManager.createQuery(query).getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}

	public UserModel get(Long id) {
		Optional<UserModel> client = repository.findById(id);
		if(client.isPresent())
			return client.get();

		return null;
	}
}