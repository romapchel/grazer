package nl.impressie.grazer.enums;

import nl.impressie.grazer.util.DatabaseUtils.Parameter;

/**
 * @author Impressie
 */
public class Tables {
	private interface DatabaseTable {
		DatabaseColumn getDBC();
	}

	private static class DatabaseColumn {
		private String columnName;
		private String columnTitle;
		private Parameter.Type type;

		DatabaseColumn(String columnName) {
			this(columnName, columnName);
		}
		DatabaseColumn(String columnName, String columnTitle) {
			this(columnName, columnTitle, null);
		}
		DatabaseColumn(String columnName, Parameter.Type type) {
			this(columnName, columnName, type);
		}
		DatabaseColumn(String columnName, String columnTitle, Parameter.Type type) {
			this.columnName = columnName;
			this.columnTitle = columnTitle;
			this.type = type;
		}

		public Parameter getParameter(Object value) {
			Parameter parameter = new Parameter(columnName, value);
			if(type != null)
				parameter.setType(type);

			return parameter;
		}

		public Parameter getParameter(Object value, Parameter.Comparator comparator) {
			return getParameter(value).setComparator(comparator);
		}

		public String getColumnTitle() { return columnTitle; }

		public Parameter getSortParameter(Parameter.SortOrder sortOrder) {
			return new Parameter(columnName, sortOrder);
		}
	}

	public enum SubscriptionPayment implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		PAYMENT_ID(new DatabaseColumn("payment_id", Parameter.Type.INT)),
		INVOICE_ID(new DatabaseColumn("invoice_id", Parameter.Type.INT)),
		TIMESTAMP(new DatabaseColumn("timestamp")),
		MOLLIE_PAYMENT_ID(new DatabaseColumn("mollie_payment_id"));

		public final static String TABLE_NAME = "subscription_payment";

		private DatabaseColumn dbc;
		SubscriptionPayment(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum UserType implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		USER_ID(new DatabaseColumn("user_id", Parameter.Type.INT)),
		TYPE_ID(new DatabaseColumn("type_id", Parameter.Type.INT)),
		END_DATE(new DatabaseColumn("end_date", Parameter.Type.INT));

		public final static String TABLE_NAME = "user_type";

		private DatabaseColumn dbc;
		UserType(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum TagPage implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		PAGE_ID(new DatabaseColumn("page_id", Parameter.Type.INT)),
		TAG_ID(new DatabaseColumn("tag_id", Parameter.Type.INT));

		public final static String TABLE_NAME = "tag_page";

		private DatabaseColumn dbc;
		TagPage(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum Tag implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		NAME(new DatabaseColumn("name")),
		TYPE(new DatabaseColumn("type")),
		CODE(new DatabaseColumn("code")),
		DESCRIPTION(new DatabaseColumn("description")),
		DISABLED(new DatabaseColumn("disabled", Parameter.Type.BOOLEAN));

		public final static String TABLE_NAME = "tag";

		private DatabaseColumn dbc;
		Tag(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum ProfileTag implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		PROFILE_ID(new DatabaseColumn("profile_id", Parameter.Type.INT)),
		TAG_ID(new DatabaseColumn("tag_id", Parameter.Type.INT));

		public final static String TABLE_NAME = "user_tag";

		private DatabaseColumn dbc;
		ProfileTag(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum Profile implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		USER_ID(new DatabaseColumn("user_id", Parameter.Type.INT)),
		PROFILE_NAME(new DatabaseColumn("profile_name")),
		CONTENT_SEARCH(new DatabaseColumn("content_search_term")),
		TITLE_SEARCH(new DatabaseColumn("title_search_term")),
		PROFILE_SITE(new DatabaseColumn("user_site")),
		PROFILE_TAG(new DatabaseColumn("profile_tag")),
		PROFILE_SEARCH(new DatabaseColumn("profile_search"));

		public final static String TABLE_NAME = "profile";

		private DatabaseColumn dbc;
		Profile(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum Payment implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		USER_ID(new DatabaseColumn("user_id", Parameter.Type.INT)),
		BUNDLE_ID(new DatabaseColumn("bundle_id", Parameter.Type.INT)),
		PAYMENT_ID(new DatabaseColumn("initial_payment_id")),
		SUBSCRIPTION_ID(new DatabaseColumn("subscription_payment_id")),
		TIMESTAMP(new DatabaseColumn("timestamp", Parameter.Type.INT));

		public final static String TABLE_NAME = "payment";

		private DatabaseColumn dbc;
		Payment(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum Navigation implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		PAGE_ID(new DatabaseColumn("page_id", Parameter.Type.INT)),
		URL(new DatabaseColumn("url")),
		PAGING(new DatabaseColumn("paging")),
		AFTER(new DatabaseColumn("after")),
		JUMP(new DatabaseColumn("jump")),
		START(new DatabaseColumn("start")),
		END(new DatabaseColumn("end"));

		public final static String TABLE_NAME = "navigation";

		private DatabaseColumn dbc;
		Navigation(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum Invoice implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		PAYMENT_ID(new DatabaseColumn("payment_id", Parameter.Type.INT)),
		BUNDLE_ID(new DatabaseColumn("bundle_id", Parameter.Type.INT)),
		USER_ID(new DatabaseColumn("user_id", Parameter.Type.INT)),
		TIMESTAMP(new DatabaseColumn("timestamp", Parameter.Type.INT)),
		DESCRIPTION(new DatabaseColumn("description")),
		COMPANY(new DatabaseColumn("company")),
		NAME(new DatabaseColumn("name")),
		ADRESS(new DatabaseColumn("adress")),
		ADRESS2(new DatabaseColumn("adress2")),
		SUBTOTAL(new DatabaseColumn("subtotal")),
		VAT(new DatabaseColumn("vat")),
		TOTAL(new DatabaseColumn("total"));

		public final static String TABLE_NAME = "invoice";

		private DatabaseColumn dbc;
		Invoice(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum Bundle implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		PRICE(new DatabaseColumn("price", Parameter.Type.INT)),
		TIMES(new DatabaseColumn("times", Parameter.Type.INT)),
		INTERVAL_NUMBER(new DatabaseColumn("interval_number", Parameter.Type.INT)),
		INTERVAL_TYPE(new DatabaseColumn("interval_type")),
		DESCRIPTION(new DatabaseColumn("description")),
		TYPE_ID(new DatabaseColumn("type_id", Parameter.Type.INT));

		public final static String TABLE_NAME = "bundle";

		private DatabaseColumn dbc;
		Bundle(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum ArticleHealth implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		INDEXED_ON(new DatabaseColumn("indexed_on", Parameter.Type.INT)),
		INDEXED_DATE(new DatabaseColumn("indexed_date", Parameter.Type.INT)),
		CHECKED_ON(new DatabaseColumn("checked_on", Parameter.Type.INT)),
		CHECKED_DATE(new DatabaseColumn("checked_date", Parameter.Type.INT)),
		INDEXED_CONTENT(new DatabaseColumn("indexed_content")),
		INDEXED_TITLE(new DatabaseColumn("indexed_title")),
		CHECKED_CONTENT(new DatabaseColumn("checked_content")),
		CHECKED_TITLE(new DatabaseColumn("checked_title")),
		SKIPPED(new DatabaseColumn("skipped")),
		HEALTHY_CONTENT(new DatabaseColumn("healthy_content", Parameter.Type.BOOLEAN)),
		HEALTHY_TITLE(new DatabaseColumn("healthy_title", Parameter.Type.BOOLEAN)),
		HEALTHY_DATE(new DatabaseColumn("healthy_date", Parameter.Type.BOOLEAN)),
		HEALTHY_OVERALL(new DatabaseColumn("healthy_overall", Parameter.Type.BOOLEAN)),
		PAGE_ID(new DatabaseColumn("page_id", Parameter.Type.INT)),
		PAGE_URL(new DatabaseColumn("page_url"));

		public final static String TABLE_NAME = "article_health";

		private DatabaseColumn dbc;
		ArticleHealth(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum AltPage implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		PAGE_ID(new DatabaseColumn("page_id", Parameter.Type.INT)),
		MOMENT(new DatabaseColumn("moment")),
		URL(new DatabaseColumn("url")),
		METHOD(new DatabaseColumn("method")),
		DATA(new DatabaseColumn("data"));

		public final static String TABLE_NAME = "altpage";

		private DatabaseColumn dbc;
		AltPage(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum AccountType implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		NAME(new DatabaseColumn("name"));

		public final static String TABLE_NAME = "type";

		private DatabaseColumn dbc;
		AccountType(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum BeforeSelector implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		NAME(new DatabaseColumn("name")),
		SELECTOR(new DatabaseColumn("selector")),
		PAGE_ID(new DatabaseColumn("page_id", Parameter.Type.INT));


		public final static String TABLE_NAME = "before";

		private DatabaseColumn dbc;
		BeforeSelector(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum TypeRole implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		TYPE_ID(new DatabaseColumn("type_id", Parameter.Type.INT)),
		ROLE_NAME(new DatabaseColumn("role_name"));

		public final static String TABLE_NAME = "type_role";

		private DatabaseColumn dbc;
		TypeRole(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum Verification implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		USER_ID(new DatabaseColumn("user_id", Parameter.Type.INT)),
		VERIFICATION_CODE(new DatabaseColumn("code")),
		CREATED_ON(new DatabaseColumn("created_on", Parameter.Type.INT));

		public final static String TABLE_NAME = "user_verification";

		private DatabaseColumn dbc;
		Verification(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum Website implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		NAME(new DatabaseColumn("name")),
		URL(new DatabaseColumn("url")),
		NIEUWSZENDER(new DatabaseColumn("nieuwszender")),
		LAST_CRAWL_DATE(new DatabaseColumn("last_crawl_date", Parameter.Type.INT)),
		MAX_DEPTH(new DatabaseColumn("max_depth", Parameter.Type.INT)),
		DATE_FORMAT(new DatabaseColumn("date_format")),
		MORE_INFO(new DatabaseColumn("more_info")),
		DATE(new DatabaseColumn("date")),
		TITLE(new DatabaseColumn("title")),
		CONTENT(new DatabaseColumn("content")),
		SNIPPET(new DatabaseColumn("snippet")),
		RECHTSGEBIED(new DatabaseColumn("rechtsgebied")),
		NIEUWS_SOORT(new DatabaseColumn("nieuws_soort")),
		RUBRIEKEN(new DatabaseColumn("rubrieken")),
		CRAWL(new DatabaseColumn("crawl")),
		ARTICLE(new DatabaseColumn("article")),
		SEARCH_KEY(new DatabaseColumn("search_key")),
		SEARCH_METHOD(new DatabaseColumn("search_method")),
		SEARCH_HEADER(new DatabaseColumn("search_header")),
		SEARCH_DATA(new DatabaseColumn("search_data")),
		JSON_URL(new DatabaseColumn("json_url")),
		PAGE_KEY(new DatabaseColumn("page_key")),
		JS(new DatabaseColumn("js")),
		JS_TEST_ID(new DatabaseColumn("js_test_id")),
		DISABLED(new DatabaseColumn("disabled", Parameter.Type.BOOLEAN)),
		HEALTHY(new DatabaseColumn("healthy", Parameter.Type.BOOLEAN)),
		DEFECTIVE(new DatabaseColumn("defective", Parameter.Type.BOOLEAN)),
		DESCRIPTION(new DatabaseColumn("description")),
		COOKIE_URL(new DatabaseColumn("cookie_url")),
		COOKIE_METHOD(new DatabaseColumn("cookie_method")),
		COOKIE_DATA(new DatabaseColumn("cookie_data")),
		COOKIE_CSS(new DatabaseColumn("cookie_css"));

		public final static String TABLE_NAME = "page";

		private DatabaseColumn dbc;
		Website(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}

	public enum User implements DatabaseTable {
		ID(new DatabaseColumn("id", Parameter.Type.INT)),
		USERNAME(new DatabaseColumn("username")),
		PASSWORD(new DatabaseColumn("password")),
		ORGANISATION(new DatabaseColumn("organisation")),
		INITIALS(new DatabaseColumn("initials")),
		TUSSENVOEGSELS(new DatabaseColumn("tsvg")),
		LASTNAME(new DatabaseColumn("lastname")),
		MAIL(new DatabaseColumn("mail")),
		MOLLIE_ID(new DatabaseColumn("mollie_id")),
		TYPE_ID(new DatabaseColumn("type_id", Parameter.Type.INT)),
		STREET(new DatabaseColumn("street")),
		HOUSENUMBER(new DatabaseColumn("housenumber")),
		ZIP(new DatabaseColumn("zip")),
		CITY(new DatabaseColumn("city")),
		ACTIVE_PROFILE_ID(new DatabaseColumn("active_profile_id", Parameter.Type.INT)),
		ENABLED(new DatabaseColumn("enabled", Parameter.Type.BOOLEAN)),
		LAST_LOGIN(new DatabaseColumn("last_login", Parameter.Type.INT)),
		CURRENT_LOGIN(new DatabaseColumn("current_login", Parameter.Type.INT));

		public final static String TABLE_NAME = "user";

		private DatabaseColumn dbc;
		User(DatabaseColumn dbc) { this.dbc = dbc; }

		@Override
		public DatabaseColumn getDBC() {
			return dbc;
		}
	}
}
