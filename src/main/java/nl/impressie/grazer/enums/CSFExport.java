package nl.impressie.grazer.enums;

import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.SeReDe;
import nl.impressie.grazer.model.DB.*;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.LogUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public enum CSFExport {
	FULL (
			"Volledige export",
			CollectionFactory.createList (
					WebsiteDao.getColumns(),
					WebsiteDao.NAV,
					WebsiteDao.BEFORE,
					WebsiteDao.ALT,
					WebsiteDao.REPLACEMENTS,
					WebsiteDao.ADDITIONAL_PAGES
			),
			CollectionFactory.createMap (
					new CollectionFactory.MapPair(WebsiteDao.NAV, "Navigatie"),
					new CollectionFactory.MapPair(WebsiteDao.BEFORE, "before"),
					new CollectionFactory.MapPair(WebsiteDao.ALT, "alt"),
					new CollectionFactory.MapPair(WebsiteDao.REPLACEMENTS, "replacements"),
					new CollectionFactory.MapPair(WebsiteDao.ADDITIONAL_PAGES, "additional_pages")
			),
			CollectionFactory.createMap (
					new CollectionFactory.MapPair(WebsiteDao.NAV, new getterFunction(Navigation.class, "CSFExport")),
					new CollectionFactory.MapPair(WebsiteDao.BEFORE, new getterFunction(BeforeSelector.class, "CSFExport", true)),
					new CollectionFactory.MapPair(WebsiteDao.ALT, new getterFunction(AltPage.class, "CSFExport", true)),
					new CollectionFactory.MapPair(WebsiteDao.REPLACEMENTS, new getterFunction(TagReplacement.class, "CSFExport", true)),
					new CollectionFactory.MapPair(WebsiteDao.ADDITIONAL_PAGES, new getterFunction(AdditionalSourceLink.class, "CSFExport", true))
			)
	),
	PARTIAL (
			"Gedeeltelijke export",
			CollectionFactory.createList (
					WebsiteDao.ID,
					WebsiteDao.NAME,
					WebsiteDao.URL,
					WebsiteDao.NIEUWSZENDER
			)
	),
	PETER (
			"Peter export",
			CollectionFactory.createList (
					WebsiteDao.ID,
					WebsiteDao.RUBRIEKEN,
					WebsiteDao.NAME,
					WebsiteDao.NIEUWSZENDER,
					WebsiteDao.DESCRIPTION,
					WebsiteDao.NIEUWS_SOORT,
					WebsiteDao.RECHTSGEBIED
			),
			CollectionFactory.createMap (
					new CollectionFactory.MapPair(WebsiteDao.RUBRIEKEN, "Maatschappelijke sector"),
					new CollectionFactory.MapPair(WebsiteDao.NAME, "Website"),
					new CollectionFactory.MapPair(WebsiteDao.NIEUWSZENDER, "Nieuwszender"),
					new CollectionFactory.MapPair(WebsiteDao.DESCRIPTION, "Over de nieuwszender"),
					new CollectionFactory.MapPair(WebsiteDao.NIEUWS_SOORT, "Informatiesoort"),
					new CollectionFactory.MapPair(WebsiteDao.RECHTSGEBIED, "Rechtsgebied")
			),
			CollectionFactory.createMap (
					new CollectionFactory.MapPair(WebsiteDao.RUBRIEKEN, new getterFunction(SeReDe.class, "getDefaultValue")),
					new CollectionFactory.MapPair(WebsiteDao.NIEUWS_SOORT, new getterFunction(SeReDe.class, "getDefaultValue")),
					new CollectionFactory.MapPair(WebsiteDao.RECHTSGEBIED, new getterFunction(SeReDe.class, "getDefaultValue"))
			)
	);

	static private class getterFunction {
		private Class objectClass;
		private String methodName;
		private boolean staticFunction;
		private boolean DBValIsList;

		public getterFunction(Class objectClass, String methodName) {
			this(objectClass, methodName, false);
		}
		public getterFunction(Class objectClass, String methodName, boolean DBValIsList) {
			this(objectClass, methodName, DBValIsList, false);
		}
		public getterFunction(Class objectClass, String methodName, boolean DBValIsList, boolean staticFunction) {
			this.objectClass = objectClass;
			this.methodName = methodName;
			this.DBValIsList = DBValIsList;
			this.staticFunction = staticFunction;
		}

		public String getValue(String column, Model model) {
			StringBuilder builder = new StringBuilder();
			try {
				if(DBValIsList) {
					List items = model.getDBValue(column, List.class);
					for(Object ob: items) {
						if(builder.length() > 0)
							builder.append(LIST_SEPERATOR);
						builder.append(runMethod(ob, objectClass, methodName, staticFunction));
					}
				} else {
					Object dbValue = model.getDBValue(column, objectClass);
					if(dbValue != null)
						builder.append(runMethod(dbValue, objectClass, methodName, staticFunction));
				}
			} catch(NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
				LogUtil.logException(e);
			}
			return builder.toString();
		}

		private String runMethod(Object object, Class objectClass, String methodName, boolean staticFunction)
				throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
			Method method = objectClass.getMethod(methodName);

			if(staticFunction)
				return method.invoke(null, object).toString();

			Object result = method.invoke(object);
			if(result != null)
				return result.toString();
			return "";
		}
	}

	private String title;
	private List<String> columns;
	private Map<String, String> columnTitles;
	private Map<String, getterFunction> exportFunctions;

	private static final String SEPERATOR = "|";
	private static final String LIST_SEPERATOR = "&@&";

	public static CSFExport getByImportedColumns(List<String> columns) {
		for(CSFExport export: CSFExport.values()) {
			if(export.getColumns().size() != columns.size())
				continue;

			List<String> titles = export.getColumnTitles();
			boolean matches = true;
			for(int i = 0; i < titles.size(); i++) {
				if(!titles.get(i).equals(columns.get(i))) {
					matches = false;
					break;
				}
			}
			if(matches)
				return export;
		}
		return null;
	}

	public static String[] splitList(String loadable) {
		return loadable.split(LIST_SEPERATOR);
	}

	CSFExport(String title, List<String> columns) {
		this(title, columns, CollectionFactory.createMap());
	}
	CSFExport(String title, List<String> columns, Map<String, String> columnTitles) {
		this(title, columns, columnTitles, CollectionFactory.createMap());
	}
	CSFExport(String title, List<String> columns, Map<String, String> columnTitles, Map<String, getterFunction> exportFunctions) {
		this.title = title;
		this.columns = columns;
		this.columnTitles = columnTitles;
		this.exportFunctions = exportFunctions;
	}

	public String getTitle() { return title; }
	public List<String> getColumns() {
		return columns;
	}
	private List<String> getColumnTitles() {
		List titles = CollectionFactory.createList();

		for(String column: columns) {
			String title = (columnTitles.containsKey(column) ? columnTitles.get(column) : column);
			titles.add(title);
		}

		return titles;
	}

	public String getSeperatorRow() {
		return String.format("\"sep=%s\"", SEPERATOR);
	}
	public String getTitleRow() {
		StringBuilder builder = new StringBuilder();
		for(String title: getColumnTitles()) {
			if(builder.length() > 0)
				builder.append(SEPERATOR);
			builder.append(title);
		}
		return builder.toString();
	}
	public String getExportRow(Model model) {
		StringBuilder builder = new StringBuilder();
		for(String column: columns) {
			if(builder.length() > 0)
				builder.append(SEPERATOR);

			String value;
			if(exportFunctions.containsKey(column)) {
				value = exportFunctions.get(column).getValue(column, model);
			} else {
				value = model.getDBValue(column);
			}
			builder.append(value);
		}
		return builder.toString();
	}
	public StringBuilder getCSFContent(List<Model> models) {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format("%s\n", getSeperatorRow()));
		builder.append(String.format("%s\n", getTitleRow()));

		for(Model model: models) {
			builder.append(String.format("%s\n", getExportRow(model)));
		}
		return builder;
	}
}

