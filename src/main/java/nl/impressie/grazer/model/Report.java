package nl.impressie.grazer.model;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.HealthReport;

import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class Report {
	private List healthReports;
	private List articles;
	private Map<String, ReportItem> items;

	public Report() {
		healthReports = CollectionFactory.createList();
		articles = CollectionFactory.createList();
		items = CollectionFactory.createMap();
	}

	public Map getItems() {
		Map map = CollectionFactory.createMap();

		Map combined = combineArticleItemMap();
		map.put("healthtotal", combined.get("healthtotal"));
		combined.remove("healthtotal");

		map.put("items", combined);
		map.put("pagetitle", "Totaal");
		return map;
	}

	public Map combineArticleItemMap() {
		Map map = CollectionFactory.createMap();

		for(Map articleMap : (List<Map>) articles) {
			Map itemMap = (Map) articleMap.get("items");
			String healthtotalKey = "healthtotal";
			itemMap.put(healthtotalKey, articleMap.get(healthtotalKey));
			for(Object key : itemMap.keySet()) {
				ReportItem articleItem = (ReportItem) itemMap.get(key);
				ReportItem collectionItem = new ReportItem();
				if(map.containsKey(key))
					collectionItem = (ReportItem) map.get(key);
				else
					map.put(key, collectionItem);

				collectionItem.addItem(articleItem);
			}
			itemMap.remove(healthtotalKey);
		}

		return map;
	}

	public List getArticles() {
		return articles;
	}

	public Report addHealthReport(HealthReport healthReport) {
		Map article = CollectionFactory.createMap();
		Map itemMap = CollectionFactory.createMap();
		article.put("pagetitle", healthReport.getPageTitle());
		articles.add(article);

		String healthKey = "healthy";
		String checkedKey = "checked";
		String totalKey = "total";

		healthReports.add(healthReport);

		Map<String, Object> health = healthReport.getHealth();
		Map<String, Object> titles = (Map) health.get("titles");
		for(String key : titles.keySet()) {
			String title = (String) titles.get(key);

			Map map = (Map) health.get(key);
			int healthy = safeGetInt(map, healthKey);
			int total = safeGetInt(map, totalKey);
			ReportItem item = new ReportItem(healthy, total);
			item.setTitle(title);

			itemMap.put(key, item);
//            item.addMap(map);
		}
		ReportItem item = new ReportItem();
		article.put("checkedtotal", item.setTitle("Gecontroleerde artikellen"));
		int checkedArticles = safeGetInt(health, checkedKey);
		item.addHealthy(checkedArticles);
		item.addTotal(safeGetInt(health, totalKey));

		item = new ReportItem();
		article.put("healthtotal", item.setTitle("Gezonde artikellen"));
		item.addHealthy(safeGetInt(health, healthKey));
		item.addTotal(checkedArticles);

		article.put("items", itemMap);
		return this;
	}

	public static int safeGetInt(Map map, Object key) {
		int ret = 0;

		if(map != null && map.containsKey(key)) {
			Object o = map.get(key);
			if(o instanceof Integer)
				return (Integer) o;
			try {
				ret = Integer.parseInt(o.toString());
			} catch(NumberFormatException e) {
			}//already caught by setting ret to 0
		}

		return ret;
	}


	public class ReportItem {
		private int used, total;
		private String title;

		public ReportItem() {
			this(0, 0);
		}

		public ReportItem(int used, int total) {
			this.used = used;
			this.total = total;
			title = "";
		}

		public ReportItem setTitle(String title) {
			this.title = title;
			return this;
		}

		public ReportItem addHealthy(int add) {
			used += add;
			return this;
		}

		public ReportItem addTotal(int add) {
			total += add;
			return this;
		}

		public ReportItem addMap(Map map) {
			if(map.containsKey("total"))
				addTotal(Integer.parseInt(map.get("total").toString()));

			if(map.containsKey("healthy"))
				addHealthy(Integer.parseInt(map.get("healthy").toString()));

			return this;
		}

		public int getHealthy() {
			return used;
		}

		public int getTotal() {
			return total;
		}

		public String getTitle() {
			return title;
		}

		public Double getPercentage() {
			if(total == 0)
				return 100.00;
			if(used == 0)
				return 0.00;

			Double dUsed = Double.valueOf(used);
			Double dTotal = Double.valueOf(total);

			Double percentage = (dUsed / dTotal) * 100.00;
			return percentage;
		}

		public ReportItem addItem(ReportItem item) {
			addTotal(item.getTotal());
			addHealthy(item.getHealthy());
			if(0 == getTitle().length())
				setTitle(item.getTitle());

			return this;
		}
	}
}
