package nl.impressie.grazer.model;

import nl.impressie.grazer.factories.CollectionFactory;

import java.util.Collection;
import java.util.Map;

/**
 * @author Impressie
 */
public class Pagination {
	private Map<Integer, Page> pages;

	public Pagination() {
		pages = CollectionFactory.createMap();
	}

	public Pagination addPage(Page... pages) {
		for(Page page : pages) {
			this.pages.put(page.getPagenumber(), page);
		}
		return this;
	}

	public Page getPage(String pageNumber) {
		int page = 0;
		try {
			page = Integer.parseInt(pageNumber);
		} catch(NumberFormatException e) {
		}

		return getPage(page);
	}

	public Page getPage(int pageNumber) {
		return pages.get(pageNumber);
	}

	public Collection<Page> getPages() {
		return pages.values();
	}

	public int size() {
		return pages.size();
	}
}
