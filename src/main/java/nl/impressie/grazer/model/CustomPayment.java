package nl.impressie.grazer.model;

import nl.impressie.grazer.dao.*;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.LogUtil;
import nl.stil4m.mollie.Client;
import nl.stil4m.mollie.ResponseOrError;
import nl.stil4m.mollie.domain.Payment;

import java.io.IOException;
import java.util.Map;

/**
 * @author Impressie
 */
public class CustomPayment {
	private int id;
	private int userId, bundleId;
	private String initialPaymentId, subscriptionPaymentId;
	private long timestamp;

	private static final String MOLLIE_STATUS_PAID = "paid";
	private static final String MOLLIE_STATUS_REFUNDED = "refunded";
	public static final String MOLLIE_STATUS_ACTIVE = "active";

	public CustomPayment(int id, int userId, int bundleId, String initialPaymentId, String subscriptionPaymentId, long timestamp) {
		this.id = id;
		this.userId = userId;
		this.bundleId = bundleId;
		this.initialPaymentId = initialPaymentId;
		this.subscriptionPaymentId = subscriptionPaymentId;
		this.timestamp = timestamp;
	}

	public int getId() {
		return id;
	}

	public String getInitialPaymentId() {
		return initialPaymentId;
	}

	public String getSubscriptionPaymentId() {
		return subscriptionPaymentId;
	}

	public Bundle getBundle() {
		return BundleDao.findById(bundleId);
	}

	public CustomUser getUser() {
		return UserDao.findById(userId);
	}

	public void addPayment(String paymentId) {
		PaymentDao.addPayment(id, paymentId);
	}

	public void addSubscription(String subscriptionId) {
		PaymentDao.addSubscription(id, subscriptionId);
	}

	public Map getMostRecentPayment() {
		return SubscriptionPaymentDao.findMostRecentForPayment(id);
	}

	public Invoice getMostRecentInvoice() {
		return InvoiceDao.findMostRecentForPayment(id);
	}

	public boolean isCompleted() {
		return timestamp != 0;
	}

	public boolean isRefunded(Client client) {
		return MOLLIE_STATUS_REFUNDED.equals(getStatus(client));
	}
	public boolean isPaid(Client client) {
		return MOLLIE_STATUS_PAID.equals(getStatus(client));
	}
	public String getStatus(Client client) {
		String status = "";
		try {
			String mollieId = getInitialPaymentId();
			ResponseOrError<Payment> result = client.payments().get(mollieId);
			Payment payment = result.getData();
			if(result.getSuccess()) {
				status = payment.getStatus();
			}
		}  catch(IOException e) {
			LogUtil.logException(e);
		}
		return status;
	}
	public Payment getPayment(Client client) {
		Payment payment = null;
		try {
			String mollieId = getInitialPaymentId();
			ResponseOrError<Payment> result = client.payments().get(mollieId);
			payment = result.getData();
		}  catch(IOException e) {
			LogUtil.logException(e);
		}
		return payment;
	}

	public CustomPayment completePayment(long timestamp) {
		CustomPayment updated = PaymentDao.completePayment(id, timestamp);
		if(updated.getTimestamp() == timestamp)
			this.timestamp = timestamp;

		return updated;
	}

	public Long getTimestamp() {
		return timestamp;
	}
}
