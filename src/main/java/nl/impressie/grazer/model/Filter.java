package nl.impressie.grazer.model;

/**
 * @author Impressie
 */
public class Filter {
	private String id;
	private Object value;
	private boolean display = false;
	private boolean displayTitle = false;

	public Filter(String id, Object value) {
		this.id = id;
		this.value = (value == null ? "" : value);
	}

	public Filter setDisplay(boolean display) {
		this.display = display;
		return this;
	}

	public Filter setDisplayTitle(boolean display) {
		this.displayTitle = display;
		return this;
	}

	public Filter displayWithTitle() {
		setDisplay(true);
		setDisplayTitle(true);
		return this;
	}

	public Object getValue() {
		return value;
	}

	public String getId() {
		return id;
	}

	public boolean displayValue() {
		return display;
	}

	public boolean displayTitle() {
		return displayTitle;
	}

	@Override
	public String toString() {
		return String.format("data-%s=\"%s\"", id, value.toString());
	}
}
