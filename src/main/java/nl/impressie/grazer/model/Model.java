package nl.impressie.grazer.model;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.LogUtil;

import java.util.Map;

/**
 * @author Impressie
 */
public abstract class Model {
	protected Object id;

	public Model(Object id) {
		this.id = id;
	}

	public Object getId() {
		return id;
	}

	public Map getFilters() {
		return CollectionFactory.createMap();
	}

	public String getFilterDataAttributes() {
		return "";
	}

	public String getDBValue(String key) {
		Object object = getDBValue(key, Object.class);
		return (object == null ? "" : object.toString());
	}
	public <T> T getDBValue(String key, Class<T> objectType) {
		T t =  null;
		try {
			objectType.newInstance();
		} catch(IllegalAccessException | InstantiationException e) {
			LogUtil.logException(e);
		}
		return t;
	}
}
