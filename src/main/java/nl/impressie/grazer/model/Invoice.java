package nl.impressie.grazer.model;

import nl.impressie.grazer.dao.UserDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.ContextProvider;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.LogUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @author Impressie
 */
public class Invoice extends Model{
	private int userId;
	private Double subtotal, vat, total;
	private Long timestamp;
	private String invoiceNumber, description, company, name, adress, adress2;

	public Invoice(int id, String invoiceNumber, String description, String company, String name, String adress, String adress2,
				   int userId, Double subtotal, Double vat, Double total, long timestamp) {
		super(id);

		this.invoiceNumber = invoiceNumber;
		this.description = description;
		this.company = company;
		this.name = name;
		this.adress = adress;
		this.adress2 = adress2;
		this.userId = userId;
		this.subtotal = subtotal;
		this.vat = vat;
		this.total = total;
		this.timestamp = timestamp;
	}

	public String getDescription() { return description; }
	public String getInvoiceNumber() { return invoiceNumber; }

	public String getDate() {
		return getDate("dd MM yyyy");
	}
	public String getDate(String format) {
		return DateUtil.convertTimestampToFormat(timestamp, format);
	}

	public CustomUser getUser() { return UserDao.findById(userId); }

	public File getInvoice() {
		String originalPdf = "WEB-INF/data/pdf/invoice.pdf";
		String targetPdf = getPath();

		try {
			File pdfFile = ContextProvider.getApplicationContext().getResource(originalPdf).getFile();
			PDDocument doc = PDDocument.load(pdfFile);

			Map<String, String> nameValues = CollectionFactory.createMap();
			nameValues.put("Factuurnummer", invoiceNumber);
			nameValues.put("Datum", getDate());
			nameValues.put("Product", description);
			nameValues.put("Bedrijfsnaam", company);
			nameValues.put("Naam", name);
			nameValues.put("Straat", adress);
			nameValues.put("Postcode", adress2);
			nameValues.put("Prijs", String.format("%.2f", subtotal));
			nameValues.put("Btw", String.format("%.2f", vat));
			nameValues.put("Totaal", String.format("%.2f", total));

			fillFieldsAndFlatten(doc, nameValues);

			doc.save(targetPdf);
			doc.close();

		} catch(IOException e) {
			LogUtil.logException(e);
		}
		return new File(targetPdf);
	}
	public static void fillFieldsAndFlatten(PDDocument doc, Map<String, String>nameValues) throws IOException {
		PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
		PDAcroForm acroForm = docCatalog.getAcroForm();

		for(String name: nameValues.keySet()) {
			PDField field = acroForm.getField( name );
			if(null != field)
				field.setValue(nameValues.get(name));
			else
				LogUtil.log(String.format("No field found with name: %s", name));
		}
		acroForm.flatten();
	}

	private String getPath() {
		return String.format("%s.pdf", invoiceNumber);
	}

	public void deletePdf() {
		File file = new File(getPath());
		if(file.exists())
			file.delete();
	}
}
