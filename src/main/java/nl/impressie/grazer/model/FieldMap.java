package nl.impressie.grazer.model;

import nl.impressie.grazer.factories.CollectionFactory;

import java.util.Map;

/**
 * @author Impressie
 */
public class FieldMap {
	public enum Attribute {
		NAME("name"), 
		TITLE("title"), 
		PLACEHOLDER("placeholder"),
		TYPE("type"),
		TOOLTIP("tooltip"),
		VALUES("values", CollectionFactory.createMap()),
		ERRORS("errors", CollectionFactory.createMap()),
		VALUE("value"),
		DATA("data"),
		ALT("alt"),
		MIN("min"),
		ON_INPUT_FOCUS("oninputfocus"),
		TITLE_CLASS("titleclass"),
		INPUT_CLASS("inputclass"),
		LABELCLASS("labelclass"),
		CONTAINER_CLASS("containerclass"),
		TITLE_LENGTH_CLASS("titlelengthclasses"),
		INPUT_LENGTH_CLASS("inputlengthclasses"),
		LABEL_LENGTH_CLASS("labellengthclass"),
		HIDE_MOUSE_OVERS("hidemouseovers"),
		REQUIRED("required"),
		CHECKED("checked"),
		AUTOSELECT("autoselect"),
		VALIDATEABLE("validateable"),
		DISABLED("disabled"),
		AUTOFOCUS("autofocus"),
		REQUIREDINFOSHOW("requiredInfoShow");

		private final String text;
		private final Object defaultValue;

		Attribute(final String text) {
			this(text, "");
		}
		Attribute(final String text, final Object defaultValue) {
			this.text = text;
			this.defaultValue = defaultValue;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	private Map<Attribute, Object> values;

	private FieldMap(Map values) {
		this.values = values;
	}

	public Object get(Attribute key) {
		if(values.containsKey(key))
			return values.get(key);
		return key.defaultValue;
	}
	public FieldMap put(Attribute key, Object value) {
		values.put(key, value);

		return this;
	}
	public static FieldMap create() {
		return new FieldMap(CollectionFactory.createMap());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		for(Attribute attribute: Attribute.values()) {
			if(!values.containsKey(attribute))
				continue;

			if(builder.length() > 0)
				builder.append(", ");

			builder.append(String.format("%s: %s", attribute.toString(), values.get(attribute)));
		}

		return builder.toString();
	}
}
