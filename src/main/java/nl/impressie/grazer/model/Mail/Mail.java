package nl.impressie.grazer.model.Mail;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.MailUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
abstract class Mail {
	private String from, to, subject;
	private List<Content> contents;
	private File[] files;

	protected Mail(String from, String to, String subject) {
		this(from, to, subject, new File[0]);
	}
	protected Mail(String from, String to, String subject, File... files) {
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.files = files;
		contents = CollectionFactory.createList();
	}

	public Mail addLine(String line) {
		return addContent(new Content(line));
	}
	protected Mail addContent(Content content) {
		contents.add(content);
		return this;
	}

	public String getMessage() {
		StringBuilder builder = new StringBuilder();
		for(Content content: contents) {
			builder.append(content.toString());
		}
		return builder.toString();
	}

	public Map<String, File> getInlineFiles() {
		Map files = new HashMap();

		for(Content content: contents) {
			files.putAll(content.getInlineAttachements());
		}

		return files;
	}

	public void send() {
		MailUtil.sendMail(from, to, subject, getMessage(), files, getInlineFiles());
	}

	public boolean empty() {
		return contents.size() == 0 && files.length == 0;
	}

	protected static class Content{
		protected String text;
		protected List<Content> contents;

		public Content(Content... contents) { this("", contents); }
		public Content(String text, Content... contents) {
			this.text = text;
			this.contents = CollectionFactory.createList(contents);
		}

		public Map<String, File> getInlineAttachements() {
			Map attachments = new HashMap();
			for(Content content: contents) {
				attachments.putAll(content.getInlineAttachements());
			}
			Map file = getInlineAttachement();
			if(file != null)
				attachments.putAll(file);

			return attachments;
		}

		public Map<String, File> getInlineAttachement() {
			return null;
		}

		protected Content addContent(Content content) {
			contents.add(content);
			return this;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			for(Content content: contents) {
				builder.append(content.toString());
			}
			builder.append(text);
			return builder.toString();
		}
	}
}
