package nl.impressie.grazer.model.Mail;

import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.MailUtil;
import org.apache.commons.lang3.NotImplementedException;

import javax.transaction.NotSupportedException;

/**
 * @author Impressie
 */
public class ExceptionMail extends HTMLMail {
	public ExceptionMail() {
		super(MailUtil.getExceptionSender(), MailUtil.getDefaultReceiver(), "Exception found");
	}
	public ExceptionMail(String from, String to, String subject) {
		super(from, to, subject);
	}

	public Mail addException(Exception exception) {
		return addException("", exception);
	}

	public Mail addException(String message, Exception exception) {
		return addContent(new MailException(message, exception));
	}

	protected class MailException extends Content {
		private String message;
		private Exception exception;
		private int occurances;

		public MailException(String message, Exception exception) {
			this.message = message;
			this.exception = exception;
			occurances = 1;
		}

		public MailException increaseOccurence() {
			occurances++;
			return this;
		}

		@Override
		public String toString() {
			String message = (occurances > 1 ? String.format("%d X %s", occurances, this.message) : this.message);

			HTMLList list = new HTMLList(false, new Paragraph(message));
			for(String line: LogUtil.exceptionToStrings(exception)) {
				list.addLine(line);
			}
			return list.toString();
		}
	}
}
