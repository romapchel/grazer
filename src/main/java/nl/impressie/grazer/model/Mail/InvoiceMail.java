package nl.impressie.grazer.model.Mail;

import nl.impressie.grazer.model.Invoice;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.MailUtil;

/**
 * @author Impressie
 */
public class InvoiceMail extends HTMLMail {
	public InvoiceMail(CustomUser user, Invoice invoice) {
		super(MailUtil.getVerificationSender(), user.getMail(), String.format("Bevestiging aankoop %s", invoice.getInvoiceNumber()), invoice.getInvoice());

		addLine(String.format("Geachte heer/mevrouw %s,", user.getFullName()));

		addLine(String.format("Hierbij ontvangt u de factuur voor uw abonnement op Grazer Legal NL. Indien u vragen heeft neem dan contact met ons op via één van de volgende e-mailadressen: "));

		HTMLList list = new HTMLList(false);
		list.addLine("Administratie@Grazer.news", "Gebruik dit e-mailadres als u vragen heeft over uw factuur of accountgegevens.");

		list.addLine("Redactie@Grazer.news", "Gebruik dit e-mailadres als u vragen van inhoudelijke aard over Grazer Legal NL heeft, als u een suggestie heeft om Grazer te verbeteren of om een nieuwssite toe te voegen.");

		list.addLine("Service@Grazer.news", "Gebruik dit e-mailadres als u vragen van technische aard heeft over Grazer Legal NL.");

		list.addLine("Sales@Grazer.news", "Gebruik dit e-mailadres als u vragen heeft over uw abonnement of bijvoorbeeld voor een maatwerkofferte.");
		addContent(list);
	}
}
