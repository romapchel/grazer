package nl.impressie.grazer.model.Mail;

import nl.impressie.grazer.model.DB.Verification;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.LinkUtil;
import nl.impressie.grazer.util.MailUtil;
import nl.impressie.grazer.util.UrlUtil;

/**
 * @author Impressie
 */
public class VerificationMail extends HTMLMail {
	private CustomUser user;
	private Verification verification;

	public VerificationMail(CustomUser user, Verification verification) {
		super(MailUtil.getVerificationSender(), user.getMail(), "Account verificatie");
		this.user = user;
		this.verification = verification;

		String url = String.format(LinkUtil.generateAbsoluteUrl("/verify?code=%s"), verification.getVerificationCode());

		addInlineImage("/img/logos/legal-logo.png");
		addLine(String.format("Geachte heer/mevrouw %s,", user.getFullName()));
		addLine(String.format("Er is een account aangemaakt op dit door u opgegeven emailadres. Door op de navolgende link te klikken activeert u dit account op Grazer Legal NL %s", url));
		addLine("Indien de link hiervoor niet werkt, kopieer deze dan naar de adresbalk van uw browser en klik op enter.");
	}
}
