package nl.impressie.grazer.model.Mail;

import nl.impressie.grazer.util.ContextProvider;
import nl.impressie.grazer.util.DateUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Impressie
 */
public class HTMLMail extends Mail {
	protected HTMLMail(String from, String to, String subject, File... files) {
		super(from, to, subject, files);
	}

	@Override
	public Mail addLine(String line) {
		return addContent(new Paragraph(line));
	}

	protected Mail addInlineImage(String path) {
		try {
			File file = ContextProvider.getApplicationContext().getResource(path).getFile();
			if(file.exists())
				return addInlineImage(file);
		} catch(IOException e) {
			e.printStackTrace();
		}
		return this;
	}
	protected Mail addInlineImage(File file) {
		return addContent(new InlineImage(file));
	}

	protected static class InlineImage extends Content {
		protected Map<String, File> file;
		public InlineImage(File file) {
			super("test.svg");

			Map fileMap = new HashMap();
			fileMap.put(this.text, file);
			this.file = fileMap;
		}

		@Override
		public Map<String, File> getInlineAttachement() {
			return file;
		}

		@Override
		public String toString() { return String.format("<img src=\"cid:%s\" />", super.toString()); }
	}

	protected static class Paragraph extends Content {
		public Paragraph(Content... contents) { this("", contents); }
		public Paragraph(String text, Content... contents) {
			super(text, contents);
		}

		@Override
		public String toString() { return String.format("<div>%s</div>", super.toString()); }
	}

	public Mail addBoldLine(String line) {
		return addContent(new Paragraph(new Bold(line)));
	}
	public Mail addItalicLine(String line) {
		return addContent(new Paragraph(new Italic(line)));
	}
	public Mail addBoldItalicLine(String line) {
		return addContent(new Paragraph(new Bold(new Italic(line))));
	}

	protected static class Bold extends Content {
		public Bold(Content... contents) { this("", contents); }
		public Bold(String text, Content... contents) {
			super(text, contents);
		}

		@Override
		public String toString() { return String.format("<b>%s</b>", super.toString()); }
	}

	protected static class HTMLList extends Content {
		protected boolean ordened;
		public HTMLList(boolean ordened, Content... contents) {
			super(contents);
			this.ordened = ordened;
		}

		public Content addLine(String... lines) {
			Paragraph paragraph = new Paragraph();
			for(String line: lines) {
				paragraph.addContent(new Paragraph(line));
			}

			return addContent(paragraph);
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			String tag = (ordened ? "li" : "ul");
			for(Content conten: contents) {
				builder.append(String.format("<%1$s>%2$s</%1$s>", tag, conten.toString()));
			}
			return builder.toString();
		}
	}

	protected static class Italic extends Content {
		public Italic(Content... contents) { this("", contents); }
		public Italic(String text, Content... contents) {
			super(text, contents);
		}

		@Override
		public String toString() { return String.format("<i>%s</i>", super.toString()); }
	}
}
