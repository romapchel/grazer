package nl.impressie.grazer.model.Mail;

import nl.impressie.grazer.dao.SubscriptionPaymentDao;
import nl.impressie.grazer.model.Bundle;
import nl.impressie.grazer.model.CustomPayment;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.MailUtil;
import nl.stil4m.mollie.domain.Links;
import nl.stil4m.mollie.domain.Payment;

import java.util.Map;
import java.util.Optional;

/**
 * @author Impressie
 */
public class TestMail extends HTMLMail {
	public TestMail() {
		super(MailUtil.getDefaultReceiver(), MailUtil.getDefaultReceiver(), "test");
	}

	public Mail addCustomPayment(CustomPayment payment) {
		HTMLList list = new HTMLList(false);

		CustomUser user = payment.getUser();
		Bundle bundle = payment.getBundle();
		Map mostRecentPayment = payment.getMostRecentPayment();

		list.addLine(String.format("id: %s", payment.getId()));

		if(null != bundle)
			list.addLine("bundle:", String.format("id: %s", bundle.getId()), String.format("description: %s", bundle.getDescription()), String.format("price: %f", bundle.getPriceAsDouble()));

		if(null != user)
			list.addLine("user:", String.format("id: %he", user.getId()), String.format("username: %s", user.getUsername()));

		if(null != mostRecentPayment)
			list.addLine(
					"Most recent payment:",
					String.format("id: %s", mostRecentPayment.get(SubscriptionPaymentDao.Attribute.ID.getColumnTitle())),
					String.format("id: %s", DateUtil.convertTimestampToFormat((Long)mostRecentPayment.get(SubscriptionPaymentDao.Attribute.TIMESTAMP.getColumnTitle()), "dd-MM-yyyy HH:mm"))
			);

		Paragraph paragraph = new Paragraph("CustomPayment: ");
		paragraph.addContent(new Paragraph(list));
		return addContent(paragraph);
	}

	public Mail addPayment(Payment payment) {
		HTMLList list = new HTMLList(false);

		list.addLine(String.format("id: %s", payment.getId()));
		list.addLine(String.format("recurring type: %s", payment.getRecurringType()));
		list.addLine(String.format("status: %s", payment.getStatus()));
		list.addLine(String.format("description: %s", payment.getDescription()));
		list.addLine(String.format("method: %s", payment.getMethod()));
		list.addLine(String.format("resource: %s", payment.getResource()));
		list.addLine(String.format("amount: %f", payment.getAmount()));
		list.addLine(String.format("amount refunded: %f", payment.getAmountRefunded()));
		list.addLine(String.format("amount remaining: %f", payment.getAmountRemaining()));
		list.addLine(String.format("canceled datetime: %s", payment.getCancelledDatetime()));
		list.addLine(String.format("country code: %s", payment.getCountryCode()));
		list.addLine(String.format("created datetime: %s", payment.getCreatedDatetime().toString()));
		list.addLine(String.format("customer id: %s", payment.getCustomerId()));
		list.addLine(String.format("Expired Datetime: %s", payment.getExpiredDatetime()));
		list.addLine(String.format("Expiry period: %s", payment.getExpiryPeriod()));
		list.addLine(String.format("locale: %s", payment.getLocale()));
		list.addLine(String.format("mode: %s", payment.getMode()));
		list.addLine(String.format("paid datetime: %s", payment.getPaidDatetime()));
		list.addLine(String.format("profile id: %s", payment.getProfileId()));

		Optional<String> subscriptionId = payment.getSubscriptionId();
		if(subscriptionId.isPresent())
			list.addLine(String.format("subscription id: %s", subscriptionId.get()));

		Optional<String> mandateId = payment.getMandateId();
		if(mandateId.isPresent())
			list.addLine(String.format("mandate id: %s", mandateId.get()));

		HTMLList detailList = new HTMLList(true);
		Map<String, Object> details = payment.getDetails();
		if(details != null)  {
			for(String key: details.keySet()) {
				detailList.addLine(String.format("%s: %s", key, details.get(key)));
			}
		}

		Map<String, Object> metadata = payment.getMetadata();
		HTMLList metaList = new HTMLList(true);
		if(metadata != null)  {
			for(String key: metadata.keySet()) {
				metaList.addLine(String.format("%s: %s", key, metadata.get(key)));
			}
		}

		Links links = payment.getLinks();
		HTMLList linkList = new HTMLList(true);
		linkList.addLine(String.format("Payment Url: %s", links.getPaymentUrl()));
		linkList.addLine(String.format("Redirect Url: %s", links.getRedirectUrl()));
		linkList.addLine(String.format("Webhook Url: %s", links.getWebhookUrl()));
		Optional<String> refunds = links.getRefunds();
		if(refunds.isPresent())
			linkList.addLine(String.format("Refunds: %s", refunds.get()));

		Optional<String> verificationUrl = links.getVerificationUrl();
		if(verificationUrl.isPresent())
			linkList.addLine(String.format("Verification Url: %s", verificationUrl.get()));

		Paragraph paragraph = new Paragraph("Payment: ");
		paragraph.addContent(new Paragraph(list));
		paragraph.addContent(new Paragraph("details: ", detailList));
		paragraph.addContent(new Paragraph("metadata: ", metaList));
		paragraph.addContent(new Paragraph("links: ", linkList));

		return addContent(paragraph);
	}
}
