package nl.impressie.grazer.model.Mail;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.MailUtil;

import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class UpdateMail extends ExceptionMail {
	private List<SiteUpdate> newSites;
	private List<SiteUpdate> unchangedSites;
	private List<SiteUpdate> brokenSites;
	private List<SiteUpdate> otherSites;
	private Map<String, Map<String, MailException>> errors;

	public UpdateMail() {
		super(MailUtil.getUpdateSender(), MailUtil.getDefaultReceiver(), "Index updateEndDate");
		newSites = CollectionFactory.createList();
		unchangedSites = CollectionFactory.createList();
		brokenSites = CollectionFactory.createList();
		otherSites = CollectionFactory.createList();
		errors = CollectionFactory.createMap();
	}

	public Mail addSite(String name, int before, int now) {
		SiteUpdate update = new SiteUpdate(name, before, now);
		if(before == 0 && now > 0)
			newSites.add(update);
		else if(now == 0)
			brokenSites.add(update);
		else if(now == before)
			unchangedSites.add(update);
		else
			otherSites.add(update);

		return this;
	}

	@Override
	public Mail addException(Exception exception) {
		return addException("", exception);
	}

	@Override
	public Mail addException(String message, Exception exception) {
		return addException("", exception, "");
	}

	public Mail addException(String message, Exception exception, String siteName) {
		Map<String, MailException> errorsForSite = (errors.containsKey(siteName) ? errors.get(siteName) : CollectionFactory.createMap());
		String exceptionTitle = LogUtil.getExceptionTitle(exception);

		MailException mailException = null;
		if(errorsForSite.containsKey(exceptionTitle))
			mailException = errorsForSite.get(exceptionTitle).increaseOccurence();

		if(mailException == null)
			mailException = new MailException(message, exception);

		errorsForSite.put(exceptionTitle, mailException);
		errors.put(siteName, errorsForSite);

		return this;
	}

	@Override
	public void send() {
		if(brokenSites.size() > 0) {
			HTMLList list = new HTMLList(false, new Bold("Broken pages"));
			for(SiteUpdate site: brokenSites) {
				list.addLine(site.toString());
			}
			addContent(list);
		}
		if(newSites.size() > 0) {
			HTMLList list = new HTMLList(false, new Bold("Newly added pages"));
			for(SiteUpdate site: newSites) {
				list.addLine(site.toString());
			}
			addContent(list);
		}
		if(unchangedSites.size() > 0) {
			HTMLList list = new HTMLList(false, new Bold("Unchanged pages"));
			for(SiteUpdate site: unchangedSites) {
				list.addLine(site.toString());
			}
			addContent(list);
		}
		if(otherSites.size() > 0) {
			HTMLList list = new HTMLList(false, new Bold("Remaining pages"));
			for(SiteUpdate site: otherSites) {
				list.addLine(site.toString());
			}
			addContent(list);
		}
		if(errors.size() > 0) {
			String errorTitle = "Errors";
			HTMLList list = new HTMLList(false, new Bold(errorTitle));
			for(String siteName: errors.keySet()) {
				if(siteName.trim().length() == 0)
					siteName = errorTitle;

				HTMLList subList = new HTMLList(false, new Bold(siteName));
				if(errors.containsKey(siteName)) {
					Map<String, MailException> exceptionsForSite = errors.get(siteName);
					for(MailException exception : exceptionsForSite.values()) {
						subList.addContent(exception);
					}
				}

				if(errorTitle.equals(siteName))
					list = subList;
				else
					list.addContent(subList);
			}
			addContent(list);
		}
		super.send();
	}

	private class SiteUpdate {
		private String name;
		private int before;
		private int now;
		private int change;

		public SiteUpdate(String Name, int Before, int Now) {
			name = Name;
			before = Before;
			now = Now;

			float difference = now - before;
			float divider = before;
			if(divider <= 0)
				divider = 1;
			change = Math.round((difference / divider) * 100.0f);
		}

		public String toString() {
			return String.format("before %s had %d articles, now it has %d articles. this is a change of %d%%", name, before, now, change);
		}
	}
}
