package nl.impressie.grazer.model;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.DatabaseUtils.CRUDObject;
import nl.impressie.grazer.util.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Impressie
 */
public class LastIndex {
	private int id, lastDate;

	public static final String TABLE_NAME = "lastindex";
	public static final RowMapper ROW_MAPPER = new RowMapper() {
		@Override
		public LastIndex mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt("id");
			int lastDate = rs.getInt("last_date");

			return new LastIndex(id, lastDate);
		}
	};

	public LastIndex(int ID, int LASTDATE) {
		id = ID;
		lastDate = LASTDATE;
	}

	private static void create(int newDate) {
		List list = CollectionFactory.createList();
		list.add(new KeyValue("last_date", newDate));
		CRUDObject.create(TABLE_NAME, list);
	}

	private static void update(int newDate, int id) {
		CRUDObject.update(TABLE_NAME, new KeyValue("id", id), new KeyValue("last_date", newDate));
	}

	public static void indexrun() {
		int newDate = DateUtil.getCurrentDateAsInt();
		List list = getList();
		if(list.size() == 0)
			create(newDate);
		else
			update(newDate, ((LastIndex) list.get(0)).id);
	}

	public static int getLastIndex() {
		int last_date = 0;
		List list = getList();
		if(list.size() == 1)
			last_date = ((LastIndex) list.get(0)).lastDate;

		return last_date;
	}

	private static List getList() {
		return CRUDObject.select(TABLE_NAME, ROW_MAPPER, CollectionFactory.createList());
	}
}
