package nl.impressie.grazer.model;

import nl.impressie.grazer.dao.BundleDao;
import nl.impressie.grazer.util.DateUtil;
import nl.stil4m.mollie.domain.CreatePayment;
import nl.stil4m.mollie.domain.CreateSubscription;

import java.util.Date;
import java.util.Optional;

/**
 * @author Impressie
 */
public class Bundle {
	private int id;
	private int price, intervalNmbr;
	private String description;
	private int typeId;
	private String intervalType;
	private int times;

	public Bundle update(int price, int times, int intervalNumber, String intervalType, String beschrijving) {
		return BundleDao.update(id, price, times, intervalNumber, intervalType, beschrijving);
	}

	public int delete() {
		return BundleDao.delete(id);
	}

	public Bundle(int id, int price, int times, int intervalNmbr, String intervalType, String description, int typeId) {
		this.id = id;
		this.price = price;
		this.intervalNmbr = intervalNmbr;
		this.intervalType = intervalType;
		this.description = description;
		this.typeId = typeId;
		this.times = times;
	}

	public int getId() {
		return id;
	}

	public String getIntervalType() {
		return intervalType;
	}

	public int getPrice() {
		return price;
	}

	public int getTypeId() {
		return typeId;
	}

	public int getIntervalNmbr() {
		return intervalNmbr;
	}

	public int getTimes() {
		return times;
	}

	public Double getPriceAsDouble() {
		Double dblPrice = price * 0.01;

		return dblPrice;
	}

	public String getDescription() {
		return description;
	}

	public String getInterval() {
		String type = "days";
		if("m".equals(intervalType))
			type = "months";
		else if("y".equals(intervalType))
			type = "years";

		return String.format("%d %s", intervalNmbr, type);
	}

	public CreateSubscription getCreateSubscription(Optional webhook) {
		String interval = getInterval();
		String paymentDescription = String.format("%s -subscription %d", description, DateUtil.getTimestamp());
		Date start = DateUtil.calculateDate(intervalType, intervalNmbr);
		int times = this.times - 1;

		Optional what = Optional.empty();
		if(times > 1)
			what = Optional.of(times);

		return new CreateSubscription(getPriceAsDouble(), what, interval, start, paymentDescription, Optional.empty(), webhook);
	}

	public CreatePayment getCreatePayment(String redirectUrl, Optional webhook) {
		Double price = getPriceAsDouble();
		String description = this.description;

		return new CreatePayment(Optional.empty(), price, description, redirectUrl, webhook, null);
	}
}
