package nl.impressie.grazer.model.Crawler;

import lombok.Setter;
import nl.impressie.grazer.controller.Controller;
import nl.impressie.grazer.dao.NewSearchTermDao;
import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.model.User.Profile;
import nl.impressie.grazer.util.*;
import nl.impressie.grazer.util.crawlerUtils.IndexManager;
import nl.impressie.grazer.util.jstlUtils.TagUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.*;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Impressie
 */
public class ArticleSearch {
	public final static String PREV_SEARCH_KEY = "previous-article-search-object";
	public final static String DEFAULT_SEPERATOR = ", ";

	private String sites = "";
	private String rechtsgebieden = "";
	private String soorten = "";
	private String rubrieken = "";
	private String contentSearch = "";
	private String titleSearch = "";

	@Setter
	private boolean advancedSearch = false;
	@Setter
	private boolean excludesJurisprudentie = false;
	@Setter
	private boolean showsNews = false;

	@Setter
	private Long dateRangeStart = 0L, dateRangeEnd = 0L;
	private int startFromItemNumber = 0, goUntillItemNumber = 0;
	private int page = 0;

	private boolean feedSearch = false;

	private Sort sort = Article.getDefaultSorter();

	private boolean ignoreDisabledPages = true;

	public ArticleSearch() {
		loadparameters(CollectionFactory.createMap());
	}

	public ArticleSearch(Profile activeProfile) {
		try {
			Map parameters = CollectionFactory.createMap();

			boolean paramsEmpty = parameters.size() == 0;
			paramsEmpty = paramsEmpty || (parameters.size() == 1 && (parameters.containsKey("js") || parameters.containsKey("feed")));
			paramsEmpty = paramsEmpty || (removeNonSearchParams(parameters).size() == 0);

			if(paramsEmpty && null != activeProfile)
				loadDefaults(activeProfile);
			else
				loadparameters(parameters);

			this.excludesJurisprudentie = parameters.containsKey("exlcude_jurisprudentie");
			this.showsNews = parameters.containsKey("show_news");


			Object feed = parameters.get("feed");
			if(feed instanceof String[])
				feed = ((String[]) feed)[0];

			if(null != feed)
				this.feedSearch = Boolean.parseBoolean(feed.toString());

		} catch(Exception e) {
			LogUtil.logException(e);
		}
	}

	public ArticleSearch(Map parameters) {
		try {
			CustomUser user = AuthenticationUtil.getCurrentUser();
			Profile profile = (user == null ? null : user.getActiveProfile());

			boolean paramsEmpty = parameters.size() == 0;
			paramsEmpty = paramsEmpty || (parameters.size() == 1 && (parameters.containsKey("js") || parameters.containsKey("feed")));
			paramsEmpty = paramsEmpty || (removeNonSearchParams(parameters).size() == 0);

			if(paramsEmpty && null != profile)
				loadDefaults(profile);
			else
				loadparameters(parameters);

			this.excludesJurisprudentie = parameters.containsKey("exlcude_jurisprudentie");
			this.showsNews = parameters.containsKey("show_news");


			Object feed = parameters.get("feed");
			if(feed instanceof String[])
				feed = ((String[]) feed)[0];

			if(null != feed)
				this.feedSearch = Boolean.valueOf(feed.toString());

		} catch(Exception e) {
			LogUtil.logException(e);
		}
	}

	private Map removeNonSearchParams(Map params) {
		Map clone = CollectionFactory.clone(params);

		clone.remove("js");

		clone.remove("feed");

		clone.remove("sub_page");

		clone.remove("page");

		return clone;
	}

	public ArticleSearch setIgnoreDisabledPages(boolean ignoreDisabledPages) {
		this.ignoreDisabledPages = ignoreDisabledPages;
		return this;
	}

	public ArticleSearch addTags(Collection<Tag> tags) {
		StringBuilder rechtBuilder = new StringBuilder();
		rechtBuilder.append(rechtsgebieden);

		StringBuilder rubriekBuilder = new StringBuilder();
		rubriekBuilder.append(rubrieken);

		StringBuilder nieuwsBuilder = new StringBuilder();
		nieuwsBuilder.append(soorten);

		for(Tag tag : tags) {
			String type = tag.getType();
			StringBuilder builder = null;
			if(Tag.RECHTSGEBIED.equals(type))
				builder = rechtBuilder;
			else if(Tag.RUBRIEK.equals(type))
				builder = rubriekBuilder;
			else if(Tag.NIEUWSSOORT.equals(type))
				builder = nieuwsBuilder;

			if(null == builder)
				continue;

			if(builder.length() > 0)
				builder.append(", ");
			builder.append(tag.getId());
		}

		soorten = nieuwsBuilder.toString();
		rechtsgebieden = rechtBuilder.toString();
		rubrieken = rubriekBuilder.toString();

		return this;
	}

	private void loadDefaults(Profile activeProfile) {
		startFromItemNumber = 0;
		setGoUntillItemNumber(Article.ITEMS_PER_PAGE);
		page = 0;
		dateRangeEnd = DateUtil.getTodayAsTimestamp();

//      The way profiles are currently set up they don't actually have a date range, todo: Discuss with Peter wetehr or not this is needed?
//        int daysAfterToday = -1 * activeProfile.getDefRange();
		int daysAfterToday = -1 * Profile.DEFAULT_RANGE;
		daysAfterToday = 0;
		dateRangeStart = DateUtil.getDaysAfterTodayAsTimestamp(daysAfterToday);
		if(null != activeProfile) {
			titleSearch = activeProfile.getTitleSearchString();
			contentSearch = activeProfile.getContentSearchString();

			rubrieken = StringUtils.join(activeProfile.getRubrieken().keySet(), ", ");
			rechtsgebieden = StringUtils.join(activeProfile.getRechtsgebieden().keySet(), ", ");
			soorten = StringUtils.join(activeProfile.getNieuwssoorten().keySet(), ", ");
			sites = StringUtils.join(activeProfile.getSites().keySet(), ", ");
		}
//        sort = getSort(CollectionFactory.createMap());

		if(titleSearch.length() > 0 || contentSearch.length() > 0)
			sort = Article.getScoreSorter();
		else
			sort = Article.getDateSorter();
	}

	private void loadparameters(Map parameters) {
		advancedSearch = getBoolean(parameters, "advanced_search");

		if(!advancedSearch) {
			CustomUser user = AuthenticationUtil.getCurrentUser();
			if(user != null) {
				loadDefaults(user.getActiveProfile());

				if (parameters.containsKey(Tag.RECHTSGEBIED)) rechtsgebieden = getString(parameters, Tag.RECHTSGEBIED);
				if (parameters.containsKey(Tag.NIEUWSSOORT)) soorten = getString(parameters, Tag.NIEUWSSOORT);
				if (parameters.containsKey(Tag.RUBRIEK)) rubrieken = getString(parameters, Tag.RUBRIEK);

				contentSearch = getString(parameters, Article.GENERIC_SEARCH);
				return;
			}
		}

		int from = 0;
		page = getInt(parameters, "page");
		int to = Article.ITEMS_PER_PAGE;
		String type = getString(parameters, "search");
		if(type.equals("next"))
			to = to * (page + 1);
		else if(page > 0)
			page = 0;

		sites = getString(parameters, "site");
		rechtsgebieden = getString(parameters, Tag.RECHTSGEBIED);
		soorten = getString(parameters, Tag.NIEUWSSOORT);
		rubrieken = getString(parameters, Tag.RUBRIEK);
		contentSearch = getString(parameters, Article.CONTENT_SEARCH);
		titleSearch = getString(parameters, Article.TITLE_SEARCH);
		NewSearchTermDao.addNewTerms(contentSearch, "Inhoud doorzoeken");
		NewSearchTermDao.addNewTerms(titleSearch, "Titel doorzoeken");

		dateRangeEnd = getDate(parameters, "to");
		if(dateRangeEnd == 0)
			dateRangeEnd = DateUtil.getTodayAsTimestamp();
		dateRangeStart = getDate(parameters, "from");
		if(dateRangeStart == 0)
			dateRangeStart = DateUtil.getTodayAsTimestamp();

		startFromItemNumber = from;
		setGoUntillItemNumber(to);

	}

	private boolean getBoolean(Map map, Object key) {
		return Boolean.valueOf(getString(map, key));
	}

	private String getString(Map map, Object key) {
		Object o = map.get(key);
		if(o != null) {
			String asString = getAsString(o);
			if(asString != null)
				return asString;
			if(o instanceof Integer)
				return String.valueOf(o);
		}
		return "";
	}

	private int getInt(Map map, Object key) {
		Object o = map.get(key);
		if(o != null) {
			if(o instanceof Integer)
				return (Integer) o;
			if(o instanceof TotalHits)
				return (int) ((TotalHits) o).value;
			String asString = getAsString(o);
			if(asString != null) {
				int result = 0;
				try {
					result = Integer.parseInt(asString);
				} catch(NumberFormatException e) { }
				return result;
			}
		}
		return 0;
	}

	private Long getDate(Map map, Object key) {
		if(!map.containsKey(key))
			return new Long(0);

		String date = getAsString(map.get(key));

		long result = 0;
		try {
			if(0 < date.length())
				result = DateUtil.convertDateToTimestamp(date, "yyyy-MM-dd");
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private Sort getSort(Map map) {
		Object o = map.get("order");
		if(o != null && o instanceof Sort)
			return (Sort) o;
		String asString = getAsString(o);
		if(asString != null)
			return Article.getSortFromString(asString);
		return Article.getDefaultSorter();
	}

	private String getAsString(Object o) {
		if(o instanceof String)
			return (String) o;
		if(o instanceof String[])
			return String.join(", ", (String[]) o);
		return null;
	}

	public ArticleSearch increasePage() {
		page++;
		goUntillItemNumber += Article.ITEMS_PER_PAGE;

		return this;
	}

	public boolean containsTag(String tagType, String tagId) {
		String container = getContainer(tagType);
		if(0 == container.length())
			return false;

		Matcher matcher = Pattern.compile(String.format("\\b%s\\b", tagId)).matcher(container);
		boolean found = matcher.find();
		return found;
	}

	public boolean containsFacet(String facet) {
		return containsTag(Tag.RUBRIEK, facet);
	}

	public boolean isTagEmpty(String tagType) {
		String container = getContainer(tagType);
		return 0 == container.length();
	}

	public boolean isFeedSearch() {
		return feedSearch;
	}

	private String getContainer(String tagType) {
		String container = "empty";
		if(tagType.equals(Tag.RUBRIEK))
			container = rubrieken;
		else if(tagType.equals(Tag.RECHTSGEBIED))
			container = rechtsgebieden;
		else if(tagType.equals(Tag.NIEUWSSOORT))
			container = soorten;

		return container;
	}

	public String getTitle() {
		return titleSearch;
	}

	public String getContent() {
		return contentSearch;
	}

	public boolean isAdvanced() {
		return advancedSearch;
	}
	public boolean excludesJurisprudentie() {
		return excludesJurisprudentie;
	}
	public boolean showsNews() {
		return showsNews;
	}

	public boolean isSortOrder(String sortOrder) {
		Sort used = Article.getSortFromString(sortOrder);
		return used.toString().equals(sort.toString());
	}

	public String getStartDate(String format) {
		Long dateRangeStart = this.dateRangeStart;
		boolean isToday = dateRangeStart.equals(DateUtil.getTodayAsTimestamp());
		boolean rangeIsToday = dateRangeStart.equals(dateRangeEnd);
		if(isToday && rangeIsToday)
			dateRangeStart = DateUtil.getDaysAfterTodayAsTimestamp(-365);

		return DateUtil.convertTimestampToFormat(dateRangeStart, format);
	}

	public String getEndDate(String format) {
		return DateUtil.convertTimestampToFormat(dateRangeEnd, format);
	}

	public ArticleSearch toggleFacet(String facet) {
		if(Controller.Selections.ALL.matches(facet)) {
			rubrieken = TagDao.getAsIdString(TagDao.getType(Tag.RUBRIEK));
		} else if(Controller.Selections.NONE.matches(facet)) {
			rubrieken = "";
		} else {
			Matcher matcher = Pattern.compile(String.format("\\b%s\\b", facet)).matcher(rubrieken);

			if(matcher.find()) {
				int start = matcher.start();
				//removes the ", " if it is not the first
				if(start > 2)
					start -= 2;
				String first = "";
				if(start > 0)
					first = rubrieken.substring(0, start);

				int end = matcher.end();
				String second = "";
				if(end < rubrieken.length())
					second = rubrieken.substring(end);

				rubrieken = String.format("%s%s", first, second).trim();
				if(rubrieken.startsWith(","))
					rubrieken = rubrieken.substring(1).trim();
			} else {
				if(0 != rubrieken.length())
					rubrieken += ", ";
				rubrieken += facet;

				//if(rubrieken.length() == TagDao.getAsIdString(TagDao.getType(Tag.RUBRIEK)).length())
				//	rubrieken = "";
			}
		}
		return this;
	}

	private void removeParentTags() {
		Map<String, Tag> tags = CollectionFactory.createMap();
		for(String id: rechtsgebieden.split(", ")) {
			if(id.length() == 0)
				continue;

			Tag tag = TagDao.findById(id);
			tags.put(tag.getId(), tag);
		}
		List<TagUtil.TagFamily> tagFamilies = TagUtil.getTagFamilies(tags.values());
		for(TagUtil.TagFamily family: tagFamilies) {
			if(family.getChildren().size() == 0)
				continue;

			String parentId = family.getParent().getId();
			if(tags.containsKey(parentId))
				tags.remove(parentId);
		}
		rechtsgebieden = "";
		addTags(tags.values());
		// debug
	}

	public Map search() {
		Map results = CollectionFactory.createMap();
		List articles = CollectionFactory.createList();
		int totalHits = 0;

		removeParentTags();

		try {
			BooleanQuery query = buildQuery();
			BooleanQuery fullQuery = buildFullQuery(query);
			Map searchResults;

			System.out.println(String.format("Final query: [%s]", fullQuery));

			if(startFromItemNumber == 0 && goUntillItemNumber == 0)
				searchResults = IndexManager.getInstance().search(fullQuery, sort);
			else
				searchResults = IndexManager.getInstance().search(fullQuery, startFromItemNumber, goUntillItemNumber, sort);

			List<Document> documents = CollectionFactory.createList();
			if(searchResults != null && searchResults.containsKey("documents")) {
				Object o = searchResults.get("documents");
				if(o instanceof List)
					documents = (List<Document>) o;
			}
			for(Document doc : documents) {
				Article article = new Article(doc);
				articles.add(article);
			}

			totalHits = getInt(searchResults, IndexManager.TOTAL_HITS);

			results.put(IndexManager.FACET_FIELD, IndexManager.getInstance().countFacets(query));
		} catch(Exception e) {
			LogUtil.logException(e);
		}

		results.put(IndexManager.TOTAL_HITS, totalHits);
		results.put("currentHits", articles.size());
		results.put("artikels", articles);
		results.put("page", page);

		return results;
	}

	private BooleanQuery buildQuery() {
		BooleanQuery.Builder builder = new BooleanQuery.Builder();

		builder.add(new MatchAllDocsQuery(), BooleanClause.Occur.MUST);

		//limit search to active sites (String of ids which is turned into a query when not all websites are enabled)
//		BooleanQuery.Builder cachedBuilder = SearchUtil.addWebsiteSearchQuery(new BooleanQuery.Builder(), DEFAULT_SEPERATOR, ignoreDisabledPages);
//		BooleanQuery cachedQuery = cachedBuilder.build();
//		if(cachedQuery.clauses().size() > 0)
//			builder.add(cachedQuery, BooleanClause.Occur.MUST);

		builder = SearchUtil.addContentSearchQuery(builder, contentSearch, Article.CONTENT_SEARCH, Article.TITLE_SEARCH);
		builder = SearchUtil.addSearchTermQuery(builder, Article.TITLE_SEARCH, titleSearch);
		builder = SearchUtil.addNewSplitWebsiteQuery(builder, Article.WEBSITE_URL_SEARCH, sites, DEFAULT_SEPERATOR, BooleanClause.Occur.MUST);

		if(dateRangeEnd - dateRangeStart > 0)
			builder = SearchUtil.addLongRangeQuery(builder, Article.INDEXED_ON_SEARCH, dateRangeStart, dateRangeEnd);

		CustomUser user = AuthenticationUtil.getCurrentUser();
		if(this.isFeedSearch() && null != user) {
			Long feedStart = DateUtil.getDaysAfterTodayAsTimestamp(user.getDaysSinceLastLogin());
			Long feedEnd = DateUtil.getDaysAfterTodayAsTimestamp(0);
			builder = SearchUtil.addLongRangeQuery(builder, Article.INDEXED_ON_SEARCH, feedStart, feedEnd);
		}

		/*
		if(this.excludesJurisprudentie()) {
			Tag tag = TagDao.findByName("jurisprudentie", Tag.NIEUWSSOORT);
			if(tag == null)
				LogUtil.logException(new Exception("Jurisprudentie tag not found"));
			builder = SearchUtil.addtagExclusion(builder, tag);
		}

		if(!this.showsNews) {
			Tag tag = TagDao.findByName("nieuws- en persberichten", Tag.NIEUWSSOORT);
			if(tag == null)
				LogUtil.logException(new Exception("News tag not found"));
			builder = SearchUtil.addtagExclusion(builder, tag);
		}

		*/
		return builder.build();
	}

	private BooleanQuery buildFullQuery(BooleanQuery query) {
		int clauses = query.clauses().size();
		if(BooleanQuery.getMaxClauseCount() < clauses)
			BooleanQuery.setMaxClauseCount(clauses);

		BooleanQuery.Builder fullQueryBuilder = new BooleanQuery.Builder();

		BooleanQuery.Builder facetQueryBuilder = new BooleanQuery.Builder();
		facetQueryBuilder = SearchUtil.addNewSplitTermTagQuery(facetQueryBuilder, Tag.RECHTSGEBIED, rechtsgebieden, DEFAULT_SEPERATOR);
		facetQueryBuilder = SearchUtil.addNewSplitTermTagQuery(facetQueryBuilder, Tag.NIEUWSSOORT, soorten, DEFAULT_SEPERATOR);
		facetQueryBuilder = SearchUtil.addNewSplitTermTagQuery(facetQueryBuilder, Tag.RUBRIEK, rubrieken, DEFAULT_SEPERATOR);
		fullQueryBuilder.add(query, BooleanClause.Occur.MUST);
		BooleanQuery facetQuery = facetQueryBuilder.build();
		if(facetQuery.clauses().size() > 0)
			fullQueryBuilder.add(facetQuery, BooleanClause.Occur.FILTER);

		return fullQueryBuilder.build();
	}


	public static ArticleSearch createSearch(HttpServletRequest request) {
		Map map = request.getParameterMap();
		System.out.println(Arrays.toString(map.entrySet().toArray()));
		String facetParamKey = "facet";
		boolean reSearch = map.containsKey(Controller.Selections.PAGE) || map.containsKey(facetParamKey);

		ArticleSearch search = (reSearch ? (ArticleSearch) SessionUtil.getAndRemoveFromSession(request, PREV_SEARCH_KEY) : new ArticleSearch(map));

		String param = request.getParameter(Controller.Selections.PAGE);
		if(Controller.Actions.NEXT_PAGE.equals(param))
			search.increasePage();

		param = request.getParameter(facetParamKey);
		if(null != param)
			search.toggleFacet(param);

		SessionUtil.addToSession(request, PREV_SEARCH_KEY, search);

		return search;
	}

	private ArticleSearch setGoUntillItemNumber(int goUntillItemNumber) {
		this.goUntillItemNumber = goUntillItemNumber;
		return this;
	}
}
