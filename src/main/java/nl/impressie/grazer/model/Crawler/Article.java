package nl.impressie.grazer.model.Crawler;

import nl.impressie.grazer.dao.ArticleHealthDao;
import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.exception.IndexManagerException;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.DB.Website;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.crawlerUtils.IndexManager;
import org.apache.lucene.document.*;
import org.apache.lucene.facet.FacetField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class Article {

    private String url;
    private String title;
    private String date;
    private Long indexedOn;
    private String content;
    private String snippet;
    private String websiteUrl;
    private String nieuwsSoort;
    private String rubrieken;
    private String rechtsgebied;
    private String depth;
	private String uid;

    public final static String ORDER_DATE_DESCENDING = "date:DESC";
    public final static String ORDER_SCORE_DESCENDING = "score:DESC";
    public final static int SNIPPETMAX = 160;
    public final static int SNIPPETMIN = 100;
    public final static int ITEMS_PER_PAGE = 6;
    public final static int TITLEMAX = 95;

	public final static String URL = "URL", DEPTH = "depth", TITLE_SEARCH = "title", TITLE_STORE = "title_stored", WEBSITE_NAME_SEARCH = "page", CONTENT_SEARCH = "contents", GENERIC_SEARCH = "search", CONTENT_STORE = "contents_stored",
			SNIPPET = "snippet", WEBSITE_URL = "webURL", WEBSITE_URL_SEARCH = "webURL_search", DATE_SEARCH = "lastCrawledOn", DATE_STORE = "lastCrawledOn_stored", DATE_SORT = "lastCrawledOn_sorter",
			INDEXED_ON_SEARCH = "indexedOn", INDEXED_ON_STORE = "indexedOn_stored", INDEXED_ON_SORT = "indexedOn_sorter", UID = "uid";

	private static Map APPROXIMATE_FIELDS = CollectionFactory.createMap();

	public final static String SEPERATOR = ", ";

	private Article(String title, String content, String date, String url, String depth, Website page, String nieuwsSoort, String rechtsgebieden, String rubrieken) {
        this.title = stringOrDefault(title, "");
        this.content = stringOrDefault(content, "");
        this.date = stringOrDefault(date, "0");
        this.url = stringOrDefault(url, "");
        this.depth = stringOrDefault(depth, "0");
		this.websiteUrl = stringOrDefault(page.getUrl().toString(), "");
        this.nieuwsSoort = stringOrDefault(nieuwsSoort, "");
        this.rechtsgebied = stringOrDefault(rechtsgebieden, "");
        this.rubrieken = stringOrDefault(rubrieken, "");

        indexedOn = DateUtil.getTodayAsTimestamp();

		uid = String.format("%s:%s:%d", websiteUrl, this.url, indexedOn);
    }

    public Article(Document doc) {
		uid = doc.get(UID);

		url = doc.get(URL);
        websiteUrl = doc.get(WEBSITE_URL);

		depth = getFromDocumentOrDefault(doc, DEPTH, "0");
		title = getFromDocumentOrDefault(doc, TITLE_STORE, "");
		content = getFromDocumentOrDefault(doc, CONTENT_STORE, "");
		// System.out.println("Indexed contents: " +content);
		snippet = getFromDocumentOrDefault(doc, SNIPPET, "");
		date = getFromDocumentOrDefault(doc, DATE_STORE, "0");
		String indexedOn_stored = getFromDocumentOrDefault(doc, INDEXED_ON_STORE, "0");
        if(null != indexedOn_stored)
            indexedOn = Long.decode(indexedOn_stored);

		nieuwsSoort = getFromDocumentOrDefault(doc, Tag.NIEUWSSOORT, "");
		rechtsgebied = getFromDocumentOrDefault(doc, Tag.RECHTSGEBIED, "");
		rubrieken = getFromDocumentOrDefault(doc, Tag.RUBRIEK, "");
    }

	private static String getFromDocumentOrDefault(Document doc, String field, String defaultValue) {
		String[] values = doc.getValues(field);
		if(values.length == 0)
			return defaultValue;

		StringBuilder builder = new StringBuilder();
		for(String value : values) {
			if(builder.length() > 0)
				builder.append(SEPERATOR);
			builder.append(value);
		}

		return builder.toString();
	}

	private static String stringOrDefault(String string, String defaultString) {
        if(null == string)
            string = defaultString;
        return string;
    }

	public void makeSnippet() {
        char[] chars = content.toCharArray();

        if(chars.length <= SNIPPETMAX)
        {
            snippet = new String(chars);
            return;
        }
        int lastSpace = 0;
        int finalCharacter = 0;
        for(int index = SNIPPETMAX; index > SNIPPETMIN; index--)
        {
            //keep going untill the end of a sentence is found
            if(chars[index] == '.' || chars[index] == '!' || chars[index] == '?')
            {
                finalCharacter = index + 1;
                break;
            }
            //Save the locations of any spaces found, incase it doesn't find a sentence ending symbol
            else if(chars[index] == ' ' && lastSpace == 0)
                lastSpace = index;
        }
        //if there is no ending synbol found, simply use the last space as ending
        if(finalCharacter == 0)
            finalCharacter = lastSpace;
        //if there was also no space found, just cut of the sentence at 150 characters
        if(finalCharacter == 0)
            finalCharacter = SNIPPETMAX;

        snippet = new String(java.util.Arrays.copyOfRange(chars, 0, finalCharacter)).trim();
        if(snippet.indexOf("\\&nbsp;+") != -1){
            snippet = snippet.replaceAll("\\&nbsp;+", " ");
        }
        if(snippet.indexOf("\\s;+") != -1){
            snippet = snippet.replaceAll("\\s;+", " ");
        }
        snippet = snippet.trim();
    }

    public int delete()
    {
		return Article.deleteNoCheck(uid);
    }

	public Article update(String uid) {
        Article article = null;
        try {
			article = Article.update(this, uid);
        } catch(Exception e) {
            LogUtil.logException(e);
        }

        return article;
    }

	public Article update() {
		return update(uid);
    }

    public Article removeAllTags(String type)
    {
        if(type.equals(Tag.RECHTSGEBIED))
            rechtsgebied = "";
        else if(type.equals(Tag.NIEUWSSOORT))
            nieuwsSoort = "";
        else if(type.equals(Tag.RUBRIEK))
            rubrieken = "";
        return update();
    }

    public Article removeTags(Map<String, Tag> toRemove) {
        List<Tag> tags = getTags(Tag.NIEUWSSOORT);
        tags.addAll(getTags(Tag.RECHTSGEBIED));
        tags.addAll(getTags(Tag.RUBRIEK));

        StringBuilder nRe = new StringBuilder(), nRu = new StringBuilder(), nNi = new StringBuilder();
        for(Tag tag: tags) {
            String id = tag.getId();
            if(toRemove.containsKey(id))
                continue;

            StringBuilder t;
            String type = tag.getType();
            if(Tag.RECHTSGEBIED.equals(type))
                t = nRe;
            else if(Tag.NIEUWSSOORT.equals(type))
                t = nNi;
            else if(Tag.RUBRIEK.equals(type))
                t = nRu;
            else
                continue;

            if(t.length() > 0)
				t.append(SEPERATOR);

            t.append(id);
        }

        rechtsgebied = nRe.toString().trim();
        nieuwsSoort = nNi.toString().trim();
        rubrieken = nRu.toString().trim();

        return update();
    }

    public Article addTag(Tag tag) {
        if(tag == null || hasTag(tag))
            return this;

		String type = tag.getType();
		if(type.equals(Tag.RECHTSGEBIED))
            rechtsgebied = addTag(rechtsgebied, tag);
		else if(type.equals(Tag.NIEUWSSOORT))
            nieuwsSoort = addTag(nieuwsSoort, tag);
		else if(type.equals(Tag.RUBRIEK))
            rubrieken = addTag(rubrieken, tag);

        return update();
    }

	private String addTag(String currentTags, Tag newTag) {
		if(hasTag(newTag))
			return currentTags;

		if(newTag.getType().equals(Tag.RECHTSGEBIED)) {
			String name = newTag.getName();
			int parentEndIndex = name.indexOf(":");
			if(parentEndIndex > 0) {
				String parentName = name.substring(0, parentEndIndex).toLowerCase();
				Tag parent = TagDao.findByName(parentName, Tag.RECHTSGEBIED);
				if(parent != null && (!hasTag(parent)))
					currentTags = addTag(currentTags, parent);
			}
		}

		StringBuilder newTags = new StringBuilder();

		newTags.append(currentTags);

		if(newTags.length() > 0)
			newTags.append(SEPERATOR);

		newTags.append(newTag.getId());

		return newTags.toString();
    }

	public Article addTags(Collection<Tag> tags)
    {
        for(Tag tag: tags)
        {
            addTag(tag);
        }
        return this;
    }

    public boolean hasTag(Tag tag)
    {
        String gebied = "";
        if(tag.getType().equals(Tag.RECHTSGEBIED))
            gebied = rechtsgebied;
        else if(tag.getType().equals(Tag.NIEUWSSOORT))
            gebied = nieuwsSoort;
        else if(tag.getType().equals(Tag.RUBRIEK))
            gebied = rubrieken;

        String[] tagIds = gebied.split(", ");
        for(String id: tagIds)
        {
            if (id.equals(tag.getId()))
                return true;
        }

        return false;
    }

    //getters
    public String getTitle() {return title;}
    public String getDate() { return date; }

    @Override
    public String toString() {
        return "Article{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", date='" + date + '\'' +
                ", indexedOn=" + indexedOn +
                ", content='" + content + '\'' +
                ", snippet='" + snippet + '\'' +
                ", websiteUrl='" + websiteUrl + '\'' +
                ", nieuwsSoort='" + nieuwsSoort + '\'' +
                ", rubrieken='" + rubrieken + '\'' +
                ", rechtsgebied='" + rechtsgebied + '\'' +
                ", depth='" + depth + '\'' +
                ", uid='" + uid + '\'' +
                '}';
    }

    public String getDate(String dateFormat) {
        if(1 == date.length())
            return "";

        return DateUtil.convertTimestampToFormat(Long.decode(date), dateFormat);
    }
    public Long getIndexedOn() { return indexedOn; }
	public String getIndexedOn(String dateFormat) {
		return DateUtil.convertTimestampToFormat(indexedOn, dateFormat);
	}

	public String getWebsiteName() {
		Website page = WebsiteDao.findByUrl(websiteUrl);
		if(page == null) {
			return websiteUrl;
		}
		return page.getName();
	}
    public String getContents() {return content;}
    public String getUrl() {return url;}

	public String getUid() {
		return uid;
	}
    public String getWebsiteUrl() {return websiteUrl;}
    public String getDepth() { return depth;}
    public String getSnippet() {

        boolean snippetExists = null != snippet && 0 < snippet.length();
        if(!snippetExists && 0 < getContents().length())
            makeSnippet();

        return snippet.trim();
    }
    public String getRechtsgebied() { return rechtsgebied.trim(); }
    public List getRechtsgebiedTags() { return getTags(Tag.RECHTSGEBIED); }
    public List getNieuwsSoortTags() { return getTags(Tag.NIEUWSSOORT); }

    public List getTags() {
        return getTags(null);
    }
    public List getTags(String tagType) {
        StringBuilder idString = new StringBuilder();
        if(null == tagType || Tag.NIEUWSSOORT.equals(tagType))
            idString.append(nieuwsSoort);

		if(null == tagType || Tag.RECHTSGEBIED.equals(tagType)) {
			if(idString.length() > 0)
				idString.append(SEPERATOR);

			idString.append(rechtsgebied);
		}

		if(null == tagType || Tag.RUBRIEK.equals(tagType)) {
			if(idString.length() > 0)
				idString.append(SEPERATOR);

			idString.append(rubrieken);
		}

		String[] ids = idString.toString().split(SEPERATOR);

		List tags = CollectionFactory.createList();

        for(String id: ids) {
            if(id.length() <= 0){
                continue;
            }

            Tag tag = TagDao.findById(id);
            if(tag != null)
                tags.add(tag);
        }

        return tags;
    }

    public String[] getSquareHits()
    {
        List<Tag> tags = getTags(Tag.RUBRIEK);
        Tag tag = null;
        if(tags.size() > 0)
            tag = tags.get(0);

        String code = "DE";
        String name = "Default";
        if(tag != null) {
            code = tag.getImageCode();
            name = tag.getName();
        }
        String[] square = new String[2];
        square[0] = code;
        square[1] = name;

        return square;
    }

    //setters
    public void setTitle(String newTitle) {title = newTitle;}
    public void setDate(String Date) {date = Date;}
    public void setSnippet(String Snippet)
    {
        Snippet = Snippet.replaceAll("[\\s ]+", " ");
        snippet = Snippet.trim();
        if(snippet.length() > SNIPPETMAX) {
            snippet = snippet.substring(0, SNIPPETMAX);
        }

        snippet = Jsoup.clean(snippet, Whitelist.relaxed());

        String ending = snippet.substring(snippet.length()-3);
        if(!ending.equals("..."))
            snippet += "...";
    }
    public void setContents(String Contents)
    {
        content = Contents;
        makeSnippet();
    }

    public static List<Article> findByWebUrl(String Url)
    {
		return find(WEBSITE_URL_SEARCH, Url);
    }

	public static Article findByUid(String uid) {
        Article article = null;

		List<Article> articles = find(UID, uid);
        if(articles.size() > 0)
            article = articles.get(0);

        return article;
    }

	private static List find(String field, String value) {
		List articles = CollectionFactory.createList();

        Query q = new TermQuery(new Term(field, value));

        Map results = IndexManager.getInstance().search(q);
        ArrayList<Document> docs = (ArrayList<Document>)results.get("documents");

		for(Document doc : docs) {
            articles.add(new Article(doc));
        }

        return articles;
    }

	public static List<Article> getAll() {
		List articles = CollectionFactory.createList();

		Map results = IndexManager.getInstance().search(new MatchAllDocsQuery());
        List<Document> docs = (ArrayList<Document>)results.get("documents");

		for(Document doc : docs) {
            Article article = new Article(doc);
            articles.add(article);
        }

        return articles;
    }

	public static int delete(String uid) {
		if(Article.findByUid(uid) == null)
            return 0;
		return deleteNoCheck(uid);
    }

    public static int deleteByPage(String pageUrl) {
        ArticleHealthDao.deleteByPageUrl(pageUrl);
		return deleteNoCheck(WEBSITE_URL_SEARCH, pageUrl.toLowerCase());
    }

	public static int deleteNoCheck(String uid) {
		return deleteNoCheck(UID, uid);
	}
    public static int deleteNoCheck(String field, String value)
    {
        int deleted = 0;
        try {
            Query q = new TermQuery(new Term(field, value));
            deleted = IndexManager.deleteDocuments(q);
        }
        catch(IOException e) {
            LogUtil.logException(e);
        }
        return deleted;
    }

	public static Boolean deleteAll() {
		try {
			IndexManager.getInstance().startWriting();
            Query query = new MatchAllDocsQuery();
			IndexManager.deleteDocuments(query);
			IndexManager.getInstance().finishWritingAndClose();
        } catch(IOException e) {
            LogUtil.logException(e);
            return false;
        }
        return true;
    }

    public static Article update(Article article)
    {
		return Article.update(article, article.getUid());
    }

	public static Article update(Article article, String uid) {
        //updateEndDate doesn't work with queries, but it is simply a combination of delete and add so this does the same thing
		delete(uid);
        create(article);

        return article;
    }

	public static Article create(String Title, String Content, String Date, String Url, String Depth, Website page) {
		return create(Title, Content, Date, Url, Depth, page, "", "", "");
//		return create(Title, Content, Date, Url, Depth, page, page.getNieuwssoort().getDefaultValue(), "", page.getRubrieken().getDefaultValue());
    }

	public static Article create(String Title, String Content, String Date, String Url, String Depth, Website page, String NieuwsSoort, String Rechtsgebied, String Rubrieken) {
		Article article = new Article(Title, Content, Date, Url, Depth, page, NieuwsSoort, Rechtsgebied, Rubrieken);
        article.makeSnippet();
        return Article.create(article);
    }
    public static Article create(Article article) {
        try {
            IndexManager manager = IndexManager.getInstance();
            boolean stop = manager.startWriting();
			Document luceneDoc = Article.getLuceneDocument(article);
            manager.addDocument(luceneDoc);
            if(stop)
                manager.finishWriting();
        } catch(IOException e) {
            LogUtil.logException(e);
        } catch(IndexManagerException e) {
            LogUtil.logException(e);
        }

        return article;
    }

	public static Document getLuceneDocument(Article article) {
		//longpoint for queries, String for storage and NumericDoc for sorting
        Document luceneDoc = new Document();

		luceneDoc.add(new StringField(UID, article.getUid(), Field.Store.YES));

        luceneDoc.add(new StringField(URL, article.getUrl(), Field.Store.YES));
        luceneDoc.add(new TextField(DEPTH, article.getDepth(), Field.Store.YES));
		luceneDoc.add(new TextField(SNIPPET, article.getSnippet(), Field.Store.YES));

		luceneDoc.add(new TextField(WEBSITE_URL, article.getWebsiteUrl(), Field.Store.YES));
		luceneDoc.add(new StringField(WEBSITE_URL_SEARCH, article.getWebsiteUrl().toLowerCase(), Field.Store.YES));
		luceneDoc.add(new TextField(WEBSITE_NAME_SEARCH, article.getWebsiteName().toLowerCase(), Field.Store.NO));

		String title = article.getTitle();
		luceneDoc.add(new TextField(TITLE_SEARCH, title.toLowerCase(), Field.Store.NO));
		luceneDoc.add(new TextField(TITLE_STORE, title, Field.Store.YES));

		String contents = article.getContents();
		luceneDoc.add(new TextField(CONTENT_SEARCH, contents.toLowerCase(), Field.Store.NO));
		luceneDoc.add(new StoredField(CONTENT_STORE, contents));

		Long indexedOn = article.getIndexedOn();
		luceneDoc.add(new LongPoint(INDEXED_ON_SEARCH, indexedOn));
		luceneDoc.add(new StoredField(INDEXED_ON_STORE, indexedOn));
		luceneDoc.add(new NumericDocValuesField(INDEXED_ON_SORT, indexedOn));

        List tags = article.getTags();
        luceneDoc = addTagsToDocument(luceneDoc, tags);

        Long date = Long.valueOf(article.getDate());
        luceneDoc.add(new LongPoint(DATE_SEARCH, date));
		luceneDoc.add(new StoredField(DATE_STORE, date));
        luceneDoc.add(new NumericDocValuesField(DATE_SORT, date));

        return luceneDoc;
    }

	private static Document addTagsToDocument(Document doc, List<Tag> tags) {
        for(Tag tag: tags) {
            String tagType = tag.getType();
            String tagId = tag.getId();
            String indexFieldName = IndexManager.getTagFacetField(tagType);

            doc.add(new TextField(tagType, tagId, Field.Store.YES));
            doc.add(new TextField("tag", tag.getName(), Field.Store.YES));
            doc.add(new FacetField(indexFieldName, tagId));
        }

        return IndexManager.getInstance().buildLuceneFacets(doc);
    }

    public static Sort getDateSorter() {
        Sort sorter = new Sort();

		SortField[] dateSortFields = getDateSortFields();
        SortField[] sorters = new SortField[dateSortFields.length + 1];
        for(int index = 0; index < dateSortFields.length; index++) {
            sorters[index] = dateSortFields[index];
        }

        sorters[dateSortFields.length] = SortField.FIELD_SCORE;

        sorter.setSort(sorters);

        return sorter;
    }
    public static Sort getScoreSorter() {
		SortField[] dateSortFields = getDateSortFields();
        SortField[] sorters = new SortField[dateSortFields.length + 1];
        sorters[0] = SortField.FIELD_SCORE;

        int base = 1;
        for(int index = 0; index < dateSortFields.length; index++) {
            sorters[base + index] = dateSortFields[index];
        }

        return new Sort(sorters);
    }

    public static SortField[] getDateSortFields() {
        SortField[] sortFields = new SortField[1];

        String field = INDEXED_ON_SORT;
        Boolean descending = true;
        SortField sortfield = new SortedNumericSortField(field, SortField.Type.LONG, descending);
        sortFields[0] = sortfield;

//        field = INDEXED_ON_SORT;
//        descending = false;
//        sortfield = new SortedNumericSortField(field, SortField.Type.LONG, descending);
//        sortFields[1] = sortfield;

        return sortFields;
    }

    public static Sort getDefaultSorter() {
        return getDateSorter();
    }

    public static Sort getSortFromString(String sortBy) {
        if(ORDER_DATE_DESCENDING.equals(sortBy))
            return getDateSorter();
        else if(ORDER_SCORE_DESCENDING.equals(sortBy))
            return getScoreSorter();

        return Article.getDefaultSorter();
    }

	public static boolean isApproximateField(String field) {
		if(APPROXIMATE_FIELDS.size() == 0) {
			APPROXIMATE_FIELDS.put(CONTENT_SEARCH, true);
			APPROXIMATE_FIELDS.put(TITLE_SEARCH, true);
		}

		return APPROXIMATE_FIELDS.containsKey(field);
	}


}
