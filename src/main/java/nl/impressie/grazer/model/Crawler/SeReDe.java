package nl.impressie.grazer.model.Crawler;

import nl.impressie.grazer.util.EncodeUtil;
import nl.impressie.grazer.util.RegexUtil;
import nl.impressie.grazer.util.UrlUtil;
import nl.impressie.grazer.util.crawlerUtils.IndexUtil;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Map;
import java.util.regex.Matcher;

/**
 * @author Impressie
 */
public class SeReDe {
	public final static String SELECTOR = "$selector$";
	public final static String REGEX = "$regex$";
	public final static String DEFAULT = "$default$";
	public final static String PROPERTY = "$property$";

	public final static String ENCODING_SYMBOL = "$";

	public final static String SEPERATOR = ", ";

	public final static String EMPTY_SEREDE_STRING = new SeReDe().toString();

	private String selector;
	private String property;
	private String regex;
	private String defaultValue;

	public String getProperty() {
		return property;
	}

	public String getSelector() {
		return getSelector(false);
	}

	public String getSelector(boolean encode) {
		if(encode)
			return UrlUtil.encodeTrimWhitespace(selector);
		return selector;
	}

	public String getRegex() {
		return getRegex(false);
	}

	public String getRegex(boolean encode) {
		if(encode)
			return UrlUtil.encodeTrimWhitespace(regex);
		return regex;
	}

	public String getDefaultValue() {
		return getDefaultValue(false);
	}

	public String getDefaultValue(boolean encode) {
		if(encode)
			return UrlUtil.encodeTrimWhitespace(defaultValue);
		return defaultValue;
	}


	public SeReDe() {
		this("", "", "", "");
	}

	public SeReDe(String encodedString) {
		this(EncodeUtil.load(encodedString, SELECTOR), EncodeUtil.load(encodedString, PROPERTY), EncodeUtil.load(encodedString, REGEX), EncodeUtil.load(encodedString, DEFAULT));
	}

	public SeReDe(String selector, String property, String regex, String defaultValue) {
		this.selector = clean(selector);
		this.property = clean(property);
		this.regex = StringNotNull(regex);
		this.defaultValue = StringNotNull(defaultValue);
	}

	private String clean(String toClean) {
		toClean = StringNotNull(toClean);
		toClean = toClean.replaceAll("\\n+", " ");
		toClean = toClean.replaceAll("\\s+", " ");
		return toClean.trim();
	}

	private String StringNotNull(String string) {
		if(null == string) {
			string = "";
		}
		return string;
	}

	public boolean isEmpty() {
		return toString().equals(EMPTY_SEREDE_STRING);
	}

	@Override
	public String toString() {
		String encodedSelector = EncodeUtil.encode(selector, SELECTOR);
		String encodedProperty = EncodeUtil.encode(property, PROPERTY);
		String encodedRegex = EncodeUtil.encode(regex, REGEX);
		String encodedDefaultValue = EncodeUtil.encode(defaultValue, DEFAULT);

		String encodedFull = String.format("%1$s%2$s%3$s%4$s", encodedSelector, encodedProperty, encodedRegex, encodedDefaultValue);
		SeReDe compare = SeReDe.loadSeReDe(encodedFull);

		//compare
		if(!compareSeReDe(compare, this))
			return null;
		return encodedFull;
	}

	public Map addToCurrent(String name, Map current) {

		if(!selector.isEmpty() || !regex.isEmpty() || !defaultValue.isEmpty())
			current.put(name, true);

		current.put(String.format("serede-%s-selector", name), selector);
		current.put(String.format("serede-%s-property", name), property);
		current.put(String.format("serede-%s-regex", name), regex);
		current.put(String.format("serede-%s-default", name), defaultValue);

		return current;
	}

	public static boolean compareSeReDe(SeReDe one, SeReDe two) {
		if(!one.getDefaultValue().equals(two.getDefaultValue()))
			return false;
		if(!one.getRegex().equals(two.getRegex()))
			return false;
		if(!one.getSelector().equals(two.getSelector()))
			return false;
		return one.getProperty().equals(two.getProperty());
	}

	public static SeReDe loadSeReDe(String loadableString) {
		return new SeReDe(loadableString);
	}

	public static boolean isLoadableSeReDeString(String string) {
		SeReDe seReDe = loadSeReDe(string);
		return seReDe.toString().equals(string);
	}

	public Elements getElements(Element document, Elements before) {
		return getElements(new Elements(document), before);
	}

	public Elements getElements(Elements document, Elements before) {
		if(0 == selector.length())
			return new Elements(document);

		Elements elements = IndexUtil.getElements(document, selector);

		if(null != before)
			elements.addAll(IndexUtil.getElements(before, selector));

		return elements;
	}

	public String ElementsToString(Elements elements, String seperator) {
		StringBuilder builder = new StringBuilder();
		for(String text : IndexUtil.getTextContent(elements, property)) {
			if(text.trim().length() == 0)
				continue;

			if(0 < builder.length())
				builder.append(seperator);
			builder.append(text);
		}
		return builder.toString();
	}

	public String runRegex(String content) {
		if(0 == regex.length())
			return content;

		//make the search quote insensitive(double and single quotes are counted as the same)
		String regex = this.regex.replaceAll("\"", "'");
		content = content.replaceAll("\"", "'");

		//replace nbsp with a normal space
		regex = regex.replaceAll(" ", " ");
		content = content.replaceAll(" ", " ");

		StringBuilder textBuilder = new StringBuilder();
		Matcher matcher = RegexUtil.getCaseInsensitiveMatcher(regex, content);

		while(matcher.find()) {
			String found = matcher.group(0);
			if(found.trim().length() == 0)
				continue;

			if(0 < textBuilder.length())
				textBuilder.append(SEPERATOR);
			textBuilder.append(found);
		}

		if(0 < textBuilder.length())
			content = textBuilder.toString();

		return content;
	}

	private String addDefault(String content) {
		StringBuilder builder = new StringBuilder();
		if(0 < content.trim().length())
			builder.append(content);

		if(0 < defaultValue.trim().length()) {
			if(0 < builder.length())
				builder.append(SEPERATOR);

			builder.append(defaultValue);
		}

		return builder.toString();
	}

	public String getContent(Elements document, Elements before) {
		return getContent(document, before, SEPERATOR);
	}

	public String getContent(Elements document, Elements before, String seperator) {
		String content = ElementsToString(getElements(document, before), seperator);
		content = runRegex(content);
		content = addDefault(content);
		// content = content.replace(';', ':');

		// System.out.println("GetContent1: " + content);
		// String[] contentArray = content.split(";");
		// String returnValue = contentArray[contentArray.length-1];
		// System.out.println("GetContent2: " + returnValue);
		// return returnValue.trim();

		return content.trim();
	}
}
