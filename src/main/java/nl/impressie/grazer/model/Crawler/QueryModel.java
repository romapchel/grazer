package nl.impressie.grazer.model.Crawler;

import org.apache.lucene.document.IntPoint;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;

import java.util.List;

/**
 * @author Impressie
 */
public class QueryModel {
	private BooleanQuery.Builder query;

	public QueryModel() {
		query = new BooleanQuery.Builder();
	}

	public void addMultipleFieldQuery(List<List<String>> terms, List<String> fields) {
		int i = 0;
		for(String field : fields) {
			if(Article.isApproximateField(field))
				addApproximateFieldQuery(terms.get(i), field);
			else
				addFieldQuery(terms.get(i), field);
			i++;
		}
	}

	public void addApproximateFieldQuery(List<String> terms, String field) {
		BooleanQuery.setMaxClauseCount(2048);
		BooleanQuery.Builder mainQuery = new BooleanQuery.Builder();

		for(String term : terms) {
			term = term.trim();
			if(!term.substring(term.length() - 1).equals("*"))
				term = term + "*";
			if(term.indexOf("*") != 0)
				term = "*" + term;
			if(terms.size() > 1)
				mainQuery.add(new WildcardQuery(new Term(field, term)), BooleanClause.Occur.MUST);
			else if(terms.size() == 1)
				query.add(new WildcardQuery(new Term(field, term)), BooleanClause.Occur.MUST);
		}
		if(terms.size() > 1)
			query.add(mainQuery.build(), BooleanClause.Occur.MUST);
	}

	public void addFieldQuery(List<String> terms, String field) {
		BooleanQuery.Builder mainQuery = new BooleanQuery.Builder();

		for(String term : terms) {
			mainQuery.add(new TermQuery(new Term(field, term)), BooleanClause.Occur.SHOULD);
		}

		query.add(mainQuery.build(), BooleanClause.Occur.MUST);
	}

	public void addSplitTermQueryOrZero(String field, String term, Boolean mustMatch, String splitTerm) {
		String[] splittedTerms = term.split(splitTerm);
		if(splittedTerms.length > 1) {
			BooleanQuery.Builder mainQuery = new BooleanQuery.Builder();

			for(String partTerm : splittedTerms) {
				mainQuery.add(new TermQuery(new Term(field, partTerm)), BooleanClause.Occur.SHOULD);
			}
			mainQuery.add(new TermQuery(new Term(field, "0")), BooleanClause.Occur.SHOULD);

			BooleanClause.Occur occur = mustMatch ? BooleanClause.Occur.MUST : BooleanClause.Occur.SHOULD;

			query.add(mainQuery.build(), occur);
		} else {
			addTermQueryOrZero(field, term, mustMatch);
		}
	}

	public void addSplitTermQuery(String field, String term, Boolean mustMatch, String splitTerm, Boolean mustMatchAllTerms) {
		String[] splittedTerms = term.split(splitTerm);
		if(splittedTerms.length > 1) {
			BooleanQuery.Builder mainQuery = new BooleanQuery.Builder();

			for(String partTerm : term.split(splitTerm)) {
				BooleanClause.Occur occur = mustMatchAllTerms ? BooleanClause.Occur.MUST : BooleanClause.Occur.SHOULD;
				mainQuery.add(new TermQuery(new Term(field, partTerm)), occur);
			}

			BooleanClause.Occur occur = mustMatch ? BooleanClause.Occur.MUST : BooleanClause.Occur.SHOULD;

			query.add(mainQuery.build(), occur);
		} else {
			addTermQuery(field, term, mustMatch);
		}
	}

	public void addTermQueryOrZero(String field, String term, Boolean mustMatch) {
		BooleanClause.Occur occur = mustMatch ? BooleanClause.Occur.MUST : BooleanClause.Occur.SHOULD;
		BooleanQuery.Builder mainQuery = new BooleanQuery.Builder();

		mainQuery.add(new TermQuery(new Term(field, term)), BooleanClause.Occur.SHOULD);
		mainQuery.add(new TermQuery(new Term(field, "0")), BooleanClause.Occur.SHOULD);

		query.add(mainQuery.build(), occur);
	}

	public void addTermQuery(String field, String term, Boolean mustMatch) {
		BooleanClause.Occur occur = mustMatch ? BooleanClause.Occur.MUST : BooleanClause.Occur.SHOULD;
		query.add(new TermQuery(new Term(field, term)), occur);
	}

	public void addRangeQuery(int begin, int end, String field, Boolean orZero) {
		Query range = IntPoint.newRangeQuery(field, begin, end);

		if(orZero) {
			BooleanQuery.Builder boolSubQuery = new BooleanQuery.Builder();

			boolSubQuery.add(range, BooleanClause.Occur.SHOULD);
			range = IntPoint.newExactQuery(field, 0);
			boolSubQuery.add(range, BooleanClause.Occur.SHOULD);
			query.add(boolSubQuery.build(), BooleanClause.Occur.MUST);
		} else {
			query.add(range, BooleanClause.Occur.MUST);
		}
	}

	public BooleanQuery getQuery() {
		return query.build();
	}
}
