package nl.impressie.grazer.model.DB;

import lombok.Getter;
import nl.impressie.grazer.dao.AdditionalSourceLinkDao;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.enums.CSFExport;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.hibernate.models.AdditionalSourceLinkModel;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.EncodeUtil;
import nl.impressie.grazer.util.LogUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
@Getter
public class AdditionalSourceLink extends Model {
	private int pageId;
	private String url;

	private Website page;

	public AdditionalSourceLink() { super(0); }
	public AdditionalSourceLink(AdditionalSourceLinkModel model) {
		this(model.getId().intValue(), model.getPage().getId().intValue(), model.getUrl());
	}
	public AdditionalSourceLink(int id, Website page, String url) {
		this(id, (int)page.getId(), url);
		this.page = page;
	}
	public AdditionalSourceLink(int id, int pageId, String url) {
		super(id);
		this.pageId = pageId;
		this.url = url;
	}

	public Map getCurrent() {
		return getCurrent("");
	}
	public Map getCurrent(String prefix) {
		return CollectionFactory.createMap(
				new CollectionFactory.MapPair(prefix + AdditionalSourceLinkDao.URL, url)
		);
	}

	public String CSFExport() {
		Map current = getCurrent("");
		current.put(AdditionalSourceLinkDao.ID, id);
		current.put(AdditionalSourceLinkDao.PAGE_ID, pageId);
		return EncodeUtil.encode(current);
	}

	public static List<AdditionalSourceLink> CSFImport(String loadable, Object pageId) {
		Set<String> columns = CollectionFactory.createSet(
				AdditionalSourceLinkDao.ID,
				AdditionalSourceLinkDao.PAGE_ID,
				AdditionalSourceLinkDao.URL
		);
		List made = CollectionFactory.createList();
		for(String value: CSFExport.splitList(loadable)) {
			if(value.trim().length() == 0)
				continue;

			Map values = EncodeUtil.load(value, columns);
			if(pageId != null)
				values.put(AdditionalSourceLinkDao.PAGE_ID, pageId);
			made.add(AdditionalSourceLinkDao.updateOrCreate(values));
		}
		return made;
	}

	public URL getLink() {
		try {
			return new URL(url);
		} catch(MalformedURLException e) {
			LogUtil.logException(e);
		}
		return null;
	}

	public Website getPage() {
		if(page == null)
			page = WebsiteDao.findById(getPageId());
		return page;
	}
}
