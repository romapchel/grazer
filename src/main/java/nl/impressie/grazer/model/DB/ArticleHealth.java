package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.MapUtil;

import java.util.Map;

/**
 * @author Impressie
 */
public class ArticleHealth {
	private int pageId;
	private String pageUrl, url;
	private String checkedContent, checkedTitle, checkedDate;
	private String indexedContent, indexedTitle, indexedDate;
	private boolean compareContent, compareTitle, compareDate, overalHealth;
	private String skipped;
	private Long indecedOn, checkedOn;

	public ArticleHealth(String url, long indexedOnDate, String indexedContent, String indexedDate, String indexedTitle, long checkedOn, String checkedContent, String checkedDate,
						 String checkedTitle, boolean compareContent, boolean compareTitle, boolean compareDate, boolean overalHealth, int pageId, String pageUrl, String skipped) {
		this.url = url;
		this.indecedOn = indexedOnDate;
		this.indexedContent = indexedContent;
		this.indexedDate = indexedDate;
		this.indexedTitle = indexedTitle;
		this.checkedOn = checkedOn;
		this.checkedContent = checkedContent;
		this.checkedDate = checkedDate;
		this.checkedTitle = checkedTitle;
		this.compareContent = compareContent;
		this.compareTitle = compareTitle;
		this.compareDate = compareDate;
		this.overalHealth = overalHealth;
		this.pageId = pageId;
		this.pageUrl = pageUrl;
		this.skipped = skipped;
	}

	public Map addToWebsiteHealthReport(Map report) {
		Map before = CollectionFactory.createMap();
		for(String skip : skipped.split(", ")) {
			before.put(skip, true);
		}

		String checkedOn = "checkedOn";
		if(report.containsKey(checkedOn)) {
//            Long prevTime = (Long)report.get(checkedOn);
//            if(prevTime < this.checkedOn) {
//                report.remove(checkedOn);
//                report.put(checkedOn, this.checkedOn);
//            }
		} else {
			report.put(checkedOn, DateUtil.convertTimestampToFormat(this.checkedOn, "dd-MM-yyyy HH:mm"));
		}
		MapUtil.increaseMapValue(report, "checked");
		if(!report.containsKey("healthy"))
			report.put("healthy", 0);
		if(overalHealth) {
			MapUtil.increaseMapValue(report, "healthy");
		} else {
			Map notHealthy = CollectionFactory.createMap();
			notHealthy.put("url", url);

			if(0 < indexedTitle.length())
				notHealthy.put("name", indexedTitle);

			MapUtil.addToMapList(report, "unhealthy", notHealthy);
		}

		addToWebsiteHealthReport(report, WebsiteDao.CONTENT, compareContent, before);
		addToWebsiteHealthReport(report, WebsiteDao.TITLE, compareTitle, before);
		addToWebsiteHealthReport(report, WebsiteDao.DATE, compareDate, before);

		return report;
	}

	private Map addToWebsiteHealthReport(Map report, String key, boolean compare, Map before) {
		if(before.containsKey(key))
			return report;

		Map map = CollectionFactory.createMap();
		if(report.containsKey(key))
			map = (Map) report.get(key);
		else
			report.put(key, map);

		MapUtil.increaseMapValue(map, "total");
		if(compare)
			MapUtil.increaseMapValue(map, "healthy");

		return report;
	}
}
