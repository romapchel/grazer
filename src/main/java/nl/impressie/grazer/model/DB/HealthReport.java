package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.ArticleHealthDao;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.MapUtil;
import nl.impressie.grazer.util.crawlerUtils.IndexUtil;
import org.jsoup.select.Elements;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * @author Impressie
 */
public class HealthReport {
	private Map checked, before;
	private Website page;
	private List<ArticleHealth> articles;
	private int total;
	private Map health;
	private boolean healthy;
	private boolean added;

	public HealthReport(Website page, int total) {
		this.page = page;
		this.total = total;
		added = true;

		articles = CollectionFactory.createList();
		checked = CollectionFactory.createMap();
		before = CollectionFactory.createMap();
		for(BeforeSelector bs : page.getBeforeSelectors()) {
			bs.addSelectorToMap(before);
		}
	}

	public Map getHealth() {
		if(health != null && !added)
			return health;
		added = false;

		health = CollectionFactory.createMap();
		health.put("total", total);
		health.put("titles", WebsiteDao.getTitles());

		for(ArticleHealth articleHealth : articles) {
			if(null == articleHealth)
				MapUtil.increaseMapValue(health, "checked");
			else
				articleHealth.addToWebsiteHealthReport(health);
		}


		String healthKey = "healthy";
		String checkedKey = "checked";
		boolean hasHealthy = health.containsKey(healthKey);
		boolean hasChecked = health.containsKey(checkedKey);
		healthy = (hasHealthy && hasChecked && health.get(healthKey).equals(health.get(checkedKey)));

		return health;
	}

	public HealthReport addArticle(Article article) {
		added = true;
		String url = article.getUrl();
		ArticleHealth report = ArticleHealthDao.findHealthForPageByUrl(page.getId(), url);
		if(null == report)
			report = generateReport(article, url);
		articles.add(report);

		return this;
	}

	private ArticleHealth generateReport(Article article, String url) {
		Map found = findArticleMap(url, article);
		if(found == null)
			return null;

		String checkedContent = (String) found.get(WebsiteDao.CONTENT);
		String checkedTitle = (String) found.get(WebsiteDao.TITLE);
		String checkedDate = (String) found.get(WebsiteDao.DATE);
		if(checkedDate == null)
			checkedDate = "0";

		String indexedContent = article.getContents();
		String indexedTitle = article.getTitle();
		String indexedDate = article.getDate();

		boolean compareContent = compare(indexedContent, checkedContent);
		boolean compareTitle = compare(indexedTitle, checkedTitle);
		boolean compareDate = compare(indexedDate, checkedDate);
		boolean overalHealth = compareDate && compareTitle && compareContent;

		if(!compareContent)
			compare(indexedContent, checkedContent);

		StringBuilder skippedBuilder = new StringBuilder();
		for(Object key : before.keySet()) {
			if(0 < skippedBuilder.length())
				skippedBuilder.append(", ");
			skippedBuilder.append(key);
		}
		String skipped = skippedBuilder.toString();
		return ArticleHealthDao.create(url, article.getIndexedOn(), indexedContent, indexedDate, indexedTitle, checkedContent,
				checkedDate, checkedTitle, compareContent, compareTitle, compareDate, overalHealth, page.getId(), page.getUrl().toString(), skipped);
	}

	private boolean compare(String indexed, Object checked) {
		if((null == checked && null != indexed) || (null != checked && null == indexed))
			return false;

		String s1 = indexed.replaceAll("[\\P{Print}\\s]", "").toLowerCase();
		String s2 = checked.toString().replaceAll("[\\P{Print}\\s]", "").toLowerCase();
		return s1.equals(s2);
	}

	private Map findArticleMap(String url, Article article) {
		Elements doc = new Elements(IndexUtil.getJsoupDoc(url));
		List<Map> maps = CollectionFactory.createList();
		try {
			maps = IndexUtil.getArticles(new URL(url), doc, page, CollectionFactory.createMap());
		} catch(MalformedURLException e) {
			LogUtil.logException(e);
		}

		Map<String, Object> toTest = CollectionFactory.createMap();
		toTest.put(WebsiteDao.CONTENT, article.getContents());
		toTest.put(WebsiteDao.DATE, article.getDate());
		toTest.put(WebsiteDao.TITLE, article.getTitle());

		int matchIndex = -1;
		int highestMatch = -1;
		int index = 0;
		for(Map map : maps) {
			int matches = 0;

			for(String key : toTest.keySet()) {
				boolean foundInMap = map.containsKey(key);
				if(!foundInMap)
					continue;

				boolean notCollectedBefore = !before.containsKey(key);

				boolean valuesMatch = map.get(key).equals(toTest.get(key));
				if(notCollectedBefore && valuesMatch)
					matches++;
			}

			if(matches > highestMatch) {
				highestMatch = matches;
				matchIndex = index;
			}

			index++;
		}
		if(matchIndex >= 0) {
			return maps.get(matchIndex);
		}
		return null;
	}

	public Object get(String key) {
		return checked.get(key);
	}

	public String getPageTitle() {
		return page.getName();
	}

	public boolean isHealthy() {
		if(added)
			getHealth();
		return healthy;
	}
}
