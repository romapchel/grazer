package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.dao.TagPageDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Model;

import java.util.List;

/**
 * @author Impressie
 */
public class Tag extends Model {
	private String name;
	private String code;
	private int id;
	private String type;
	private boolean disabled;
	private String description;

	public final static String NIEUWSSOORT = "soort", RECHTSGEBIED = "rechtsgebied", RUBRIEK = "rubriek", MAILING = "E-mailattendering(en)";
	public static List<String> types;

	public Tag(int idNumber, String type, String name, String code, boolean disabled, String description) {
		super(idNumber);

		if(code == null)
			code = "";

		id = idNumber;
		this.name = name;
		this.type = type;
		this.code = code;
		this.disabled = disabled;
		this.description = description;
	}

	public static List<String> getTypes() {
		if(null == types) {
			types = CollectionFactory.createList(
					NIEUWSSOORT,
					RUBRIEK,
					RECHTSGEBIED
			);
		}

		return types;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return String.valueOf(id);
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public String getImageCode() {
		String cc = getCode();
		if(0 < cc.length())
			return cc;

		String[] words = name.split(" ");
		StringBuilder code = new StringBuilder();
		for(String word : words) {
			if(word.length() > 0)
				code.append(word, 0, 1);
		}
		return code.toString();
	}

	public static boolean isValidtagType(String tagtype) {
		return getTypes().contains(tagtype);
	}

	public int getNumberOfArticles() {
		List tags = CollectionFactory.createList(this);
		return TagDao.getArticlesForTags(tags).size();
	}
	public int getNumberOfWebsites() {
		return TagPageDao.findByTagIds(String.valueOf(id)).size();
	}

	@Override
	public String toString() {
		return String.format("type: [%s] id: [%s] name: [%s]", getType(), getId(), getName());
	}

	public void setName(String name) {
		this.name = name;
	}
}
