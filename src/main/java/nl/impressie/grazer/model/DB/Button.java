package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.KeyValue;
import nl.impressie.grazer.util.DatabaseUtils.CRUDObject;
import nl.impressie.grazer.util.XMLUtil;
import org.jsoup.nodes.Element;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Impressie
 */
public class Button {
	private int id;
	private int pageId;

	private String testId;
	private String selector;
	private int clicks;
	private int order;

	private Boolean indexAfterEachClick;

	public static final String TABLE_NAME = "page_button";
	public static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public Button mapRow(ResultSet rs, int i) throws SQLException {
			int id = rs.getInt("id");
			int pageId = rs.getInt("page_id");
			int clicks = rs.getInt("clicks");
			int order = rs.getInt("order");

			String selector = rs.getString("selector");
			String testId = rs.getString("test_id");

			boolean indexAfterClick = rs.getBoolean("index_after_each_click");

			Button button = new Button(id, pageId, selector, clicks, order, indexAfterClick, testId);

			return button;
		}
	};

	public Button(int id, int pageId, String selector, int clicks, int order, boolean indexAfterEachClick, String testId) {
		this.id = id;
		this.pageId = pageId;
		if(selector == null)
			selector = "";
		this.selector = selector;
		this.clicks = clicks;
		this.order = order;
		this.indexAfterEachClick = indexAfterEachClick;
		if(testId == null)
			testId = "";
		this.testId = testId;

	}

	public void update() {
		Button.update(id, pageId, selector, clicks, order, indexAfterEachClick, testId);
	}

	public Element toXml() {
		Element button = new Element("button");

		XMLUtil.saveAddElement(button, XMLUtil.createElement("selector", selector));
		XMLUtil.saveAddElement(button, XMLUtil.createElement("testid", testId));
		XMLUtil.saveAddElement(button, XMLUtil.createElement("clicks", String.valueOf(clicks)));
		XMLUtil.saveAddElement(button, XMLUtil.createElement("order", String.valueOf(order)));
		XMLUtil.saveAddElement(button, XMLUtil.createElement("indexAfterEachClick", String.valueOf(indexAfterEachClick)));

		return button;
	}

	//getters
	public int getId() {
		return id;
	}

	public int getPageId() {
		return pageId;
	}

	public String getSelector() {
		return selector;
	}

	public String getTestId() {
		return testId;
	}

	public int getClicks() {
		return clicks;
	}

	public int getOrder() {
		return order;
	}

	public Boolean getIndexAfterEachClick() {
		return indexAfterEachClick;
	}

	public StringBuilder toHtml(StringBuilder builder, int number) {
		String index = String.valueOf(number);
		String checked = "";
		if(indexAfterEachClick)
			checked = " checked=\"checked\"";

		builder.append("<div class=\"button\">");
		builder.append("<div class=\"testID\">");
		builder = makeLabel(builder, index, "testid", "ID van een element dat aanwezig moet zijn daarmee op de knop wordt gedrukt");
		builder = makeInput(builder, index, "testid", "text", "ID test element", testId);
		builder.append("</div>");
		builder.append("<div class=\"selector\">");
		builder = makeLabel(builder, index, "selector", "CSS selector van de knop");
		builder.append(String.format("<textarea id=\"selector%s\" name=\"selector[]\" placeholder=\"CSS selector van de knop\">%s</textarea>", index, selector));
		builder.append("</div>");
		builder.append("<div>");
		builder = makeLabel(builder, index, "clicks", "Hoe vaak te knop moet worden ingedrukt");
		builder = makeInput(builder, index, "clicks", "number", "klikken", String.valueOf(clicks));
		builder.append("</div>");
		builder.append("<div>");
		builder = makeLabel(builder, index, "order", "Dit getal geeft aan in welke volgorde de knoppen moeten worden ingedrukt(laag naar hoog)");
		builder = makeInput(builder, index, "order", "number", "volgorde", String.valueOf(order));
		builder.append("</div>");
		builder.append("<div>");
		builder = makeLabel(builder, index, "indexAfter", "moet de crawler elke keer worden uitgevoerd nadat er op de knop is gedrukt?");
		builder = makeCheckbox(builder, index, "indexAfter", checked);
		builder.append("</div>");
		builder.append("</div>");

		return builder;
	}

	private StringBuilder makeCheckbox(StringBuilder builder, String index, String name, String checked) {
		builder.append(String.format("<input type=\"checkbox\" id=\"%1$s%2$s\" name=\"%1$s[]\" value=\"True\"%3$s />", name, index, checked));
		return builder;
	}

	private StringBuilder makeInput(StringBuilder builder, String index, String name, String type, String placeholder, String value) {
		builder.append(String.format("<input type=\"%1$s\" id=\"%2$s%3$s\" name=\"%2$s[]\" placeholder=\"%4$s\" value=\"%5$s\" />", type, name, index, placeholder, value));
		return builder;
	}

	private StringBuilder makeLabel(StringBuilder builder, String index, String name, String labelText) {
		builder.append(String.format("<label for=\"%1$s%2$s\">%3$s</label>", name, index, labelText));
		return builder;
	}

	public static ArrayList<Button> findByPageId(int pageId) {
		return findByPageId(String.valueOf(pageId));
	}

	public static ArrayList<Button> findByPageId(String pageId) {
		return find("page_id", pageId);
	}

	public static Button findById(int id) {
		return findById(String.valueOf(id));
	}

	public static Button findById(String id) {
		ArrayList<Button> roles = find("id", id);

		if(roles.size() == 1)
			return roles.get(0);

		return null;
	}

	public static ArrayList<Button> find(String by, String value) {
		return (ArrayList<Button>) CRUDObject.select(TABLE_NAME, ROWMAP, by, value);
	}

	public static int update(int id, int pageId, String selector, int clicks, int order, boolean indexAfterEachClick, String testId) {
		List params = CollectionFactory.createList();
		List updates = CollectionFactory.createList();

		params.add(new KeyValue("id", id));
		updates.add(new KeyValue("page_id", pageId));
		updates.add(new KeyValue("selector", selector));
		updates.add(new KeyValue("clicks", clicks));
		updates.add(new KeyValue("order", order));
		updates.add(new KeyValue("index_after_each_click", indexAfterEachClick));
		updates.add(new KeyValue("test_id", testId));

		return CRUDObject.update(TABLE_NAME, params, updates);
	}

	public static Button create(Object pageId, String selector, int clicks, int order, boolean indexAfterEachClick, String testId) {
		if(!isNew(pageId, selector, clicks, order, indexAfterEachClick, testId))
			return null;

		List insertList = CollectionFactory.createList();
		insertList.add(new KeyValue("page_id", pageId));
		insertList.add(new KeyValue("selector", selector));
		insertList.add(new KeyValue("clicks", clicks));
		insertList.add(new KeyValue("order", order));
		insertList.add(new KeyValue("index_after_each_click", indexAfterEachClick));
		insertList.add(new KeyValue("test_id", testId));
		int id = CRUDObject.create(TABLE_NAME, insertList);

		return findById(id);
	}

	public static void deleteAll() {
		CRUDObject.delete(TABLE_NAME, CollectionFactory.createList());
	}

	public static ArrayList<Button> getAll() {
		return (ArrayList<Button>) CRUDObject.getAll(TABLE_NAME, ROWMAP);
	}

	public static boolean isNew(Object pageId, String selector, int clicks, int order, boolean indexAfterEachClick, String testId) {
		List params = CollectionFactory.createList();
		params.add(new KeyValue("page_id", pageId));
		params.add(new KeyValue("selector", selector));
		params.add(new KeyValue("clicks", clicks));
		params.add(new KeyValue("order", order));
		params.add(new KeyValue("index_after_each_click", indexAfterEachClick));
		params.add(new KeyValue("test_id", testId));

		ArrayList<Button> buttons = (ArrayList<Button>) CRUDObject.select(TABLE_NAME, ROWMAP, params);

		return buttons.size() == 0;
	}

	public static void deleteByPageId(int pageId) {
		CRUDObject.delete(TABLE_NAME, new KeyValue("page_id", pageId));
	}
}
