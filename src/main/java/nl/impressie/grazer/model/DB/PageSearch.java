package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.hibernate.models.PageSearchModel;
import org.jsoup.Connection;

import java.util.Map;

/**
 * @author Impressie
 */
public class PageSearch {
	private String key;
	private String jsonUrl;
	private String jsonPageKey;

	private String methodString;
	private Connection.Method method;

	private String headerString;
	private Map header;

	private String dataString;
	private Map data;

	public static final String METHOD_POST = AltPage.METHOD_POST, METHOD_GET = AltPage.METHOD_GET;

	public PageSearch(PageSearchModel model) {
		this(
				model.getKey(),
				model.getMethod(),
				model.getHeaders(),
				model.getData(),
				model.getJsonUrl(),
				model.getJsonPageKey()
		);
	}
	public PageSearch(String key, String method, String header, String data, String jsonUrl, String pageKey) {
		this.key = (null != key ? key : "");

		this.jsonUrl = (null != jsonUrl ? jsonUrl : "");

		this.jsonPageKey = (null != pageKey ? pageKey : "");

		methodString = (null != method ? method : METHOD_GET);
		this.method = parseMethodString(methodString);

		headerString = (null != header ? header : "");
		this.header = headerStringToMap(headerString);

		dataString = (null != data ? data : "");
		this.data = headerStringToMap(dataString);
	}

	public static Map headerStringToMap(String data) {
		return AltPage.dataStringToMap(data);
	}

	public static Connection.Method parseMethodString(String methodString) {
		return AltPage.parseMethodString(methodString);
	}

	public String getKey() {
		return key;
	}

	public String getPageKey() {
		return jsonPageKey;
	}

	public String getJsonUrl() {
		return jsonUrl;
	}

	public Connection.Method getMethod() {
		return method;
	}

	public Map getHeaders() {
		return header;
	}

	public Map getData() {
		return data;
	}

	public String getMethodString() { return methodString; }
	public String getHeaderString() { return headerString; }
	public String getDataString() { return dataString; }

	public boolean isJson() {
		return jsonUrl.length() > 0;
	}

	public Map addToCurrent(Map current) {

		if(0 == jsonPageKey.length() && 0 == key.length() && 0 == jsonUrl.length())
			return current;

		current.put("search", true);
		current.put(WebsiteDao.PAGE_KEY, jsonPageKey);
		current.put(WebsiteDao.SEARCH_KEY, key);
		current.put(WebsiteDao.SEARCH_METHOD, methodString);
		current.put(WebsiteDao.SEARCH_HEADER, headerString);
		current.put(WebsiteDao.SEARCH_DATA, dataString);
		current.put(WebsiteDao.JSON_URL, jsonUrl);

		return current;
	}

	public void addPage(int pageNumber) {
		String pageKey = getPageKey();
		if(pageKey.length() == 0)
			return;

		if(pageKey.startsWith("?") || pageKey.startsWith("&"))
			pageKey = pageKey.substring(1);

		data.remove(pageKey);

		data.put(pageKey, String.valueOf(pageNumber));
	}
}
