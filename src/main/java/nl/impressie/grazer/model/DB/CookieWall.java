package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.hibernate.models.CookieWallModel;
import nl.impressie.grazer.model.Crawler.SeReDe;
import nl.impressie.grazer.util.crawlerUtils.IndexUtil;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.Map;

/**
 * @author Impressie
 */
public class CookieWall {
	private String url;
	private String methodString;
	private String dataString;
	private String cssSeredeString;

	private Map data;
	private Connection.Method method;
	private SeReDe css;

	public static final String METHOD_GET = AltPage.METHOD_GET;
	public static final String METHOD_POST = AltPage.METHOD_POST;

	public String getUrl() { return url; }
	public String getMethodString() { return methodString; }
	public String getDataString() { return dataString; }
	public String getCssString() { return cssSeredeString; }

	public CookieWall(CookieWallModel model) {
		this(model.getUrl(), model.getMethod(), model.getData(), model.getCss());
	}
	public CookieWall(String url, String method, String data, String css) {
		this.url = url;
		this.methodString = (method != null ? method : METHOD_GET);
		this.dataString = (data != null ? data : "");

		this.data = dataStringToMap(data);
		this.method = parseMethodString(methodString);

		this.cssSeredeString = (css != null ? css : "");
		if(cssSeredeString.length() > 0)
			this.css = SeReDe.loadSeReDe(cssSeredeString);
	}

	public static Map dataStringToMap(String data) {
		return AltPage.dataStringToMap(data);
	}

	public static Connection.Method parseMethodString(String methodString) {
		return AltPage.parseMethodString(methodString);
	}

	public boolean isCookiePage(Document document) {
		if(css == null)
			return false;

		Elements els = new Elements(document);
		return css.getElements(els, new Elements()).size() > 0;
	}

	public Map getAllowedCookies() {
		if(url == null || url.length() == 0)
			return CollectionFactory.createMap();

		Connection.Response res = IndexUtil.getJsoupResponse(url, method, data, 0);
		IndexUtil.updateCookies(res);

		return res.cookies();
	}

	public Map addToCurrent(Map current) {
		if(url == null || url.length() == 0)
			return current;

		current.put("cookies", true);
		current.put(WebsiteDao.COOKIE_URL, url);
		current.put(WebsiteDao.COOKIE_DATA, dataString);
		current.put(WebsiteDao.COOKIE_METHOD, methodString);

		if(css != null)
			css.addToCurrent(WebsiteDao.COOKIE_CSS, current);

		return current;
	}
}
