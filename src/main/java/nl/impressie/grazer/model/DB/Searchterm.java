package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.KeyValue;
import nl.impressie.grazer.util.DatabaseUtils.CRUDObject;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Impressie
 */
public class Searchterm {
	public static final String TABLE_NAME = "user_searchterm";
	public static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public String mapRow(ResultSet rs, int i) throws SQLException {
			String searchterm = rs.getString("searchterm");

			return searchterm;
		}
	};

	public static String TITLE = "title";
	public static String CONTENT = "contents";

	public static String getContentTerm(int profileId) {
		ArrayList<String> terms = findByProfileId(profileId, CONTENT);
		return String.join(" ", terms);
	}

	public static String getTitleTerm(int profileId) {
		List<String> terms = findByProfileId(profileId, TITLE);
		return String.join(" ", terms);
	}

	private static ArrayList<String> findByProfileId(int profileId, String type) {
		return findByProfileId(String.valueOf(profileId), type);
	}

	private static ArrayList<String> findByProfileId(String profileId, String type) {
		return find("profile_id", profileId, type);
	}

	private static String findById(int id, String type) {
		return findById(String.valueOf(id), type);
	}

	private static String findById(String id, String type) {
		ArrayList<String> pages = find("id", id, type);

		if(pages.size() == 1)
			return pages.get(0);

		return null;
	}

	private static ArrayList<String> find(String by, String value, String type) {
		List params = CollectionFactory.createList();
		params.add(new KeyValue(by, value));
		params.add(new KeyValue("type", type));
		return (ArrayList<String>) CRUDObject.select(TABLE_NAME, ROWMAP, params);
	}

	public static void delete(int profileId, String type) {
		List params = CollectionFactory.createList();
		params.add(new KeyValue("profile_id", profileId));
		params.add(new KeyValue("type", type));
		CRUDObject.delete(TABLE_NAME, params);
	}

	public static void create(int profileId, String type, String searchTerm) {
		String current = findById(profileId, type);
		boolean same = false;
		if(current != null)
			same = current.equals(searchTerm);
		//if it exists delete it, unless it's the same as whats supposed to be added
		if(current != null && !same)
			delete(profileId, type);
		if(same || searchTerm.length() == 0)
			return;

		List insertList = CollectionFactory.createList();
		insertList.add(new KeyValue("profile_id", profileId));
		insertList.add(new KeyValue("type", type));
		insertList.add(new KeyValue("searchterm", searchTerm));
		CRUDObject.create(TABLE_NAME, insertList);
	}
}
