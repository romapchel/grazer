package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.AltPageDao;
import nl.impressie.grazer.enums.CSFExport;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.EncodeUtil;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.XMLUtil;
import org.jsoup.Connection;
import org.jsoup.nodes.Element;
import org.pmw.tinylog.Level;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
public class AltPage {
	private int id;
	private int pageId;
	private boolean pre;
	private String url;
	private String methodString, dataString;
	private Connection.Method method;
	private Map data;

	public final static String PRE = "pre", POST = "post";

	public final static String METHOD_POST = "method_post", METHOD_GET = "method_get";

	public AltPage(int id, String url, boolean pre, int pageId, String method, String data) {
		this.id = id;
		this.url = url;
		this.pre = pre;
		this.pageId = pageId;

		methodString = (method != null ? method : METHOD_GET);
		this.method = parseMethodString(methodString);

		if(null == data)
			data = "";

		this.dataString = data;
		this.data = dataStringToMap(data);
	}

	public static Connection.Method parseMethodString(String methodString) {
		Connection.Method method = Connection.Method.GET;

		if(METHOD_GET.equals(methodString))
			method = Connection.Method.GET;
		else if(METHOD_POST.equals(methodString))
			method = Connection.Method.POST;

		return method;
	}

	public static String MethodToString(Connection.Method method) {
		if(method.equals(Connection.Method.POST))
			return METHOD_POST;
		else
			return METHOD_GET;
	}
	public static String MapToDataString(Map data) {
		StringBuilder dataString = new StringBuilder();
		for(Object key: data.keySet()) {
			if(dataString.length() > 0)
				dataString.append(", ");
			dataString.append(key.toString());
			dataString.append("=");
			dataString.append(data.get(key).toString());
		}
		return dataString.toString();
	}

	public static Map dataStringToMap(String data) {
		Map found = CollectionFactory.createMap();
		if(data == null || data.length() == 0)
			return found;

		for(String dataString : data.split("(?<!\\\\),")) {
			String[] dataSet = dataString.split("(?<!\\\\)=");
			int length = dataSet.length;
			if(length > 2 || length < 1) {
				LogUtil.log(String.format("error in datastring: [%s]", dataString), Level.ERROR);
				continue;
			}

			String dataName = dataSet[0].trim();
			String dataValue = "";
			if(length == 2)
				dataValue = dataSet[1].trim();

			found.put(dataName, dataValue);
		}

		return found;
	}

	public int getId() {
		return id;
	}

	public URL getUrl() {
		URL tempurl = null;
		try {
			tempurl = new URL(url);
		} catch(MalformedURLException e) {
			LogUtil.logException(e);
		}
		return tempurl;
	}

	public Element toXml() {
		String type = POST;
		if(pre)
			type = PRE;
		return XMLUtil.createElement(type, url);
	}

	public Map getData() {
		return data;
	}

	public Connection.Method getMethod() {
		return method;
	}

	public Map getCurrent(String uid) {
		Map current = CollectionFactory.createMap();

		current.put(String.format("%s%s", uid, AltPageDao.ID), id);
		current.put(String.format("%s%s", uid, AltPageDao.PAGE_ID), pageId);
		current.put(String.format("%s%s", uid, AltPageDao.MOMENT), (pre ? PRE : POST));
		current.put(String.format("%s%s", uid, AltPageDao.METHOD), methodString);
		current.put(String.format("%s%s", uid, AltPageDao.DATA), dataString);
		current.put(String.format("%s%s", uid, AltPageDao.URL), url);

		return current;
	}

	public String CSFExport() {
		return EncodeUtil.encode(getCurrent(""));
	}
	public static List<AltPage> CSFImport(String loadable, Object pageId) {
		Set<String> columns = AltPageDao.getColumns();
		List made = CollectionFactory.createList();
		for(String value: CSFExport.splitList(loadable)) {
			if(value.trim().length() == 0)
				continue;

			Map values = EncodeUtil.load(value, columns);
			if(pageId != null)
				values.put(AltPageDao.PAGE_ID, pageId);
			made.add(AltPageDao.updateOrCreate(values));
		}
		return made;
	}
}
