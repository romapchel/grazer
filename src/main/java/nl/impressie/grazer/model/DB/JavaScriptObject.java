package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.hibernate.models.JavascriptModel;

import java.util.Map;

/**
 * @author Impressie
 */
public class JavaScriptObject {
	private boolean javascript;
	private String testId;
	private String javascriptCode;

	public JavaScriptObject(JavascriptModel model) {
		this(model.getTestId(), model.getJavascriptCode());
	}
	public JavaScriptObject() {
		this(null, null);
	}

	public JavaScriptObject(String id, String JSCode) {
		if(JSCode != null && JSCode.length() > 0) {
			if(id == null)
				id = "";
			javascriptCode = JSCode;
			testId = id;
			javascript = true;
		} else {
			javascript = false;
			testId = "";
			javascriptCode = "";
		}
	}

	public String getTestId() {
		if(testId != null)
			return testId;

		return "";
	}

	public String getJavascript() {
		if(javascriptCode != null)
			return javascriptCode;

		return "";
	}

	public boolean hasJavascript() {
		return javascript;
	}

	public Map addToCurrent(Map current) {

		if(0 == javascriptCode.length() && 0 == testId.length())
			return current;

		current.put("javascript", true);
		current.put(WebsiteDao.JS, javascriptCode);
		current.put(WebsiteDao.JS_TEST_ID, testId);

		return current;
	}
}
