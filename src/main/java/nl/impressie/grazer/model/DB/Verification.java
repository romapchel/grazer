package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.UserDao;
import nl.impressie.grazer.dao.VerificationDao;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.DateUtil;

/**
 * @author Impressie
 */
public class Verification extends Model {
	private int id;
	private int userId;
	private String verificationCode;
	private Long createdOn;

	public Verification(int id, int userId, String verificationCode, Long createdOn) {
		super(id);
		this.id = id;
		this.userId = userId;
		this.verificationCode = verificationCode;
		this.createdOn = createdOn;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public CustomUser getUser() {
		return UserDao.findById(userId);
	}

	public boolean activateUser() {
		CustomUser user = getUser();
		if(user.isActive())
			return false;

		Long twentyFourHoursAgo = DateUtil.getDaysAfterTodayAsTimestamp(-1);
		if(twentyFourHoursAgo > createdOn)
			return false;

		user = user.activate();
		return user.isActive();
	}

	public String getTimestamp() {
		return DateUtil.convertTimestampToFormat(createdOn, "dd MM yyyy HH:mm");
	}

	public boolean delete() {
		return VerificationDao.delete(id);
	}
}
