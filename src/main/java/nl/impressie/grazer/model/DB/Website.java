package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.*;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.hibernate.models.AdditionalSourceLinkModel;
import nl.impressie.grazer.hibernate.models.BeforeSelectorModel;
import nl.impressie.grazer.hibernate.models.PageModel;
import nl.impressie.grazer.hibernate.models.TagReplacementModel;
import nl.impressie.grazer.model.Crawler.Article;
import nl.impressie.grazer.model.Crawler.SeReDe;
import nl.impressie.grazer.model.Filter;
import nl.impressie.grazer.model.KeyValue;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.crawlerUtils.IndexManager;
import nl.impressie.grazer.util.crawlerUtils.IndexUtil;
import nl.impressie.grazer.util.jstlUtils.TagUtil;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.jdbc.core.RowMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Impressie
 */
public class Website extends Model {
	private int id;

	private String name;
	private String url;
	private String maxDepth;
	private String lastCrawlDate;
	private String dateFormat;
	private String moreInfo;
	private String description;
	private String nieuwszender;

	private boolean disabled;
	private boolean defective;
	private boolean healthy;

	private SeReDe date;
	private SeReDe title;
	private SeReDe crawl;
	private SeReDe article;
	private SeReDe content;
	private SeReDe snippet;
	private SeReDe rechtsgebied;
	private SeReDe nieuwsSoort;
	private SeReDe rubrieken;

	private JavaScriptObject javascript;

	private PageSearch pageSearch;

	private Map<String, Tag> tags;
	private List<TagReplacement> replacements;
	private List<BeforeSelector> beforeSelectors;
	private List<AdditionalSourceLink> addtionalLinks;

	private CookieWall cookieWall;

	public final static int NUMBER_OF_ARTICLES_TO_TEST = 10;

	public static final String TABLE_NAME = "page";
	private static final RowMapper profilePageRowMapper = new RowMapper() {
		@Override
		public Website mapRow(ResultSet rs, int i) throws SQLException {
			int id = rs.getInt("page_id");
			Website page = WebsiteDao.findById(id);

			return page;
		}
	};
	public static final String TYPE_NAME = "Website";

	public boolean needsReload(Connection.Response res) {
		try {
			Document htmlDoc = res.parse();
			return needsReload(htmlDoc);
		} catch(IOException e) {
		}

		return false;
	}
	public boolean needsReload(Document htmlDoc) {
		if(cookieWall != null && cookieWall.isCookiePage(htmlDoc)) {
			cookieWall.getAllowedCookies();
			return true;
		}
		return false;
	}

	@Override
	public <T> T getDBValue(String key, Class<T> objectType) {
		switch(key) {
			case WebsiteDao.ID:
				return objectType.cast(id);
			case WebsiteDao.NAME:
				return objectType.cast(name);
			case WebsiteDao.URL:
				return objectType.cast(url);
			case WebsiteDao.NIEUWSZENDER:
				return objectType.cast(nieuwszender);
			case WebsiteDao.LAST_CRAWL_DATE:
				return objectType.cast(lastCrawlDate);
			case WebsiteDao.MAX_DEPTH:
				return objectType.cast(maxDepth);
			case WebsiteDao.DATE_FORMAT:
				return objectType.cast(dateFormat);
			case WebsiteDao.MORE_INFO:
				return objectType.cast(moreInfo);
			case WebsiteDao.DATE:
				return objectType.cast(date);
			case WebsiteDao.TITLE:
				return objectType.cast(title);
			case WebsiteDao.CONTENT:
				return objectType.cast(content);
			case WebsiteDao.SNIPPET:
				return objectType.cast(snippet);
			case WebsiteDao.RECHTSGEBIED:
				return objectType.cast(rechtsgebied);
			case WebsiteDao.NIEUWS_SOORT:
				return objectType.cast(nieuwsSoort);
			case WebsiteDao.RUBRIEKEN:
				return objectType.cast(rubrieken);
			case WebsiteDao.CRAWL:
				return objectType.cast(crawl);
			case WebsiteDao.ARTICLE:
				return objectType.cast(article);
			case WebsiteDao.SEARCH_KEY:
				return objectType.cast(pageSearch.getKey());
			case WebsiteDao.SEARCH_METHOD:
				return objectType.cast(pageSearch.getMethod());
			case WebsiteDao.SEARCH_HEADER:
				return objectType.cast(pageSearch.getHeaderString());
			case WebsiteDao.SEARCH_DATA:
				return objectType.cast(pageSearch.getDataString());
			case WebsiteDao.JSON_URL:
				return objectType.cast(pageSearch.getJsonUrl());
			case WebsiteDao.PAGE_KEY:
				return objectType.cast(pageSearch.getPageKey());
			case WebsiteDao.JS:
				return objectType.cast(javascript.getJavascript());
			case WebsiteDao.JS_TEST_ID:
				return objectType.cast(javascript.getTestId());
			case WebsiteDao.DISABLED:
				return objectType.cast(disabled);
			case WebsiteDao.HEALTHY:
				return objectType.cast(healthy);
			case WebsiteDao.DEFECTIVE:
				return objectType.cast(defective);
			case WebsiteDao.DESCRIPTION:
				return objectType.cast(description);
			case WebsiteDao.COOKIE_URL:
				return objectType.cast(cookieWall.getUrl());
			case WebsiteDao.COOKIE_METHOD:
				return objectType.cast(cookieWall.getMethodString());
			case WebsiteDao.COOKIE_DATA:
				return objectType.cast(cookieWall.getDataString());
			case WebsiteDao.COOKIE_CSS:
				return objectType.cast(cookieWall.getCssString());
			case WebsiteDao.NAV:
				return objectType.cast(getNavigation());
			case WebsiteDao.BEFORE:
				return objectType.cast(getBeforeSelectors());
			case WebsiteDao.ALT:
				return objectType.cast(getAltPages());
			case WebsiteDao.ADDITIONAL_PAGES:
				return objectType.cast(getAddtionalLinkSources());
			case WebsiteDao.REPLACEMENTS:
				return objectType.cast(getReplacements());
			default:
				return super.getDBValue(key, objectType);
		}
	}

	public class Types {
		public final static String BUTTON = "button", ALT_PAGE = "alt_page", NAVIGATION = "navigation", BEFORES = "befores", PAGE = "page", TAG_REPLACEMENT = "tag_replacement", ADDITIONAL_SOURCE_LINK = "additional_source_link";
	}

	private Map tempTags;

	public Website(PageModel model) {
		this(
				model.getId().intValue(),
				model.getName(),
				model.getUrl(),
				model.getNieuwszender(),
				model.getMaxDepth().toString(),
				SeReDe.loadSeReDe(model.getCrawl()),
				SeReDe.loadSeReDe(model.getArticle()),
				model.getLastCrawlDate().toString(),
				model.getDateFormat(),
				model.getMoreInfo(),
				model.getDisabled(),
				model.getHealthy(),
				model.getDefective(),
				SeReDe.loadSeReDe(model.getDate()),
				SeReDe.loadSeReDe(model.getTitle()),
				SeReDe.loadSeReDe(model.getContent()),
				SeReDe.loadSeReDe(model.getSnippet()),
				SeReDe.loadSeReDe(model.getRechtsgebied()),
				SeReDe.loadSeReDe(model.getNieuwsSoort()),
				SeReDe.loadSeReDe(model.getRubrieken()),
				new PageSearch(model.getPageSearch()),
				new JavaScriptObject(model.getJavascript()),
				model.getDescription(),
				new CookieWall(model.getCookieWall()));

		List addList = CollectionFactory.createList();
		for(AdditionalSourceLinkModel addModel: model.getAddtionalLinks()) {
			addList.add(new AdditionalSourceLink(addModel));
		}
		addtionalLinks = addList;

		addList = CollectionFactory.createList();
		for(BeforeSelectorModel addModel: model.getBeforeSelectors()) {
			addList.add(new BeforeSelector(addModel));
		}
		beforeSelectors = addList;

		addList = CollectionFactory.createList();
		for(TagReplacementModel addModel: model.getReplacements()) {
			addList.add(new TagReplacement(addModel));
		}
		replacements = addList;
	}

	public Website(int id, String name, String url, String nieuwszender, String maxDepth, SeReDe crawl, SeReDe article, String lastCrawlDate,
				   String dateFormat, String moreInfo, boolean disabled, boolean healthy, boolean defective, SeReDe date, SeReDe title, SeReDe content,
				   SeReDe snippet, SeReDe rechtsgebied, SeReDe nieuwsSoort, SeReDe rubrieken, PageSearch pageSearch, JavaScriptObject javascript, String description,
				   CookieWall cookieWall) {
		super(id);

		this.description = description;

		tempTags = CollectionFactory.createMap();

		this.id = id;

		this.name = StringOrDefault(name, "");
		this.url = StringOrDefault(url, "");
		this.nieuwszender = StringOrDefault(nieuwszender, "");
		this.maxDepth = StringOrDefault(maxDepth, "0");
		this.lastCrawlDate = StringOrDefault(lastCrawlDate, "0");
		this.dateFormat = StringOrDefault(dateFormat, "");
		this.moreInfo = StringOrDefault(moreInfo, "");

		this.disabled = disabled;
		this.healthy = healthy;
		this.defective = defective;

		this.crawl = SeReDeOrEmpty(crawl);
		this.article = SeReDeOrEmpty(article);
		this.date = SeReDeOrEmpty(date);
		this.title = SeReDeOrEmpty(title);
		this.content = SeReDeOrEmpty(content);
		this.snippet = SeReDeOrEmpty(snippet);
		this.rechtsgebied = SeReDeOrEmpty(rechtsgebied);
		this.nieuwsSoort = SeReDeOrEmpty(nieuwsSoort);
		this.rubrieken = SeReDeOrEmpty(rubrieken);

		this.pageSearch = pageSearch;

		this.cookieWall = cookieWall;

		if(javascript != null) {
			this.javascript = javascript;
		} else {
			this.javascript = new JavaScriptObject();
		}
	}

	public boolean hasTag(Tag tag) {
		return getTags().containsKey(tag.getId());
	}

	public Map<String, Tag> getTags() {
		if(null == tags) {
			tags = CollectionFactory.createMap();
			tags = loadTagsFromDB();
		}

		return tags;
	}

	public List<TagReplacement> getReplacements() {
		if(null == replacements)
			replacements = TagReplacementDao.findByPageId(this.id);
		return replacements;
	}

	public List<AdditionalSourceLink> getAddtionalLinkSources() {
		if(null == addtionalLinks)
			addtionalLinks = AdditionalSourceLinkDao.findByPageId(this.id);
		return addtionalLinks;
	}

	public List<URL> getLinks() {
		List urls = CollectionFactory.createList();
		for(AdditionalSourceLink additionalSourceLink: getAddtionalLinkSources()) {
			urls.add(additionalSourceLink.getLink());
		}
		urls.add(getUrl());

		return urls;
	}

	public SeReDe SeReDeOrEmpty(SeReDe serede) {
		if(serede == null)
			return new SeReDe();
		return serede;
	}

	public String StringOrDefault(String string, String defaultValue) {
		if(string == null)
			return defaultValue;

		return string;
	}

	public boolean addTag(Tag tag) {
		if(hasTag(tag))
			return false;

		getTags().put(tag.getId(), tag);
		addTagsToSeReDe(true);
		return true;
	}

	public Website loadTagsFromSerede() {
		return loadTagsFromSerede(false);
	}
	public Website loadTagsFromSerede(boolean createNew) {
		Map newTags = CollectionFactory.createMap();
		String type = Tag.RECHTSGEBIED;
		String tags = rechtsgebied.getDefaultValue();
		addSeReDeToMap(type, tags, newTags, createNew);

		type = Tag.NIEUWSSOORT;
		tags = nieuwsSoort.getDefaultValue();
		addSeReDeToMap(type, tags, newTags, createNew);

		type = Tag.RUBRIEK;
		tags = rubrieken.getDefaultValue();
		addSeReDeToMap(type, tags, newTags, createNew);

		this.tags = loadTagsFromDB();
		return resetTags(newTags, true);
	}

	public Map loadTagsFromDB() {
		Map tmap = tagListToMap();
		addTags(tmap, false);
		return tagListToMap();
	}

	private Map tagListToMap() {
		return tagListToMap(TagPageDao.findByPageId(id));
	}

	private Map tagListToMap(List<Tag> tags) {
		Map tmap = CollectionFactory.createMap();
		for(Tag tag: tags) {
			if(tag == null)
				continue;

			tmap.put(tag.getId(), tag);
		}
		return tmap;
	}

	private static Map addSeReDeToMap(String type, String tags, Map map, boolean createNew) {
		for(String tagname : tags.split(",")) {
			Tag tag = TagDao.findByName(tagname.trim(), type);

			if(createNew && null == tag)
				tag = TagDao.create(type, tagname.trim());

			if(null == tag) continue;

			else map.put(tag.getId(), tag);
		}
		return map;
	}

	public void addTagsToSeReDe(boolean saveChanges) {
		changeTags(CollectionFactory.createMap(), CollectionFactory.createMap(), saveChanges);
	}

	public Website removeTags(Map<String, Tag> toExclude, boolean saveChanges) {
		return changeTags(toExclude, CollectionFactory.createMap(), saveChanges);
	}

	public Website addTags(Map<String, Tag> toAdd, boolean saveChanges) {
		return changeTags(CollectionFactory.createMap(), toAdd, saveChanges);
	}

	public Website resetTags(Map<String, Tag> newTags, boolean saveChanges) {
		Map toExclude = CollectionFactory.createMap();
		toExclude.putAll(getTags());
		return changeTags(toExclude, newTags, saveChanges);
	}

	public Website changeTags(Map<String, Tag> toExclude, Map<String, Tag> toAdd, boolean saveChanges) {
		Map stringBuilders = CollectionFactory.createMap();
		stringBuilders.put(Tag.RECHTSGEBIED, new StringBuilder());
		stringBuilders.put(Tag.NIEUWSSOORT, new StringBuilder());
		stringBuilders.put(Tag.RUBRIEK, new StringBuilder());

		List<Tag> DBTags = TagPageDao.findByPageId(id);
		Map<String, Tag> remove = CollectionFactory.createMap(), add = CollectionFactory.createMap();
		add.putAll(toAdd);

		for(Tag tag : DBTags) {
			String id = tag.getId();

			if(add.containsKey(id)) add.remove(id);
			else if(toExclude.containsKey(id)) {
				remove.put(id, tag);
				continue;
			}
			addTagToBuilder(stringBuilders, tag);

		}
		for(Tag tag : add.values()) {
			addTagToBuilder(stringBuilders, tag);
		}

		boolean updateRecht = !(rechtsgebied.getDefaultValue().trim().toLowerCase().equals(stringBuilders.get(Tag.RECHTSGEBIED).toString().trim().toLowerCase()));
		boolean updateNieus = !(nieuwsSoort.getDefaultValue().trim().toLowerCase().equals(stringBuilders.get(Tag.NIEUWSSOORT).toString().trim().toLowerCase()));
		boolean updateRubr = !(rubrieken.getDefaultValue().trim().toLowerCase().equals(stringBuilders.get(Tag.RUBRIEK).toString().trim().toLowerCase()));

		rechtsgebied = new SeReDe(rechtsgebied.getSelector(), rechtsgebied.getProperty(), rechtsgebied.getRegex(), stringBuilders.get(Tag.RECHTSGEBIED).toString());
		nieuwsSoort = new SeReDe(nieuwsSoort.getSelector(), nieuwsSoort.getProperty(), nieuwsSoort.getRegex(), stringBuilders.get(Tag.NIEUWSSOORT).toString());
		rubrieken = new SeReDe(rubrieken.getSelector(), rubrieken.getProperty(), rubrieken.getRegex(), stringBuilders.get(Tag.RUBRIEK).toString());

		if(saveChanges) {
			Map updates = CollectionFactory.createMap();
			if(updateRecht) updates.put(WebsiteDao.RECHTSGEBIED, rechtsgebied.toString());
			if(updateNieus) updates.put(WebsiteDao.NIEUWS_SOORT, nieuwsSoort.toString());
			if(updateRubr) updates.put(WebsiteDao.RUBRIEKEN, rubrieken.toString());
			if(0 < updates.size()) update(updates);
		}

		return (saveChanges ? TagPageDao.updatePageTags(id, add.keySet(), remove.keySet()) : this);
	}

	public void addTagToBuilder(Map<String, StringBuilder> stringBuilders, Tag tag) {
		String type = tag.getType();
		StringBuilder builder = new StringBuilder();

		if(stringBuilders.containsKey(type)) builder = stringBuilders.get(type);

		if(builder.length() > 0) builder.append(", ");

		builder.append(tag.getName());
	}

	public void removeTags(Map<String, Tag> tagMap) {
		removeTags(tagMap, true);
	}

	public List<Article> getAllArticles() {
		return Article.findByWebUrl(url.toLowerCase());
	}

	public int deleteAllArticles() {
		int removedArticles = Article.deleteByPage(url.toLowerCase());
		IndexManager.destroy();
		return removedArticles;
	}

	public String toHtml(String baseUrl) {
		String editUrl = String.format("%2$swebsite/edit?id=%1$s", id, baseUrl);
		StringBuilder builder = new StringBuilder();

		builder.append("<div class=\"site\">");
		builder.append("<a href=\"%1$s\"> <h2>%2$s</h2></a>");
		builder.append("<a href=\"%3$s\" class=\"clickable\"><h2 class=\"column-1-2\">Hoofdpagina: </h2><div class=\"column-1-2\">%3$s</div></a>");
		builder.append("</div>");

		String html = String.format(builder.toString(), editUrl, name, url);

		return html;
	}

	public List<Button> getButtons() {
		return Button.findByPageId(id);
	}

	public List<AltPage> getPrePages() {
		return AltPageDao.findByPageId(id, AltPage.PRE);
	}

	public List<AltPage> getPostPages() {
		return AltPageDao.findByPageId(id, AltPage.POST);
	}

	public List<AltPage> getAltPages() {
		List list = CollectionFactory.createList();
		list.addAll(getPrePages());
		list.addAll(getPostPages());
		return list;
	}

	public String getMaxDepth() { return maxDepth; }

	public String getDescription() {
		return description;
	}

	public Navigation getNavigation() {
		Navigation nav = NavigationDao.findByPageId(id);
		return nav;
	}

	public int delete() {
		deleteAllArticles();
		return WebsiteDao.delete(id);
	}

	public List<BeforeSelector> getBeforeSelectors() {
		if(beforeSelectors == null)
			beforeSelectors = BeforeSelectorDao.findByPageId(id);
		return beforeSelectors;
	}

	public Website update(Map updateParamaters) {
		return WebsiteDao.updateOrCreate(id, updateParamaters);
	}

	/**
	 * A method to campare two Website objects to each other by turning both of them to HTML and then comparing them
	 *
	 * @param page1 The first page object
	 * @param page2 The second page object
	 * @return true if they match false if they don't or if one of the objects is null
	 */
	public static Boolean compare(Website page1, Website page2) {
		if(page1 == null || page2 == null) {
			return false;
		}
		String pg1 = page1.toHtml("");
		String pg2 = page2.toHtml("");

		return pg1.equals(pg2);
	}

	public Map getCurrent() {
		Map current = CollectionFactory.createMap();
		try {
			current.put(WebsiteDao.ID, id);
			current.put(WebsiteDao.NAME, name);
			current.put(WebsiteDao.NIEUWSZENDER, nieuwszender);
			current.put(WebsiteDao.DESCRIPTION, description);
			current.put(WebsiteDao.URL, url);
			current.put(WebsiteDao.LAST_CRAWL_DATE, DateUtil.convertStringToFormat(lastCrawlDate, "yyyyMMdd", "yyyy-MM-dd"));
			current.put(WebsiteDao.MAX_DEPTH, maxDepth);
			current.put(WebsiteDao.DATE_FORMAT, dateFormat);
			current.put(WebsiteDao.MORE_INFO, moreInfo);
			current.put("article-count", getAllArticles().size());

			Navigation nav = getNavigation();
			if(null != nav)
				current.put("navigation", nav);

			List<Button> buttons = getButtons();
			if(0 < buttons.size())
				current.put("buttons", buttons);

			List altPages = getAltPages();
			if(0 < altPages.size())
				current.put("altpages", altPages);

			crawl.addToCurrent(WebsiteDao.CRAWL, current);
			article.addToCurrent(WebsiteDao.ARTICLE, current);
			date.addToCurrent(WebsiteDao.DATE, current);
			title.addToCurrent(WebsiteDao.TITLE, current);
			content.addToCurrent(WebsiteDao.CONTENT, current);
			snippet.addToCurrent(WebsiteDao.SNIPPET, current);
			rechtsgebied.addToCurrent(WebsiteDao.RECHTSGEBIED, current);
			nieuwsSoort.addToCurrent(WebsiteDao.NIEUWS_SOORT, current);
			rubrieken.addToCurrent(WebsiteDao.RUBRIEKEN, current);

			javascript.addToCurrent(current);

			pageSearch.addToCurrent(current);

			cookieWall.addToCurrent(current);
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}

		return current;
	}

	//getters
	public CookieWall getCookieWall() { return cookieWall; }
	public URL getUrl() {
		try {
			return new URL(url);
		} catch(MalformedURLException e) {
			LogUtil.logException(e);
			return null;
		}
	}

	public int getUsableMaxDepth() {
		int md = 0;
		try {
			md = Integer.parseInt(maxDepth);
		} catch(NumberFormatException e) {
		}

		int globalMaxDepth = IndexUtil.GLOBAL_MAX_DEPTH;
		if(globalMaxDepth != 0 && md == 0)
			md = globalMaxDepth;

		return md;
	}

	public SeReDe getCrawl() {
		return crawl;
	}

	public String getMoreInfo() {
		return moreInfo;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public SeReDe getTitle() {
		return title;
	}

	public SeReDe getDate() {
		return date;
	}

	public SeReDe getContent() {
		return content;
	}

	public SeReDe getRechtsgebied() {
		return rechtsgebied;
	}

	public SeReDe getNieuwssoort() {
		return nieuwsSoort;
	}

	public SeReDe getRubrieken() {
		return rubrieken;
	}

	public SeReDe getSnippet() {
		return snippet;
	}

	public String getName() {
		if(name != null && name.length() > 0) {
			return name;
		} else {
			return url;
		}
	}

	public PageSearch getPageSearch() {
		return pageSearch;
	}

	public boolean hasJavascript() {
		return javascript.hasJavascript();
	}

	public String getJavascript() {
		return javascript.getJavascript();
	}
	public String getNieuwszender() {
		return nieuwszender;
	}

	public JavaScriptObject getJavascriptObject() {
		return javascript;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public boolean isDefective() {
		return defective;
	}

	public boolean isHealthy() {
		return healthy;
	}

	public Object getId() {
		return id;
	}

	public boolean index() {
		IndexManager manager = IndexManager.getInstance();

		Map articleUrls = IndexUtil.populatedVisitedSites(this);
		int before = articleUrls.size();

		String searchKey = pageSearch.getKey();
		Navigation nav = getNavigation();

		for(AltPage page : getPrePages()) {
			if(IndexUtil.shouldStopIndexing())
				break;
			IndexUtil.indexUrl(page.getUrl(), 0, this, page.getMethod(), page.getData());
		}

		if(searchKey != null && searchKey.length() > 0) {
			for(TagUtil.TagFamily family: TagUtil.getTagFamilies(TagDao.getType(Tag.RECHTSGEBIED))) {
				List<Tag> rechtsgebieden = new ArrayList(family.getChildren());
				rechtsgebieden.add(family.getParent());
				for(Tag rechtsgebied: rechtsgebieden) {
					if(IndexUtil.shouldStopIndexing())
						break;
					addTempTag(rechtsgebied);

					String urlString = url;

					if(pageSearch.isJson())
						urlString = pageSearch.getJsonUrl();
					String rechtsgebiedName = rechtsgebied.getName();
					String link = "=";
					if(searchKey.startsWith("/") && searchKey.endsWith("/"))
						link = "";

					urlString = String.format("%1$s%2$s%4$s%3$s", urlString, searchKey, rechtsgebiedName, link);

					searchPage(urlString);

					removeTempTag(rechtsgebied);
				}
			}
		} else if(pageSearch.isJson()) {
			searchPage(pageSearch.getJsonUrl());
		} else if(nav != null) {
			boolean keepgoing = true;
			int index = 0;
			while(manager.isWriting() && keepgoing) {
//				System.out.println(String.format("page #%s", index));
				URL navUrl = nav.getUrl(index);
				if(navUrl == null)
					keepgoing = false;
				else {
					keepgoing = IndexUtil.indexUrl(navUrl, 0, this);
				}
				index++;
			}
		} else {
			for(URL url: getLinks()) {
				IndexUtil.indexUrl(url, 0, this);
			}
		}

		for(AltPage page : getPostPages()) {
			if(IndexUtil.shouldStopIndexing())
				break;
			IndexUtil.indexUrl(page.getUrl(), 0, this, page.getMethod(), page.getData());
		}

		int after = getAllArticles().size();

		IndexUtil.addToMail(getName(), before, after);

		//Stop iterating if the thread has been stopped
		if(IndexUtil.shouldStopIndexing())
			return false;

		IndexUtil.updateDate(url, String.valueOf(IndexUtil.getNewDateForPage()));

		return true;
	}

	private void removeTempTag(Tag tempTag) {
		String key = tempTag.toString();
		if(null != tempTags)
			tempTags.remove(key);
	}

	private void addTempTag(Tag tempTag) {
		if(hasTag(tempTag))
			return;

		String key = tempTag.toString();
		if(tempTags.containsKey(key))
			return;

		tempTags.put(key, tempTag);
	}

	public Collection<Tag> getTempTags() {
		return tempTags.values();
	}

	private void searchPage(String urlString) {
		if(pageSearch.isJson()) {
			PageSearch search = pageSearch;

			String pagekey = search.getPageKey();
			Set<String> jsonUrls = CollectionFactory.createSet();
			Navigation nav = getNavigation();

			if(pagekey.length() > 0 || nav != null) {
				int jumps = 0;

				String lasturlString = urlString;

				if(pagekey.length() > 0) {
					pageSearch.addPage(jumps + 1);
				} else if(nav != null) {
					URL url = nav.getUrl(jumps, urlString);
					if(null != url)
						lasturlString = url.toString();
					else
						lasturlString = "";
				}

				Set newUrls = CollectionFactory.createSet();
				if(lasturlString.length() > 0)
					newUrls = IndexUtil.getUrlsFromJson(lasturlString, this);

				while(lasturlString.length() > 0 && newUrls.size() > 0 && IndexManager.getInstance().isWriting()) {
					jsonUrls.addAll(newUrls);
					jumps++;
					if(nav != null) {
						URL url = nav.getUrl(jumps, urlString);
						lasturlString = (url == null ? "" : url.toString());
					} else {
						pageSearch.addPage(jumps + 1);

						lasturlString = urlString;
						if(jumps >= getUsableMaxDepth())
							lasturlString = "";
					}

					if(lasturlString.length() > 0)
						newUrls = IndexUtil.getUrlsFromJson(lasturlString, this);
				}
			} else {
				jsonUrls = IndexUtil.getUrlsFromJson(urlString, this);
			}

			for(String jsonUrlString : jsonUrls) {
				//Stop iterating if the thread has been stopped
				if(IndexUtil.shouldStopIndexing())
					break;
				if(!IndexUtil.isNew(jsonUrlString, IndexUtil.getVisitedURLs()))
					continue;

				try {
					URL jsonUrl = new URL(jsonUrlString);
					if(!IndexUtil.indexUrl(jsonUrl, 0, this))
						break;
				} catch(MalformedURLException e) {
					LogUtil.logException(e);
				}//should never happen as the link gets tested for validity earlier
			}
		} else {
			URL tempurl = null;
			try {
				tempurl = new URL(urlString);
			} catch(MalformedURLException e) {
				LogUtil.logException(e);
			}

			if(tempurl != null)
				IndexUtil.indexUrl(tempurl, 0, this);
		}
	}

	public SeReDe getArticle() {
		return article;
	}

	public void resetUniques() {
		id = 0;
		url = "";
		name = "";
	}

	public HealthReport generateHealthReport(boolean checkAll) {
		List<Article> articles = getAllArticles();
		List<Article> articlesToCheck = getArticlesToCheck(articles, NUMBER_OF_ARTICLES_TO_TEST, checkAll);

		HealthReport report = new HealthReport(this, articles.size());
		for(Article article : articlesToCheck) {
			report.addArticle(article);
		}

		setHealthy(report.isHealthy());

		return report;
	}

	private Website setHealthy(boolean healthy) {
		if(this.healthy == healthy)
			return this;

		List params = CollectionFactory.createList();
		params.add(new Parameter(WebsiteDao.HEALTHY, healthy));
		return WebsiteDao.update(id, params);
	}

	private List getArticlesToCheck(List articles, int ammountOfArticlesToTest, boolean checkAll) {
		if(checkAll)
			ammountOfArticlesToTest = articles.size();

		List<Article> checkedArticles;
		if(ammountOfArticlesToTest >= articles.size())
			checkedArticles = articles;
		else
			checkedArticles = getRandomListItems(ammountOfArticlesToTest, articles);

		return checkedArticles;
	}

	private List getRandomListItems(int numberOfItems, List items) {
		List randomItems = CollectionFactory.createList();
		Random randomGenerator = new Random();
		for(int i = 0; i < numberOfItems; i++) {
			int randomNumber = randomGenerator.nextInt(items.size());
			randomItems.add(items.get(randomNumber));
		}
		return randomItems;
	}

	public List getSeredeList() {
		List allSeredes = CollectionFactory.createList();

		List replacements;

		replacements = CollectionFactory.createList();
		replacements.add(new KeyValue("%2520", " "));
		replacements.add(new KeyValue(" ", " "));
		replacements.add(new KeyValue("\\s+", " "));

		if(!content.isEmpty())
			addToSeredeList(allSeredes, WebsiteDao.CONTENT, content, replacements);

		replacements = CollectionFactory.createList();
		if(!title.isEmpty())
			addToSeredeList(allSeredes, WebsiteDao.TITLE, title, replacements);

		if(!date.isEmpty())
			addToSeredeList(allSeredes, WebsiteDao.DATE, date, replacements);

		for(TagReplacement replacement: getReplacements()) {
			replacements.add(new KeyValue(replacement.getReplaceText(), replacement.getReplaceWith()));
		}
		if(!rechtsgebied.isEmpty())
			addToSeredeList(allSeredes, WebsiteDao.RECHTSGEBIED, rechtsgebied, replacements);

		return allSeredes;
	}

	private List addToSeredeList(List seredeList, String title, SeReDe serede, List replacements) {
		Map seredeValues = CollectionFactory.createMap();
		seredeValues.put("title", title);
		seredeValues.put("serede", serede);
		seredeValues.put("replacements", replacements);

		seredeList.add(seredeValues);
		return seredeList;
	}

	public Map<String, List> getBeforeMap(Element HTMLDoc) {
		return getBeforeMap(new Elements(HTMLDoc));
	}

	public Map<String, List> getBeforeMap(Elements HTMLDoc) {
		Map<String, List> befores = CollectionFactory.createMap();
		for(BeforeSelector bs : getBeforeSelectors()) {
			bs.addFoundElementsToMap(befores, HTMLDoc);
		}
		return befores;
	}

	@Override
	public Map getFilters() {
		Map filters = super.getFilters();
		Filter filter = new Filter(WebsiteDao.URL, getUrl());
		filters.put(filter.getId(), filter);

		filter = new Filter(WebsiteDao.NAME, getName());
		filters.put(filter.getId(), filter);

		filter = new Filter(WebsiteDao.NIEUWSZENDER, getNieuwszender());
		filters.put(filter.getId(), filter.displayWithTitle());

		filter = new Filter(WebsiteDao.NIEUWS_SOORT, getNieuwssoort());
		filters.put(filter.getId(), filter.displayWithTitle());

		return filters;
	}

	@Override
	public String getFilterDataAttributes() {
		StringBuilder attributes = new StringBuilder();
		attributes.append(super.getFilterDataAttributes());

		Map<String, Object> filters = getFilters();
		for(String key : filters.keySet()) {
			if(attributes.length() > 0)
				attributes.append(" ");

			attributes.append(filters.get(key).toString());
		}

		return ((attributes.length() > 0) ? " " + attributes.toString() : "");
	}
}
