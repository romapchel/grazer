package nl.impressie.grazer.model.DB;

import lombok.Getter;
import nl.impressie.grazer.dao.BeforeSelectorDao;
import nl.impressie.grazer.enums.CSFExport;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.hibernate.models.BeforeSelectorModel;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.EncodeUtil;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
public class BeforeSelector extends Model {
	private int id;
	@Getter
	private String name;
	@Getter
	private String selector;

	public BeforeSelector(BeforeSelectorModel model) {
		this(model.getId().intValue(), model.getType(), model.getSelector());
	}
	public BeforeSelector(int id, String name, String selector) {
		super(id);

		this.id = id;
		this.name = name;
		this.selector = selector;
	}

	public Map addSelectorToMap(Map map) {
		map.put(name, selector);
		return map;
	}

	public Map addFoundElementsToMap(Map befores, Element doc) {
		if(null == doc)
			return befores;

		return addFoundElementsToMap(befores, new Elements(doc));
	}

	public Map addFoundElementsToMap(Map befores, Elements doc) {
		Elements elements = doc.select(selector);
		if(elements.size() > 0)
			befores.put(name, elements);

		return befores;
	}

	public Map getCurrent(String uid) {
		Map current = CollectionFactory.createMap();

		current.put(String.format("%s%s", uid, BeforeSelectorDao.ID), id);
		current.put(String.format("%s%s", uid, BeforeSelectorDao.NAME), name);
		current.put(String.format("%s%s", uid, BeforeSelectorDao.SELECTOR), selector);

		return current;
	}

	public String CSFExport() {
		return EncodeUtil.encode(getCurrent(""));
	}
	public static List<BeforeSelector> CSFImport(String loadable, Object pageId) {
		Set<String> columns = BeforeSelectorDao.getColumns();
		List made = CollectionFactory.createList();
		for(String value: CSFExport.splitList(loadable)) {
			if(value.trim().length() == 0)
				continue;

			Map values = EncodeUtil.load(value, columns);
			if(pageId != null)
				values.put(BeforeSelectorDao.PAGE_ID, pageId);
			made.add(BeforeSelectorDao.updateOrCreate(values));
		}
		return made;
	}
}