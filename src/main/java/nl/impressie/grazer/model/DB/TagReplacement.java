package nl.impressie.grazer.model.DB;

import lombok.Getter;
import nl.impressie.grazer.dao.TagReplacementDao;
import nl.impressie.grazer.dao.WebsiteDao;
import nl.impressie.grazer.enums.CSFExport;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.hibernate.models.TagReplacementModel;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.EncodeUtil;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
@Getter
public class TagReplacement extends Model {
	private int pageId;
	private String tagType;
	private String replaceText;
	private String replaceWith;

	private Website page;

	public TagReplacement() { super(0); }
	public TagReplacement(TagReplacementModel model) {
		this(
				model.getId().intValue(),
				model.getPage().getId().intValue(),
				model.getTagType(),
				model.getReplaceText(),
				model.getReplaceWith()
			);
	}
	public TagReplacement(int id, Website page, String tagType, String replaceText, String replaceWith) {
		this(id, (int)page.getId(), tagType, replaceText, replaceWith);
		this.page = page;
	}
	public TagReplacement(int id, int pageId, String tagType, String replaceText, String replaceWith) {
		super(id);
		this.pageId = pageId;
		this.tagType = tagType;
		this.replaceText = replaceText;
		this.replaceWith = replaceWith;
	}

	public Map getCurrent() {
		return getCurrent("");
	}
	public Map getCurrent(String prefix) {
		return CollectionFactory.createMap(
				new CollectionFactory.MapPair(prefix + TagReplacementDao.TAG_TYPE, tagType),
				new CollectionFactory.MapPair(prefix + TagReplacementDao.REPLACE_TEXT, replaceText),
				new CollectionFactory.MapPair(prefix + TagReplacementDao.REPLACE_WITH, replaceWith)
		);
	}

	public Website getPage() {
		if(page == null)
			page = WebsiteDao.findById(getPageId());
		return page;
	}

	public String CSFExport() {
		Map current = getCurrent("");
		current.put(TagReplacementDao.ID, id);
		current.put(TagReplacementDao.PAGE_ID, pageId);
		return EncodeUtil.encode(current);
	}

	public static List<TagReplacement> CSFImport(String loadable, Object pageId) {
		Set<String> columns = CollectionFactory.createSet(
				TagReplacementDao.TAG_TYPE,
				TagReplacementDao.REPLACE_TEXT,
				TagReplacementDao.ID,
				TagReplacementDao.PAGE_ID,
				TagReplacementDao.REPLACE_WITH
		);
		List made = CollectionFactory.createList();
		for(String value: CSFExport.splitList(loadable)) {
			if(value.trim().length() == 0)
				continue;

			Map values = EncodeUtil.load(value, columns);
			if(pageId != null)
				values.put(TagReplacementDao.PAGE_ID, pageId);
			made.add(TagReplacementDao.updateOrCreate(values));
		}
		return made;
	}


	@Override
	public String toString() {
		return String.format("type: [%s] id: [%s] replace: [%s] with: [%s]", getTagType(), getId(), getReplaceText(), getReplaceWith());
	}

	public String replace(String text) {
		return text.replaceAll(getReplaceText(), getReplaceWith());
	}
}
