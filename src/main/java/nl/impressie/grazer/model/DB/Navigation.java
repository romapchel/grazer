package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.dao.NavigationDao;
import nl.impressie.grazer.enums.CSFExport;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.KeyValue;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.DatabaseUtils.CRUDObject;
import nl.impressie.grazer.util.EncodeUtil;
import nl.impressie.grazer.util.LogUtil;
import nl.impressie.grazer.util.crawlerUtils.IndexUtil;
import org.springframework.jdbc.core.RowMapper;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Impressie
 */
public class Navigation extends Model {
	private int id;
	private int pageId;
	private String url;
	private String paging;
	private String after;
	private int start;
	private int jump;
	private int numberOfJumps;

	public static final String TABLE_NAME = "navigation";
	public static final RowMapper ROWMAP = new RowMapper() {
		@Override
		public Navigation mapRow(ResultSet rs, int rowNum) throws SQLException {
			int id = rs.getInt("id");
			int pageId = rs.getInt("page_id");
			String paging = rs.getString("paging");
			String after = rs.getString("after");
			String url = rs.getString("url");
			int jump = rs.getInt("jump");
			int statrt = rs.getInt("start");
			int numberOfJumps = rs.getInt("end");

			Navigation nav = new Navigation(id, pageId, url, paging, after, statrt, jump, numberOfJumps);

			return nav;
		}
	};

	public Navigation(int id, int pageId, String url, String paging, String after, int start, int jump, int numberOfJumps) {
		super(id);

		this.id = id;
		this.pageId = pageId;
		this.url = url;
		this.paging = paging;
		this.after = after;
		this.start = start;
		this.jump = jump;
		this.numberOfJumps = numberOfJumps;
	}

	public URL getUrl(int jumps) {
		return getUrl(jumps, url);
	}

	public URL getUrl(int jumps, String url) {
		int checkJumps = numberOfJumps;
		if(checkJumps < 0)
			checkJumps = IndexUtil.GLOBAL_MAX_DEPTH;

		if(jumps >= checkJumps)
			return null;
		int increase = jump * jumps;
		int current = start + increase;

		String addonWithPage = String.format(paging, String.valueOf(current));

		String link = String.format("%1$s%2$s%3$s", url, addonWithPage, after);
		URL linkurl = null;
		try {
			linkurl = new URL(link);
		} catch(MalformedURLException e) {
			LogUtil.logException(e);
		}
		return linkurl;
	}

	public static Navigation create(Object pageId, String url, String paging, String after, int start, int jump, int numberOfJumps) {
		if(url.length() == 0)
			return null;
		if((Integer) pageId == 0)
			return null;
		boolean isnew = isNew(pageId, url, paging, after, start, jump, numberOfJumps);
		if(!isnew)
			return null;

		url = url.trim();
		paging = paging.trim();

		Map values = CollectionFactory.createMap();

		values.put(NavigationDao.PAGE_ID, pageId);
		values.put(NavigationDao.URL, url);
		values.put(NavigationDao.PAGING, paging);
		values.put(NavigationDao.AFTER, after);
		values.put(NavigationDao.START, start);
		values.put(NavigationDao.JUMP, jump);
		values.put(NavigationDao.END, numberOfJumps);

		return NavigationDao.create(values);
	}

	public static boolean isNew(Object pageId, String url, String paging, String after, int start, int jump, int numberOfJumps) {
		url = url.trim();
		paging = paging.trim();
		after = after.trim();

		List params = CollectionFactory.createList();
		params.add(new KeyValue("page_id", pageId));
		params.add(new KeyValue("url", url));
		params.add(new KeyValue("paging", paging));
		params.add(new KeyValue("after", after));
		params.add(new KeyValue("start", start));
		params.add(new KeyValue("jump", jump));
		params.add(new KeyValue("end", numberOfJumps));

		List items = CRUDObject.select(TABLE_NAME, ROWMAP, params);

		return items.size() == 0;
	}

	public boolean delete() {
		return delete(id);
	}

	public static boolean delete(int id) {
		return delete(String.valueOf(id));
	}

	public static boolean delete(String id) {
		return CRUDObject.delete(TABLE_NAME, "id", id) == 1;
	}

	public Map getCurrent(String uid) {
		Map current = CollectionFactory.createMap();

		current.put(String.format("%s%s", uid, NavigationDao.ID), id);
		current.put(String.format("%s%s", uid, NavigationDao.PAGE_ID), pageId);
		current.put(String.format("%s%s", uid, NavigationDao.URL), url);
		current.put(String.format("%s%s", uid, NavigationDao.PAGING), paging);
		current.put(String.format("%s%s", uid, NavigationDao.AFTER), after);
		current.put(String.format("%s%s", uid, NavigationDao.START), start);
		current.put(String.format("%s%s", uid, NavigationDao.JUMP), jump);
		current.put(String.format("%s%s", uid, NavigationDao.END), numberOfJumps);

		return current;
	}

	public String CSFExport() {
		return EncodeUtil.encode(getCurrent(""));
	}

	public static List<Navigation> CSFImport(String loadable, Object pageId) {
		Set<String> columns = NavigationDao.getColumns();
		List made = CollectionFactory.createList();
		for(String value: CSFExport.splitList(loadable)) {
			if(value.trim().length() == 0)
				continue;

			Map values = EncodeUtil.load(value, columns);
			if(pageId != null)
				values.put(NavigationDao.PAGE_ID, pageId);
			made.add(NavigationDao.updateOrCreate(values));
		}
		return made;
	}
}
