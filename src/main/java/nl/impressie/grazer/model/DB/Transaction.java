package nl.impressie.grazer.model.DB;

import nl.impressie.grazer.util.DateUtil;

/**
 * @author Impressie
 */
public class Transaction {
	private int id;
	private int userId;
	private int price, users, days;
	private Long made;

	private String type;
	private String currency;

	private Boolean completed;

	public static final String EURO = "EUR";

	public Transaction(int Id, int UserId, int Price, String Currency, int Users, int Days, Long Made, String Type, boolean Completed) {
		id = Id;
		userId = UserId;
		price = Price;
		currency = Currency;
		users = Users;
		days = Days;
		made = Made;
		type = Type;
		completed = Completed;
	}

	public String toHtml() {
		StringBuilder builder = new StringBuilder();
		builder.append("<div class=\"transaction\">");
		builder.append("<div class=\"col\">");
		if(currency.equals(EURO))
			builder.append("€");
		else
			System.err.println(String.format("UNKNOWN CURRENCY: %s", currency));
		builder.append(String.format("%.2f", price / 100.00));
		builder.append("</div>");
		builder.append("<div class=\"col\">");
		builder.append(users);
		builder.append("</div>");
		builder.append("<div class=\"col\">");
		builder.append(days);
		builder.append("</div>");
		builder.append("<div class=\"col\">");
		builder.append(DateUtil.convertTimestampToFormat(made, "dd-MMM-yyyy HH:mm"));
		builder.append("</div>");
		builder.append("<div class=\"col\">");
		builder.append(type);
		builder.append("</div>");
		builder.append("</div>");

		return builder.toString();
	}
}
