package nl.impressie.grazer.model;


import nl.impressie.grazer.model.Crawler.Article;

import java.util.List;

public class Email {
    private String email;
    private String profile;
    private List<Article> articleList;
    private String username;
    public String getEmail() {
        return email;
    }

    public Email(String email, String profile, List<Article> articleList, String username) {
        this.email = email;
        this.profile = profile;
        this.articleList = articleList;
        this.username = username;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }
}
