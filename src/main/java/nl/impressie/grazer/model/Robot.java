package nl.impressie.grazer.model;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.LogUtil;
import org.pmw.tinylog.Level;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.util.List;

/**
 * @author Impressie
 */
public class Robot {
	private List<String> allowes, disallowes;
	private final String USER_AGENT = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)";

	public Robot(String domain) {
		populateRules(domain);
	}

	//This method runs a very basic test
	//It only tests if there are rules allowing the page or rules forbidding the page
	//The only priority is that allow trumps disallow, always
	//TODO: Expand upoon this method so it follows all normal regulations
	public boolean check(String link) {
		if(allowes == null && disallowes == null)
			return true;
		try {
			URL url = new URL(link);
			String nullFragment = null;
			URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery(), nullFragment);
			link = uri.getPath();
		} catch(MalformedURLException e) {
			String message = String.format("MalformedURLException: '%s' is not a valid URL", link);
			System.out.println(message);
		} catch(URISyntaxException e) {
			System.out.println("URISyntaxException");
		}
		for(String rule : allowes) {
			if(matches(link, rule))
				return true;
		}

		for(String rule : disallowes) {
			if(matches(link, rule))
				return false;
		}
		return true;
	}

	private Boolean matches(String text, String rule) {
		String modifiedRule = rule;
		//escape the forward slashes
		modifiedRule = modifiedRule.replace("/", "\\/");
		//escape the periods
		modifiedRule = modifiedRule.replace(".", "\\.");
		//Replace the robots wildcard with the regex wildcard
		modifiedRule = modifiedRule.replace("*", ".*");
		//add the rule for forcing the regex to match the start
		modifiedRule = "^" + modifiedRule;
		//If the rule ends with a slash it is(presumed to be) a folder and will cover all subfiles
		if(modifiedRule.charAt(modifiedRule.length() - 1) == '/' && modifiedRule.length() > 1)
			modifiedRule = modifiedRule + ".*";

		return text.matches(modifiedRule);
	}

	private void populateRules(String domain) {
		populateRules(domain, true);
	}

	private void populateRules(String domain, boolean firstTime) {
		allowes = CollectionFactory.createList();
		disallowes = CollectionFactory.createList();
		String seperator = " ";
		String lookingForUserAgent = "*";
		domain = domain.toLowerCase();
		try {
			//read in all the rules from robots.txt
			String robotUrlString = String.format("%s/robots.txt", domain);
			URL robotURL = new URL(robotUrlString);
			URLConnection connection = robotURL.openConnection();

			connection.addRequestProperty("User-Agent", USER_AGENT);

			BufferedReader in = null;
			List<String> lines = CollectionFactory.createList();

			try {
				InputStream inputStream = connection.getInputStream();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				in = new BufferedReader(inputStreamReader);
				String line;

				while((line = in.readLine()) != null) {
					lines.add(line);
				}

				lines = trimRobots(lines, seperator, lookingForUserAgent);
			} catch(UnknownHostException e) {
				if(firstTime) {
					String newDo = domain;
					if(newDo.startsWith("https"))
						newDo = newDo.substring(8);

					populateRules(String.format("http://%s", newDo), false);
					return;
				}
				LogUtil.logException(e);
			} catch(Exception e) {
				LogUtil.log("could not acces robots.txt on: " + robotURL.toString() + " " + e.getClass().getName() + ": " + e.getMessage(), Level.ERROR);
			} finally {
				if(in != null)
					in.close();
			}

			for(String line : lines) {
				//split the text in the line at the seperator
				//todo: look into replacing this with removing spaces and splitting it at the colon(:)
				String[] commandRule = line.split(seperator);
				if(commandRule.length < 2)
					continue;
				//The part before the seperator defines what the line does
				String command = commandRule[0];
				//The part after the seperator defines what is affected by the rule
				String rule = commandRule[1];

				if(command.toLowerCase().equals("allow:"))
					allowes.add(rule);
				if(command.toLowerCase().equals("disallow:"))
					disallowes.add(rule);
			}
		} catch(IOException e) {
			LogUtil.logException(e);
		}

		if(allowes.size() == 0 && disallowes.size() == 0) {
			//if no rules are the defined, allow everything and disallow nothing
			allowes.add("/*");
			disallowes.add("/");
		}
	}

	private List trimRobots(List<String> Lines, String Seperator, String UserAgentToFind) {
		//trim all the rules before the user agent we are looking for
		int index = 0;
		String command = "none";

		while(index < Lines.size() && (!command.equals("user-agent: " + UserAgentToFind))) {
			command = Lines.get(index).toLowerCase();
			index++;
		}
		List tempArray = CollectionFactory.createList();
		tempArray.addAll(Lines.subList(index, Lines.size()));
		Lines = tempArray;

		//trim everything after the user agent we're looking for
		index = 0;
		command = "none";
		while(index < Lines.size() && (!command.equals("user-agent:"))) {
			command = Lines.get(index).split(Seperator)[0].toLowerCase();
			index++;
		}

		//create a sublist from lines, convert the sublist from list to ArrayList, save the new ArrayList as lines
		tempArray = CollectionFactory.createList();
		if(index >= 0) {
			int listUpperLimit = index;
			if(index > 0)
				listUpperLimit--;
			tempArray.addAll(Lines.subList(0, listUpperLimit));
		}

		return tempArray;
	}
}
