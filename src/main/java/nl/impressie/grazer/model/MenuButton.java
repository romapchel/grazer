package nl.impressie.grazer.model;

/**
 * @author Impressie
 */
public class MenuButton {
	private String url;
	private String character;
	private String messageCode;

	public MenuButton(String url, String character, String messageCode) {
		if(!url.startsWith("http") && !url.startsWith("/"))
			url = "/" + url;

		this.url = url;
		this.character = character;
		this.messageCode = messageCode;
	}

	public String getUrl() {
		return url;
	}

	public String getCharacter() {
		return character;
	}

	public String getMessageCode() {
		return messageCode;
	}
}
