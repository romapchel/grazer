package nl.impressie.grazer.model;

import nl.impressie.grazer.dao.TagDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Tag;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author Impressie
 */
public class Facet implements Comparable<Facet> {
	private Tag tag;
	private int amount;

	public Facet(String tagId, int amount) {
		tag = TagDao.findById(tagId);
		this.amount = amount;
	}

	public void increase() {
		amount++;
	}

	public String toHtml(String id) {
		//only show a facet if there are more than 3
		if(amount <= 3)
			return "";

		String cssClass;
		if(CollectionFactory.createSet(id.split(", ")).contains(tag.getId()))
			cssClass = "filter button unselectable active";
		else
			cssClass = "filter button unselectable";
		String html = "<div class=\"%4$s\" data-id=\"%1$s\">" +
				"<span class=\"title\">%2$s</span>" +
				"<span class=\"amount\">%3$s</span>" +
				"</div>";
		html = String.format(html, tag.getId(), tag.getName(), amount, cssClass);
		return html;
	}

	public static List<Facet> hashmapToFacetList(HashMap map) {
		List<Facet> myFacets = CollectionFactory.createList();
		for(Object tagObject : map.keySet()) {
			String tagId = (String) tagObject;
			Object object = map.get(tagId);

			int amount = 0;
			if(object instanceof Integer)
				amount = (Integer) map.get(tagId);

			myFacets.add(new Facet(tagId, amount));
		}

		Collections.sort(myFacets);

		return myFacets;
	}

	/**
	 * This methods allows the use of Collections.sort(collection) to sort.
	 * The order used is to first sort by amount(descending) and if that matches to sort by tag name(ascending)
	 *
	 * @param facet
	 * @return
	 */
	@Override
	public int compareTo(Facet facet) {
		int amountComparison = Integer.compare(facet.amount, amount);
		if(amountComparison != 0)
			return amountComparison;
		return tag.getName().compareTo(facet.tag.getName());
	}
}
