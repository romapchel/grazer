package nl.impressie.grazer.model;

import nl.impressie.grazer.dao.UserDao;
import nl.impressie.grazer.model.User.CustomUser;
import nl.impressie.grazer.util.DateUtil;

/**
 * @author Impressie
 */
public class NewSearchTerm {
	private int id, userId;
	private String term;
	private Long timestamp;
	private String type;
	private int frequency = 1;

	public NewSearchTerm(int Id, int UserId, String Term, Long Timestamp, String Type) {
		id = Id;
		userId = UserId;
		term = Term;
		timestamp = Timestamp;
		type = Type;
	}

	public CustomUser getUser() {
		CustomUser user = null;
		if(userId != 0)
			user = UserDao.findById(userId);
		return user;
	}

	public void increaseFrequency() {
		frequency++;
	}

	public int getFrequency() {
		return frequency;
	}

	public String getTerm() {
		return term;
	}

	public String getDate(String Dateformat) {
		return DateUtil.convertTimestampToFormat(timestamp, Dateformat);
	}

	public String getType() {
		return type;
	}
}
