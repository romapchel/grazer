package nl.impressie.grazer.model;

import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.util.UrlUtil;

import java.util.List;

/**
 * @author Matthijs Bierman
 */
public class UriParameters {

	private List<KeyValue<String, String>> parameters = CollectionFactory.createList();

	public void addParam(String key, String value) {
		KeyValue<String, String> kvp = new KeyValue<String, String>(key, value);
		parameters.add(kvp);
	}

	@Override
	public String toString() {
		StringBuilder uri = new StringBuilder();
		for(KeyValue<String, String> parameter : parameters) {
			if(uri.length() != 0) uri.append("&");
			uri.append(UrlUtil.encode(parameter.getKey()));
			uri.append("=");
			uri.append(UrlUtil.encode(parameter.getValue()));
		}
		return uri.toString();
	}

}
