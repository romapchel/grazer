package nl.impressie.grazer.model;

import lombok.Getter;

/**
 * @author Impressie
 */
public class Page {
	private int pagenumber;
	private String type, contentType;
	@Getter
	private boolean showParents = true, showChildren = true;

	public Page(int pagenumber) {
		this.pagenumber = pagenumber;
		this.type = "";
	}

	public Page setType(String type) {
		this.type = type;
		return this;
	}
	public Page setShowParents(boolean showParents) {
		this.showParents = showParents;
		return this;
	}
	public Page setShowChildren(boolean showChildren) {
		this.showChildren = showChildren;
		return this;
	}

	public Page setContentType(String contentType) {
		this.contentType = contentType;
		return this;
	}

	public int getPagenumber() {
		return pagenumber;
	}

	public String getType() {
		return type;
	}

	public String getContentType() {
		return contentType;
	}

	public String getMessageCode() {
		if(type.length() > 0)
			return type;
		if(contentType.length() > 0)
			return contentType;

		StringBuilder messageCode = new StringBuilder();

		if(contentType.length() > 0)
			messageCode.append(contentType);

		if(type.length() > 0) {
			if(messageCode.length() > 0)
				messageCode.append(".");

			messageCode.append(type);
		}

		return messageCode.toString();
	}
}
