package nl.impressie.grazer.model;

import nl.impressie.grazer.util.SessionUtil;
import nl.impressie.grazer.util.UrlUtil;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.web.util.HtmlUtils.htmlEscape;

/**
 * @author Impressie
 */
public class JSPMessage {
	private String type;
	private String content;

	public static final String CONTENT_PARAMETER = "message";
	public static final String TYPE_PARAMETER = "message_type";

	public static final String ERROR = "error";
	public static final String PARAMS = "params";

	/*  classes */
	public static final String SUCCES = "succes";
	public static final String FAILURE = "fail";
	public static final String INFORMATION = "info";

	/*  lines   */
	public static final String INFO_BUSY = "De applicatie is is momenteel de index aan het bijwerken. Hierdoor zijn enkele functies niet beschikbaar";

	public static final String ERROR_BUSY = "De applicatie is is momenteel de index aan het bijwerken. Deze actie kan niet worden voltooid zolang de index wordt bijgewerkt";
	public static final String ERROR_FAIL = "Het systeem kan deze actie niet voltooien";
	public static final String ERROR_LENGTH = "error.tooltip.format.size";
	public static final String ERROR_CAPTCHA_INVALID = "error.captcha.invalid";
	public static final String ERROR_SHORT = "error.tooltip.minimum";
	public static final String ERROR_LONG = "error.tooltip.maximum";
	public static final String ERROR_UNKNOWN = "Er is iets fout gegaan, propeer het nog een keer";
	public static final String ERROR_404 = "De pagina die u probeerde te bezoeken is niet beschikbaar";
	public static final String ERROR_USER_EXPIRED = "Uw sessie is verlopen. Dit gebeurt automatisch na een bepaalde tijd zonder activiteit of als er te veel sessies op dit account actief zijn";
	public static final String ERROR_NOT_FOUND_TYPE = "Het systeem kan de %1$s niet laden";
	public static final String ERROR_NOT_FOUND_TYPE_HET = "Het systeem kan het %1$s niet laden";
	public static final String ERROR_ALREADY_EXISTS_TYPE = "Er bestaat al een %1$s met de gekozen naam";
	public static final String ERROR_ALREADY_EXISTS_SPECIFIC = "Een %1$s met de %2$s \"%3$s\" bestaat al";
	public static final String ERROR_ALREADY_LOGGED_IN = "error.login.already";
	public static final String ERROR_INCORRECT_LOGIN = "error.login.failed";
	public static final String ERROR_MATCH = "error.tooltip.match";
	public static final String ERROR_UNIQUE = "error.tooltip.unique";
	public static final String ERROR_REQUIRED = "error.tooltip.required";
	public static final String ERROR_INCORRECT_PASS = "Het ingevulde wachtwoord komt niet overeen met het huidige wachtwoord";
	public static final String ERROR_DB_CONNECTION = "Er kon geen verbinding worden gemaakt met de database";
	public static final String ERROR_REMOVE_USER = "messages.error.deleted.account";
	public static final String ERROR_NOT_AUTHORIZED = "U bent niet geautoriseerd om dat te doen";
	public static final String ERROR_MUST_BE_LOGGED_IN = "U moet ingelogd zijn om deze actie te voltooien";
	public static final String ERROR_PARSE_FAIL_TYPE = "De %1$s kon niet worden gelezen";
	public static final String ERROR_INVALID = "De waarde voor \"%1$s\" is ongeldig";
	public static final String ERROR_CRAWLER_NOT_RUNNING = "De indexer is al stopgezet";
	public static final String ERROR_CRAWLER_IS_RUNNING = "De indexer is al bezig";
	public static final String ERROR_NO_LAST_LOGIN = "De laatste login";

	public static final String SUCCES_SAVED_YOUR_TYPE = "Uw %1$s zijn opgeslagen";
	public static final String SUCCES_SAVED_YOUR_TYPE_SINGLE = "Uw %1$s is opgeslagen";
	public static final String SUCCES_SAVED_THE_SPECIFIC = "De %1$s \"%2$s\" is opgeslagen";
	public static final String SUCCES_REMOVE_USER = "messages.success.deleted.account";
	public static final String SUCCES_REMOVED_YOUR_SPECIFIC = "Uw %1$s \"%2$s\" is verwijderd";
	public static final String SUCCES_REMOVED_THE_TYPE = "De %1$s is verwijderd";
	public static final String SUCCES_LOAD = "Uw %1$s \"%2$s\" is geladen";
	public static final String SUCCES_LOGOUT = "Uw bent succesvol uitgelogd";
	public static final String SUCCES_FINISHED_TYPE = "Het %1$s is voltooid";
	public static final String SUCCES_CRAWLER_STOPPED = "messages.indexing.stop";
	public static final String SUCCES_CRAWLER_STARTED = "messages.indexing.start";

	public JSPMessage() {
		this("");
	}

	public JSPMessage(String Content) {
		this(Content, true);
	}

	public JSPMessage(String Content, boolean escape) {
		this(Content, INFORMATION, escape);
	}

	public JSPMessage(String Content, String Type) {
		this(Content, Type, true);
	}

	public JSPMessage(String Content, String Type, boolean escape) {
		if(escape) {
			content = htmlEscape(Content);
			type = htmlEscape(Type);
		} else {
			content = Content;
			type = Type;
		}
	}

	public String getType() {
		return type;
	}

	public String getContent() {
		return content;
	}

	public String getAsParameters() {
		if(content.length() == 0 && type.length() == 0)
			return "";
		String messageParamaterName = "message";
		String typeParamaterName = "message_type";
		String msg = UrlUtil.encode(content);
		String msg_type = UrlUtil.encode(type);
		String parameters = "%1$s=%2$s&%3$s=%4$s";

		return String.format(parameters, messageParamaterName, msg, typeParamaterName, msg_type);
	}

	public void addToRequest(HttpServletRequest Request) {
		SessionUtil.addToSession(Request, "message", content);
		SessionUtil.addToSession(Request, "message_type", type);
	}

	public JSONObject toJson() {
		JSONObject message = new JSONObject();

		message.put("type", type);
		message.put("content", content);

		return message;
	}
}
