
package nl.impressie.grazer.model.User;

import nl.impressie.grazer.dao.ProfileDao;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.DB.Tag;
import nl.impressie.grazer.model.Model;

import java.util.*;

/**
 * @author Impressie
 */
public class Profile extends Model {
	public static final int DEFAULT_RANGE = 31;

	private int id;
	private String name;
	private String contentSearch, titleSearch;
	private int userId;
	private Map sites;
	private Map<String, Map> tags;

	private String mailOption;


	public Profile(int id, String name, int userId, Map tags, Map sites, String contentSearch, String titleSearch, String mailOption) {
		super(id);

		this.id = id;
		this.name = name;
		this.userId = userId;

		this.contentSearch = (contentSearch == null ? "" : contentSearch);
		this.titleSearch = (titleSearch == null ? "" : titleSearch);

		this.sites = sites;
		this.tags = CollectionFactory.createMap();

		this.mailOption = mailOption;
		for(String type : Tag.getTypes()) {
			this.tags.put(type, CollectionFactory.createMap());
		}
		for(Tag tag : (Collection<Tag>) tags.values()) {
			Map map = CollectionFactory.createMap();
			String type = tag.getType();

			if(this.tags.containsKey(type))
				map = this.tags.get(type);
			else
				this.tags.put(type, map);

			map.put(tag.getId(), tag);
		}
	}

	public String getName() {
		return name;
	}

	public int getUserId() {
		return userId;
	}

	public String getMailOption() {
		return mailOption;
	}

	public void setMailOption(String mailOption) {
		this.mailOption = mailOption;
	}

	public Profile updateMail(String option){
		return ProfileDao.updateMail(id,option);
	}
	public String getDisplay() {
		if(name != null && name.length() > 0)
			return getName();
		else
			return String.valueOf(getId());
	}

	public Map getRechtsgebieden() {
		return getTagsByType(Tag.RECHTSGEBIED);
	}

	public Map getNieuwssoorten() {
		return getTagsByType(Tag.NIEUWSSOORT);
	}

	public Map getRubrieken() {
		return getTagsByType(Tag.RUBRIEK);
	}

	private Map returnNotNullMap(Map map) {
		return null == map ? CollectionFactory.createMap() : map;
	}

	public Map getTagsByType(String type) {
		return returnNotNullMap(tags.get(type));
	}

	public Map getTags() {
		Map map = CollectionFactory.createMap();
		for(String key: tags.keySet()) {
			map.putAll(tags.get(key));
		}
		return CollectionFactory.clone(map);
	}

	public Map getSites() {
		return sites;
	}

	public String getTitleSearchString() {
		return titleSearch;
	}

	public String getContentSearchString() {
		return contentSearch;
	}

	public Profile updateTags(Collection newTagids) {
		return updateTagsByType(null, newTagids);
	}

	public Profile updateTagsByType(String type, String[] newTagids) {
		return updateTagsByType(type, CollectionFactory.createList(newTagids));
	}

	public Profile updateTagsByType(String type, Collection<String> newTagids) {
		Map tagsToRemove = CollectionFactory.createMap();
		if(type != null && 0 < type.length())
			tagsToRemove.putAll(tags.get(type));
		else
			tagsToRemove.putAll(getTags());

		List tagsToAdd = CollectionFactory.createList();

		toToRemoveAndToAdd(tagsToRemove, tagsToAdd, newTagids);

		return ProfileDao.updateTags(id, tagsToRemove, tagsToAdd);
	}

	public Profile updateSites(String[] newSiteids) {
		return updateSites(CollectionFactory.createList(newSiteids));
	}

	public Profile updateSites(Collection<String> newSiteids) {
		Map sitesToRemove = CollectionFactory.createMap();
		sitesToRemove.putAll(sites);

		List sitesToAdd = CollectionFactory.createList();

		toToRemoveAndToAdd(sitesToRemove, sitesToAdd, newSiteids);

		return ProfileDao.updateSites(id, sitesToRemove, sitesToAdd);
	}

	public boolean isGlobal() {
		return userId == 0;
	}

	public void toToRemoveAndToAdd(Map toRemove, List toAdd, Collection<String> newIds) {
		for(String newTagId : newIds) {
			if(toRemove.containsKey(newTagId))
				toRemove.remove(newTagId);
			else
				toAdd.add(newTagId);
		}
	}


	public Profile updateSearch(String titleSearch, String contentSearch) {
		return ProfileDao.update(id, titleSearch, contentSearch);
	}

	public Profile updateName(String name) {
		Profile prof = ProfileDao.update(id, name);
		this.name = prof.getName();
		return prof;
	}

	public Profile updateGlobal(boolean newGlobalStatus) {
		if(isGlobal() == newGlobalStatus)
			return this;

		return ProfileDao.update(id, newGlobalStatus);
	}

	public boolean hasTag(Tag tag) {
		return getTagsByType(tag.getType()).containsKey(tag.getId());
	}
}
