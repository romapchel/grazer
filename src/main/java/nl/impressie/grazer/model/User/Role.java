package nl.impressie.grazer.model.User;

import nl.impressie.grazer.dao.RoleDao;
import nl.impressie.grazer.util.DateUtil;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * @author Impressie
 */
public class Role {
	public static final String ADMIN = "ROLE_ADMIN";
	public static final String USER = "ROLE_USER";
	public static final String NONE = "ROLE_NONE";
	public static final String GLOBAL_EDIT = "ROLE_EDIT_GLOBAL";
	public static final String MODERATOR = "ROLE_MODERATOR";

	private int id;
	private String username;
	private String name;
	private int endDate;

	public Role(int Id, String Name, String UserName, int EndDate) {
		id = Id;
		username = UserName;
		name = Name;
		endDate = EndDate;
	}

	public String getName() {
		return name;
	}

	public void updateEndDate(int endDate) {
		this.endDate = endDate;
		RoleDao.updateEnd(id, endDate);
	}

	/**
	 * This method tests if this object is a certain role
	 * if the role is inactive then it will always return false
	 *
	 * @param Rolename The name of the role that is being looked for
	 * @return false if this role is marked as inactive or if the names don't match
	 */
	public boolean isRole(String Rolename) {
		return name.equals(Rolename);
	}

	public int remainingDays() {
		return DateUtil.getDifferenceBetweenDateAndToday(endDate);
	}

	public String toHtml() {
		StringBuilder builder = new StringBuilder();

		builder.append("<div class=\"role\">");
		builder.append(String.format("<div class=\"col\">%s</div>", getDisplay()));
		builder.append(String.format("<div class=\"col\">%s</div>", DateUtil.convertIntToFormat(endDate, "dd-MM-yyyy")));
		builder.append("</div>");

		return builder.toString();
	}

	public String toHtml(int index) {
		String html = "";
		String checked = "";
		String untilString = "";
		String date = "0";

		boolean active = isActive();

		if(active)
			date = String.valueOf(DateUtil.getDifferenceBetweenDateAndToday(endDate));

		String remainingDaysId = id + "_daysRemaining";
		String remainingdaysName = "rolesTime[]";

		untilString = String.format("<label for=\"%3$s\">Days remaining:</label><input id=\"%2$s\" name=\"%2$s\" type=\"number\" value=\"%1$s\" min=\"0\" />", date, remainingdaysName, remainingDaysId);

		html += "<div class=\"role\">";
		html += "<div class=\"column-1-2\">";
		html += String.format("<h4>%1$s</h4>", name);
		if(active)
			checked = " checked=\"true\"";
		else
			checked = "";
		html += String.format("<input id=\"%2$s\" type=\"radio\" name=\"%1$s\" value=\"true\"%3$s />", name, id + "_enabled", checked);
		html += String.format("<label for=\"%1$s\">on</label>", id + "_enabled");
		if(!active)
			checked = " checked=\"true\"";
		else
			checked = "";
		html += String.format("<input id=\"%2$s\" type=\"radio\" name=\"%1$s\" value=\"false\"%3$s />", name, id + "_disabled", checked);
		html += String.format("<label for=\"%1$s\">off</label>", id + "_disabled");
		html += "</div>";
		html += "<div class=\"column-1-2\">";
		html += untilString;
		html += "</div>";
		html += "</div>";

		return html;
	}

	public GrantedAuthority toAuthority() {
		return new SimpleGrantedAuthority(name);
	}

	public int getEndDate() {
		return endDate;
	}

	public String getDisplay() {
		if(isRole(ADMIN))
			return "Administrator account";
		else if(isRole(USER))
			return "Gebruiker account";
		else
			return "Geen abonnement";
	}

	public String remainingDaysMessage() {
		StringBuilder builder = new StringBuilder();

		if(isActive() && !isRole(NONE)) {
			int days = 0;
			if(0 != endDate)
				days = remainingDays();

			if(days > 0) {
				builder.append(String.format("Dit abonnement vervalt op: %s", DateUtil.convertStringToFormat(String.valueOf(endDate), "yyyyMMdd", "dd-MM-yyyy")));
				builder.append(" (nog ");
				builder.append(days);
				builder.append(" dag");
				if(days > 1)
					builder.append("en");
				builder.append(")");
			} else {
				builder.append("Dit abonnement loopt nooit af");
			}
		} else if(!isRole(NONE)) {
			builder.append("Dit abonnement is afgelopen");
		}

		return builder.toString();
	}

	public boolean isActive() {
		int current = DateUtil.getCurrentDateAsInt();
		return endDate == 0 || endDate > current;
	}

	public static String[] allRoles() {
		String[] roles = {NONE, USER, ADMIN};
		return roles;
	}
}
