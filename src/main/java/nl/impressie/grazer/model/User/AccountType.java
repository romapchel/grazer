package nl.impressie.grazer.model.User;

import nl.impressie.grazer.dao.TypeRoleDao;
import nl.impressie.grazer.model.Model;
import nl.impressie.grazer.util.DateUtil;
import org.springframework.security.core.GrantedAuthority;

import java.util.Map;

/**
 * @author Impressie
 */
public class AccountType extends Model {
	private String name;
	private int userTypeId = -1, endDate;
	private Map<String, GrantedAuthority> roles;

	public AccountType(Object id, String name) {
		super(id);
		this.name = name;
		roles = TypeRoleDao.findAuthoritieMapByTypeId(id);
	}

	public String getName() {
		return name;
	}
	public int getEndDate() {
		return endDate;
	}
	public String getEndDate(String format) {
		String endDateString = String.valueOf(endDate);
		if(endDate <= 0)
			return endDateString;

		try {
			return DateUtil.convertIntToFormat(endDate, format);
		} catch(IllegalArgumentException e) {
			return endDateString;
		}
	}
	public void setUserType(int userTypeId, int endDate) {
		this.userTypeId = userTypeId;
		this.endDate = endDate;
	}
	public int getUserTypeId() { return userTypeId; }

	public boolean hasRole(String rolename) {
		return roles.containsKey(rolename);
	}

	public Map<String, GrantedAuthority> getAuthorities() { return roles; }
	public void setAuthorities(Map roles) { this.roles = roles; }
}
