package nl.impressie.grazer.model.User;

//import com.paypal.api.payments.Amount;

import nl.impressie.grazer.dao.*;
import nl.impressie.grazer.exception.PaymentException;
import nl.impressie.grazer.factories.CollectionFactory;
import nl.impressie.grazer.model.Bundle;
import nl.impressie.grazer.model.CustomPayment;
import nl.impressie.grazer.model.DB.Verification;
import nl.impressie.grazer.model.Invoice;
import nl.impressie.grazer.util.AuthenticationUtil;
import nl.impressie.grazer.util.DatabaseUtils.Parameter;
import nl.impressie.grazer.util.DateUtil;
import nl.impressie.grazer.util.LogUtil;
import nl.stil4m.mollie.Client;
import nl.stil4m.mollie.ResponseOrError;
import nl.stil4m.mollie.concepts.Subscriptions;
import nl.stil4m.mollie.domain.CreateCustomer;
import nl.stil4m.mollie.domain.Customer;
import nl.stil4m.mollie.domain.Page;
import nl.stil4m.mollie.domain.Subscription;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 * @author Impressie
 */
public class CustomUser implements UserDetails {
	private int id;
	private int activeProfileId;
	private String username, password, mollieId;
	private boolean enabled;
	private int lastLogin, currentLogin;

	private String organisation;
	private String initials, tussenvoegsel, lastname;
	private String mail;
	private String street, housenumber, zip, city;
	private Long created;

	private AccountType type;


	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public CustomUser(int id, String username, String password, String mollieId, String organisation, String initials, String tsvg, String lastname, String mail, int activeProfielId, boolean enabled, int lastLogin, int currentLogin, String street, String housenumber, String zip, String city) {

		this.id = id;

		this.username = username;
		this.activeProfileId = activeProfielId;
		this.enabled = enabled;
		this.lastLogin = lastLogin;
		this.currentLogin = currentLogin;
		this.password = password;
		this.mollieId = mollieId;

		this.organisation = organisation;
		this.initials = initials;
		this.tussenvoegsel = tsvg;
		this.lastname = lastname;
		this.mail = mail;

		this.street = street;
		this.housenumber = housenumber;
		this.zip = zip;
		this.city = city;

		created = DateUtil.getTimestamp();
	}

	public int delete() {
		return UserDao.delete(id);
	}

	public boolean setActiveProfileId(Object profileId) {
		boolean changed = false;
		if(null == ProfileDao.findById(profileId))
			return changed;

		int pid = Integer.valueOf(profileId.toString());

		if(pid == activeProfileId)
			return changed;

		changed = UserDao.setActiveProfileId(pid, id);
		if(changed) {
			activeProfileId = pid;
			AuthenticationUtil.addToUpdatedUsers(id);
		}

		return changed;
	}

	public boolean isAdmin() {
		return hasRole(Role.ADMIN);
	}

	public boolean hasRole(String rolename) {
		return getAccountType().hasRole(rolename);
	}

	public Profile getActiveProfile() {
		return ProfileDao.findById(activeProfileId);
	}

	public int getId() {
		return id;
	}

	public String getMail() {
		return mail;
	}

	public AccountType getAccountType() {
		if(type == null && isActive())
			type = UserTypeDao.getTypeByUserId(id);

		if(type == null)
			type = AccountTypeDao.findById(AccountTypeDao.DEFAULT_TYPE_ID);

		return type;
	}

	public CustomUser rescindBundle(Bundle bundle) {
		return updateBundle(bundle, false);
	}
	public CustomUser updateAccountType(Bundle bundle) {
		return updateBundle(bundle, true);
	}

	private CustomUser updateBundle(Bundle bundle, boolean addDays) {
		int typeId = bundle.getTypeId();
		int days = DateUtil.calculateDate(getAccountType().getEndDate(), bundle, addDays);
		return updateAccountType(typeId, days, true);
	}

	public CustomUser updateAccountType(int typeId, int days) {
		return updateAccountType(typeId, days, false);
	}
	public CustomUser updateAccountType(int typeId, int days, boolean daysIsEndDate) {
		return updateAccountType(typeId, days, daysIsEndDate, true);
	}

	public CustomUser updateAccountType(int typeId, int days, boolean daysIsEndDate, boolean addDays) {
		AccountType madeType = UserTypeDao.updateType(this, typeId, days, daysIsEndDate, addDays);
		type = madeType;
		AuthenticationUtil.addToUpdatedUsers(this);

		return this;
	}

	public String getUsername() {
		return username;
	}

	public String getMollieId() {
		return mollieId;
	}

	public CustomPayment getPayment() {
		return PaymentDao.getByUserId(id);
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public boolean isActive() {
		return enabled;
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof CustomUser) && (((CustomUser) o).getId() == this.id);
	}

	@Override
	public int hashCode() {
		int hash = Objects.hash(id);
		return hash;
	}

	public List<Profile> getProfiles() {
		return ProfileDao.findByUserId(id);
	}
	public List<Profile> getUsableProfiles() {
		List<Profile> userProfiles = this.getProfiles();
		List globalProfiles = ProfileDao.findByUserId(0);
		List<Profile> profiles = CollectionFactory.createList(globalProfiles);
		profiles.addAll(userProfiles);
		return profiles;
	}

	@Override
	public List<GrantedAuthority> getAuthorities() {
		return new ArrayList(getAccountType().getAuthorities().values());
	}

	@Override
	public String getPassword() {
		return password;
	}

	public int getDaysSinceLastLogin() {
		int daysSinceLastLogin = 0;
		if(lastLogin > 0)
			daysSinceLastLogin = DateUtil.getDifferenceBetweenDateAndToday(lastLogin);

		return daysSinceLastLogin;
	}


	public int login() {
		int currentDate = DateUtil.getCurrentDateAsInt();

		if(currentLogin == currentDate)
			return 0;

		Map map = CollectionFactory.createMap();
		map.put("last_login", currentLogin);
		map.put("current_login", currentDate);
		return UserDao.update(id, map);
	}

	public Map loadAccountValues() {
		return UserDao.loadAccountValues(username, organisation, initials, tussenvoegsel, lastname, street, housenumber, zip, city, mail);
	}

	public Boolean needsToBeUpdated(Map<Object, Long> updateTimes) {
		//if all users need to be updated
		if(updateTimes.containsKey("all") && updateTimes.get("all") > created)
			return true;
		//if this user needs to be updated
		if(updateTimes.containsKey(id) && updateTimes.get(id) > created)
			return true;
		//if this user object is over an hour old
		return (DateUtil.getCurrentDateAsInt() - created) > (1000 * 60 * 60);

	}

	public Map getCurrent() {
		Map current = loadAccountValues();
		current.put(UserDao.ID, id);
		current.put(UserDao.ACTIVE_PROFILE_ID, activeProfileId);
		current.put(UserDao.ENABLED, enabled);
		current.put(UserDao.TYPE_ID, getAccountType().getId());

		return current;
	}

	public String getName() { return lastname; }
	public String getOrganisation() { return organisation; }

	public CustomUser activate () {
		List params = CollectionFactory.createList();
		params.add(new Parameter(UserDao.ENABLED, true));

		if(!enabled)
			enabled = UserDao.update(id, params).isEnabled();

		return this;
	}

	public String getStreetAddress() {
		return String.format("%s %s", street, housenumber);
	}

	public String getCityAddress() {
		return String.format("%s %s", zip, city);
	}

	public String createMollieId(Client client) {
		String mollie_customer_id = "";
		try {
			ResponseOrError<Customer> customerResponseOrError = client.customers().create(new CreateCustomer(username, mail, Optional.empty(), null));
			if(!customerResponseOrError.getSuccess())
				throw PaymentException.getException(customerResponseOrError);

			Customer customer = customerResponseOrError.getData();
			Map values = CollectionFactory.createMap();
			mollie_customer_id = customer.getId();
			values.put(UserDao.MOLLIE_ID, mollie_customer_id);
			UserDao.update(id, values);
			this.mollieId = mollie_customer_id;
		} catch(IOException | PaymentException e) {
			e.printStackTrace();
		}
		return mollie_customer_id;
	}

	public List<Invoice> getInvoices() {
		return InvoiceDao.findForUser(id);
	}

	public CustomPayment getActiveSubscription(Client client) {
		return getActiveSubscription(client, true);
	}
	private CustomPayment getActiveSubscription(Client client, boolean firstRun) {
		CustomPayment active = null;
		if(mollieId != null && mollieId.length() > 0) {
			Subscriptions subs = client.subscriptions(mollieId);
			try {
				ResponseOrError<Page<Subscription>> result = subs.list(Optional.empty(), Optional.empty());
				if(result.getSuccess()) {
					Page<Subscription> pg = result.getData();
					for(Subscription sub : pg.getData()) {
						if(CustomPayment.MOLLIE_STATUS_ACTIVE.equals(sub.getStatus())) {
							active = PaymentDao.getBySubscription(sub.getId());
							break;
						}
					}
				} else {
					throw PaymentException.getException(result);
				}
			} catch(PaymentException e) {
				if(firstRun && e.isCustomerError()) {
					createMollieId(client);
					return getActiveSubscription(client, false);
				} else {
					LogUtil.logException(e);
				}
			} catch(URISyntaxException | IOException e) {
				LogUtil.logException(e);
			}
		}
		return active;
	}
	public List<Subscription> getActiveSubscriptions(Client client) {
		List subscriptions = CollectionFactory.createList();
		Subscriptions subs = client.subscriptions(mollieId);
		System.out.println("Debug subslist: " + subs.toString() + " mollieId: " + mollieId);
		try {
			ResponseOrError<Page<Subscription>> result = subs.list(Optional.empty(), Optional.empty());
			System.out.println("Debug resultStatus: " + result.getSuccess());

			if(result.getSuccess()) {
				System.out.println("Debug result is success");
				Page<Subscription> pg = result.getData();
				for(Subscription sub: pg.getData()) {
					System.out.println("Debug status: " + sub.getStatus());
					if(CustomPayment.MOLLIE_STATUS_ACTIVE.equals(sub.getStatus())) {
						subscriptions.add(sub);
					}
				}
			} else {
				throw PaymentException.getException(result);
			}
		} catch(URISyntaxException | PaymentException | IOException e) {
			LogUtil.logException(e);
		}
		return subscriptions;
	}

	public String getFullName() {
		String initials = this.initials;
		if(!initials.endsWith("."))
			initials += ".";

		String tussenvoegsel = this.tussenvoegsel;
		if(tussenvoegsel.length() > 0)
			tussenvoegsel += " ";

		return String.format("%s %s%s", initials, tussenvoegsel, lastname);
	}

	public Verification getVerification() {
		return VerificationDao.findByUserId(id);
	}
}
