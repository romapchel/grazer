package nl.impressie.grazer.factories;

import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

/**
 * @author Impressie
 */
public class CollectionFactory {

	public static class MapPair<T, S> {
		private Object key, value;

		public <T, S> MapPair(T key, S value) {
			this.key = key;
			this.value = value;
		}

		private Map addToMap(Map map) {
			map.put(key, value);
			return map;
		}
	}

	public static Map createMap() {
		return new LinkedHashMap();
	}
	public static <T, S> Map<T, S> createMap(MapPair<T, S>... pairs) {
		Map<T, S> map = createMap();
		for(MapPair pair: pairs) {
			map = pair.addToMap(map);
		}
		return map;
	}

	public static List createList() {
		return new ArrayList();
	}
	public static <T> List<T> createList(Collection<T> items, T... moreItems) {
		T[] array = (T[])items.toArray();
		array = ArrayUtils.addAll(array, moreItems);
		return createList(array);
	}
	public static <T> List<T> createList(Collection<T> items) {
		T[] array = (T[])items.toArray();
		return createList(array);
	}
	public static <T> List<T> createList(T... items) {
		List<T> list = createList();
		for(T item: items) {
			list.add(item);
		}
		return list;
	}

	public static Set createSet() {
		return new LinkedHashSet();
	}
	public static <T> Set<T> createSet(Collection<T> items) {
		T[] array = (T[])items.toArray();
		return createSet(array);
	}
	public static <T> Set<T> createSet(T... items) {
		Set set = createSet();
		for(T item: items) {
			set.add(item);
		}
		return set;
	}

	public static Deque createQueue() {
		return new ArrayDeque();
	}


	public static <T> List<T> clone(List<T> toClone) {
		List cloned = createList();
		cloned.addAll(toClone);

		return cloned;
	}

	public static <S, T> Map<S, T> clone(Map<S, T> toClone) {
		Map cloned = createMap();
		cloned.putAll(toClone);

		return cloned;
	}

	public static <T> Set<T> clone(Set<T> toClone) {
		Set cloned = createSet();
		cloned.addAll(toClone);

		return cloned;
	}
}
