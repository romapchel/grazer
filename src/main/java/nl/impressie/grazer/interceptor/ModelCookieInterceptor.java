package nl.impressie.grazer.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Impressie
 */
@Component
public class ModelCookieInterceptor extends HandlerInterceptorAdapter {

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
		if(modelAndView != null) {
			for(Object value : modelAndView.getModel().values()) {
				if(value instanceof Cookie)
					response.addCookie((Cookie) value);
			}
		}
	}

}