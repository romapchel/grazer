<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-06-04
  Time: 9:30
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="type" required="true" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>

<%@ attribute name="path_is_absolute_url" required="false" type="java.lang.Boolean" %>

<c:if test="${empty path_is_absolute_url}"><c:set var="path_is_absolute_url" value="False" /></c:if>

<c:if test="${empty ALREADY_LOADED_RESOURCES_GLOBAL}">
    <jsp:useBean id="ALREADY_LOADED_RESOURCES_GLOBAL" class="java.util.HashMap" scope="request"/>
</c:if>

<c:if test="${not path_is_absolute_url}">
    <c:set var="path" value="${base_url}${path}" />
</c:if>

<c:if test="${not ALREADY_LOADED_RESOURCES_GLOBAL.containsKey(path)}">
    <c:choose>
        <c:when test="${type eq 'css'}">
            <link rel="stylesheet" type="text/css" href="${path}">
        </c:when>
        <c:otherwise>
            <script src="${path}"></script>
        </c:otherwise>
    </c:choose>
    ${ALREADY_LOADED_RESOURCES_GLOBAL.put(path, true)}
</c:if>
