<%--
  Created by IntelliJ IDEA.
  User: Julian
  Date: 2018-05-08
  Time: 09:55
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="ctags" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<%@ attribute name="input_id" required="true" type="java.lang.String" %>
<%@ attribute name="input_name" required="true" type="java.lang.String" %>
<%@ attribute name="input_value" required="true" type="java.lang.String" %>
<%@ attribute name="warning_text" required="true" type="java.lang.String" %>
<%@ attribute name="button_name" required="true" type="java.lang.String" %>
<%@ attribute name="button_value" required="true" type="java.lang.String" %>

<input id="${input_id}" type="checkbox" name="${input_name}" value="${input_value}" class="radio-check expand-check hidden delete-check" />
<c:set var="alt_text"><ctags:message key="actions.action_delete.title" /></c:set>
<ctags:checkboxButton forid="${input_id}" text="<span aria-label='${alt_text}' title='${alt_text}' class='fa-times large fa'></span>" additional_label_class="red p-o delete-label no-icon" span_class="flex-basis-100" />
<div class="expand border-square">
    <div class="col-12">${warning_text}</div>
    <button class="button" type="submit" name="${button_name}" value="${button_value}">Ja</button>
    <ctags:checkboxButton forid="${input_id}" text="Nee" base_class="button" />
</div>
