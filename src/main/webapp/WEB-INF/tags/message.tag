<%--
  Created by IntelliJ IDEA.
  User: Julian
  Date: 2019-01-02
  Time: 11:04
--%>
<%@ tag body-content="empty" dynamic-attributes="formatValues" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="cfn" uri="/WEB-INF/jstl-functions.tld"%>
<%@ taglib prefix="ctags" tagdir="/WEB-INF/tags" %>

<%@ attribute name="key" required="true" %>
<%@ attribute name="defaultValue" required="false"  %>

<c:set var="key" value="${fn:trim(key)}" />
<c:set var="msg">
    <fmt:message key="${key}">
        <c:forEach var="item" items="${formatValues}">
            <c:forEach var="value" items="${cfn:safeForeach(item.value)}">
                <fmt:param>
                    <ctags:message key="${value}" />
                </fmt:param>
            </c:forEach>
        </c:forEach>
    </fmt:message>
</c:set>

<c:if test="${fn:startsWith(msg,'???')}">
    <c:if test="${empty defaultValue}">
        <c:set var="defaultValue" value=" ${key}" />
    </c:if>
    <c:set var="msg" value=" ${defaultValue}" />
</c:if>
${msg}