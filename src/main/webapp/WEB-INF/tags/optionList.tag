<%--
  Created by IntelliJ IDEA.
  User: Arjan
  Date: 2020-08-03
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cfn" uri="/WEB-INF/jstl-functions.tld" %>

<%@ attribute name="name" required="false" type="java.lang.String" %>
<%@ attribute name="addLabelClass" required="false" type="java.lang.String" %>
<%@ attribute name="addAllNoneButtonClass" required="false" type="java.lang.String" %>
<%@ attribute name="addContainerClass" required="false" type="java.lang.String" %>
<%@ attribute name="selectAllTitle" required="false" type="java.lang.String" %>
<%@ attribute name="groupByFirstCharacter" required="false" type="java.lang.String" %>
<%@ attribute name="subRechtsgebieden" required="false" type="java.lang.String" %>
<%@ attribute name="groupByParent" required="false" type="java.lang.String" %>
<%@ attribute name="alignRight" required="false" type="java.lang.String" %>
<%@ attribute name="filterByParents" required="false" type="java.lang.String" %>
<%@ attribute name="addParentName" required="false" type="java.lang.Boolean" %>

<c:if test="${empty name}"><c:set var="name" value="items" /></c:if>
<c:if test="${empty alignRight}"><c:set var="alignRight" value="false" /></c:if>
<c:if test="${empty filterByParents}"><c:set var="alignRight" value="false" /></c:if>
<c:if test="${empty groupByFirstCharacter}"><c:set var="groupByFirstCharacter" value="False" /></c:if>
<c:if test="${empty groupByParent}"><c:set var="groupByFirstParent" value="False" /></c:if>
<c:if test="${not empty addLabelClass}"><c:set var="addLabelClass" value=" ${addLabelClass}" /></c:if>
<c:if test="${not empty addAllNoneButtonClass}"><c:set var="addAllNoneButtonClass" value=" ${addAllNoneButtonClass}" /></c:if>
<c:if test="${empty subRechtsgebieden}"><c:set var="subRechtsgebieden" value="false" /></c:if>
<c:if test="${empty addParentName}"><c:set var="addParentName" value="${false}" /></c:if>

<c:set var="parentMap" value="${cfn:emptyMap()}" />
<c:if test="${groupByParent}">
    <c:set var="groupByFirstCharacter" value="False" />
    <c:set var="parentMap" value="${cfn:getTagFamilieMap(autocompleteItems)}" />
<%--    <c:set var="lastParent" value="${cfn:getTagFamilieMap(autocompleteItems)}" />--%>
</c:if>

<c:set var="labels" value="" />
<c:set var="uneditable" value="${not empty editableProfile && not editableProfile}" />
<c:set var="for_id" value="" />
<c:set var="currentParentName" value="" />

<div class="row m-0 p-0">
    <c:choose>
        <c:when test="${alignRight}">
            <div class="col-6 m-0 p-0 mobile-col-12">&nbsp;</div>
        </c:when>
        <c:otherwise>
            <div class="col-6 m-0 p-0 mobile-col-12">&nbsp;</div>
        </c:otherwise>
    </c:choose>
    <div class="optionlist-container col-6 m-0 p-0 mobile-col-12">

        <c:forEach var="item" items="${autocompleteItems}" varStatus="loop">
            <c:set var="temp_item_name" value="${item.getName()}" />

            <c:if test="${parentMap.containsKey(item.getId())}">
                <c:set var="lastParent" value="${parentMap.get(item.getId())}" />
            </c:if>
            <c:if test="${addParentName == true}">
                <c:set var="temp_item_name" value="${lastParent.getParent().getName()}: ${temp_item_name}" />
            </c:if>

            <c:choose>

                <c:when test="${subRechtsgebieden != 'true'}">

                    <c:choose>
                        <c:when test="${parentMap.containsKey(item.getId()) && lastParent.getChildren().size() > 0}">

                            <c:set var="temp_previous_first_character" value="${temp_item_name_first_character}" />
                            <c:set var="temp_first_character_check_id" value="${name}-${temp_item_name_first_character}" />
                            <c:set var="temp_name" value="" />
                            <c:set var="temp_val" value="" />
                            <c:set var="temp_checked" value="" />
                            <c:set var="temp_activated" value="" />
                            <c:set var="temp_id" value="${lastParent.getParent().getId()}" />
                            <c:set var="temp_first_character_check_id" value="${name}-${temp_id}" />
                            <c:if test="${not empty selectedItems && selectedItems.containsKey(temp_id)}">
                                <c:set var="temp_checked" value=" checked='checked'" />
                                <c:set var="temp_activated" value="active" />
                            </c:if>

                            <input id="${temp_first_character_check_id}" name="${name}[]" value="${temp_id}" type="checkbox"${temp_item_name}${temp_val}${temp_checked} class="hidden" />
                            <ctags:checkboxButton forid="${temp_first_character_check_id}" text="${temp_item_name}" additional_label_class="list-button rechtsgebied-top ${temp_id} bgc-light-primary col-12 no-icon" activated_class="${temp_activated}" />
                        </c:when>
                        <c:otherwise>
                            <c:if test="${selectedItems.containsKey(item.getId())}">
                                <c:set var="temp_id" value="${item.getId()}" />
                                <c:set var="temp_first_character_check_id" value="${name}-${temp_id}" />

                                <input id="${temp_first_character_check_id}" type="checkbox" checked="checked" class="hidden" name="${name}[]" value="${temp_id}"/>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                </c:when>


                <c:otherwise>
                    <c:if test="${parentMap.containsKey(item.getId()) && lastParent.getChildren().size() > 0 }">
                        <c:set var="currentParentName" value="parentid-${item.getId()}" />
                    </c:if>

                    <c:set var="tagIsChildTag" value="${lastParent.getParent().getId() != item.getId() || lastParent.getChildren().size() == 0 }" />

                    <c:choose>
                        <c:when test="${filterByParents != 'true'}">
                            <c:set var="parentTagIsSelected" value="${true}" />
                        </c:when>
                        <c:otherwise>
                            <c:set var="parentTagIsSelected" value="${empty selectedItems || (selectedItems.size() == 0 || selectedItems.containsKey(lastParent.getParent().getId()))}" />
                        </c:otherwise>
                    </c:choose>

                    <c:choose>
                        <c:when test="${tagIsChildTag}">
                            <c:if test="${parentTagIsSelected}">
                                <c:set var="temp_previous_first_character" value="${temp_item_name_first_character}" />
                                <c:set var="temp_first_character_check_id" value="${name}-${temp_item_name_first_character}" />
                                <c:set var="temp_name" value="" />
                                <c:set var="temp_val" value="" />
                                <c:set var="temp_checked" value="" />
                                <c:set var="temp_activated" value="" />
                                <c:set var="temp_id" value="${item.getId()}" />
                                <c:set var="temp_first_character_check_id" value="${name}-${temp_id}" />
                                <c:if test="${not empty selectedItems && selectedItems.containsKey(temp_id)}">
                                    <c:set var="temp_checked" value=" checked='checked'" />
                                    <c:set var="temp_activated" value="active" />
                                </c:if>

                                <input id="${temp_first_character_check_id}" type="checkbox"${temp_item_name}${temp_val}${temp_checked} class="hidden" name="${name}[]" value="${temp_id}"/>
                                <ctags:checkboxButton forid="${temp_first_character_check_id}" text="${temp_item_name}" additional_label_class="list-button rechtsgebied-sub ${currentParentName} bgc-light-primary col-12 no-icon" activated_class="${temp_activated}" />
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${not empty selectedItems && (selectedItems.size() == 0 || selectedItems.containsKey(lastParent.getParent().getId()))}">

                                <c:set var="temp_id" value="${item.getId()}" />
                                <c:set var="temp_first_character_check_id" value="${name}-${temp_id}" />

                                <input id="${temp_first_character_check_id}" type="checkbox" checked="checked" class="hidden" name="${name}[]" value="${temp_id}"/>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>

        </c:forEach>

    </div>
</div>

