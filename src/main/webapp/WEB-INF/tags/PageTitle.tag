<%--
  Created by IntelliJ IDEA.
  User: Julian
  Date: 2018-05-08
  Time: 09:55
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="ctags" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<%@ attribute name="title" required="true" type="java.lang.String" %>
<%@ attribute name="description" required="true" type="java.lang.String" %>

<c:set var="temp_page_title_tag_var"><ctags:message key="${title}" /></c:set>
<c:if test="${temp_page_title_tag_var ne title}">
    <div id="page-title-container">
        <ctags:info message="${description}" buttonText="${title}" iconClasses="mouseover-below" />
    </div>
</c:if>