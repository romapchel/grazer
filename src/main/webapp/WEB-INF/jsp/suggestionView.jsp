<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/contact.css"/>

<c:set var="label_size" value="col-12 col-lg-3 p-0" />
<c:set var="input_size" value="col" />

<div class="screen">
    <div class="${content_size_class} shadowed white">
        <div class="col-12 p-0">
            <h1 class="${page_title_class}">
                <ctags:message key="suggestion.header"/>
            </h1>
            <form id="contactform" action="${base_url}suggestion" class="validatable col-12 black" method="POST">
                <div class="col-12 p-0">
                    <c:set var="standard_mail" value="" />
                    <c:if test="${not empty user}"><c:set var="standard_mail" value="${user.mail}" /></c:if>

                    <c:set var="title"><ctags:message key="sugestion.input.mail.label"/></c:set>
                    <ctags:input name="mail" title="${title}" placeholder="${title}" type="email"
                                 required="True" values="${previous}" errors="${errors}" inputLengthClasses="${input_size}" titleLengthClasses="${label_size}" />
                </div>

                <c:set var="checked" value="" />
                <c:if test="${empty values || values.get(name) == 'Site'}"><c:set var="checked" value="checked=\"checked\"" /></c:if>
                <input id="site-suggestion" name="suggestion-type" value="Site" type="radio" class="hidden"${checked} />

                <c:set var="checked" value="" />
                <c:if test="${not empty values && values.get(name) == 'General'}"><c:set var="checked" value="checked=\"checked\"" /></c:if>
                <input id="general-suggestion" name="suggestion-type" value="General" type="radio" class="hidden" />

                <div id="type-div" class="col-12 p-0 ${flexwrap}">
                    <label class="button inactive ${label_size}">
                        <ctags:message key="sugestion.input.type.label"/>
                    </label>
                    <div class="${input_size} p-0 ${flexwrap}">
                        <c:set var="labelClass" value="button bgc-light-primary white col-12 col-lg-6"/>
                        <label id="site-suggestion-label" class="${labelClass}" for="site-suggestion">
                            <ctags:message key="sugestion.input.type.website.label"/>
                        </label>

                        <label id="general-suggestion-label" for="general-suggestion" class="${labelClass}">
                            <ctags:message key="sugestion.input.type.general.label"/>
                        </label>
                    </div>
                </div>
                <c:set var="title"><ctags:message key="sugestion.input.site.label"/></c:set>
                <c:set var="placeholder"><ctags:message key="sugestion.input.site.placeholder"/></c:set>
                <div class="col-12 p-0">
                    <ctags:input name="site" title="${title}" placeholder="${placeholder}"
                                 values="${previous}"
                                 errors="${errors}" inputLengthClasses="${input_size}" titleLengthClasses="${label_size}" />
                </div>
                <div class="col-12 p-0">
                    <label for="contactform-message" class="button col-12 p-0">
                        <ctags:message key="sugestion.input.message.label"/>
                    </label>
                    <c:set var="placeholder"><ctags:message key="sugestion.input.message.placeholder"/></c:set>
                    <textarea id="contactform-message" class="col-12" name="message" placeholder="${placeholder}"
                              required="true"></textarea>
                </div>
                <div class="col-12 centered">
                    <button type="submit" class="button text-white bgc-light-primary submit-button">
                        <ctags:message key="sugestion.button.submit"/>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>