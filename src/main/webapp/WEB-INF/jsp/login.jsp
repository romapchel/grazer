<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

    <style>
        #login_form input {
            padding: 5px;
            width: 100%;
        }
    </style>
<div class="screen">
        <div class="content col-12 col-lg-6 shadowed justify-content-center">
            <h1 class="${page_title_class}">
                <ctags:message key="login.header.description"/>
            </h1>
            <form id="login_form" action="${base_url}j_spring_security_check" method='POST'>
                <c:set var="size" value="col-12 col-md-6" />

                <c:set var="val" value="" />
                <c:set var="autofocus" value="${true}" />

                <c:if test="${not empty username}">
                    <c:set var="val" value="${username}" />
                    <c:set var="autofocus" value="${false}" />
                </c:if>

                <c:set var="title">
                    <ctags:message key="login.input.username.title"/>
                </c:set>
                <ctags:input name="username" placeholder="${title}" title="${title}" value="${val}"
                             autofocus="${autofocus}"/>
                <c:set var="title">
                    <ctags:message key="login.input.password.title"/>
                </c:set>
                <ctags:input name="password" placeholder="${title}" title="${title}" type="password"
                             autofocus="${not autofocus}"/>

                <button type="submit" class="button bordered col-12 centered text-white bgc-light-primary">
                    <ctags:message key="login.button.login"/>
                </button>

                <p class="col-12 centered">
                    <ctags:message key="login.format.register.link" formatValue="${base_url}register" />
                </p>
            </form>
        </div>
    </div>
    <div style="display: none;">This string only exists so that Ajax requests know that the user is not logged in</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>