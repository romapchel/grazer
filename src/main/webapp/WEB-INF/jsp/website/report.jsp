<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen bgc-grey">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">
        <%@include file="/WEB-INF/jsp/website/includes/select.jsp" %>
    </div>
</div>

<div class="screen bgc-light-primary">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">
        <c:set var="articles" value="${report.getArticles()}" />

        <c:if test="${articles.size() > 1}">
            <c:set var="rep" value="${report.getItems()}" />
            <%@include file="/WEB-INF/jsp/includes/website/report.jsp" %>
        </c:if>

        <c:forEach var="page" items="${articles}">
            <c:set var="rep" value="${page}" />
            <%@include file="/WEB-INF/jsp/includes/website/report.jsp" %>
        </c:forEach>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>