<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 22-3-2018
  Time: 9:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>
<ctags:resourceImport type="css" path="css/item-select.css"/>
<ctags:resourceImport type="js" path="js/item-select.js"/>

<c:if test="${not empty selected}"><c:set var="selectedId" value="${selected.getId()}" /></c:if>

<div>
    <h1 class="light-primary">Websites</h1>
    <div class="centered margin-bottom">
        <div class="col-12 text-center p-1">
            <a href="${base_url}website/reset" class="button bgc-white dark-primary">
                <ctags:message key="manage.website.delete.all" />
            </a>
        </div>
        <div class="col-12 mb-3 mt-3 ${flexwrap} justify-content-center">

            <input name="website_import_export" id="website_import_check" type="checkbox" class="radio-check expand-check hidden" />
            <ctags:checkboxButton forid="website_import_check" text="Import" additional_label_class="bgc-light-primary no-icon sel-bgc-dark-primary" />
            <form class="col-12 black expand order-5" action="${base_url}website/import" method="POST" enctype="multipart/form-data">
                <label>
                    <span>
                        Importeer bestand
                    </span>
                    <input type="file" name="import" />
                </label>
                <ctags:input type="checkbox" value="true" labelclass="m-0" name="save_changes" title="Log aanpassingen" checked="true" />
                <ctags:input type="checkbox" value="true" labelclass="m-0" name="save_unchanged" title="Log welke pagina's niet zijn geupdate" checked="true" />
                <ctags:input type="checkbox" value="true" labelclass="m-0" name="exclude_urls" title="Update urls niet" checked="true" />
                <ctags:input type="checkbox" value="true" labelclass="m-0" name="skip_new" title="Update alleen bestaande websites" checked="true" />
                <button type="submit" class="button col-12 white bgc-light-primary">Import</button>
            </form>

            <input name="website_import_export" id="website_export_check" type="checkbox" class="radio-check expand-check hidden" />
            <ctags:checkboxButton forid="website_export_check" text="Export" additional_label_class="bgc-light-primary no-icon sel-bgc-dark-primary" />
            <form class="col-12 black expand order-5" action="${base_url}website/export" method="POST">
                <c:if test="${not empty exportOptions}">
                    <select name="export_style">
                        <c:forEach var="export" items="${exportOptions}">
                            <option value="${export.name()}">
                                ${export.getTitle()}
                            </option>
                        </c:forEach>
                    </select>
                </c:if>
                <button type="submit" class="button col-12 white bgc-light-primary">Exporteer</button>
            </form>

        </div>
      <%-- Method for importing old style xml --%>
        <%--<form action="${base_url}website/upload" class="col-12 black" method="POST" enctype='multipart/form-data'>--%>
            <%--<input type="file" name="xml" id="xml"/>--%>

            <%--<input id="website_import_radio_tags" name="update_mode" value="tags" class="expand-check hidden" type="radio" checked="checked" />--%>
            <%--<ctags:checkboxButton additional_label_class="bgc-light-primary col-12 no-icon" forid="website_import_radio_tags" text="Update tags" />--%>
            <%--<div class="expand">--%>
                <%--<label class="col-12">--%>
                    <%--<input type="checkbox" name="options[tags][]" value="include_zender" />--%>
                    <%--<span>Inclusief nieuwszenders</span>--%>
                <%--</label>--%>
                <%--<label class="col-12">--%>
                    <%--<input type="checkbox" name="options[tags][]" value="include_description" />--%>
                    <%--<span>Inclusief beschrijving</span>--%>
                <%--</label>--%>
            <%--</div>--%>

            <%--<input id="website_import_radio_update" name="update_mode" value="update" class="expand-check hidden" type="radio" />--%>
            <%--<ctags:checkboxButton additional_label_class="bgc-light-primary col-12 no-icon" forid="website_import_radio_update" text="Update xml" />--%>
            <%--<div class="expand">--%>
                <%--<label class="col-12">--%>
                    <%--<input type="checkbox" name="options[update][ignore_selectors]" checked="checked"/>--%>
                    <%--<span>Negeer selector gegevens</span>--%>
                <%--</label>--%>
                <%--<label class="col-12">--%>
                    <%--<input type="checkbox" name="options[update][ignore_existing]" checked="checked"/>--%>
                    <%--<span>Negeer Bestaande websites</span>--%>
                <%--</label>--%>
                <%--<label class="col-12">--%>
                    <%--<input type="checkbox" name="options[update][disable_updated]" checked="checked"/>--%>
                    <%--<span>Schakel aangepaste/nieuwe pagina's uit</span>--%>
                <%--</label>--%>
            <%--</div>--%>
            <%--<button type="submit" class="black">laad xml</button>--%>
        <%--</form>--%>
    </div>
    <form id="website_select_action" data-updates="#website_select_action, .website.edit > form"
          class="bordered flex ajaxable" action="${base_url}website/action" method="POST">
        <input type="hidden" name="db-filter-default" value="false" />
        <button type="submit" class="hidden" name="<%= Actions.DEFAULT %>"></button>

        <c:set var="def" value="${empty filters}" />

        <c:set var="checked" value=' checked="checked"' />
        <c:set var="filterCheck" value="${not def and not filters.containsKey('db-filter-enabled-hide')}" />
        <c:if test="${def or filterCheck}"><c:set var="checked" value="" /></c:if>
        <input type="checkbox" id="sites-db-filter-enabled-hide" name="db-filter-enabled-hide"${checked} value="true" class="hidden db-filter-enabled-hide display-filter-check validatable" data-select="div.hidden-button-container:has(.item:not(.db-disabled))" />

        <c:set var="checked" value=' checked="checked"' />
        <c:if test="${not def and not filters.containsKey('db-filter-disabled-hide')}"><c:set var="checked" value="" /></c:if>
        <input type="checkbox" id="sites-db-filter-disabled-hide" name="db-filter-disabled-hide"${checked} class="hidden db-filter-disabled-hide display-filter-check validatable" data-select="div.hidden-button-container:has(.item.db-disabled)" />

        <c:set var="checked" value=' checked="checked"'/>
        <c:if test="${not def and not filters.containsKey('db-filter-defective-hide')}"><c:set var="checked" value=""/></c:if>
        <input type="checkbox" id="sites-db-filter-defective-hide" name="db-filter-defective-hide"${checked}
               class="hidden db-filter-defective-hide display-filter-check validatable"
               data-select="div.hidden-button-container:has(.item.db-defective)"/>

        <div class="padded flex between black">
            <button type="submit" name="<%= Actions.ENABLE %>" value="<%= Selections.ALL %>" title="enable"
                    alt="Activate all non defective pages" class="button toggle"><i class="fa fa-toggle-off large"></i>Activeer
                alle niet defecte pagina's
            </button>
            <div class="centered nobreak white">
                <label for="sites-db-filter-enabled-hide" class="button db-filter-enabled-hide">Geactiveerde pagina's worden<span class="show-on-check"> niet </span> getoond</label>
                <label for="sites-db-filter-disabled-hide" class="button db-filter-disabled-hide">Uitgeschakelde pagina's worden<span class="show-on-check"> niet </span> getoond</label>
                <label for="sites-db-filter-defective-hide" class="button db-filter-defective-hide">Kapotte pagina's
                    worden<span class="show-on-check"> niet </span> getoond</label>
            </div>
            <button type="submit" name="<%= Actions.DISABLE %>" value="<%= Selections.ALL %>" title="disable"
                    alt="Deactivate all non defective pages" class="button toggle"><i class="fa fa-toggle-on large"></i>Deactiveer
                alle niet defecte pagina's
            </button>
        </div>

        <c:set var="filter_attributes" value=""/>
        <c:if test="${items.size() > 0}">
            <c:forEach var="filter" items="${items.get(0).getFilters().values()}" varStatus="filter_loop">
                <c:set var="filter_attributes"><c:if
                        test="${not filter_loop.first}">${filter_attributes}, </c:if>data-${filter.getId()}</c:set>
            </c:forEach>
        </c:if>

        <c:set var="text">
            <ctags:message key="format.filter.on" formatValue="attribute.name.display.short" />
        </c:set>

        <ctags:input name="db-filter-name" values="${filters}" inputclass="display-filter-input black"
                     data='data-filter-attribute="${filter_attributes}"' labelclass="black" title="${text}"
                     placeholder="nieuws"/>

        <c:set var="checkName"><%= Selections.SELECTION %>[]</c:set>
        <div class="item-list display-filterable">
            <c:forEach items="${items}" var="site" varStatus="loop">
                <c:set var="divClass" value="item sel-bgc-dark-primary bgc-light-primary white"/>
                <c:set var="linkClass" value='button white grower' />
                <c:set var="checked" value='' />

                <c:choose>
                    <c:when test="${site.isDefective()}">
                        <c:set var="divClass" value='${divClass} db-defective'/>
                    </c:when>
                    <c:when test="${site.isDisabled()}">
                        <c:set var="divClass" value='${divClass} db-disabled'/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="divClass" value='${divClass} db-enabled'/>
                    </c:otherwise>
                </c:choose>

                <c:if test="${site.isHealthy() == false}"><c:set var="divClass" value='${divClass} db-unhealthy' /></c:if>

                <c:if test="${not empty id_map and id_map.containsKey(site.getId())}">
                    <c:set var="checked" value=' checked="checked"' />
                    <c:set var="linkClass" value='${linkClass} inactive' />
                </c:if>

                <div class="hidden-button-container"${site.getFilterDataAttributes()}>
                    <input type="checkbox" name="${checkName}" id="site-${loop.index}" value="${site.getId()}"  class="hidden-button selected"${checked} />
                    <div class="${divClass}">
                        <label for="site-${loop.index}" class="${linkClass}">${site.getName()}</label>
                        <button type="submit" name="<%= Actions.INDEX %>" value="${site.getId()}" title="index" alt="Index page" class="button white"><i class="fa fa-search-plus large"></i></button>
                        <button type="submit" name="<%= Actions.TOGGLE %>" value="${site.getId()}" title="toggle" alt="Toggle wether or not this website is active" class="button white toggle"><i class="fa large"></i></button>
                        <button type="submit" name="<%= Actions.DETAIL %>" value="${site.getId()}" title="Health" alt="Check page health" class="button white"><i class="fa fa-medkit large"></i></button>
                        <button type="submit" name="<%= Actions.EDIT %>" value="${site.getId()}" title="Edit" alt="Edit this website" class="button white"><i class="fa fa-pencil large"></i></button>
                        <button type="submit" name="<%= Actions.COPY %>" value="${site.getId()}" title="Copy" alt="Copy into new website" class="button white"><i class="fa fa-files-o large"></i></button>
                        <button type="submit" name="<%= Actions.EMPTY %>" value="${site.getId()}" title="empty" alt="empty this website" class="button white"><i class="fa fa-trash-o large"></i></button>
                        <button type="submit" name="<%= Actions.EXPORT %>" value="${site.getId()}" title="download" alt="download this websites setting" class="no-ajax button white"><i class="fa fa-download large"></i></button>
                        <button type="submit" name="<%= Actions.IMPORT %>" value="${site.getId()}" title="import" alt="download this websites setting" class="no-ajax button white"><i class="fa fa-upload large"></i></button>

                        <c:choose>
                            <c:when test="${site.isDefective()}">
                                <button type="submit" name="<%= Actions.BREAK %>" value="${site.getId()}" title="broken"
                                        alt="mark page as not defective" class="button white"><i
                                        class="fa fa-link large"></i></button>
                            </c:when>
                            <c:otherwise>
                                <button type="submit" name="<%= Actions.BREAK %>" value="${site.getId()}" title="broken"
                                        alt="mark page as defective" class="button white"><i
                                        class="fa fa-chain-broken large"></i></button>
                            </c:otherwise>
                        </c:choose>

                        <ctags:deleteWithConfirm input_id="delete-page-${loop.index}" input_name="delete-page" input_value="true"
                                                 warning_text="Weet u zeker dat u deze pagina wilt verwijderen?"
                                                 button_name="<%= Actions.DELETE %>" button_value="${site.getId()}" />
                    </div>
                </div>
            </c:forEach>
        </div

        <div class="black">

<%--            <div class="black col-12 noJavaScript-hidden">--%>
<%--                <c:set var="selector" value="div:not(.hidden):not([style='display: none;']) > input[name='${checkName}']" />--%>
<%--                <button type="button" class="selectAll" data-select="${selector}">Selecteer alles</button>--%>
<%--                <button type="button" class="deselectAll" data-select="${selector}">Deselecteer alles</button>--%>
<%--            </div>--%>

<%--            <div class="black col-12">--%>
<%--                <button type="submit" name="<%= Actions.EMPTY %>" value="<%= Selections.SELECTION %>" title="empty" alt="Delete the articles from selected pages" class="button"><i class="fa fa-trash-o large"></i> verwijder artikellen van geselecteerde pagina's</button>--%>
<%--                <button type="submit" name="<%= Actions.DELETE %>" value="<%= Selections.SELECTION %>" title="delete" alt="Delete the selected pages" class="button"><i class="fa fa-times-circle large"></i> verwijder geselecteerde pagina's</button>--%>
<%--                <button type="submit" name="<%= Actions.ENABLE %>" value="<%= Selections.SELECTION %>" title="enable" alt="Activate the selected pages" class="button"><i class="fa fa-toggle-off large"></i>activeer geselecteerde pagina's</button>--%>
<%--                <button type="submit" name="<%= Actions.DISABLE %>" value="<%= Selections.SELECTION %>" title="disable" alt="Deactivate the selected pages" class="button"><i class="fa fa-toggle-on large"></i>deactiveer geselecteerde pagina's</button>--%>
<%--                <button type="submit" name="<%= Actions.HEALTH %>" value="<%= Selections.SELECTION %>" title="Health" alt="Generate health report for selection" class="button"><i class="fa fa-medkit large"></i>--%>
<%--                    Genereer rapport van selectie--%>
<%--                </button>--%>
<%--            </div>--%>

        </div>

    </form>
</div>
