<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>
<style>
    .toReplace {
        font-weight: bold;
        font-size: 2em;
    }
</style>
<div class="screen">
    <div class="${content_size_class} shadowed">
        <c:set var="privacy_name">
            <format:message key="privacy.name.format.name">
                <format:param>
                    <format:param>
                        <ctags:message key="format.name"/>
                    </format:param>
                </format:param>
            </format:message>
        </c:set>
        <h1 class="${page_title_class}">
            <format:message key="privacy.header.format.privacy.name">
                <format:param>
                    ${privacy_name}
                </format:param>
            </format:message>
        </h1>

        <c:set var="paragraph_class" value="col-12 mb-2"/>

        <div class="col-12">

            <div class="${paragraph_class}">
                <span class="font-weight-bold">
                    <ctags:message key="privacy.paragraph.1.header"/>
                </span>
                <format:message
                        key="privacy.paragraph.1.content.format.privacy.name.format.address.format.autoriteitpersoonsgegevens.id">
                    <format:param>
                        ${privacy_name}
                    </format:param>
                    <format:param>
                        <ctags:message key="format.address"/>
                    </format:param>
                    <format:param>
                        <ctags:message key="format.autoriteitpersoonsgegevens.id"/>
                    </format:param>
                </format:message>
            </div>

            <div class="${paragraph_class}">
                <span class="font-weight-bold">
                    <ctags:message key="privacy.paragraph.2.header"/>
                </span>
                <format:message key="privacy.paragraph.2.content.format.privacy.name">
                    <format:param>
                        ${privacy_name}
                    </format:param>
                </format:message>
            </div>

            <div class="${paragraph_class}">
                <span class="font-weight-bold">
                    <ctags:message key="privacy.paragraph.3.header"/>
                </span>
                <format:message key="privacy.paragraph.3.content.format.privacy.name">
                    <format:param>
                        ${privacy_name}
                    </format:param>
                    <format:param>
                        ${privacy_name}
                    </format:param>
                </format:message>
            </div>

            <div class="${paragraph_class}">
                <span class="font-weight-bold">
                    <ctags:message key="privacy.paragraph.4.header"/>
                </span>
                <format:message key="privacy.paragraph.4.content.format.privacy.name">
                    <format:param>
                        ${privacy_name}
                    </format:param>
                </format:message>
            </div>

            <div class="${paragraph_class}">
                <span class="font-weight-bold">
                    <ctags:message key="privacy.paragraph.5.header"/>
                </span>
                <format:message key="privacy.paragraph.5.content.format.privacy.name">
                    <format:param>
                        ${privacy_name}
                    </format:param>
                </format:message>
            </div>

            <div class="${paragraph_class}">
                <span class="font-weight-bold">
                    <ctags:message key="privacy.paragraph.6.header"/>
                </span>
                <format:message key="privacy.paragraph.6.content.format.privacy.name.format.info.mail">
                    <format:param>
                        ${privacy_name}
                    </format:param>
                    <format:param>
                        <ctags:message key="format.info.mail"/>
                    </format:param>
                </format:message>
            </div>

            <div class="${paragraph_class}">
                <span class="font-weight-bold">
                    <ctags:message key="privacy.paragraph.7.header"/>
                </span>
                <format:message key="privacy.paragraph.7.content.format.privacy.name">
                    <format:param>
                        ${privacy_name}
                    </format:param>
                </format:message>
            </div>

            <div class="${paragraph_class}">
                <span class="font-weight-bold">
                    <ctags:message key="privacy.paragraph.8.header"/>
                </span>
                <format:message key="privacy.paragraph.8.content.format.privacy.name">
                    <format:param>
                        ${privacy_name}
                    </format:param>
                </format:message>
            </div>

        </div>

    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>