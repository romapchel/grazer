<%@ page import="nl.impressie.grazer.factories.CollectionFactory" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12">
            <h1 class="${page_title_class}">
                <ctags:message key="about.header"/>
            </h1>
            <p>
                <ctags:message key="about.paragraph.1"/>
            </p>
            <p>
                <ctags:message key="about.contact.format.tel.mail" formatValues='<%= CollectionFactory.createList("contact.tel", "contact.mail", "//matchingapps.nl") %>' />
            </p>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>