<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/profile.css"/>
<ctags:resourceImport type="js" path="js/contact.js"/>

<style>
    .fith {
        max-width: 20%;
    }
</style>
<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0">
        <form action="${base_url}terms/delete" method="POST">
            Verwijder alle zoekacties die zijn uitgevoerd voor:
            <input type="date" name="until" value="${until}" />
            <button type="submit">Verwijder</button>
        </form>
        <div id="terms" class="${flexwrap}">
            <c:set var="rowClass" value="${flexwrap} col-12 justify-content-center" />
            <c:set var="halfClass" value="col-12 col-lg-6" />
            <c:set var="thirdClass" value="col-12 col-lg-4" />
            <c:set var="fithClass" value="col-12 fith" />
            <div class="${rowClass} col-lg-6">
                <div class="${rowClass}">
                    <div class="${halfClass}">
                        <h2>Zoekterm</h2>
                    </div>
                    <div class="${halfClass}">
                        <h2>Aantal keer gezocht</h2>
                    </div>
                </div>
            </div>
            <div class="${rowClass} col-lg-6">
                <div class="${rowClass} recent">
                    <div class="col-12">
                        <h2 style="text-align: center;">Meest recent</h2>
                    </div>
                    <div class="${rowClass}">
                        <div class="${thirdClass}">
                            <h3>Door</h3>
                        </div>
                        <div class="${thirdClass}">
                            <h3>Op</h3>
                        </div>
                        <div class="${thirdClass}">
                            <h3>Voor</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 ${flexwrap}">
                <c:forEach var="term" items="${terms}" varStatus="termLoop">
                    <div class="${rowClass}">
                        <div class="${fithClass}">
                                ${term.getTerm()}
                        </div>
                        <div class="${fithClass}">
                                ${term.getFrequency()}
                        </div>
                        <div class="${fithClass}">
                                ${term.getUser().getUsername()}
                        </div>
                        <div class="${fithClass}">
                                ${term.getDate("dd-MM-yyyy")}
                        </div>
                        <div class="${fithClass}">
                                ${term.getType()}
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div id="noTermsFound">
                <h3>Er zijn geen onbekende zoektermen gebruikt<h3>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>