<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<c:set var="square" value="${article.getSquareHits()}" />

<div class="h-100 article p-0">
    <c:set var="temp_article_url" value="${article.getUrl()}" />
    <div class="h-100 d-flex flex-nowrap">
        <div class="p-0 col text ${flexwrap}">
            <div class="col-12 p-0 ${flexwrap}">
                <span class="d-block col-12 bgc-light-primary align-self-start white">${square[1]}</span>
                <div class="col-12 pt-3">
                    <div>
                        <p class="date col-12 p-0 white">${article.getIndexedOn("dd MM yyyy")}</p>
                        <a href="${temp_article_url}" class="button p-0 col-12 text-white hover-white" target="_blank">
                            <p class="title col-12 p-0">${article.getTitle()}</p>
                            <p class="description black col-12 p-0">
                                ${fn:escapeXml(article.getSnippet())}
                            </p>
                        </a>
                        <div class="tags small text-white col-12 p-0">
                            <div class="recht">
                                <span>
                                    <c:set var="id" value="<%= Tag.RECHTSGEBIED %>"/>
                                    <ctags:message key="article.${id}.display"/>
                                </span>
                                <c:forEach var="family" items="${cfn:getTagFamilies(article.getRechtsgebiedTags())}" varStatus="parent_loop">
                                    <span>${family.getParent().getName()}</span><c:if test="${not parent_loop.last}">,</c:if>
                                    <c:set var="children" value="${family.getChildren()}" />
                                    <c:if test="${children.size() > 0}">
                                        <c:set var="info" value="" />
                                        <c:forEach var="tag" items="${children}" varStatus="loop">
                                            <c:set var="info">${info}<c:if test="${not loop.first}">, </c:if><span>${tag.getName()}</span></c:set>
                                        </c:forEach>
                                        <ctags:info message="${info}" iconClasses="d-inline-block small" mouseoverClasses="left-0" />
                                    </c:if>
                                </c:forEach>
                            </div>
                            <p class="nieuws">
                                <span>
                                    <c:set var="id" value="<%= Tag.NIEUWSSOORT %>"/>
                                    <ctags:message key="article.${id}.display"/>
                                </span>
                                <c:forEach var="tag" items="${article.getNieuwsSoortTags()}" varStatus="loop">
                                    <span>${tag.getName()}</span><c:if test="${not loop.last}">,</c:if>
                                </c:forEach>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <ctags:socialMediaShare url="${temp_article_url}" />
        </div>
        <a href="${temp_article_url}" class="button p-0" target="_blank">
            <div class="vertically-centered bgc-dark-primary arrow white">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </div>
        </a>
    </div>
</div>