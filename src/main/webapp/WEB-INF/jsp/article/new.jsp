<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen faq-page">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0">
            <h1>Test</h1>
            <c:forEach var="gebied" items="${gebieden}">
                <div>
                    <a href="<c:url value="${gebied.url}" />">
                        ${gebied.title} (${gebied.hits})
                    </a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>