<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2019-02-14
  Time: 15:29
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0 ${flexwrap}">
            <h1 class="${page_title_class}">
                <ctags:message key="user.subscription.header" />
            </h1>

            <%@include file="/WEB-INF/jsp/user/parts/subscription.jsp" %>

        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>