<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2019-02-14
  Time: 15:29
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-6 col-md-8 p-0">
    <h2 class="light-primary mobile-h2">
        <ctags:message key="user.subscription.active.header" />
    </h2>
    <c:choose>
        <c:when test="${not empty active}">
            <c:forEach var="sub" items="${active}">
                <c:set var="status">
                    <ctags:message key="mollie.status.${sub.getStatus()}" />
                </c:set>
                <div class="col-12 ${flexwrap}">
                    <span class="col-12 col-md-6"><ctags:message key="user.subscription.payment.status" />:</span>
                    <span class="col-12 col-md-6">${status}</span>
                </div>
                <div class="col-12 ${flexwrap}">
                    <span class="col-12 col-md-6"><ctags:message key="user.subscription.payment.type" />:</span>
                    <span class="col-12 col-md-6">${sub.getDescription()}</span>
                </div>
                <div class="col-12 ${flexwrap}">
                    <c:set var="times" value="${sub.getTimes()}" />
                    <c:choose>
                        <c:when test="${empty times}">
                            <c:set var="times" value="Automatisch vernieuwend" />
                        </c:when>
                        <c:otherwise>
                            <span class="col-12 col-md-6">Herhaald:</span>
                        </c:otherwise>
                    </c:choose>
                    <span class="col-12 col-md-6">${times}</span>
                </div>
                <div class="col-12 ${flexwrap}">
                    <span class="col-12 col-md-6">Gestart op:</span>
                    <span class="col-12 col-md-6">
                        ${DateUtil.convertDateToFormat(sub.getStartDate(), "dd MM yyyy")}
                    </span>
                </div>
                <a href="${base_url}${AccountController.USER_SUBSCRIPTION_CANCEL_URL}/${sub.getId()}">
                    Abonnement annuleren
                </a>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <p>
                Er is geen actief Abonnement.
            </p>
            <p>
                <c:set var="enddate" value='${accountType.getEndDate("dd MM yyyy")}' />
                <c:choose>
                    <c:when test='${enddate == "0"}'>
                        Dit account is momenteel niet actief
                    </c:when>
                    <c:otherwise>
                        De huidige inschrijving is geldig tot: ${enddate}
                    </c:otherwise>
                </c:choose>
            </p>
        </c:otherwise>
    </c:choose>
</div>

<div class="col-6 col-md-4 p-0">
    <h2 class="light-primary mobile-h2">
        <ctags:message key="user.subscription.history.header" />
    </h2>
    <div class="col-12" style="max-height: 45em; overflow: auto;">
        <c:forEach var="invoice" items="${invoices}">
            <div class="col-12">
                <a href="${base_url}mollie/invoice/download?id=${invoice.getId()}">
                    ${invoice.getDate()}: ${invoice.getDescription()}
                </a>
            </div>
        </c:forEach>
    </div>
</div>