<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2019-03-26
  Time: 14:08
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0">
            <h1 class="${page_title_class}">
                <ctags:message key="verified.header"/>
            </h1>
            <div class="text-center">
                <c:choose>
                    <c:when test="${succes}">
                        <ctags:message key="verified.messages.succes.format.link" link="${base_url}mollie/paywall" />
                    </c:when>
                    <c:otherwise>
                        <ctags:message key="verified.messages.failure" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>