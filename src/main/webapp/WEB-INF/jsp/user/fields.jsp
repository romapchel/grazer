<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2019-03-12
  Time: 13:44
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cfn" uri="/WEB-INF/jstl-functions.tld" %>
<%@ taglib prefix="ctags" tagdir="/WEB-INF/tags" %>

<%@ page import="nl.impressie.grazer.dao.UserDao" %>
<%@ page import="nl.impressie.grazer.model.FieldMap" %>

<c:set var="fields" value="${cfn:emptyMap()}" scope="request" />

<c:set var="key" value="${UserDao.LASTNAME}" />
<c:set var="title" ><ctags:message key="user.input.name.title"/></c:set>
<c:set var="tooltip"><ctags:message key="tooltip.required" /></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.USERNAME}" />
<c:set var="title" ><ctags:message key="register.input.username.title"/></c:set>
<c:set var="tooltip">
    <ctags:message key="tooltip.required" />
    <ctags:message key="tooltip.format.minimum" formatValue="3" />
</c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                .put(FieldMap.Attribute.REQUIRED, true)
                .put(FieldMap.Attribute.AUTOSELECT, true)
                .put(FieldMap.Attribute.AUTOFOCUS, true)
                .put(FieldMap.Attribute.VALIDATEABLE, true)
                .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.PASSWORD}" />
<c:set var="title" ><ctags:message key="register.input.password.title"/></c:set>
<c:set var="tooltip">
    <ctags:message key="tooltip.required" />
    <ctags:message key="tooltip.format.minimum" formatValue="5" />
</c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.TYPE, "password")
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.PASSWORD}-repeat" />
<c:set var="title" ><ctags:message key="register.input.password.repeat.title"/></c:set>
<c:set var="tooltip">
    <ctags:message key="tooltip.format.repeat" formatValue="register.input.password.title" />
</c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.TYPE, "password")
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.ORGANISATION}" />
<c:set var="title" ><ctags:message key="user.input.organisation.title"/></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.INITIALS}" />
<c:set var="title" ><ctags:message key="user.input.initials.title"/></c:set>
<c:set var="tooltip"><ctags:message key="tooltip.required" /></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.TUSSENVOEGSELS}" />
<c:set var="title" ><ctags:message key="user.input.prefix.title"/></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.LASTNAME}" />
<c:set var="title" ><ctags:message key="user.input.name.title"/></c:set>
<c:set var="tooltip"><ctags:message key="tooltip.required" /></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.STREET}" />
<c:set var="title" ><ctags:message key="user.input.street.title"/></c:set>
<c:set var="tooltip"><ctags:message key="tooltip.required" /></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.HOUSENUMBER}" />
<c:set var="title" ><ctags:message key="user.input.housenumber.title"/></c:set>
<c:set var="tooltip"><ctags:message key="tooltip.required" /></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.ZIP}" />
<c:set var="title" ><ctags:message key="user.input.zip.title"/></c:set>
<c:set var="tooltip"><ctags:message key="tooltip.required" /></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.CITY}" />
<c:set var="title" ><ctags:message key="user.input.city.title"/></c:set>
<c:set var="tooltip"><ctags:message key="tooltip.required" /></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />

<c:set var="key" value="${UserDao.MAIL}" />
<c:set var="title" ><ctags:message key="user.input.mail.title"/></c:set>
<c:set var="tooltip"><ctags:message key="tooltip.required" /></c:set>
<%
    pageContext.setAttribute("temp",
            FieldMap.create()
                    .put(FieldMap.Attribute.NAME, pageContext.getAttribute("key"))
                    .put(FieldMap.Attribute.TITLE, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.PLACEHOLDER, pageContext.getAttribute("title"))
                    .put(FieldMap.Attribute.TOOLTIP, pageContext.getAttribute("tooltip"))
                    .put(FieldMap.Attribute.REQUIRED, true)
                    .put(FieldMap.Attribute.AUTOSELECT, true)
                    .put(FieldMap.Attribute.VALIDATEABLE, true)
                    .put(FieldMap.Attribute.ERRORS, pageContext.getAttribute("errors"))
                    .put(FieldMap.Attribute.VALUES, pageContext.getAttribute("values"))
                    .put(FieldMap.Attribute.TYPE, "email")
                    .put(FieldMap.Attribute.REQUIREDINFOSHOW, true)
    );
%>
<c:set target="${fields}" property="${key}" value="${temp}" />