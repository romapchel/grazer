<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2019-02-14
  Time: 15:29
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0">
            <h1 class="${page_title_class}">
                Invoices
            </h1>
            <c:forEach var="invoice" items="${invoices}">
                <div class="col-12">
                    <h3>
                        ${invoice.getDate()}
                    </h3>
                    <span>Klik</span>
                    <a href="${base_url}mollie/invoice/download?id=${invoice.getId()}">hier</a>
                    om uw factuur te downloaden
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>