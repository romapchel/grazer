<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2020-01-07
  Time: 15:43
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="demo" class="vertically-centered" style="border: 0;">
    <div class="">
        <div class="">
            <h1 class="caps dark-primary col-12">
                <ctags:message key="home.demo.header"/>
            </h1>
            <div class="bgc-white col-12 p-0">
                <div class="col-12 text centered">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/TvnYmWpD_T8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
