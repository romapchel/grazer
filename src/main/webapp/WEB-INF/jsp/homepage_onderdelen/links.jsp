<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2020-01-07
  Time: 15:43
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="links" class="screen vertically-centered" style="border: 0; padding: 10px;">
    <div class="${container_class}">
        <div class="${content_size_class} shadowed">
            <div class="bgc-white col-12 p-0">
                <c:set var="rowClass" value="row-12 p-0 pt-2 pb-2 text-center" />
                <div class="${rowClass}">
                    <a href="#wat">
                        <ctags:message key="home.wat.header" />
                    </a>
                </div>
                <div class="${rowClass}">
                    <a href="#waarom">
                        <ctags:message key="home.waarom.header" />
                    </a>
                </div>
                <div class="${rowClass}">
                    <a href="#demo">
                        <ctags:message key="home.demo.header" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
