<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2020-01-07
  Time: 15:43
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="waarom" class="screen vertically-centered" style="border: 0; padding: 10px;">
    <div class="${container_class}">
        <div class="${content_size_class} shadowed overflow-hidden">
            <div class="bgc-white col-12 p-0 col-lg vertically-centered">
                <h1 class="caps dark-primary col-12">
                    <ctags:message key="home.waarom.header" />
                </h1>
            </div>
            <div class="bgc-white col-12 p-0">
                <div class="col-12 text centered">
                    <input id="home_waarom_content_short_long" type="checkbox" class="hidden content_short_long" />
                    <div class="content_short">
                        <ctags:message key="home.waarom.content.short"/>
                        <ctags:checkboxButton forid="home_waarom_content_short_long" text="Toon meer" additional_label_class="short_long_button bgc-overide bgc-light-primary rounded col-12" />
                    </div>
                    <div class="content_long">
                        <ctags:message key="home.waarom.content"/>
                        <ctags:checkboxButton forid="home_waarom_content_short_long" text="Toon minder" additional_label_class="short_long_button bgc-overide bgc-light-primary rounded col-12 reverse-icons" />
                    </div>
                </div>
            </div>
            <div class="legal-grazer-page p-0 example col-12 mt-1">
                <div class="col-12 p-0 text-center">
                    <span>
                        <ctags:message key="home.description.example.format.name" formateValue="home.header.grazer.legal.nl" />
                    </span>
                </div>
                <div>
                    <c:if test="${not empty legal_article}">
                        <c:set var="article" value="${legal_article}"/>
                        <%@include file="/WEB-INF/jsp/article/view.jsp" %>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
