<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen bgc-grey">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">

        <c:forEach var="tag" items="${tags}" varStatus="loop">
            <div class="col-12">
                <div class="col-12 col-sm-6">
                    ${tag.getTag()}
                </div>
                <div class="col-12 col-sm-6">
                    ${tag.getUrl()}
                </div>
            </div>
        </c:forEach>

    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>