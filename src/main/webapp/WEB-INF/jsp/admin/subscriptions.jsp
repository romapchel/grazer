<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0 ${flexwrap}">
            <c:set var="prevDate" value="" />
            <c:forEach var="invoice" items="${invoices}">
                <c:if test="${prevDate != invoice.getDate()}">
                    <h2>
                        <c:set var="prevDate" value="${invoice.getDate()}" />
                        ${prevDate}
                    </h2>
                </c:if>
                <a class="col-12 button text-center white bgc-light-primary" href="${base_url}mollie/invoice/download?id=${invoice.getId()}">
                    ${invoice.getUser().getUsername()}
                </a>
            </c:forEach>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>