<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0 ${flexwrap}">
            <c:set var="linkClass" value="col-12 text-center mb-1 button white bgc-light-primary" />
            <a class="${linkClass}" href="${base_url}${AdminController.ADMIN_WEBSITE_URL}">Manage websites</a>
            <a class="${linkClass}" href="${base_url}${AdminController.ADMIN_USER_URL}">Manage users</a>
            <a class="${linkClass}" href="${base_url}${AdminController.ADMIN_TAG_URL}">Manage tags</a>
            <a class="${linkClass}" href="${base_url}${AdminController.ADMIN_UNKNOWN_TAG_URL}">View found tags</a>
            <a class="${linkClass}" href="${base_url}${AdminController.ADMIN_BUNDLE_URL}">Manage bundles</a>
            <a class="${linkClass}" href="${base_url}${AdminController.ADMIN_INDEX_URL}">Manage index</a>
            <a class="${linkClass}" href="${base_url}${AdminController.ADMIN_SUBSCRIPTION_VIEW_URL}">View recent purchases</a>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>