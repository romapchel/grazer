<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0">
            <h1 class="${page_title_class}">
                <ctags:message key="registered.header" />
            </h1>
            <div class="col-12">
                <c:if test="${not empty user}">
                    <c:set var="verification" value="${user.getVerification()}" />
                    <c:if test="${not empty verification}">
                        <div class="mb-3">
                            <ctags:message key="registered.verification.send.format.mail" mail="${user.getMail()}" />
                        </div>
                    </c:if>
                    <div class="mb-3">
                        <ctags:message key="registered.verification.send.inform" />
                    </div>
                    <div class="mb-3">
                        <ctags:message key="registered.verification.new.format.link" link="${base_url}verify/resend" />
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>