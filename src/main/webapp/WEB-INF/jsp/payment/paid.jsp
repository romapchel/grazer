<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen vertically-centered" style="display:block";>
    <div class="content col-12 col-lg-6 shadowed bgc-dark-primary white strongly-rounded centered" style="margin-left:auto;margin-right:auto;margin-top:50px;">
        <h1 class="${page_title_class} text-white">
            <c:choose>
                <c:when test="${alreadyCompleted || succesful}">
                    <ctags:message key="paid.header.format.name" formatValue="${user.username}" />
                </c:when>
                <c:otherwise>
                    <ctags:message key="paid.message.fail.status" />
                </c:otherwise>
            </c:choose>
        </h1>
        <c:choose>
            <c:when test="${alreadyCompleted || succesful}">
                <div class="col-12 p-0 ${flexwrap}">
                    <c:set var="container" value="col-12 ${flexwrap}" />
                    <c:set var="link_container" value="col-12 bgc-dark-primary text-center p-0 pr-1 align-items-center ${flexwrap}" />
                    <c:set var="link" value="col button white" />
                    <div class="${container}">
                        <div class="${link_container}">
                            <a href="${base_url}profile/create" class="${link}">
                                <ctags:message key="paid.configure.button" />
                            </a>
                            <div class="mouse_overs">
                                <ctags:info message="paid.configure.tooltip" iconClasses="white" mouseoverClasses="bgc-dark-primary white" />
                            </div>
                        </div>
                    </div>
                    <c:if test="${not empty invoiceId}">
                        <div class="${container}">
                            <div class="${link_container}">
                                <a target="_blank" href="${base_url}mollie/invoice/download?id=${invoiceId}" class="${link}">
                                    <ctags:message key="paid.invoice.button" />
                                </a>
                            </div>
                        </div>
                    </c:if>
                    <div class="${container}">
                        <div class="${link_container}">
                            <a href="${base_url}search" class="${link}">
                                <ctags:message key="paid.search.button" />
                            </a>
                            <div class="mouse_overs">
                                <ctags:info message="paid.search.tooltip" iconClasses="white" mouseoverClasses="bgc-dark-primary white" />
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${failed}">
                <p>
                    <ctags:message key="paid.message.fail.contact.format.link" formatValue="${base_url}contact" />
                </p>
            </c:when>
            <c:otherwise>
                <p>
                    De betaling is nog in behandeling.
                </p><p>
                    U krijgt toegang tot de applicatie zodra de betaling succesvol is uitgevoerd.
                </p>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>