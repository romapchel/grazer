<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0">
            <c:forEach items="${bundles}" var="bundle">
                <a href="${base_url}${AdminController.ADMIN_BUNDLE_URL}/${bundle.id}" class="mb-1 d-block col-12 button white bgc-light-primary">
                    ${bundle.description}
                </a>
            </c:forEach>
            <form method="POST">
                <h2>
                    Nieuwe bundel
                </h2>
                <ctags:input name="${BundleDao.DESCRIPTION}" type="textarea" title="Beschrijving" />
                <button type="submit" class="col-12 bgc-light-primary white button">Nieuwe bundel aanmaken</button>
            </form>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>