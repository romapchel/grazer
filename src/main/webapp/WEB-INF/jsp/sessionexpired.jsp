<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/search.css"/>
<ctags:resourceImport type="css" path="css/home.css"/>

        <div class="screen vertically-centered">
            <div class="content col-sm-8 col-md-6 centered">
                <h1>Sessie verlopen</h1>
                <div>
                    <p>
                        Uw sessie is verlopen. Log opnieuw in.
                    </p>
                </div>
                <br />
            </div>
        </div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>