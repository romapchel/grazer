<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen bgc-grey">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">
        <%@include file="/WEB-INF/jsp/includes/account/selectView.jsp" %>
    </div>
</div>

<div class="screen bgc-light-primary">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">
        <%@include file="/WEB-INF/jsp/includes/account/editView.jsp" %>
    </div>
    <div class="white">
        <div class="fixed">
            <div class="margin-bottom">
                <label for="account-submit-button" class="button bordered">Wijzigingen opslaan</label>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>