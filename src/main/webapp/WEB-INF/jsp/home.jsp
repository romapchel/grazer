<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/search.css"/>
<ctags:resourceImport type="css" path="css/home.css"/>


<div class="screen">
    <div class="${content_size_class} p-0">
        <div class="col-12">

            <h1 class="${page_title_class}">
                GRAZER
            </h1>

<p>
    <span style="color:rgb(107, 156, 52); font-weight:bold;">Wat is Grazer?</span><br/>
    Grazer® is een programma waarmee publieke websites worden doorzocht op nieuws. Grazer® graast voor u het internet af aan de hand van door u zelf ingestelde interesseprofielen. De gevonden nieuwsberichten worden overzichtelijk gepresenteerd. U hoeft dus niet zélf alle relevante websites te bezoeken om op de hoogte te blijven van het voor u relevante nieuws. U bespaart zo niet alleen veel tijd, maar zorgt er ook voor dat uw vakkennis up-to-date blijft, wat tegenwoordig niet eenvoudig is door het enorme aanbod van informatie.
</p>
<ul>
    <li>Nieuwsgaring voor professionals</li>
    <li>Instellen van één of meerdere interesseprofielen mogelijk</li>
    <li>Doorzoekt alleen relevante nieuwszenders</li>
    <li>Interesseprofielen en gevonden nieuws zijn deelbaar</li>
    <li>Gevonden nieuws wordt overzichtelijke gepresenteerd</li>
    <li>Tijdbesparend</li>
    <li>Up-to-date</li>
</ul>

<p>
    <span style="color:rgb(107, 156, 52); font-weight:bold;">Wat zijn interesseprofielen?</span><br/>
     U kunt aan de hand van de beschikbare rubrieken kiezen welke nieuwsberichten u gepresenteerd krijgt. U kunt deze keuzes vastleggen in één of meerdere interesseprofielen. Door een interesseprofiel aan te klikken, krijgt u de daarbijhorende keuze van nieuwsberichten gepresenteerd. Alleen een voor u relevante selectie van nieuwszenders wordt bezocht. Uw interesseprofielen kunt u via uw computer delen met uw collega’s met gelijke interesses.
    Standaard kunt u 5 profielen gebruiken.
</p>

<p>
    <span style="color:rgb(107, 156, 52); font-weight:bold;">Kan ik het gevonden nieuws delen?</span><br/>
    Het gevonden nieuws is deelbaar via Twitter, LinkedIn of Facebook, indien u een account heeft voor de genoemde sociale media.
</p>

<p>
    <span style="color:rgb(107, 156, 52); font-weight:bold;">Is Grazer ook te integreren?</span><br/>
    Grazer® kan als zelfstandig product worden gebruikt of geïntegreerd in een ander informatievoorzieningssysteem; zoals een intranet of website. Indien u interesse heeft in de mogelijkheden van Grazer®, neem contact op met ons.
</p>

<p>
    Een voorbeeld van een zelfstandig product op basis van de technologie van Grazer is: <i>Grazer® Legal NL</i>. Klik op het logo en zie wat <i>Grazer® Legal NL</i> u te bieden heeft.
</p>

<p>
    <a href="<c:url value="/search" />">
        <img src="<c:url value="/img/logos/grazer_legal_logo_origineel.jpg"/> " style="display:block;max-height:100px;" />
    </a>
</p>


        </div>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>