<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Roma
  Date: 25.07.2022
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<img src="<c:url value="/img/logos/legal-logo.svg"/>" >

<c:forEach items="${articlesList}" var="articel">
    ${articel.getTitle()}
</c:forEach>

</body>
</html>
