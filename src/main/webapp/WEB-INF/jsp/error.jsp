<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/search.css"/>
<ctags:resourceImport type="css" path="css/home.css"/>

        <div class="screen vertically-centered">
            <div class="content col-sm-8 col-md-6 shadowed centered">
                <h1>Error</h1>
                <div>
                    <p>
                        <span class="text-danger">${exception}</span> <span>occured on</span> <span>${date}</span> <span>while trying to load</span> <span>${url}</span>
                    </p>
                    <p>
                        Probeer het later opnieuw of neem <a href="${base_url}contact">contact</a> op met ons als deze fout blijft voorkomen.
                    </p>
                </div>
                <br />
            </div>
        </div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>