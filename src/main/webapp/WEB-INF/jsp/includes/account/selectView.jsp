<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 22-3-2018
  Time: 9:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<ctags:resourceImport type="css" path="css/item-select.css"/>
<ctags:resourceImport type="js" path="js/item-select.js"/>

<c:if test="${not empty selected}"><c:set var="selectedId" value="${selected.getId()}" /></c:if>

<div>
    <h1 class="light-primary">Gebruikers</h1>
    <div class="${flexwrap} col-12 p-0 dark-primary justify-content-around">
        <a href="${base_url}account/reset" class="col-12 col-lg-5 button bgc-white centered mb-2">Verwijder alle gebruikers</a>
        <a href="${base_url}${AdminController.ADMIN_USER_URL}?id=-1" class="col-12 col-lg-5 button bgc-white centered mb-2">Nieuwe gebruiker toevoegen</a>
    </div>
    <div class="bordered flex">
        <c:set var="text">
            <ctags:message key="format.filter.on" formatValue="attribute.name.display.short" />
        </c:set>
        <ctags:input name="" inputclass="display-filter-input black" labelclass="black" title="${text}" placeholder="gebruiker" />
        <div class="item-list display-filterable">
            <c:forEach items="${items}" var="item">
            <c:set var="divClass" value="item sel-bgc-dark-primary bgc-light-primary white"/>
                <c:set var="linkElement" value='a' />
                <c:set var="linkUrl" value='href="?id=${item.getId()}" ' />
                <c:set var="linkClass" value='button white grower' />
                <c:if test="${not empty selectedId and selectedId eq item.getId()}">
                    <c:set var="divClass" value="${divClass} selected" />
                    <c:set var="linkElement" value='span' />
                    <c:set var="linkUrl" value='' />
                    <c:set var="linkClass" value='${linkClass} inactive' />
                </c:if>

                <div class="${divClass}">
                    <${linkElement} ${linkUrl}class="${linkClass}">${item.getUsername()}</${linkElement}>
                    <a href="${base_url}${AdminController.ADMIN_USER_URL}?id=${item.getId()}" title="Edit" alt="Edit this user" class="button white"><i class="fa fa-pencil large"></i></a>
                    <a href="${base_url}account/delete?id=${item.getId()}" title="delete" alt="Delete this user" class="button white"><i class="fa fa-trash-o large"></i></a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
