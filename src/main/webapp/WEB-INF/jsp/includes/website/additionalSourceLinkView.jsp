<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 5-7-2018
  Time: 10:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<%@ page import="nl.impressie.grazer.dao.*" %>

<c:set var="temp_type_name" value="<%= Types.ADDITIONAL_SOURCE_LINK %>"/>
<c:set var="uid" value="${temp_type_name}_${index}_"/>

<c:set var="asl_current" value=""/>
<c:set var="asl_id" value=""/>
<c:if test="${not empty item}">
    <c:set var="asl_current" value="${item.getCurrent(uid)}"/>
    <c:set var="asl_id" value="${item.getId()}"/>
</c:if>

<div class="additional_source_link grouping">
    <c:set var="name" value="<%= AdditionalSourceLinkDao.ID %>"/>
    <ctags:input name="${uid}${name}" type="hidden" value="${asl_id}"/>

    <c:set var="name" value="<%= AdditionalSourceLinkDao.URL %>"/>
    <ctags:input name="${uid}${name}" title="With this text" placeholder="www.page.nl/page2" values="${asl_current}"
                 errors="${errors}" onInputFocus="$(this).select();"/>
</div>