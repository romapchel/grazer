<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 21-3-2018
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="nl.impressie.grazer.dao.WebsiteDao" %>
<%@ include file="/WEB-INF/jsp/includes/libraries.jsp" %>
<div class="health">
    <c:set var="health" value="${report.getHealth()}" />
    <h2>${page.getName()}</h2>
    <div>
        <span>last checked on:</span> <span>${health.get('checkedOn')}</span>
    </div>
    <c:set var="checked" value="${health.get('checked')}" />
    <c:if test="${empty checked}"><c:set var="checked" value="0" /></c:if>
    <c:set var="total" value="${health.get('total')}" />
    <c:if test="${empty total}"><c:set var="total" value="0" /></c:if>
    <c:set var="healthy" value="${health.get('healthy')}" />
    <c:if test="${empty healthy}"><c:set var="healthy" value="0" /></c:if>
    <div>
        <c:choose>
            <c:when test="${total == 0}">
                <c:set var="percentage" value="0" />
            </c:when>
            <c:otherwise>
                <c:set var="percentage" value="${checked / total}" />
            </c:otherwise>
        </c:choose>
        <span>Number of articles checked:</span> <span>${checked}</span>/<span>${total}</span> (<span><fmt:formatNumber value="${percentage}" type="percent" maxFractionDigits="2" /></span>)
        <a href="${base_url}website/health?id=${page.getId()}&all=true">Check all articles</a>
    </div>
    <div>
        <c:choose>
            <c:when test="${checked == 0}">
                <c:set var="percentage" value="0" />
            </c:when>
            <c:otherwise>
                <c:set var="percentage" value="${healthy / checked}" />
            </c:otherwise>
        </c:choose>
        <span>Number of healthy articles</span> <span>${healthy}</span>/<span>${checked}</span> (<span><fmt:formatNumber value="${percentage}" type="percent" maxFractionDigits="2" /></span>)
    </div>
    <c:if test="${health.get('before').size() > 0}">
        <div>
            <h4>Not checked:</h4>
            <div>
                <c:forEach var="beforeCode" items="${health.get('before').keySet()}">
                    <div>${health.get('titles').get(beforeCode)}</div>
                </c:forEach>
            </div>
        </div>
    </c:if>
    <div>
        <c:if test="${checked != 0}">
            <h4>Individual:</h4>
            <div>
                <c:set var="key" value="<%= WebsiteDao.TITLE %>" />
                <%@include file="/WEB-INF/jsp/includes/website/healthBar.jsp" %>

                <c:set var="key" value="<%= WebsiteDao.DATE %>" />
                <%@include file="/WEB-INF/jsp/includes/website/healthBar.jsp" %>

                <c:set var="key" value="<%= WebsiteDao.CONTENT %>" />
                <%@include file="/WEB-INF/jsp/includes/website/healthBar.jsp" %>
             </div>
        </c:if>
    </div>
    <c:if test="${health.get('unhealthy').size() > 0}">
        <div>
            <h4>Unhealthy:</h4>
            <c:forEach var="article" items="${health.get('unhealthy')}">
                <p>
                    <a href="${article.get('url')}">
                        ${article.get('name')}
                    </a>
                </p>
            </c:forEach>
        </div>
    </c:if>
</div>