<%--<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>--%>
<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 21-3-2018
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>
<c:if test="${health.containsKey(key)}">
    <div>
        <span>${health.get("titles").get(key)}</span>
        <span>
            <c:set var="healthy">${health.get(key).get("healthy")}</c:set>
            <c:set var="total">${health.get(key).get("total")}</c:set>
            <span class="healthy">${healthy}</span>/<span class="total">${total}</span>
            (<span class="percentage"><fmt:formatNumber value="${healthy / total}" type="percent" maxFractionDigits="2" /></span>)
        </span>
    </div>
</c:if>