<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<div class="website report">
    <h2>${rep.get('pagetitle')}</h2>
    <c:if test="${rep.containsKey('checkedtotal')}">
        <c:set var="item" value="${rep.get('checkedtotal')}" />
        <%@include file="/WEB-INF/jsp/includes/reportItemView.jsp" %>
    </c:if>
    <c:if test="${rep.containsKey('healthtotal')}">
        <c:set var="item" value="${rep.get('healthtotal')}" />
        <%@include file="/WEB-INF/jsp/includes/reportItemView.jsp" %>
    </c:if>
    <c:forEach var="item" items="${rep.get('items').values()}">
        <%@include file="/WEB-INF/jsp/includes/reportItemView.jsp" %>
    </c:forEach>
</div>