<%--Libraries--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="cfn" uri="/WEB-INF/jstl-functions.tld"%>
<%@taglib prefix="ctags" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<%@ page import="java.util.Map" %>

<%@ page import="nl.impressie.grazer.model.DB.Tag" %>
<%@ page import="nl.impressie.grazer.model.Crawler.Article" %>
<%@ page import="nl.impressie.grazer.dao.TagDao" %>
<%@ page import="nl.impressie.grazer.dao.WebsiteDao" %>
<%@ page import="nl.impressie.grazer.dao.BundleDao" %>
<%@ page import="nl.impressie.grazer.model.DB.Website.Types" %>
<%@ page import="nl.impressie.grazer.dao.ProfileDao" %>
<%@ page import="nl.impressie.grazer.dao.UserDao" %>
<%@ page import="nl.impressie.grazer.model.User.Role" %>

<%@ page import="nl.impressie.grazer.controller.Controller.Selections" %>
<%@ page import="nl.impressie.grazer.controller.Controller.Actions" %>
<%@ page import="nl.impressie.grazer.controller.AdminController" %>
<%@ page import="nl.impressie.grazer.controller.AccountController" %>
<%@ page import="nl.impressie.grazer.controller.WebsiteController" %>
<%@ page import="nl.impressie.grazer.controller.TagController" %>

<c:url var="base_url"  value="/" scope="request" />

<c:if test="${empty logged_in_user}">
    <c:set var="logged_in_user" value="${cfn:getCurrentUser()}" />
</c:if>

<%-- Javascript --%>
<ctags:resourceImport type="js" path="js/libs/jquery-3.1.0.min.js" />
<ctags:resourceImport type="js" path="js/libs/js.cookie.js" />
<ctags:resourceImport type="js" path="js/default.js" />
<ctags:resourceImport type="js" path="js/validation.js" />
<ctags:resourceImport type="js" path="js/libs/bootstrap-select-1.5.4.js"/>
<ctags:resourceImport type="js" path="css/bootstrap-4.4.1/js/bootstrap.js" />
<%-- CSS --%>
<ctags:resourceImport type="css" path="css/font-awesome-4.7.0/css/font-awesome.min.css" />
<ctags:resourceImport type="css" path="css/bootstrap-4.4.1/css/bootstrap.min.css" />
<ctags:resourceImport type="css" path="css/default.css" />
<ctags:resourceImport type="css" path="css/colors.css"/>
<ctags:resourceImport type="css" path="css/menu.css" />
<ctags:resourceImport type="css" path="css/responsive.css"/>