<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<c:if test="${empty sub_page}">
    <c:set var="sub_page" value="default"/>
</c:if>

<c:set var="flexwrap" value="d-flex flex-wrap" scope="request" />
<c:set var="page_title_class" value="page_title col-12 light-primary" scope="request" />
<c:set var="container_class" value="col-12 d-flex p-0 justify-content-center" scope="request" />
<c:set var="content_size_class" value="content col-12 col-lg-10 col-xl-8 ${flexwrap}" scope="request" />
<c:url var="current_url" value="${requestScope['javax.servlet.forward.request_uri']}" />

<html class="noJavaScript ${sub_page}-grazer-page">
<head>
    <%--favicon--%>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta name="robots" content="Index, Follow" />
    <title>Grazer</title>

    <script>
        window.base_url = "${base_url}";
        $(document).ready(function(){
            $('#mobileMenu').on('click', function(e) {
                if(e.target !== this)
                    return;
                $('#hamburger').click();
            });
            $('#startIndex').on('click', function(){
                var url = base_url + "index/ajax/start";
                var data = [];
                var succes = function(data)
                {
                    log(data);
                };

                ajaxCall(url, data, succes);
                setMessage("The crawler has started running", "info");

                return false;
            });
            $('.selectpicker').selectpicker({
                style: 'btn-info',
                size: 4
            });
        });
    </script>
    <div id="javascript_errors" style="display: none;">
        <div id="required">
            <ctags:message key="error.tooltip.required"/>
        </div>
        <div id="match">
            <ctags:message key="error.tooltip.match"/>
        </div>
        <div id="minimum">
            <ctags:message key="error.tooltip.minimum"/>
        </div>
    </div>
</head>
<body class="${flexwrap}">
<div id="fb-root"></div>

<input id="mobileMenuCheckbox" type="checkbox" class="hidden" />
<div id="menuBar" class="clearfix centered bgc-dark-primary floating">
    <div class="col-3" style="z-index: 20;">
        <label id="hamburger" for="mobileMenuCheckbox">
            <img src="${base_url}img/IconImages/icon-header-menu-hamburger.svg" />
        </label>
    </div>
    <div class="logo-container col-3">
        <div id="floating-logo" class="p-2">
            <a href="#fb-root" class="white">Back to top</a>
        </div>
        <div id="logo">
            <a href="${base_url}search">
                <img src="${base_url}img/logos/${sub_page}-logo.svg"/>
            </a>
        </div>
    </div>
    <div id="right-buttons" class="col-3">
        <c:set var="temp_button_class" value="button centered"/>
        <c:choose>
            <c:when test="${not empty user}">
                <span class="mr-1 welcome-header-message"><ctags:message key="header.format.message.welcome" formatValue="${user.getUsername()}" /></span>
                <a href="${base_url}logout" class="${temp_button_class} foreground"><ctags:message key="header.link.logout"/></a>
            </c:when>
            <c:otherwise>
                <a href="${base_url}login" class="${temp_button_class} mr-1"><ctags:message key="header.link.login"/></a>
                <a href="${base_url}register" class="${temp_button_class} mobile-hide"><ctags:message key="header.link.register"/></a>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<div class="messageContentBox">
    <input id="hide_messageBox_check" type="checkbox" <c:if test="${not (fn:length(message.getContent()) > 0)}"> checked </c:if> />
    <label id="messageBox" for="hide_messageBox_check" class="bg-white strongly-rounded ${message.getType()}">
        <div class="content">
            <ctags:message key="${message.getContent()}" />
        </div>
        <span id="hide_messageBox_label">X</span>
    </label>
</div>
<div id="mobileMenu" class="hidden">
    <div id="mobileMenuSidebarCover" class="col-12">
        <div id="mobileMenuSidebar">
            <c:forEach var="button" items="${buttons}" varStatus="menuLoop">
                <ctags:menubutton button="${button}" />
            </c:forEach>
        </div>
    </div>
</div>
<div id="page-content">
