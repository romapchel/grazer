<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script>
    function setItemsPerPage(NumberOfItemsPerPage)
    {
        perpage = NumberOfItemsPerPage;
        if(!online){
            var items = $('#content > .page > div');
            totalPages = Math.floor(items.length / perpage);
            if(items.length % perpage == 0 && totalPages > 0) {
                totalPages--;
            }
        }
        update();
    }
    function setupPagination(getUpdatesOnline)
    {
        online = getUpdatesOnline;
        $('#pagination').removeClass('hidden');

        if(${empty page}) {
            page = 0;
        } else {
            <c:if test="${not empty page}">
                page = ${page};
            </c:if>
        }
        <c:if test="${not empty totalPages}">
            if(online) {
                totalPages = ${totalPages};
            }
        </c:if>
        if(!online && ${empty perpage}) {
            setItemsPerPage(5);
        } else {
            <c:if test="${not empty perpage}">
                setItemsPerPage(${perpage});
            </c:if>
        }

        $('#pagination > .pager').on('click', function(){
            changePage($(this));
            return false;
        });
        update();
    }
    function managePaginationDisplay()
    {
        $('#previousPage').removeClass("inactive");
        $('#nextPage').removeClass("inactive");
        $('#firstPage').removeClass("inactive");
        $('#lastPage').removeClass("inactive");

        if(page >= totalPages){
            $('#nextPage').addClass("inactive");
            $('#lastPage').addClass("inactive");
        }
        if(page <= 0){
            $('#previousPage').addClass("inactive");
            $('#firstPage').addClass("inactive");
        }

        $('#current_page').html(parseInt(page) + 1);
        $('#total_pages').html(parseInt(totalPages) + 1);
    }
    function changePage(button)
    {
        if(button.hasClass("inactive") || (typeof updating !== 'undefined' && updating) ) {
            return false;
        }
        var buttonId = button.prop('id');
        if(buttonId == "nextPage") {
            page++;
            if(page > totalPages) {
                page = totalPages;
            }
        } else if(buttonId == "previousPage") {
            page--;
            if(page < 0) {
                page = 0;
            }
        } else if(buttonId == "firstPage") {
            page = 0;
        } else if(buttonId == "lastPage") {
            page = totalPages;
        } else {
            log("unknown pagination button: " + buttonId);
        }

        update();
    }
    function update()
    {
        if(online){
            updating = true;
            onlineUpdate();
        } else {
            offlineUpdate();
        }
        managePaginationDisplay();
    }
    function offlineUpdate()
    {
        var items = $('#content > .page > div');
        var minimum = page * perpage;
        var maximum = minimum + perpage;
        $.each(items, function(index, item){
            if(index >= minimum && index < maximum) {
                $(item).show();
            } else {
                $(item).hide();
            }
        });

        if (navigator.userAgent.match(/Chrome|AppleWebKit/)) {
            window.location.href = "#paginationTop";
            window.location.href = "#paginationTop";  /* these take twice */
        } else {
            window.location.hash = "paginationTop";
        }
    }
</script>
<style>
    #pagination{
        width: 100%;
        white-space: nowrap;
        margin-bottom: 20px;
    }
    #pagination > .inactive{
        opacity: 0;
        cursor: default;
    }
    #pagination > .pager, #pagination > .pager_style{
        width: 20%;
        display: inline-block;
        text-align: center;
        text-decoration: none;
        color: black;
    }
    #pagination > .pager{
        border: 1px solid #555555;
    }
</style>
<div class="centeredBox">
    <div id="pagination" class="hidden">
        <a id="firstPage" class="pager<c:if test="${page <= 0}"> inactive</c:if>" href="#">Eerste</a>
        <a id="previousPage" class="pager<c:if test="${page <= 0}"> inactive</c:if>" href="#">Vorige</a>
        <span class="pager_style">pagina <span id="current_page"></span> / <span id="total_pages"></span></span>
        <a id="nextPage" class="pager<c:if test="${page >= totalPages}"> inactive</c:if>" href="#">Volgende</a>
        <a id="lastPage" class="pager<c:if test="${page >= totalPages}"> inactive</c:if>" href="#">Laatste</a>
    </div>
</div>