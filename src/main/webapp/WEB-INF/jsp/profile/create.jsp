<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2019-02-27
  Time: 10:10
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>


<div class="screen">
    <div class="content col-12 col-lg-10 col-xl-8 shadowed">
        <div class="col-12 p-0">
            <h1 class="${page_title_class}">
                Profiel maken
            </h1>
            <form id="create-profile-form" method="POST">
                <c:set var="current" value="" />

                <div class="row step step-1 p-0 m-0">
                    <div class="col-1 p-0"><span class="step-icon">1</span></div>
                    <div class="col-11 lh-high p-0 m-0">
                        <c:if test="${not empty previous && previous.containsKey(ProfileDao.PROFILE_NAME)}">
                            <c:set var="current" value="${previous}" />
                        </c:if>
                        <ctags:input name="${ProfileDao.PROFILE_NAME}" values="${current}"  errors="${errors}" title="${ProfileDao.PROFILE_NAME}.display" inputclass="lh-2" required="true" hideMouseOvers="true" /><br/>
                        <span onclick="openNextStep('step-2');hideContinueButton(this);" class="col-2 mobile-col-6 mp-2 button white p-2 bgc-dark-primary float-right text-align-center">Ga verder</span>
                    </div>
                </div>

                <div class="row step step-2 hide p-0 m-0 mt-4">
                    <div class="col-1 p-0 mobile-pt-1"><span class="step-icon">2</span></div>
                    <div class="col-11 step-text p-0 m-0">
                        <span style="position:relative;">
                            Kies uw hoofdrechtsgebied(en)
                            <div class="mouseover mouseover-below tip d-inline-block small">
                                <div class="mouseover-msg">
                                    <span>
                                        Grazer Legal NL maakt gebruik van de rechtsgebiedenindeling van Rechtspraak.nl. Een uitgebreidere toelichting hierop vindt u onder Veelgestelde vragen in de voetregel.
                                    </span>
                                </div>
                            </div>
                        </span>
                        <c:set var="temp_text">
                            <ctags:message key="${Tag.RECHTSGEBIED}.display.short" />
                        </c:set>
                        <c:set var="autocompleteItems" value="${TagDao.getType(Tag.RECHTSGEBIED)}" scope="request" />
                        <c:if test="${not empty previous && previous.containsKey(Tag.RECHTSGEBIED)}">
                            <c:set var="selectedItems" value="${previous.get(Tag.RECHTSGEBIED)}" scope="request" />
                        </c:if>
                        <ctags:optionList addContainerClass="no-icon" name="${Tag.RECHTSGEBIED}" selectAllTitle="${temp_text}" groupByParent="${true}" subRechtsgebieden="false" alignRight="true" />

                        <br/>

                        <div>
                            <span onclick="openNextStep('step-4');hideParentContinueButton(this);" class="ml-2 col-4 mobile-col-6 mp-2 button white p-2 bgc-dark-primary float-right text-align-center">Kies álle subrechtsgebieden</span>
                            <span onclick="openNextStep('step-3');hideParentContinueButton(this);" class="col-4 mobile-col-6 mp-2 button white p-2 bgc-dark-primary float-right text-align-center">Kies enige subrechtsgebieden</span>
                        </div>
                    </div>
                </div>

                <div class="row step step-3 hide p-0 m-0 mt-4">
                    <div class="col-1 p-0 mobile-pt-1"><span class="step-icon">3</span></div>
                    <div class="col-11 step-text p-0 m-0">
                        Kies uw sub-rechtsgebied(en)

                        <c:set var="temp_text">
                            <ctags:message key="${Tag.RECHTSGEBIED}.display.short" />
                        </c:set>
                        <c:set var="autocompleteItems" value="${TagDao.getType(Tag.RECHTSGEBIED)}" scope="request" />
                        <c:if test="${not empty previous && previous.containsKey(Tag.RECHTSGEBIED)}">
                            <c:set var="selectedItems" value="${previous.get(Tag.RECHTSGEBIED)}" scope="request" />
                        </c:if>
                        <ctags:optionList addContainerClass="no-icon" name="${Tag.RECHTSGEBIED}" selectAllTitle="${temp_text}" groupByParent="${true}" subRechtsgebieden="true" alignRight="true" addParentName="${true}" />
                        <br/>
                        <span onclick="openNextStep('step-4');hideContinueButton(this);" class="col-2 mobile-col-6 mp-2 button white p-2 bgc-dark-primary float-right text-align-center">Ga verder</span>
                    </div>
                </div>

                <%--
                <ctags:tagAutocomplete addContainerClass="no-icon" name="${Tag.RECHTSGEBIED}" selectAllTitle="${temp_text}" groupByParent="${true}" />
                --%>


                <div class="row step step-4 hide m-0 p-0 mt-4">
                    <div class="col-12 m-0 p-0">
                        <div class="row m-0 p-0">
                            <div class="col-6 m-0 p-0 pr-2">

                                <button type="submit" name="submit" value="opslaan-zoek" class="col-12 button bgc-light-primary white m-0">
                                    Opslaan en zoeken
                                </button>
                            </div>
                            <div class="col-6 m-0 p-0 pl-2">
                                <button type="submit" name="submit" value="opslaan-verfijn" class="col-12 button bgc-light-primary white m-0">
                                    Verfijn uw profiel
                                </button>
                                <div class="mouseover tip tip-right">
                                    <div class="mouseover-msg  below light-primary" style="bottom:unset">
                                        <ctags:message key="
                                            Met verfijn uw profiel krijgt u de mogelijkheid om Informatiesoorten die u wilt volgen te kiezen en de maatschappelijke geleding waarin de afzenders van het nieuws actief zijn."/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    clearAllSubRG();

    jQuery(".list-button").click(function(){
        var method = "";
        // switch on off
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            method = "off";
        } else {
            $(this).addClass("active");
            method = "on";
        }

        // check top level
        if ($(this).hasClass("rechtsgebied-top")) {
            var rgParentClassname = 'parentid-'+$(this).attr('class').split(' ')[5];

            if (method == "on") {
                $('.rechtsgebied-sub.'+rgParentClassname).removeClass("hidden");
            } else {
                $('.rechtsgebied-sub.'+rgParentClassname).addClass("hidden");
            }
        }

    });

    function clearAllSubRG() {
        $(".rechtsgebied-sub").addClass("hidden");
    }

    function openNextStep(id) {
        $("div."+id).removeClass("hide");
    }

    function hideContinueButton(obj) {
        obj.style.display = "none";
    }

    function hideParentContinueButton(obj) {
        obj.parentNode.style.display = "none";
    }



    $('form#create-profile-form input').keydown(function (e) {
        if (e. keyCode == 13) {
            e. preventDefault();
            return false;
        }
    });

</script>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>