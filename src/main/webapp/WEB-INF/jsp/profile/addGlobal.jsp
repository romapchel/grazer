<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-06-06
  Time: 12:28
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen vertically-centered">
    <c:set var="title">
        <ctags:message key="profile.global.filter.title"/>
    </c:set>
    <c:set var="placeholder">
        <ctags:message key="profile.global.filter.placeholder"/>
    </c:set>
    <div class="${content_size_class} shadowed">
        <a href="${base_url}profile/show" class="col-12 text-center mb-4 button text-white">
            <ctags:message key="profile.global.select.personal.switch"/>
        </a>
        <ctags:input name="db-filter-name" values="${filters}" inputclass="display-filter-input" labelclass="mb-0"
                     title="${title}" placeholder="${placeholder}"/>
        <div class="col-12 p-0 display-filterable item-list limited-45">

            <c:set var="temp_previous_first_character" value="not a character" />
            <c:set var="temp_open" value="${false}" />
            <c:forEach var="global_profile" items="${global_profiles}" varStatus="global_profiles_loop">

                <c:set var="temp_item_name" value="${global_profile.getName()}" />
                <c:set var="temp_item_name_first_character" value="${fn:substring(temp_item_name, 0, 1)}" />

                <c:if test="${fn:toLowerCase(temp_item_name_first_character) != fn:toLowerCase(temp_previous_first_character)}">
                    <c:if test="${temp_open}">
                        <c:set var="temp_open" value="${false}" />
                            </div>
                        </div>
                    </c:if>

                    <c:set var="temp_previous_first_character" value="${temp_item_name_first_character}" />
                    <c:set var="temp_first_character_check_id" value="${global_profiles_loop.index}-${temp_item_name_first_character}" />
                    <c:set var="temp_open" value="${true}" />

                    <div class="col-12 filterable-display-parent">
                        <input id="${temp_first_character_check_id}" type="checkbox" class="hidden expand-check" />
                        <ctags:checkboxButton forid="${temp_first_character_check_id}" text="${temp_item_name_first_character}" additional_label_class="black col-12" />
                        <div class="expand col-12 p-0">
                </c:if>

                            <form action="${base_url}profile/global">
                                <input type="hidden" name="<%= ProfileDao.ID %>" value="${global_profile.getId()}"/>
                                <div class="col-12 ${flexwrap} display-filterable border-bottom">
                                    <div class="col-12 col-md-5 vertically-centered p-0 filterable">
                                        ${temp_item_name}
                                    </div>
                                    <div class="col-12 col-md-7 ${flexwrap} p-1">
                                        <c:set var="temp_button_class" value="button col-6 border p-0"/>
                                        <c:set var="action" value="<%= Actions.VIEW %>" />
                                        <button class="${temp_button_class}" type="submit" name="<%= Selections.ACTION %>"
                                                value="${action}">
                                            <ctags:message key="actions.${action}.title"/>
                                        </button>
                                        <button class="${temp_button_class}" type="submit" name="<%= Selections.ACTION %>"
                                                value="<%= Actions.COPY %>">
                                            <ctags:message key="profile.global.button.add"/>
                                        </button>
                                    </div>
                                </div>
                            </form>
            </c:forEach>

            <c:if test="${temp_open}">
                        </div>
                    </div>
                <c:set var="temp_open" value="${false}" />
            </c:if>
        </div>
    </div>
</div>
<c:if test="${not empty selected_profile}">
    <c:set var="temp_display_profile" value="${selected_profile}" scope="request"/>
    <c:set var="temp_display_pagination" value="${pagination}" scope="request"/>
    <%@ include file="/WEB-INF/jsp/profile/display.jsp" %>
</c:if>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>