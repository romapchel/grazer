<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-09-24
  Time: 10:02
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/profile.css"/>

<c:set var="landing_button_container_class" value="white col-12 mb-2 ${flexwrap}"/>
<c:set var="landing_button_class"
       value="bgc-dark-primary p-3 white col-12 button text-center hover-white strongly-rounded"/>

<div class="screen col-12 d-flex justify-content-center p-0">
    <div class="${content_size_class} p-0">
        <div class="col-12 p-0">

            <h1 class="${page_title_class} pt-3">Profielenbeheer ${amount_profiles}</h1>

            <div class="col-12 bc-grey bgc-white ${flexwrap} justify-content-center mb-5 mobile-margin-bottom-0">

                <c:set var="name" value="${user.getUsername()}"/>
                <div class="col-12 p-0">
                    <form id="active-profile" data-id="${user.getActiveProfile().getId()}">
                        <c:set var="temp_select_dropdown_pre" scope="page">
                            <ctags:message key="profile.active"/>:
                        </c:set>
                        <%@ include file="/WEB-INF/jsp/profile/select_dropdown.jsp" %>
                    </form>
                </div>

                <div class="col-12 p-0 mt-3 ${flexwrap}">
                    <div class="${landing_button_container_class} col-lg p-0 pr-lg-1">
                        <a href="${base_url}search" class="${landing_button_class}">
                            <ctags:message key="profile.buttons.search"/>
                        </a>
                    </div>

                    <div class="${landing_button_container_class} col-lg-6 p-0 pl-lg-1">
                        <a id="<%= Actions.CREATE %>" class="${landing_button_class}" href="${base_url}profile/create">
                            <ctags:message key="profile.buttons.create"/>
                        </a>
                    </div>

                </div>
            </div>

            <div class="col-12 p-0">

                <div class="${landing_button_container_class}">
                    <a href="${base_url}profile/show" class="${landing_button_class}">
                        <ctags:message key="profile.buttons.edit.existing"/>
                    </a>
                </div>


                <div class="noJavaScript-hidden ${landing_button_container_class} mobile-hide">
                    <a id="<%= Actions.EXPORT %>" target="_blank" class="mouseover mouseover-below tip d-block text-white hidden ${landing_button_class}" download="export.xml" style="color:white;">
                        <ctags:message key="profile.buttons.export"/>
                        <div class="mouseover-msg below light-primary" style="bottom:unset">
                            <ctags:message key="Indien u een profiel wilt exporteren, dan kan het zijn dat uw systeem de volgende melding geeft: 'Export.xml is geblokkeerd omdat dit type bestand schadelijk kan zijn voor uw apparaat.' We kunnen deze melding niet onderdrukken omdat hij systeemafhankelijk is. U kunt de melding negeren door op 'Verwijderen' te klikken, waarna u gewoon het profiel kunt exporteren als Excel-bestand dat door iemand anders die ook Grazer heeft geïmporteerd kan worden." />
                        </div>

                    </a>
                </div>

                <div class="advanced-search mouseover tip" id="toggle_button_eenvoudig" style="display:none;float:right;width:auto;">
                    <span class="advanced-search-button-top" onclick="toggleAdvanced();$('#search-check').click()">
                        <ctags:message key="Eenvoudig zoeken" />
                        <div class="mouseover-msg  below light-primary" style="bottom:unset">
                            <ctags:message key="
                                Eenvoudig zoeken biedt de gebruiker de mogelijkheid om op basis van het actieve profiel de zoekresultaten te verfijnen door de volgende verfijnopties: 'Zoeken bínnen overzicht nieuwsberichten'. U kunt hier zoektermen invoegen zodat binnen de gevonden resultaten alleen die nieuwsberichten worden getoond waarin de door u ingevoerde zoekterm voorkomt.
                                'Toon alleen nieuwsberichten die voldoen aan het actieve profiel sinds de laatste keer dat u bent ingelogd' zorgt ervoor dat u uitsluitend de nieuwste berichten getoond krijgt. Wilt u álle nieuwsberichten zien die voldoen aan uw profiel, dus ook die verschenen zijn vóór de laatste keer dat u inlogde, dan kunt u klikken op 'Toon álle nieuwsberichten die voldoen aan het actieve profiel'.
                                Met 'Verberg formatiesoort Jurisprudentie' kunt u het zoekresultaat verfijnen door jurisprudentie te verbergen. Het grootste deel van het gepubliceerde nieuws bestaat uit jurisprudentie en dat zorgt ervoor dat deze informatiesoort de overige informatiesoorten in de resultaten overheerst. Door 'Verberg Informatiesoort Jurisprudentie' te gebruiken ziet u sneller welke zoekresultaten de overige Informatiesoorten hebben opgeleverd.
                            " />
                        </div>
                    </span>
                </div>



                <div class="noJavaScript-only ${landing_button_container_class} mobile-hide">
                    <span class="mouseover mouseover-below tip d-inline small">
                        <span class="mouseover-msg">
                            <span>
                                <ctags:message key="profile.information.export"/>
                            </span>
                        </span>
                    </span>
                </div>

                <div class="noJavaScript-only ${landing_button_container_class} mobile-hide">
                    <form action="${base_url}profile/import" class="col-12" method="POST" enctype='multipart/form-data'>
                        <input id="xml" type="file" name="xml" class="black"/>
                        <button type="submit" class="black">
                            <ctags:message key="profile.buttons.xml.load"/>
                        </button>
                    </form>
                </div>
                <div class="noJavaScript-hidden ${landing_button_container_class} mobile-hide">
                    <a id="<%= Actions.IMPORT %>" class="${landing_button_class}" href="#">
                        <ctags:message key="profile.buttons.import"/>
                    </a>
                    <script>
                        $(document).ready(function () {
                            loadExportUrl();
                            $('#xml').on('change', function () {
                                $(this).parent().submit();
                            });
                            $("#<%= Actions.IMPORT %>").on('click', function () {
                                $('#xml').click()
                            });
                        });

                        function loadExportUrl() {
                            var id = $('#active-profile').data('id');
                            var url = '${base_url}profile/export';
                            var data = {
                                <%= ProfileDao.ID %>:
                                id,
                            };
                            var succes = function (data) {
                                data = data.trim();
                                if (0 < data.length) {
                                    var nurl = 'data:Application/octet-stream,' + encodeURIComponent(data);
                                    var link = $("#<%= Actions.EXPORT %>");
                                    link.attr('href', nurl);
                                    link.removeClass('hidden');
                                }
                            };

                            ajaxCall(url, data, succes, 'text');
                        }
                    </script>
                </div>

                <c:if test="${show_globals}">
                    <div class="${landing_button_container_class}">
<%--                        <a href="${base_url}profile/global" class="${landing_button_class}">--%>
<%--                            <ctags:message key="profile.select.global.switch"/>--%>
<%--                        </a>--%>

                        <c:set var="temp_landing_check_id" value="explain-default-profiles-check"/>
                        <input id="${temp_landing_check_id}" type="checkbox" class="hidden expand-check"/>
                        <c:set var="button_text">
                            <ctags:message key="profile.buttons.default.explain"/>
                        </c:set>
                        <div class="mouse_overs d-flex text-center col-12 p-0">
                            <div class="mouseover tip col-12 p-0">
                                <span class="">${button_text}</span>
                                <div class="dark-primary mouseover-msg">
                                    <ctags:message key="profile.default.explain"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>

            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>