<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-05-17
  Time: 15:44
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/profile.css"/>

<%@ include file="/WEB-INF/jsp/profile/select.jsp" %>

<c:if test="${not empty active}">
    <c:choose>
        <c:when test="${edit}">
            <%@ include file="/WEB-INF/jsp/profile/edit.jsp" %>
        </c:when>
        <c:otherwise>
            <c:set var="temp_display_profile" value="${active}" scope="request"/>
            <c:set var="temp_display_pagination" value="${global_pagination}" scope="request"/>
            <%@ include file="/WEB-INF/jsp/profile/display.jsp" %>
        </c:otherwise>
    </c:choose>
</c:if>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>