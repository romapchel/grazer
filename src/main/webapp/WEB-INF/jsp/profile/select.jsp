<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-05-17
  Time: 15:44
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<div class="screen col-12 d-flex justify-content-center">
    <div class="${content_size_class} p-1">
        <div class="col-12 p-0 h-100">
            <h1 class="${page_title_class}">Profielenbeheer</h1>

            <form id="profile-select-form" class="col-12 p-0 mt-3 strongly-rounded"
                  action="${base_url}profile/action" method="post">
                <input id="display-profiles" type="checkbox" class="hidden"<c:if test="${not empty active}"> checked="checked"</c:if> />
                <c:set var="button_text">
                    <ctags:message key="profile.buttons.switch.profile"/>
                </c:set>
                <ctags:checkboxButton forid="display-profiles" text="${button_text}"
                                      base_class="bgc-light-primary white col-12 p-3 button"/>

                <c:set var="activeProfId" value="${user.getActiveProfile().getId()}" />
                <div id="profiles" class="border mt-3" style="border-radius:10px;">
                    <c:forEach var="profile" items="${profiles}" varStatus="profileLoop">
                        <c:set var="curProfId" value="${profile.getId()}" />
                        <c:set var="curProfname" value="${profile.getName()}" />
                        <c:set var="rowClass" value="sel-bgc-dark-primary sel-white bgc-white p-o profile col-12"/>
                        <c:set var="type" value="submit" />
                        <c:if test="${activeProfId eq curProfId}">
                            <c:set var="rowClass" value="selected ${rowClass}" />
                            <c:set var="type" value="button" />
                        </c:if>
                        <div class="${rowClass}" style="padding:0;border-radius:10px;">
                            <c:set var="inputName" value="<%= ProfileDao.ID %>" />
                            <input id="delete-profile-cancel" type="radio" name="${inputName}" class="hidden" />

                            <c:if test="${empty defaultAction}">
                                <c:set var="defaultAction">
                                    <c:choose>
                                        <c:when test="${edit}">
                                            <%= Actions.EDIT %>
                                        </c:when>
                                        <c:otherwise>
                                            <%= Actions.VIEW %>
                                        </c:otherwise>
                                    </c:choose>
                                </c:set>
                            </c:if>

                            <input type="hidden" name="defaultAction" value="${defaultAction}"/>
                            <button type="submit" class="button col-12" name="${defaultAction}" value="${curProfId}">
                                <ctags:message key="actions.${defaultAction}.title" />: ${curProfname}
                            </button>
                            <div class="icon-container p-0 mr-1 white bgc-light-primary col-12 justify-content-center">

                                <c:set var="action" value="<%= Actions.VIEW %>" />
                                <c:set var="alt_text"><ctags:message key="actions.${action}.title" /></c:set>
                                <button type="submit" class="button border mr-1" name="${action}"
                                        value="${curProfId}" title="${alt_text}" aria-label="${alt_text}">
                                    <i class="fa fa-eye large" aria-hidden="true"></i>
                                </button>

                                <c:set var="action" value="<%= Actions.EDIT %>" />
                                <c:set var="alt_text"><ctags:message key="actions.${action}.title" /></c:set>
                                <button type="submit" class="button border mr-1" name="${action}"
                                        value="${curProfId}" title="${alt_text}" aria-label="${alt_text}">
                                    <i class="fa fa-pencil large" aria-hidden="true"></i>
                                </button>

                                <c:set var="warning">
                                    <ctags:message key="profile.format.button.delete.warning" formatValue="${curProfname}" />
                                </c:set>
                                <ctags:deleteWithConfirm input_id="delete-profile-${profileLoop.index}"
                                                         input_name="${inputName}" input_value="${curProfId}"
                                                         warning_text="${warning}"
                                                         button_name="<%= Selections.ACTION %>"
                                                         button_value="<%= Actions.DELETE %>"/>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </form>

        </div>
    </div>
</div>