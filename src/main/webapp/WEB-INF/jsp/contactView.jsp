<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/contact.css"/>

<div class="screen">
    <div class="${content_size_class} shadowed white">
        <div class="col-12">
            <h1 class="${page_title_class}">
                <ctags:message key="contact.header"/>
            </h1>
            <form id="contactform" class="col-12 ${flexwrap} justify-content-center mb-1 black p-0" action="${base_url}contact"
                  method="POST">
                <c:set var="labelLengthClasses" value="col-12 col-sm-3 p-0" />
                <c:set var="inputLengthClasses" value="col" />

                <c:set var="title"><ctags:message key="contact.input.mail.label"/></c:set>
                <c:set var="placeholder"><ctags:message key="contact.input.mail.placeholder"/></c:set>
                <ctags:input name="mail" title="${title}" placeholder="${placeholder}" type="mail"
                             required="True" values="${previous}" errors="${errors}"
                             titleLengthClasses="${labelLengthClasses}" inputLengthClasses="${inputLengthClasses}"/>

                <c:set var="title"><ctags:message key="contact.input.subject.label"/></c:set>
                <ctags:input name="subject" title="${title}" placeholder="${title}" type="text"
                             required="True" values="${previous}" errors="${errors}"
                             titleLengthClasses="${labelLengthClasses}" inputLengthClasses="${inputLengthClasses}"/>

                <c:set var="title"><ctags:message key="contact.input.message.label"/></c:set>
                <label for="contactform-message" class="button col-12 p-0">${title}:</label>
                <textarea id="contactform-message" class="col-12 " name="message" placeholder="${title}"
                          required="true"></textarea>

                <button id="contactform-submit" type="submit" class="button submit-button bgc-override bgc-light-primary text-white">
                    <ctags:message key="contact.button.submit"/>
                </button>
            </form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>