/**
 * Created with IntelliJ IDEA.
 * User: Impressie
 * Date: 23-5-17
 * Time: 16:30
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function() {
    $('form.validatable').on('submit', function(event) {
        var form = $(this).closest('form');
        var inputs = form.find('input');
        validate(inputs);
        var invalids = form.find('label.invalid');
        if(invalids.length !== 0) {
            event.preventDefault();
            invalids.first().focus();
        }
    });
    $('form.validatable .invalid input.validatable').on('change', function() {
        clearErrors($(this));
    });
    // $('form.validatable input.validatable').on('focus', function() {
    //     $(this).addClass('display-tooltip');
    //     clearErrors($(this));
    // });
    // $('form.validatable input.validatable').on('focusout', function() {
    //     validate($(this));
    //     $(this).removeClass('display-tooltip');
    // });
    //$('input[autofocus]').first().addClass('display-tooltip');
});

function validate(inputs) {
    var empty = $('#javascript_errors > #required').text();
    var noMatch = $('#javascript_errors > #match').text();
    $.each(inputs, function(index, input) {
        var input = $(input);
        clearErrors(input);
        var value = input.val();
        var name = input.attr('name');
        if(name.endsWith('-repeat')) {
            var originalName = name.slice(0, -7);
            var original = $('input[name={0}]'.format(originalName));
            if(value.length > 0 && original.val() !== value) {
                addError(input, noMatch.format(getDisplay(original)));
                addError(original, noMatch.format(getDisplay(input)));
            }
        }
        if(input.parent().hasClass("required") && value.trim().length === 0) {
            addError(input, empty);
        }
        if(typeof customvalidation ==='function') {
            customvalidation(input);
        }
    });
}
function addError(input, errorMessage)
{
    var error = input.siblings('.error');
    input.parent().addClass('invalid');
    var html = error.html();
    if(typeof html === 'undefined' || html.indexOf(errorMessage) === -1) {
        error.append('<p>{0}</p>'.format(errorMessage));
    }
}
function clearErrors(input)
{
    input.siblings('.error').html('');
    input.parent().removeClass('invalid');
}
function getDisplay(element)
{
    var display = element.attr('placeholder');
    if(display.length === 0) {
        display = element.prev().html();
        if(display.length === 0) {
            display = element.attr('name');
        }
    }
    return display;
}