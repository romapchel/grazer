
INSERT INTO "bundle" (price, interval_number, interval_type, description, times, type_id)
    VALUES (1, 1, 'd', 'test 1 cent per dag', 1, 2),
           (1995, 1, 'm', 'Betaal maandelijks', 1, 2),
           (14365, 1, 'y', 'Betaal jaarlijks (40% korting op maandprijs)', 1, 2);

INSERT INTO "user" (id, username, password, enabled)
    VALUES  (0, 'global user', 'not a real password since this is not a real user', true),
            (1, 'admin', '$2a$10$UZ23Q5ZHZJ.seri0qjgZvuu6LQXe/VESqh8zVwtDzYfA.s7ATKezC', true),
            (2, 'user', '$2a$10$lTIMHirqs68rE74OSL7E6uX6e4WEIAW44bM6MzrjEFACTI8WTy0Gq', true);

INSERT INTO "type" (id, name)
    VALUES  (0, 'None'),
            (1, 'Admin'),
            (2, 'User'),
            (3, 'Moderator');

INSERT INTO "type_role" (type_id, role_name)
    VALUES  (1, 'ROLE_USER'),
            (1, 'ROLE_ADMIN'),
            (1, 'ROLE_MODERATOR'),
            (1, 'ROLE_EDIT_GLOBAL'),
            (2, 'ROLE_USER'),
            (3, 'ROLE_USER'),
            (3, 'ROLE_MODERATOR'),
            (3, 'ROLE_EDIT_GLOBAL');

INSERT INTO "user_type" (type_id, user_id, end_date)
    VALUES  (1, 1, 0);