DROP TABLE IF EXISTS "invoice";
CREATE TABLE "invoice" (
    id SERIAL PRIMARY KEY,
    invoice_number VARCHAR UNIQUE,
    user_id INTEGER REFERENCES "user"(id) ON DELETE SET NULL ON UPDATE CASCADE,
    bundle_id INTEGER REFERENCES "bundle"(id) ON DELETE SET NULL ON UPDATE CASCADE,
    payment_id INTEGER REFERENCES "payment"(id) ON DELETE SET NULL ON UPDATE CASCADE,
    timestamp BIGINT,
    description TEXT,
    company VARCHAR,
    name VARCHAR,
    adress VARCHAR,
    adress2 VARCHAR,
    subtotal INTEGER,
    vat INTEGER,
    total INTEGER
);

DROP TABLE IF EXISTS "payment";
CREATE TABLE "payment" (
    id SERIAL PRIMARY KEY,
    bundle_id INTEGER REFERENCES "bundle"(id) ON DELETE SET NULL ON UPDATE CASCADE,
    user_id INTEGER REFERENCES "user"(id) ON DELETE SET NULL ON UPDATE CASCADE,
    timestamp BIGINT,
    initial_payment_id INTEGER,
    subscription_payment_id VARCHAR
);

DROP TABLE IF EXISTS "subscription_payment";
CREATE TABLE "subscription_payment" (
       id SERIAL PRIMARY KEY,
       payment_id INTEGER REFERENCES "payment"(id) ON DELETE SET NULL ON UPDATE CASCADE,
       invoice_id INTEGER REFERENCES "invoice"(id) ON DELETE SET NULL ON UPDATE CASCADE,
       timestamp BIGINT,
       mollie_payment_id VARCHAR
);

DROP TABLE IF EXISTS "bundle";
CREATE TABLE "bundle" (
    id SERIAL PRIMARY KEY,
    price INTEGER,
    interval_number INTEGER,
    interval_type VARCHAR,
    description TEXT,
    times INTEGER,
    type_id INTEGER REFERENCES "type"(id) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS "user";
CREATE TABLE "user" (
    id SERIAL PRIMARY KEY,
    username VARCHAR,
    password VARCHAR,
    organisation VARCHAR,
    department VARCHAR,
    initials VARCHAR,
    tsvg VARCHAR,
    lastname VARCHAR,
    street VARCHAR,
    housenumber VARCHAR,
    zip VARCHAR,
    city VARCHAR,
    phone VARCHAR,
    mail VARCHAR,
    user INTEGER,
    enabled BOOLEAN DEFAULT FALSE,
    active_profile INTEGER REFERENCES "profile"(id) ON DELETE SET NULL ON UPDATE CASCADE,
    last_login INTEGER,
    current_login INTEGER,
    mollie_id VARCHAR
);

DROP TABLE IF EXISTS "type";
CREATE TABLE "type" (
    id SERIAL PRIMARY KEY,
    name VARCHAR
);

DROP TABLE IF EXISTS "type_role";
CREATE TABLE "type_role" (
    id SERIAL PRIMARY KEY,
    type_id INTEGER REFERENCES "type"(id) ON DELETE CASCADE ON UPDATE CASCADE,
    role_name VARCHAR
);

DROP TABLE IF EXISTS "user_type";
CREATE TABLE "user_type" (
    id SERIAL PRIMARY KEY,
    type_id INTEGER REFERENCES "type"(id) ON DELETE CASCADE ON UPDATE CASCADE,
    user_id INTEGER REFERENCES "user"(id) ON DELETE CASCADE ON UPDATE CASCADE,
    end_date INTEGER
);

DROP TABLE IF EXISTS "profile_tag";
CREATE TABLE "profile_tag" (
    id SERIAL PRIMARY KEY,
    profile_id INTEGER REFERENCES "profile"(id) ON DELETE SET NULL ON UPDATE CASCADE,
    tag_id INTEGER REFERENCES "tag"(id) ON DELETE CASCADE ON UPDATE CASCADE,
);

DROP TABLE IF EXISTS "tag";
CREATE TABLE "tag" (
    id SERIAL PRIMARY KEY,
    name VARCHAR,
    type VARCHAR,
    code VARCHAR,
    enabled BOOLEAN,
    description TEXT
);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO grazer;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO grazer;