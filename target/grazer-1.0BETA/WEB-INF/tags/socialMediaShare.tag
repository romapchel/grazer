<%@ tag %>

<%@ attribute name="url" required="true" type="java.lang.String" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="ctags" tagdir="/WEB-INF/tags"%>

<%--<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>--%>
<ctags:resourceImport type="js" path="//platform.linkedin.com/in.js" path_is_absolute_url="${true}" />
<ctags:resourceImport type="js" path="https://platform.twitter.com/widgets.js" path_is_absolute_url="${true}" />
<ctags:resourceImport type="js" path="js/facebook.js" />

<div class="social-media col-12 pb-3">

    <%--<div class="fb-share-button"--%>
         <%--data-href="${url}"--%>
         <%--data-layout="button">--%>
    <%--</div>--%>
    <div style="background: #4267b2; border-radius: 4px;">
        <c:url value="https://www.facebook.com/sharer/sharer.php" var="facebookurl">
            <c:param name="u" value="${url}" />
        </c:url>
        <a href="${facebookurl}" target="_blank" class="${flexwrap} button" style="padding: 1px 3px;">
            <svg style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" color="#00000">
                <path fill="#FFFFFF" fill-rule="evenodd" d="M8 14H3.667C2.733 13.9 2 13.167 2 12.233V3.667A1.65 1.65 0 0 1 3.667 2h8.666A1.65 1.65 0 0 1 14 3.667v8.566c0 .934-.733 1.667-1.667 1.767H10v-3.967h1.3l.7-2.066h-2V6.933c0-.466.167-.9.867-.9H12v-1.8c.033 0-.933-.266-1.533-.266-1.267 0-2.434.7-2.467 2.133v1.867H6v2.066h2V14z"></path>
            </svg>
            <span class="white" style="padding: 0 4px; font-family: Helvetica, Arial, sans-serif; font-weight: bold; font-size: 11px;">
                Share
            </span>
        </a>
    </div>

    <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="Gevonden via Grazer.news https://grazer.news/" data-url="${url}" data-hashtags="GrazerLegal, Grazer" data-show-count="false">Tweet</a>

    <script type="IN/Share" data-url="${temp_article_url}"></script>

</div>