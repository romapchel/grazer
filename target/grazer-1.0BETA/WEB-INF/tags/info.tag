<%--
  Created by IntelliJ IDEA.
  User: Julian
  Date: 2019-01-02
  Time: 11:04
--%>
<%@ taglib prefix="ctags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="message" required="true" type="java.lang.String" %>
<%@ attribute name="buttonText" required="false" type="java.lang.String" %>
<%@ attribute name="mouseoverClasses" required="false" type="java.lang.String" %>
<%@ attribute name="iconClasses" required="false" type="java.lang.String" %>

<c:set var="temp_info_button_text"><ctags:message key="${buttonText}" /></c:set>
<c:set var="temp_info_message_text"><ctags:message key="${message}" /></c:set>

<div class="mouseover tip ${iconClasses}">
    ${fn:trim(temp_info_button_text)}
    <div class="mouseover-msg ${mouseoverClasses}">
        ${fn:trim(temp_info_message_text)}
    </div>
</div>