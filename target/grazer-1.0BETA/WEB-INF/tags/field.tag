<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2019-03-12
  Time: 15:44
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctags" tagdir="/WEB-INF/tags" %>

<%@ attribute name="fieldMap" required="false" type="nl.impressie.grazer.model.FieldMap" %>
<%@ tag import="nl.impressie.grazer.model.FieldMap" %>

<%
    FieldMap field = (FieldMap)jspContext.getAttribute("fieldMap");
    jspContext.setAttribute("name", field.get(FieldMap.Attribute.NAME));
    jspContext.setAttribute("title", field.get(FieldMap.Attribute.TITLE));
    jspContext.setAttribute("placeholder", field.get(FieldMap.Attribute.PLACEHOLDER));
    jspContext.setAttribute("type", field.get(FieldMap.Attribute.TYPE));
    jspContext.setAttribute("autoSelect", field.get(FieldMap.Attribute.AUTOSELECT));
    jspContext.setAttribute("tooltip", field.get(FieldMap.Attribute.TOOLTIP));
    jspContext.setAttribute("required", field.get(FieldMap.Attribute.REQUIRED));
    jspContext.setAttribute("values", field.get(FieldMap.Attribute.VALUES));
    jspContext.setAttribute("errors", field.get(FieldMap.Attribute.ERRORS));
    jspContext.setAttribute("value", field.get(FieldMap.Attribute.VALUE));
    jspContext.setAttribute("labelclass", field.get(FieldMap.Attribute.LABELCLASS));
    jspContext.setAttribute("checked", field.get(FieldMap.Attribute.CHECKED));
    jspContext.setAttribute("titleLengthClasses", field.get(FieldMap.Attribute.TITLE_LENGTH_CLASS));
    jspContext.setAttribute("hideMouseOvers", field.get(FieldMap.Attribute.HIDE_MOUSE_OVERS));
    jspContext.setAttribute("alt", field.get(FieldMap.Attribute.ALT));
    jspContext.setAttribute("inputLengthClasses", field.get(FieldMap.Attribute.INPUT_LENGTH_CLASS));
    jspContext.setAttribute("autofocus", field.get(FieldMap.Attribute.AUTOFOCUS));
    jspContext.setAttribute("containerclass", field.get(FieldMap.Attribute.CONTAINER_CLASS));
    jspContext.setAttribute("data", field.get(FieldMap.Attribute.DATA));
    jspContext.setAttribute("disabled", field.get(FieldMap.Attribute.DISABLED));
    jspContext.setAttribute("inputclass", field.get(FieldMap.Attribute.INPUT_CLASS));
    jspContext.setAttribute("labelLengthClasses", field.get(FieldMap.Attribute.LABEL_LENGTH_CLASS));
    jspContext.setAttribute("min", field.get(FieldMap.Attribute.MIN));
    jspContext.setAttribute("onInputFocus", field.get(FieldMap.Attribute.ON_INPUT_FOCUS));
    jspContext.setAttribute("titleclass", field.get(FieldMap.Attribute.TITLE_CLASS));
    jspContext.setAttribute("validatable", field.get(FieldMap.Attribute.VALIDATEABLE));
    jspContext.setAttribute("requiredInfoShow", field.get(FieldMap.Attribute.REQUIREDINFOSHOW));
%>
<ctags:input name="${name}" title="${title}" placeholder="${placeholder}" type="${type}" autoSelect="${autoSelect}" tooltip="${tooltip}" required="${required}" values="${values}" errors="${errors}" value="${value}" labelclass="${labelclass}" checked="${checked}" titleLengthClasses="${titleLengthClasses}" inputLengthClasses="${inputLengthClasses}" hideMouseOvers="${hideMouseOvers}" alt="${alt}" autofocus="${autofocus}" containerclass="${containerclass}" data="${data}" disabled="${disabled}" inputclass="${inputclass}" labelLengthClasses="${labelLengthClasses}" min="${min}" onInputFocus="${onInputFocus}" titleclass="${titleclass}" validatable="${validatable}" requiredInfoShow="${requiredInfoShow}" />