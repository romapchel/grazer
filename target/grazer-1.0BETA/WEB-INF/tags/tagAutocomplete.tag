<%--
  Created by IntelliJ IDEA.
  User: Julian
  Date: 2018-05-08
  Time: 09:55
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cfn" uri="/WEB-INF/jstl-functions.tld" %>

<%@ attribute name="name" required="false" type="java.lang.String" %>
<%@ attribute name="addLabelClass" required="false" type="java.lang.String" %>
<%@ attribute name="addAllNoneButtonClass" required="false" type="java.lang.String" %>
<%@ attribute name="addContainerClass" required="false" type="java.lang.String" %>
<%@ attribute name="selectAllTitle" required="false" type="java.lang.String" %>
<%@ attribute name="groupByFirstCharacter" required="false" type="java.lang.String" %>
<%@ attribute name="groupByParent" required="false" type="java.lang.String" %>

<c:if test="${empty name}"><c:set var="name" value="items" /></c:if>
<c:if test="${empty groupByFirstCharacter}"><c:set var="groupByFirstCharacter" value="False" /></c:if>
<c:if test="${empty groupByParent}"><c:set var="groupByFirstParent" value="False" /></c:if>
<c:if test="${not empty addLabelClass}"><c:set var="addLabelClass" value=" ${addLabelClass}" /></c:if>
<c:if test="${not empty addAllNoneButtonClass}"><c:set var="addAllNoneButtonClass" value=" ${addAllNoneButtonClass}" /></c:if>

<c:set var="parentMap" value="${cfn:emptyMap()}" />
<c:if test="${groupByParent}">
    <c:set var="groupByFirstCharacter" value="False" />
    <c:set var="parentMap" value="${cfn:getTagFamilieMap(autocompleteItems)}" />
</c:if>

<c:set var="labels" value="" />
<c:set var="uneditable" value="${not empty editableProfile && not editableProfile}" />
<c:set var="for_id" value="" />

<div class="tag-autocomplete grouping padded col-12">
    <c:set var="temp_tagAutocomplete_hidden_addon" value="" />
    <c:if test="${uneditable}">
        <c:set var="temp_tagAutocomplete_hidden_addon" value=" style=\"display: none!important\"" />
    </c:if>
    <div class="noJavaScript-hidden ${flexwrap} col-12"${temp_tagAutocomplete_hidden_addon}>
        <c:set var="button_class" value="col-12 col-sm-6 button white border mb-1" />
        <c:set var="text_all"><ctags:message key="button.select.all" /></c:set>
        <c:set var="text_none"><ctags:message key="button.select.none" /></c:set>
        <c:if test="${not empty selectAllTitle}">
            <c:set var="text_all">
                <ctags:message key="button.select.all.fmt.title" formatValue="${selectAllTitle}" />
            </c:set>
            <c:set var="text_none">
                <ctags:message key="button.select.none.fmt.title" formatValue="${selectAllTitle}" />
            </c:set>
        </c:if>
        <button class="${button_class}${addAllNoneButtonClass}" type="button" onClick="$('.tag-autocomplete .selected input:not(:checked)').click();">
            ${text_all}
        </button>
        <button class="${button_class}${addAllNoneButtonClass}" type="button" onClick="$('.tag-autocomplete .selected input:checked').click();">
            ${text_none}
        </button>
    </div>
    <div class="selected limited-20 text-dark">
        <c:set var="temp_previous_first_character" value="not a character" />

        <c:set var="open" value="False" />
        <c:forEach var="item" items="${autocompleteItems}" varStatus="loop">
            <c:set var="skip" value="False" />
            <c:if test="${groupByParent}">
                <c:set var="group" value="True" />
                <c:choose>
                    <c:when test="${parentMap.containsKey(item.getId())}">
<%--                        <c:set var="skip" value="True" />--%>
                        <c:set var="lastParent" value="${parentMap.get(item.getId())}" />
                        <c:if test="${lastParent.getChildren().size() == 0}"><c:set var="group" value="False" /><c:set var="skip" value="False" /></c:if>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${(lastParent == null || not lastParent.containsChild(item))}">
                            <c:set var="group" value="False" />
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </c:if>
            <c:if test="${not skip}">
                <c:set var="iid" value="${item.getId().toString()}" />
                <c:set var="uid" value="${name}-${iid}-${loop.index}" />

                <c:set var="checked" value="" />
                <c:set var="labelAddon" value="" />
                <c:set var="divAddon" value="" />
                <c:if test="${not empty addContainerClass}" >
                    <c:set var="divAddon" value=" ${addContainerClass}" />
                </c:if>
                <c:if test="${not empty selectedItems && selectedItems.containsKey(iid)}">
                    <c:set var="checked" value="checked='checked'" />
                    <c:set var="divAddon" value="${divAddon} startHidden" />
                </c:if>

                <c:set var="temp_item_name" value="${item.getName()}" />

                <c:choose>
                    <c:when test="${not uneditable}">
                        <c:set var="for_id" value="${uid}" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="labelAddon" value="${labelAddon} inactive" />
                        <c:set var="divAddon" value="${divAddon} no-icon"/>
                    </c:otherwise>
                </c:choose>

                <c:set var="des" value="${item.getDescription()}" />
                <c:set var="info_id" value="${uid}-info" />
                <c:set var="info" value="" />
                <c:if test="${0 < des.length()}">
                    <c:set var="info">
                        <input id="${info_id}" type="checkbox" class="hidden expand-check" />
                        <label for="${info_id}"
                               class="expandable button col bgc-white hovering-info centered">${des}</label>
                    </c:set>
                </c:if>

                <c:set var="label">
                    <div class="col-12 p-0 white pr-1 bgc-dark-primary display-filterable selected-label mb-1 ${flexwrap}${divAddon}">
                        <c:set var="filter_data" value=""/>
                        <c:set var="button_text" value="${item.getName()}"/>
                        <c:forEach var="filter" items="${item.getFilters().values()}" varStatus="filterLoop">
                            <c:if test="${loop.first}">
                                <c:set var="filter_attributes"><c:if
                                        test="${not filterLoop.first}">${filter_attributes}, </c:if>data-${filter.getId()}
                                </c:set>
                            </c:if>

                            <c:set var="filter_data"
                                   value="${filter_data} data-${filter.getId()}=\"${filter.getValue()}\""/>
                            <c:if test="${filter.displayValue()}">
                                <c:set var="button_text">
                                    ${item.getName()}
                                    <span class="${filter.getId()}">
                                        <c:if test="${filter.displayTitle()}">
                                            <span class="title"><ctags:message key="${filter.getId()}.display"/>: </span>
                                        </c:if>
                                        <span class="content">${filter.getValue()}</span>
                                    </span>
                                </c:set>
                            </c:if>
                        </c:forEach>

                        <c:if test="${groupByParent && parentMap.containsKey(item.getId()) && parentMap.get(item.getId()).getChildren().size() > 0}">
<%--                                <c:set var="button_text"><ctags:message key="rubriek.search.all" /> ${button_text}</c:set>--%>
                                <c:set var="info_text">
                                    <ctags:message key="rubriek.search.all.format.info" formatValue="${button_text}" />
                                </c:set>
                                <c:set var="button_text">
                                    <ctags:message key="rubriek.search.all.format" formatValue="${button_text}" />
                                    <ctags:info message="${info_text}" iconClasses="pl-2" />
                                </c:set>
                        </c:if>
                        <ctags:checkboxButton forid="${for_id}"
                                              base_class="bgc-white filterable col button${addLabelClass}${labelAddon}"
                                              span_class="${flexwrap} justify-content-center"
                                              text="${button_text}" data="${filter_data}" />

                        <div class="icon-container">
                            <c:if test="${0 < des.length()}">
                                <ctags:checkboxButton forid="${info_id}"
                                                      additional_label_class="no-icon light-primary info-button"
                                                      text='<i class="fa fa-info-circle vertically-centered"></i>'/>
                            </c:if>
                        </div>
                    </div>
                </c:set>

                <div class="col-12">
                    <input type="checkbox" id="${uid}" name="${name}[]" class="hidden selected-check" value="${iid}"${checked}${editable} />
                    ${label}
                </div>

                <%--<c:set var="labels" value="${labels} ${label}" />--%>
                <c:set var="labels">
                    ${labels}
                    <c:if test="${groupByParent && (!group) && open}">
                                <c:set var="open" value="False" />
                            </div>
                        </div>
                    </c:if>

                    <c:set var="temp_item_name_first_character" value="${fn:substring(temp_item_name, 0, 1)}" />

                    <c:if test="${groupByParent}"><c:set var="temp_item_name_first_character" value="${lastParent.getParent().getName()}" /></c:if>
                    <c:if test="${(groupByFirstCharacter || (groupByParent && group)) && temp_item_name_first_character != temp_previous_first_character}">

                        <c:if test="${open}">
                            <c:set var="open" value="False" />
                                </div>
                            </div>
                        </c:if>

                        <c:set var="temp_previous_first_character" value="${temp_item_name_first_character}" />
                        <c:set var="temp_first_character_check_id" value="${name}-${temp_item_name_first_character}" />
                        <c:set var="temp_name" value="" />
                        <c:set var="temp_val" value="" />
                        <c:set var="temp_checked" value="" />
                        <c:if test="${groupByParent}">
                            <c:set var="temp_id" value="${lastParent.getParent().getId()}" />
<%--                            <c:set var="temp_val" value=" value=\"${temp_id}\"" />--%>
                            <c:set var="temp_first_character_check_id" value="${name}-${temp_id}" />
<%--                            <c:set var="temp_name" value=" name=\"${name}[]\"" />--%>
                            <c:if test="${not empty selectedItems && selectedItems.containsKey(temp_id)}">
                                <c:set var="temp_checked" value=" checked='checked'" />
                            </c:if>
                        </c:if>
                        <c:set var="open" value="True" />
                        <div class="col-12 filterable-display-parent">
                            <input id="${temp_first_character_check_id}" type="checkbox"${temp_name}${temp_val}${temp_checked} class="hidden expand-check" />
                            <ctags:checkboxButton forid="${temp_first_character_check_id}" text="${temp_item_name_first_character}" additional_label_class="black col-12" />
                            <div class="expand col-12 p-0">
                    </c:if>
                    ${label}
                    <c:if test="${groupByFirstCharacter && loop.last && open}">
                            <c:set var="open" value="False" />
                            </div>
                        </div>
                    </c:if>
                </c:set>
                <c:set var="infos" value="${infos} ${info}" />
            </c:if>
        </c:forEach>
    </div>

    <div${temp_tagAutocomplete_hidden_addon}>

        <c:set var="text">
            <ctags:message key="format.filter.on" formatValue="attribute.name.display.short" />
        </c:set>
        <ctags:input name="" values="${filters}" inputclass="display-filter-input" title="${text}"
                     labelLengthClasses="col-12" labelclass="m-0"
                     data="data-filter-attribute=\"${filter_attributes}\""/>

        <div class="limited-20 bgc-white display-filterable">
            <div class="autocomplete dark-primary">
                ${labels}
            </div>
        </div>
    </div>

    <div class="info-container">${infos}</div>
</div>

<ctags:resourceImport type="css" path="css/tags/tagAutocomplete.css" />
<script>
    $('.selected-check').off('change');
    $('.selected-check').on('change', function() {
       var id = $(this).attr('id');
       var box = $('.autocomplete label[for={0}]'.format(id)).parent();
       var checked = $(this).prop('checked');
       if(checked) {
           box.addClass('startHidden');
       } else {
           box.removeClass('startHidden');
       }
    });
</script>