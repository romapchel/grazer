<%@ tag import="nl.impressie.grazer.factories.CollectionFactory" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.Map" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctags" tagdir="/WEB-INF/tags" %>

<%@ attribute name="name" required="true" type="java.lang.String" %>
<%@ attribute name="type" required="false" type="java.lang.String" %>
<%@ attribute name="title" required="false" type="java.lang.String" %>
<%@ attribute name="placeholder" required="false" type="java.lang.String" %>

<%@ attribute name="titleclass" required="false" type="java.lang.String" %>
<%@ attribute name="titleLengthClasses" required="false" type="java.lang.String" %>

<%@ attribute name="labelclass" required="false" type="java.lang.String" %>
<%@ attribute name="labelLengthClasses" required="false" type="java.lang.String" %>

<%@ attribute name="containerclass" required="false" type="java.lang.String" %>

<%@ attribute name="inputclass" required="false" type="java.lang.String" %>
<%@ attribute name="inputLengthClasses" required="false" type="java.lang.String" %>

<%@ attribute name="value" required="false" type="java.lang.String" %>
<%@ attribute name="tooltip" required="false" type="java.lang.String" %>
<%@ attribute name="alt" required="false" type="java.lang.String" %>
<%@ attribute name="min" required="false" type="java.lang.String" %>
<%@ attribute name="onInputFocus" required="false" type="java.lang.String" %>
<%@ attribute name="data" required="false" type="java.lang.String" %>

<%@ attribute name="autofocus" required="false" type="java.lang.Boolean" %>
<%@ attribute name="required" required="false" type="java.lang.Boolean" %>
<%@ attribute name="requiredInfoShow" required="false" type="java.lang.Boolean" %>

<%@ attribute name="disabled" required="false" type="java.lang.Boolean" %>
<%@ attribute name="validatable" required="false" type="java.lang.Boolean" %>
<%@ attribute name="checked" required="false" type="java.lang.Boolean" %>
<%@ attribute name="autoSelect" required="false" type="java.lang.Boolean" %>

<%@ attribute name="hideMouseOvers" required="false" type="java.lang.Boolean" %>

<%@ attribute name="errors" required="false" type="java.util.Map" %>
<%@ attribute name="values" required="false" type="java.util.Map" %>

<c:if test="${not empty title && fn:startsWith(title, 'messagecode:')}">
    <c:set var="title">
        <ctags:message key="${fn:substringAfter(title, 'messagecode:')}"/>
    </c:set>
</c:if>

<c:if test="${not empty errors && errors.containsKey(name)}">
    <c:set var="error">
        <c:forEach var="errorMap" items="${errors.get(name)}">
            <ctags:message key="${errorMap.get('error')}" formatParams="${errorMap.get('params')}" />
        </c:forEach>
    </c:set>
</c:if>
<c:set var="codeWords" value="" />
<c:set var="val" value="${value}" />
<c:if test="${not empty value}">
    <c:set var="value" value=" value='${value}'" />
</c:if>
<c:if test="${not empty values && values.containsKey(name)}">
    <c:set var="curVal" value="${fn:replace(values.get(name), '\"', \"'\")}" />
    <c:choose>
        <c:when test="${type eq 'checkbox' || type eq 'radio'}">
            <c:if test="${empty val || curVal eq val}">
                <c:set var="codeWords" value="checked='checked'" />
            </c:if>
        </c:when>
        <c:otherwise>
            <c:if test="${empty value}">
                <c:set var="value" value=' value="${curVal}"' />
            </c:if>
        </c:otherwise>
    </c:choose>
</c:if>
<c:if test="${checked}">
    <c:set var="codeWords" value="checked='checked'"/>
</c:if>

<c:if test="${autoSelect}">
    <c:set var="onInputFocus" value="$(this).select();${onInputFocus}" />
</c:if>
<c:if test="${not empty onInputFocus}">
    <c:set var="onInputFocus" value=" onFocus=\"${onInputFocus}\"" />
</c:if>

<c:if test="${empty titleLengthClasses}">
    <c:set var="titleLengthClasses" value="col-12 col-lg-6 m-0 p-0" />
</c:if>
<c:if test="${empty inputLengthClasses}">
    <c:set var="inputLengthClasses" value="col" />
</c:if>

<c:if test="${autofocus}">
    <c:set var="codeWords" value="${codeWords} autofocus" />
</c:if>
<c:if test="${required}">
    <c:set var="codeWords" value="${codeWords} required" />
    <c:set var="containerclass" value="${containerclass} required" />
</c:if>
<c:if test="${disabled}">
    <c:set var="codeWords" value="${codeWords} disabled" />
    <c:set var="containerclass" value="${containerclass} disabled" />
    <c:set var="name" value="" />
</c:if>
<c:if test="${fn:length(error) > 0}">
    <c:set var="containerclass" value="${containerclass} invalid" />
</c:if>

<c:set var="type" value="${(empty type) ? 'text' : type}" />
<c:set var="inside" value="" />
<c:choose>
    <c:when test="${type == 'textarea'}">
        <c:set var="element" value="textarea" />
        <c:set var="inside" value="${value.substring(8, value.length()-1)}" />
    </c:when>
    <c:otherwise>
        <c:set var="element" value="input" />
    </c:otherwise>
</c:choose>
<c:if test="${not empty min}">
    <c:set var="min" value=" min='${min}'" />
</c:if>
<c:if test="${not empty name}">
    <c:set var="name" value=" name='${name}'" />
</c:if>
<c:if test="${not empty alt}">
    <c:set var="alt" value=" alt='${alt}'" />
</c:if>
<c:if test="${not empty placeholder}">
    <c:set var="placeholder"><ctags:message key="${placeholder}" /></c:set>
    <c:set var="placeholder" value=" placeholder='${placeholder}'" />
</c:if>
<c:if test="${not empty data}">
    <c:set var="data" value=" ${data}" />
</c:if>

<c:if test="${type eq 'hidden'}">
    <c:set var="labelclass" value="${labelclass} hidden" />
</c:if>

<c:if test="${type eq 'no-label'}">
    <c:set var="showNoLabel" value="true" />
    <c:set var="type" value="text" />
</c:if>

<c:if test="${empty labelLengthClasses}">
    <c:set var="labelLengthClasses" value="col-12" />
</c:if>


<c:if test="${not empty type}">
    <c:set var="type" value=" type='${type}'" />
</c:if>

<c:if test="${empty validatable || validatable}">
    <c:set var="labelLengthClasses" value="${labelLengthClasses} validatable-input-label" />
    <c:set var="inputLengthClasses" value="${inputLengthClasses} validatable" />
</c:if>

<label class="${labelLengthClasses} ${labelclass}">
    <div class="vertically-centered d-flex flex-wrap p-0 m-0 ${containerclass}">
        <c:if test="${showNoLabel ne 'true'}">
            <div class="${titleLengthClasses} ${titleclass}"><ctags:message key="${title}" />
                <c:if test="${requiredInfoShow !=  false}"> *</c:if>
            </div>
        </c:if>

        <c:set var="autocomplete"></c:set>
        <c:if test="${element == 'input'}"><c:set var="autocomplete">autocomplete="off"</c:set></c:if>
        <${element}${type}${name}${codeWords}${placeholder}${alt}${value}${min}${onInputFocus} class='${inputLengthClasses} ${inputclass}'${data} ${autocomplete}>${inside}</${element}>
        <c:if test="${fn:length(tooltip) > 0}">
            <div class="ttip message">
                <c:forEach var="line" items="${fn:split(tooltip, '.')}">
                    <c:if test="${fn:length(line) > 0}">
                        <p>${line}.</p>
                    </c:if>
                </c:forEach>
            </div>
        </c:if>

        <div class="mouse_overs d-flex<c:if test="${hideMouseOvers}"> hidden</c:if>">
            <c:if test="${fn:length(error) > 0}">
                <div class="mouseover error">
                    <div>
                        <div class="mouseover-msg">
                            ${error}
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${empty requiredInfoShow && fn:length(tooltip) > 0}">
                <div class="mouseover tip">
                    <div class="mouseover-msg">
                        ${tooltip}
                    </div>
                </div>
            </c:if>
        </div>

        <c:if test="${false}">
            <c:if test="${empty requiredInfoShow && fn:length(tooltip) > 0}">
                <div class="ttip message">
                    <c:forEach var="line" items="${fn:split(tooltip, '.')}">
                        <c:if test="${fn:length(line) > 0}">
                            <p>${line}.</p>
                        </c:if>
                    </c:forEach>
                </div>
            </c:if>
            <div class="error message"><p>${error}</p></div>
        </c:if>
    </div>
</label>