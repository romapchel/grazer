<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctags" tagdir="/WEB-INF/tags" %>

<%@ attribute name="button" required="true" type="nl.impressie.grazer.model.MenuButton" %>

<a href="<c:url value="${button.getUrl()}" />" class="menuOption white">
    <p>
        <ctags:message key="${button.getMessageCode()} "/>
    </p>
</a>