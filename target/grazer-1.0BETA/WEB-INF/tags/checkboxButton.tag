<%--
  Created by IntelliJ IDEA.
  User: Julian
  Date: 2018-05-08
  Time: 09:55
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="ctags" tagdir="/WEB-INF/tags" %>

<%@ attribute name="forid" required="true" type="java.lang.String" %>
<%@ attribute name="text" required="true" type="java.lang.String" %>

<%@ attribute name="additional_label_class" required="false" type="java.lang.String" %>
<%@ attribute name="base_class" required="false" type="java.lang.String" %>
<%@ attribute name="span_class" required="false" type="java.lang.String" %>
<%@ attribute name="activated_class" required="false" type="java.lang.String" %>

<%@ attribute name="data" required="false" type="java.lang.String" %>

<c:if test="${not empty span_class}"><c:set var="span_class" value=" ${span_class}"/></c:if>
<c:if test="${not empty data}"><c:set var="data" value=" ${data}"/></c:if>
<c:if test="${empty base_class}"><c:set var="base_class" value="button expand-label no-caps" /></c:if>
<c:if test="${not empty additional_label_class && not empty base_class}"><c:set var="additional_label_class" value=" ${additional_label_class}" /></c:if>
<c:if test="${empty activated_class}"><c:set var="activated_class" value=""/></c:if>

<c:set var="temp_checkboxButton_label_class" value="${base_class}${additional_label_class}" />
<label for="${forid}" class="${temp_checkboxButton_label_class} noJavaScript-only"${data}>
    <span class="col"><ctags:message key="${text}" /></span>
</label>
<button type="button" class="${temp_checkboxButton_label_class} noJavaScript-hidden ${activated_class}"
        <c:if test="${forid.length() > 0}"> onClick="$('#${forid}').click()"</c:if>${data}>
    <span class="inherit_font_size${span_class}"><ctags:message key="${text}" /></span>
</button>
