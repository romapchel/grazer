<%@ tag %>

<%@ attribute name="lastSearch" required="false" type="java.util.List" %>
<%@ attribute name="items" required="true" type="java.util.List" %>
<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="name" required="true" type="java.lang.String" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>
<%@ attribute name="aditional_container_classes" required="false" type="java.lang.String" %>

<%@taglib prefix="cfn" uri="/WEB-INF/jstl-functions.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="ctags" tagdir="/WEB-INF/tags"%>

<div class="expandable col-12 p-0 strongly-rounded ${aditional_container_classes}">
    <c:set var="facetMapType" value="" />
    <c:set var="facetType" value="${name}facets" />
    <c:if test="${not empty facetMap}"><c:set var="facetMapType" value="${facetMap.get(facetType)}" /></c:if>
    <c:set var="checked" value="" />
    <%--<c:if test="${not search.isTagEmpty(name)}"><c:set var="checked" value=" checked=\"checked\"" /></c:if>--%>
    <c:set var="inputId" value="${id}-expand-check" />
    <c:if test="${not empty param[inputId]}"><c:set var="checked" value=" checked=\"checked\"" /></c:if>
    <input id="${inputId}" name="${inputId}" type="checkbox" class="hidden expand-check"${checked} />
    <ctags:checkboxButton forid="${id}-expand-check" text="${title}" additional_label_class="col-12 m-0" />
    <div class="col-12 bgc-halflight-primary border-square expand limited-20 light-primarys">
        <c:forEach var="item" items="${items}" varStatus="itemLoop">
            <c:set var="check_id" value="${id}-${itemLoop.index}" />
            <c:set var="amount" value="" />
            <c:set var="classAddon" value="" />
            <c:if test="${not empty facetMapType}">
                <c:choose>
                    <c:when test="${facetMapType.containsKey(item.getId())}">
                        <c:set var="amount" value=" (${facetMapType.get(item.getId())})" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="amount" value=" (0)" />
                        <c:set var="classAddon" value=" hidden" />
                    </c:otherwise>
                </c:choose>
            </c:if>
            <c:set var="checked" value="" />
            <c:if test="${search.containsTag(name, item.getId())}"><c:set var="checked" value=" checked=\"checked\"" /></c:if>
            <input id="${check_id}"${checked} name="${name}" type="checkbox" class="hidden item-check autosubmit" value="${item.getId()}" />
            <ctags:checkboxButton forid="${check_id}" text="${item.getName()}${amount}"
                                  base_class="button bgc-override white item-label${classAddon}"/>
        </c:forEach>
    </div>
</div>