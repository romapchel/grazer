<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <h1 class="${page_title_class}">
            <ctags:message key="tac.header"/>
        </h1>

        <p>
            <ctags:message key="tac.content"/>
        </p>

    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>