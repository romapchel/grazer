<%@ page import="nl.impressie.grazer.spring.beans.Captcha" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/validation.css"/>
<div class="screen vertically-centered bgc-grey">
    <div class="${content_size_class} shadowed">
        <h1 class="${page_title_class} m-0">
            <ctags:message key="register.header"/>
        </h1>
        <div class="col-12 text-center p-0 ${flexwrap} justify-content-center">
            <div class="col-12 text-left mb-3 mt-3">
                <p>
                    <ctags:message key="user.paragraph.2"/>
                </p>
                <ctags:message key="user.paragraph.3"/>
            </div>
            <p class="col-12">
                <ctags:message key="user.paragraph.1"/>
            </p>
        </div>

        <form action="${base_url}register" class="validatable" method="POST">
            <div>
                <c:set var="values" value="${previous}" scope="request" />
                <c:set var="errors" value="${errors}" scope="request" />
                <%@include file="/WEB-INF/jsp/user/fields.jsp" %>

                <ctags:field fieldMap="${fields.get(UserDao.USERNAME)}" />
                <ctags:field fieldMap="${fields.get(UserDao.PASSWORD)}" />
                <ctags:field fieldMap="${fields.get(UserDao.PASSWORD.concat('-repeat'))}" />
                <ctags:field fieldMap="${fields.get(UserDao.ORGANISATION)}" />
                <ctags:field fieldMap="${fields.get(UserDao.INITIALS)}" />
                <ctags:field fieldMap="${fields.get(UserDao.TUSSENVOEGSELS)}" />
                <ctags:field fieldMap="${fields.get(UserDao.LASTNAME)}" />
                <ctags:field fieldMap="${fields.get(UserDao.STREET)}" />
                <ctags:field fieldMap="${fields.get(UserDao.HOUSENUMBER)}" />
                <ctags:field fieldMap="${fields.get(UserDao.ZIP)}" />
                <ctags:field fieldMap="${fields.get(UserDao.CITY)}" />
                <ctags:field fieldMap="${fields.get(UserDao.MAIL)}" />

                <div id="captcha" <c:if test="${errors.containsKey(Captcha.PARAM_NAME)}"> class="error"</c:if>>
                    <c:if test="${errors.containsKey(Captcha.PARAM_NAME)}">
                        <div class="error">
                            <c:forEach var="errorMap" items="${errors.get(Captcha.PARAM_NAME)}">
                                <ctags:message key="${errorMap.get('error')}" formatParams="${errorMap.get('params')}" />
                            </c:forEach>
                        </div>
                    </c:if>
                    <%= Captcha.getForm() %>
                </div>

            </div>

            <button type="submit" class="button submit-button bordered col-12 centered bgc-light-primary text-white">
                <ctags:message key="header.link.register"/>
            </button>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>