<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/validation.css"/>
<script>
    /*function customvalidation(input) {
        var short = '{0} moet minimaal {1} characters lang zijn';
        var unique = 'Deze {0} bestaat al';

        switch(input.attr('name')) {
            case 'username':
                testLength(input, 'De gebruikersnaam', 3);
                if(!input.parent().hasClass('invalid')) {
                    $.ajax({ type: "post", url: base_url + 'account/exists', data: {'username': value}, dataType: 'json' }).done(function(data) { log(data); if(data) { addError(input, unique.format('gebruikersnaam')); } }).fail(function(jqXHR, textStatus, errorThrown) { log(errorThrown); });
                }
                break;
            case 'password':
                testLength(input, 'Het wachtwoord', 5);
                break;
        }
    }
    function testLength(input, name, length) {
        var value = input.val();
        if(value.length > 0 && value.length < length) {
            addError(input, short.format(name, length));
        }
    }*/
</script>
<div class="screen vertically-centered bgc-grey">
    <div class="${content_size_class} justify-content-center shadowed">
        <h1 class="${page_title_class} m-0">
            <ctags:message key="user.header" />
        </h1>
        <p>
            <ctags:message key="user.paragraph.1" />
        </p>
        <c:set var="subheader_class" value="font-weight-bold"/>
        <form action="${base_url}user/edit" class="validatable" method="POST">
            <div>
                <input type="hidden" name="id" value="${uid}" />
                <p class="${subheader_class}">
                    <ctags:message key="user.subheader.1" />
                </p>
                <c:set var="values" value="${current}" scope="request" />
                <c:set var="errors" value="${errors}" scope="request" />
                <%@include file="/WEB-INF/jsp/user/fields.jsp" %>

                <ctags:field fieldMap="${fields.get(UserDao.ORGANISATION)}" />
                <ctags:field fieldMap="${fields.get(UserDao.INITIALS)}" />
                <ctags:field fieldMap="${fields.get(UserDao.TUSSENVOEGSELS)}" />
                <ctags:field fieldMap="${fields.get(UserDao.LASTNAME)}" />

                <p class="${subheader_class}">
                    <ctags:message key="user.subheader.2" />
                </p>
                <ctags:field fieldMap="${fields.get(UserDao.MAIL)}" />
                <ctags:field fieldMap="${fields.get(UserDao.STREET)}" />
                <ctags:field fieldMap="${fields.get(UserDao.HOUSENUMBER)}" />
                <ctags:field fieldMap="${fields.get(UserDao.ZIP)}" />
                <ctags:field fieldMap="${fields.get(UserDao.CITY)}" />

                <p class="${subheader_class}">
                    <ctags:message key="user.subheader.3" />
                </p>
                <ctags:field fieldMap='<%= ((FieldMap)
                                                    ((Map)
                                                        request.getAttribute("fields"))
                                                    .get(UserDao.PASSWORD))
                                            .put(FieldMap.Attribute.REQUIRED, false) %>' />
                <ctags:field fieldMap='<%= ((FieldMap)
                                                    ((Map)
                                                        request.getAttribute("fields"))
                                                    .get(UserDao.PASSWORD + "-repeat"))
                                            .put(FieldMap.Attribute.REQUIRED, false) %>' />

                <p class="${subheader_class}">
                    <ctags:message key="user.subheader.4"/>
                </p>
                <c:set var="title"><ctags:message key="user.input.password.current.title"/></c:set>
                <c:set var="tooltip">
                    <ctags:message key="tooltip.required"/>
                    <ctags:message key="user.input.password.current.tooltip"/>
                </c:set>
                <ctags:input name="oldpassword" title="${title}" placeholder="${title}" type="password" required="true"
                             tooltip="${tooltip}" values="${current}" errors="${errors}"/>
            </div>

            <button type="submit" class="button submit-button bordered col-12 centered bgc-light-primary text-white">
                <ctags:message key="user.button.submit"/>
            </button>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>