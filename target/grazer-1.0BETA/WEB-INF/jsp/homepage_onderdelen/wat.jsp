<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2020-01-07
  Time: 15:43
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="wat" class="screen vertically-centered" style="border: 0; padding: 10px;">
    <div class="${container_class}">
        <div class="${content_size_class} shadowed">
            <h1 class="caps dark-primary col-12">
                <ctags:message key="home.wat.header" />
            </h1>
            <div class="grazer col-12 ${flexwrap}">
                <div class="col-12 text-center">
                    <ctags:message key="home.wat.content"/>
                </div>
            </div>

        </div>
    </div>
</div>