<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2020-01-07
  Time: 15:43
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="explain" class="screen vertically-centered bgc-grey">
    <div class="${container_class}">
        <div class="${content_size_class} bgc-grey d-flex">

            <h1 class="caps bgc-grey mt-2 mb-2">
                <ctags:message key="home.explain.header"/>
            </h1>
            <div class="row col-12 p-0">

                <div class="col-12 col-md-4">

                    <p class="centered">
                        <img src="${base_url}img/IconImages/icon-easy-steps-1.svg" />
                    </p>
                    <div class="centered col-12 d-md-none p-0">
                        <ctags:message key="home.explain.paragraph.1"/>
                    </div>

                </div>

                <div class="col-12 col-md-4">

                    <p class="centered">
                        <img src="${base_url}img/IconImages/icon-easy-steps-2.svg" />
                    </p>
                    <div class="centered col-12 d-md-none p-0">
                        <ctags:message key="home.explain.paragraph.2"/>
                    </div>

                </div>

                <div class="col-12 col-md-4">

                    <p class="centered">
                        <img src="${base_url}img/IconImages/icon-easy-steps-3.svg" />
                    </p>
                    <div class="centered col-12 d-md-none p-0">
                        <ctags:message key="home.explain.paragraph.3"/>
                    </div>

                </div>

            </div>

            <div class="d-none d-md-flex row col-12 p-0">

                <div class="col-4 centered">
                    <ctags:message key="home.explain.paragraph.1"/>
                </div>
                <div class="col-4 centered">
                    <ctags:message key="home.explain.paragraph.2"/>
                </div>
                <div class="col-4 centered">
                    <ctags:message key="home.explain.paragraph.3"/>
                </div>

            </div>

        </div>
    </div>
</div>
