<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-05-22
  Time: 16:42
--%>
<%@ page contentType="text/xml;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${not empty profile}">
    <?xml version="1.0" encoding="utf-8"?>
    <profile>
        <exportdate>${date}</exportdate>
        <name>${profile.getName()}</name>
        <c:set var="tags" value="${profile.getTags().values()}" />
        <c:if test="${0 < tags.size()}">
            <tags>
                <c:forEach var="tag" items="${tags}">
                    <tag>
                        <id>${tag.getId()}</id>
                        <type>${tag.getType()}</type>
                        <name>${tag.getName()}</name>
                    </tag>
                </c:forEach>
            </tags>
        </c:if>
        <c:set var="sites" value="${profile.getSites().values()}" />
        <c:if test="${0 < sites.size()}">
            <sites>
                <c:forEach var="site" items="${sites}">
                    <site>
                        <id>${site.getId()}</id>
                        <url>${site.getUrl()}</url>
                        <name>${site.getName()}</name>
                    </site>
                </c:forEach>
            </sites>
        </c:if>
        <titlesearch>
                ${profile.getTitleSearchString()}
        </titlesearch>
        <contentsearch>
                ${profile.getContentSearchString()}
        </contentsearch>
    </profile>
</c:if>