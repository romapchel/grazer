<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0">
            <div class="col-12 p-0 ${flexwrap}">
                <div class="col-12 p-0 mt-4 ${flexwrap}">
                    <c:choose>
                        <c:when test="${not empty profile}">
                            <c:set var="container" value="col-12 col-lg-6 ${flexwrap} flex-column" />
                            <c:set var="link_container" value="col-12 bgc-light-primary text-center p-0 pr-1 align-items-center ${flexwrap}" />
                            <c:set var="link" value="col button white" />
                            <div class="${container}">
                                <div class="bgc-dark-primary white col-12" style="flex: 1 1 auto; flex-grow: 1">
                                    <ctags:message key="profile.create.landing.search.tooltip" />
                                </div>
                                <div class="${link_container}" style="flex: 0 0 auto;">
                                    <a href="${base_url}search?${ProfileDao.TABLE_NAME}-${ProfileDao.ID}=${profile.id}" class="${link}">
                                        <ctags:message key="profile.create.landing.search.button" />
                                    </a>
                                </div>
                            </div>
                            <div class="${container}">
                                <div class="bgc-dark-primary white col-12" style="flex: 1 1 auto; flex-grow: 1">
                                    <ctags:message key="profile.create.landing.configure.tooltip" />
                                </div>
                                <div class="${link_container}" style="flex: 0 0 auto;">
                                    <a href="${base_url}profile/show?id=${profile.id}&action_edit=true&<%= Selections.PAGE %>=2" class="${link}">
                                        <ctags:message key="profile.create.landing.configure.button" />
                                    </a>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div>
                                <ctags:message key="profile.create.landing.denied.format.link" link="${base_url}profile" />
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>