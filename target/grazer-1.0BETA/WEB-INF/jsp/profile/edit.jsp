<%@ page import="nl.impressie.grazer.factories.CollectionFactory" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-05-23
  Time: 15:13
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<div class="col-12 d-flex justify-content-center mb-4">
    <div class="${content_size_class} p-0">



        <form id="update-profile-form" class="screen p-0 ajaxable" action="${base_url}profile/save"
              method="GET">
            <c:set var="siteType" value="<%= ProfileDao.PROFILE_SITE %>" />
            <c:set var="tagType" value="<%= ProfileDao.PROFILE_TAG %>" />

            <c:set var="page" value="${pagination.getPage(current_page)}" />
            <c:set var="message_code" value="${page.getMessageCode()}"/>
            <c:set var="page_content_type" value="${page.getContentType()}" />

            <c:set var="autocomplete" value="true" />
            <c:choose>
                <c:when test="${page_content_type eq siteType}">
                    <c:set var="selectedItems" value="${active.getSites()}" scope="request" />
                    <c:set var="autocompleteItems" value="${allSites.values()}" scope="request" />
                </c:when>
                <c:when test="${page_content_type eq tagType}">
                    <c:set var="selectedItems" value="${active.getTags()}" scope="request" />
                    <c:set var="autocompleteItems" value="${allTags.get(page.getType())}" scope="request" />
                </c:when>
                <c:otherwise>
                    <c:set var="autocomplete" value="false" />
                </c:otherwise>
            </c:choose>

            <%--<input type="hidden" name="id" value="${active.getId()}" />--%>
            <input id="edit_page_number" type="hidden" name="<%= Selections.PAGE %>" value="${current_page}" />

            <%-- Create selection screen --%>
            <div class="col-12 p-0">
                <c:set var="title">messagecode:<%= ProfileDao.PROFILE_NAME %>.display</c:set>
                <ctags:input name="<%= ProfileDao.PROFILE_NAME %>" value="${active.getName()}" title="${title}"
                             containerclass="col-6 p-0" labelclass="${flexwrap} col-12"
                             titleLengthClasses="col-12 p-0" titleclass="" inputLengthClasses="col-12"/>

                <h1 class="${page_title_class}">
                    <ctags:message key="${page.getMessageCode()}.display"/>
                    <div class="mouseover mouseover-below tip d-inline-block" style="font-size:0.4em;">
                        <div class="mouseover-msg">
                    <span style="text-transform: none;font-size: 16px;">
                        <ctags:message key="${page.getMessageCode()}.description"/>
                    </span>
                        </div>
                    </div>
                </h1>

                <div class="col-12 white p-0 mt-4">
                    <c:choose>
                        <c:when test="${autocomplete}">
                            <c:set var="temp_text">
                                <ctags:message key="${page.getMessageCode()}.display.short" />
                            </c:set>

                            <ctags:optionList addAllNoneButtonClass="bgc-light-primary" addContainerClass="no-icon" name="items" selectAllTitle="${temp_text}" groupByParent="${true}" subRechtsgebieden="${page.isShowChildren() ? 'true' : 'false'}" filterByParents="${page.getType() == Tag.RECHTSGEBIED && page.isShowChildren() ? 'true' : 'false'}" addParentName="${current_page == 2 ? true : false}" />

                            <script>
                                jQuery(".list-button").click(function() {
                                    var method = "";
                                    // switch on off
                                    if ($(this).hasClass("active")) {
                                        $(this).removeClass("active");
                                        method = "off";
                                    } else {
                                        $(this).addClass("active");
                                        method = "on";
                                    }
                                });

                            </script>

                        </c:when>
                        <c:otherwise>
                            <c:set var="title">messagecode:<%= ProfileDao.TITLE_SEARCH %>.display</c:set>
                            <ctags:input name="<%= ProfileDao.TITLE_SEARCH %>" title="${title}"
                                         value="${active.getTitleSearchString()}"/>

                            <c:set var="title">messagecode:<%= ProfileDao.CONTENT_SEARCH %>.display</c:set>
                            <ctags:input name="<%= ProfileDao.CONTENT_SEARCH %>" title="${title}"
                                         value="${active.getContentSearchString()}"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>

            <%-- pagination --%>
            <c:set var="base" value="button col-12 col-sm-5 white font-weight-bold" />
            <c:set var="nextPrev" value="bgc-dark-primary p-3 mt-4" />
            <div class="col-12 p-0">
                <div class="col-12 ${flexwrap} justify-content-between p-0">
                    <c:if test="${1 < current_page}">
                        <button type="submit" class="${base} ${nextPrev} update_page" data-page="#edit_page_number" name="<%= Selections.ACTION %>" value="<%= Actions.PREV_PAGE %>">
                            <ctags:message key="profile.pagination.format.type.display" formatValue="${pagination.getPage(current_page - 1).getMessageCode()}.display" />
                        </button>
                    </c:if>

                    <c:choose>
                        <c:when test="${pagination.size() > current_page}">
                            <button type="submit" class="${base} ${nextPrev} ml-auto update_page" data-page="#edit_page_number" name="<%= Selections.ACTION %>" value="<%= Actions.NEXT_PAGE %>">
                                <ctags:message key="profile.pagination.format.type.display" formatValue="${pagination.getPage(current_page + 1).getMessageCode()}.display" />
                            </button>
                        </c:when>
                        <c:otherwise>
                            <button type="submit" class="${base} ${nextPrev} ml-auto update_page hover-white no-ajax" name="<%= Selections.ACTION %>" value="<%= Actions.NEXT_PAGE %>">
                                <ctags:message key="profile.pagination.button.finish" />
                            </button>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>

            <div class="col-12 pagination justify-content-center">
                <%--        <c:if test="${1 < current_page}">--%>
                <%--            <button type="submit" class="${base} update_page" data-page="#edit_page_number" name="<%= Selections.ACTION %>" value="<%= Actions.PREV_PAGE %>">--%>
                <%--                <ctags:message key="profile.pagination.format.type.display" formatValue="${pagination.getPage(current_page - 1).getMessageCode()}.display" />--%>
                <%--            </button>--%>
                <%--        </c:if>--%>

                <span class="col-12 col-sm-2 font-weight-bold justify-content-center align-items-center ${flexwrap}" style="padding: 0.5em;">
            <c:set var="list" value="<%= CollectionFactory.createList() %>" />
            <c:set var="void" value="${list.add(current_page)}" />
            <c:set var="void" value="${list.add(pagination.size())}" />
            <ctags:message key="profile.pagination.format.page1.page2" formatValue="${list}" />
        </span>

                <%--        <c:choose>--%>
                <%--            <c:when test="${pagination.size() > current_page}">--%>
                <%--                <button type="submit" class="${base} update_page" data-page="#edit_page_number" name="<%= Selections.ACTION %>" value="<%= Actions.NEXT_PAGE %>">--%>
                <%--                    <ctags:message key="profile.pagination.format.type.display" formatValue="${pagination.getPage(current_page + 1).getMessageCode()}.display" />--%>
                <%--                </button>--%>
                <%--            </c:when>--%>
                <%--            <c:otherwise>--%>
                <%--                <button type="submit" class="${base} update_page hover-white no-ajax" name="<%= Selections.ACTION %>" value="<%= Actions.NEXT_PAGE %>">--%>
                <%--                    <ctags:message key="profile.pagination.button.finish" />--%>
                <%--                </button>--%>
                <%--            </c:otherwise>--%>
                <%--        </c:choose>--%>
            </div>
        </form>

    </div>
</div>
