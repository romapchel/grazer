<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-09-24
  Time: 15:59
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<p class="col-9 p-0 m-0">
    <c:choose>
        <c:when test="${not empty temp_include_global_profiles && temp_include_global_profiles == true}">
            <c:set var="temp_usable_profiles" value="${user.getUsableProfiles()}"/>
        </c:when>
        <c:otherwise>
            <c:set var="temp_usable_profiles" value="${user.getProfiles()}"/>
        </c:otherwise>
    </c:choose>


    <c:choose>
        <c:when test="${temp_usable_profiles.size() > 0}">
            <c:set var="temp_active_profile_id" value="0"/>
            <c:if test="${empty profile}">
                <c:set var="profile" value="${user.getActiveProfile()}"/>
            </c:if>
            <c:if test="${not empty profile}">
                <c:set var="temp_active_profile_id" value="${profile.getId()}"/>
            </c:if>
            <c:if test="${not empty temp_select_dropdown_pre}">
                <span class="mobile-text-smaller">
                        ${temp_select_dropdown_pre}
                </span>
            </c:if>
            <select id="select_profile" name="<%= ProfileDao.TABLE_NAME %>-<%= ProfileDao.ID %>">
                <c:forEach var="profiel" items="${temp_usable_profiles}">
                    <c:set var="temp_current_profile_id" value="${profiel.getId()}"/>
                    <c:set var="temp_selected" value=""/>
                    <c:if test="${temp_current_profile_id eq temp_active_profile_id}">
                        <c:set var="temp_selected" value='selected="selected"'/>
                    </c:if>
                    <option value="${temp_current_profile_id}"${temp_selected}>
                            ${profiel.getName()}
                    </option>
                </c:forEach>
            </select>
            <c:if test="${not empty temp_select_dropdown_post}">
                <span>
                        ${temp_select_dropdown_post}
                </span>
            </c:if>
            <button type="submit" class="button noJavaScript-only">
                <ctags:message key="search.button.select.submit"/>
            </button>
        </c:when>
        <c:otherwise>
            <span>
                <ctags:message key="search.settings.paragraph.1.empty.select"/>
            </span>
        </c:otherwise>
    </c:choose>
</p>

<script>
    $(document).ready(function () {
        var profileselect = $('#select_profile');

        profileselect.on('change', function () {
            profileselect.parents('form').submit();
        });
    });
</script>