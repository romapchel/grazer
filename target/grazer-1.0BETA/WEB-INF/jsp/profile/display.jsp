<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-07-24
  Time: 16:03
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/libraries.jsp" %>


<c:set var="editableProfile" value="false" scope="request"/>

<div class="screen vertically-centered display">
    <div class="${content_size_class} shadowed justify-content-center">
        <c:set var="siteContentType" value="<%= ProfileDao.PROFILE_SITE %>"/>
        <c:set var="tagContentType" value="<%= ProfileDao.PROFILE_TAG %>"/>
        <c:set var="mailContentType" value="<%= ProfileDao.MAIL_OPTION%>"/>

        <c:set var="temp_title_search" value="${temp_display_profile.getTitleSearchString()}" />
        <c:set var="temp_content_search" value="${temp_display_profile.getContentSearchString()}" />
        <c:set var="display_temp_empty_search" value="${fn:length(temp_title_search) == 0 && fn:length(temp_content_search) == 0}" />

        <c:set var="last_type" value="" />
        <c:forEach var="page" items="${temp_display_pagination.getPages()}">
            <c:set var="temp_autocomplete" value="False"/>

            <c:set var="content_type" value="${page.getContentType()}"/>
            <c:set var="type" value="${page.getType()}"/>

            <c:set var="isNew" value="${type != last_type}"/>
            <c:set var="last_type" value="${type}"/>

            <c:if test="${isNew}">

                <c:choose>
                    <c:when test="${content_type eq siteContentType}">
                        <c:set var="selectedItems" value="${temp_display_profile.getSites()}" scope="request"/>
                        <c:set var="temp_autocomplete" value="True"/>
                    </c:when>
                    <c:when test="${content_type eq tagContentType}">
                        <c:set var="selectedItems" value="${temp_display_profile.getTagsByType(type)}" scope="request"/>
                        <c:set var="temp_autocomplete" value="True"/>
                    </c:when>
                    <c:when test="${content_type eq mailContentType}">
                        <c:set var="selectedItems" value="${temp_display_profile.getMailOption()}" scope="request"/>
                        <c:set var="temp_autocomplete" value="True"/>
                    </c:when>
                </c:choose>

                <c:set var="autocompleteItems" value="${selectedItems.values()}" scope="request"/>
                <c:set var="display_tempt_check_id" value="page-${page.getPagenumber()}"/>

                <c:set var="display_temp_empty_autocomplete" value="${autocompleteItems.size() == 0}"/>
                <c:set var="display_tempt_add_label_class" value=""/>
                <c:set var="display_tempt_add_div_class" value=""/>
                <c:choose>
                    <c:when test="${(temp_autocomplete && display_temp_empty_autocomplete) || (not temp_autocomplete && display_temp_empty_search)}">
                        <c:set var="display_tempt_add_label_class" value=" black no-icon inactive"/>
                        <c:set var="display_tempt_add_div_class" value=" bordered bc-black"/>
                        <c:set var="display_tempt_check_id" value=""/>
                    </c:when>
                    <c:otherwise>
                        <input id="${display_tempt_check_id}" type="checkbox" class="hidden expand-check"
                               checked="checked"/>
                    </c:otherwise>
                </c:choose>
                <c:set var="display_tempt_button_text">
                    <ctags:message key="${page.getMessageCode()}.display" />
                </c:set>
                <ctags:checkboxButton forid="${display_tempt_check_id}" text="${display_tempt_button_text}"
                                      additional_label_class="bgc-dark-primary caps inactive-light-primary bgc-override mt-2 no-caps col-12${display_tempt_add_label_class}"/>

                <div class="expand border-square bgc-dark-primary col-12 p-0">
                    <c:choose>
                        <c:when test="${temp_autocomplete}">
                            <c:if test="${not display_temp_empty_autocomplete}">
                                <ctags:tagAutocomplete/>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${fn:length(temp_title_search) > 0}">
                                <ctags:input name="${ProfileDao.TITLE_SEARCH}" title="Zoektermen titel"
                                             value="${temp_title_search}" disabled="true"/>
                            </c:if>

                            <c:if test="${fn:length(temp_content_search) > 0}">
                                <ctags:input name="${ProfileDao.CONTENT_SEARCH}" title="Zoektermen content"
                                             value="${temp_content_search}" disabled="true"/>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:if>
        </c:forEach>
    </div>
</div>