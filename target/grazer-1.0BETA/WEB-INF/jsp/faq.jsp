<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen faq-page">
    <div class="${content_size_class} shadowed">
        <div class="col-12 p-0">
            <h1 class="${page_title_class}">
                <ctags:message key="faq.header"/>
            </h1>

            <c:set var="temp_line_class" value="col-12 border-bottom row mb-3 m-lg-0"/>
            <c:set var="temp_question_class"
                   value="col-12 col-lg-3 border-bottom border-bottom-lg-0 border-lg-right align-items-center"/>
            <c:set var="temp_answer_class" value="col-12 col-lg-9 align-items-center"/>

            <c:forEach begin="1" end="14" varStatus="loop">
                <div class="${temp_line_class}">
                    <div class="${temp_question_class}">
                        <ctags:message key="faq.question.${loop.index}"/>
                    </div>
                    <div class="${temp_answer_class}">
                        <ctags:message key="faq.answer.${loop.index}"/>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>