<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen bgc-grey">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">

        <c:forEach var="type" items="${item_map.keySet()}" varStatus="loop">
            <c:set var="items" value="${item_map.get(type)}" scope="request" />
            <c:set var="labels">${labels}<label for="${type}-tab"
                                                class="button dark-primary button-label tab"><s:message
                    code="${type}.display"/></label></c:set>
            <c:set var="windows">
                ${windows}
                <input id="${type}-tab" type="radio" value="${type}" class="hidden type-tab expand-check"<c:if test="${(empty tab && loop.first) || tab eq type}"> checked="checked"</c:if> name="tab" />
                <div class="tab-window expandable">
                    <%@include file="/WEB-INF/jsp/tag/includes/select.jsp" %>
                </div>
            </c:set>
        </c:forEach>
        <div class="tab-container flex autosize">${labels}</div>
        <div class="tab-window-container">${windows}</div>
    </div>
</div>

<div class="screen bgc-light-primary">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">
        <c:choose>
            <c:when test="${not empty id_map}">
                <c:forEach var="item" items="${id_map.values()}" varStatus="loop">
                    <%@include file="/WEB-INF/jsp/tag/includes/edit.jsp" %>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <%@include file="/WEB-INF/jsp/tag/includes/edit.jsp" %>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<div class="screen bgc-grey">
    <div class="content col-12 col-sm-8 col-md-6 shadowed black">
        <span>Mass edit</span>
        <form action="${base_url}${TagController.TAG}/import" method="POST" enctype="multipart/form-data">
            <select name="type">
                <c:forEach var="tag" items="${Tag.getTypes()}">
                    <option>${tag}</option>
                </c:forEach>
            </select>
            <input type="file" name="csv" />
            <button type="submit">Verander</button>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>