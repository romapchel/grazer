<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 22-3-2018
  Time: 9:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<%@ page import="nl.impressie.grazer.dao.TagDao" %>

<form action="${base_url}tag/save" method="POST">
    <c:if test="${not empty item}">
        <p>
            This tag already exists and is currently being edited
        </p>
        <input type="hidden" name="<%= TagDao.ID %>" value="${item.getId()}" />
    </c:if>
    <div class="hidden">
        <c:set var="id" value="<%= TagDao.TYPE %>" />
        <c:forEach var="type" items="${item_map.keySet()}" varStatus="loop">
            <input class="type-input" type="radio" name="${id}" value="${type}"<c:if test="${loop.first}"> checked="checked"</c:if> />
        </c:forEach>
    </div>

    <div class="flex autosize">
        <c:set var="id" value="<%= TagDao.NAME %>" />
        <label for="${id}" class="button button-label">Naam:</label>
        <input id="${id}" name="${id}" type="text" class="black selectOnFocus"<c:if test="${not empty item}"> value="${item.getName()}"</c:if> />
    </div>

    <c:if test="${not empty title_map}">
        <div class="flex autosize">
            <c:set var="id" value="<%= TagDao.TYPE %>" />
            <label for="${id}" class="button button-label">Soort tag:</label>
            <select id="${id}" name="${id}" class="black" style="border-left:1px;">
                <c:forEach var="type" items="${title_map.keySet()}">
                    <option value="${type}"<c:if test="${not empty item && type eq item.getType()}"> selected="selected"</c:if>>${title_map.get(type)}</option>
                </c:forEach>
            </select>
        </div>
    </c:if>

    <div class="flex autosize">
        <c:set var="id" value="<%= TagDao.NAME %>" />
        <label for="${id}" class="button button-label">Code:</label>
        <input id="${id}" name="${id}" type="text" class="black selectOnFocus"<c:if test="${not empty item}"> value="${item.getCode()}"</c:if> />
    </div>

    <div class="flex autosize">
        <c:set var="id" value="<%= TagDao.DESCRIPTION %>" />
        <label for="${id}" class="button button-label">Beschrijving:</label>
        <input id="${id}" name="${id}" type="text" class="black selectOnFocus"<c:if test="${not empty item}"> value="${item.getDescription()}"</c:if> />
    </div>

    <button type="submit" class="black">Tag opslaan</button>
</form>

<script type="text/javascript">
    <c:forEach var="type" items="${item_map.keySet()}" varStatus="loop">
        prepType('${type}');
    </c:forEach>

    $(document).ready(function(){
       $('.type-tab').on('change', function() {
           $('.type-input[value=' + $(this).val() + ']').prop('checked', true);
       });
    });

    function prepType(type) {
        $('\#{0}-tab'.format(type)).on('click', function() {
            $('#type option[value={0}]'.format(type)).prop('selected', true);
        });
    }
</script>