<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 22-3-2018
  Time: 9:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<ctags:resourceImport type="css" path="css/item-select.css"/>
<ctags:resourceImport type="js" path="js/item-select.js"/>

<c:set var="title">
    <ctags:message key="${type}.display"/>
</c:set>
<c:set var="base_button_class" value="fa no-icon large" />
<div>
    <c:set var="items" value="${item_map.get(type)}" />
    <h1 class="light-primary">
        ${title}
    </h1>
    <form class="bordered flex" action="${base_url}tag/action" method="POST">
        <input type="hidden" name="db-filter-default" value="false" />
        <input type="hidden" name="tab" value="${type}" />

        <c:set var="def" value="${empty filters}" />

        <c:set var="checked" value=' checked="checked"' />
        <c:set var="filterCheck" value="${not def and not filters.containsKey('db-filter-enabled-hide')}" />
        <c:if test="${def or filterCheck}"><c:set var="checked" value="" /></c:if>
        <input type="checkbox" id="sites-db-filter-enabled-hide" name="db-filter-enabled-hide"${checked} class="hidden db-filter-enabled-hide display-filter-check validatable" data-select="div.hidden-button-container:has(.item:not(.db-disabled))" />

        <c:set var="checked" value=' checked="checked"' />
        <c:if test="${not def and not filters.containsKey('db-filter-disabled-hide')}"><c:set var="checked" value="" /></c:if>
        <input type="checkbox" id="sites-db-filter-disabled-hide" name="db-filter-disabled-hide"${checked} class="hidden db-filter-disabled-hide display-filter-check validatable" data-select="div.hidden-button-container:has(.item.db-disabled)" />

        <div class="padded flex between black">
            <button type="submit" name="<%= Actions.ENABLE %>" value="<%= Selections.ALL %>" title="enable" alt="Activate all pages" class="button toggle"><i class="${base_button_class} fa-toggle-off"></i>Activeer alle pagina's</button>
            <div class="centered nobreak white">
                <label for="sites-db-filter-enabled-hide" class="button db-filter-enabled-hide">Toon actieve items <span class="show-on-check"> niet</span></label>
                <label for="sites-db-filter-disabled-hide" class="button db-filter-disabled-hide">Toon inactieve items <span class="show-on-check"> niet</span></label>
            </div>
            <button type="submit" name="<%= Actions.DISABLE %>" value="<%= Selections.ALL %>" title="disable" alt="Deactivate all pages" class="button toggle"><i class="${base_button_class} fa-toggle-on"></i>Deactiveer alle pagina's</button>
        </div>

        <c:set var="text">
            <ctags:message key="format.filter.on" formatValue="attribute.name.display.short" />
        </c:set>
        <ctags:input name="db-filter-name" values="${filters}" inputclass="display-filter-input black" labelclass="black" title="${text}" placeholder="nieuws" />

        <c:set var="checkName"><%= Selections.SELECTION %>[]</c:set>
        <div class="item-list display-filterable">
            <c:forEach items="${items}" var="item" varStatus="loop">
                <c:set var="divClass" value="item sel-bgc-dark-primary bgc-light-primary white"/>
                <c:set var="linkClass" value='button white grower' />
                <c:set var="checked" value='' />

                <c:if test="${item.isDisabled() == true}"><c:set var="divClass" value='${divClass} db-disabled' /></c:if>
                <c:if test="${not empty id_map and id_map.containsKey(item.getId())}">
                    <c:set var="checked" value=' checked="checked"' />
                    <c:set var="linkClass" value='${linkClass} inactive' />
                </c:if>

                <div class="hidden-button-container">
                    <c:set var="inputId" value="${item.getType()}-${loop.index}" />
                    <c:set var="itemId" value="${item.getId()}" />

                    <input type="checkbox" name="${checkName}" id="${inputId}" value="${itemId}"  class="hidden-button selected"${checked} />
                    <div class="${divClass}">
                        <label for="${inputId}" class="${linkClass}">
                            <span>${item.getName()}</span>
<%--                            <span>(${item.getNumberOfArticles()} Nieuwsberichten)</span>--%>
                            <span>(${tagsUseByPages.containsKey(item.getId()) ? tagsUseByPages.get(item.getId()) : 0} Pagina's)</span>
                        </label>
                        <button type="submit" name="<%= Actions.TOGGLE %>" value="${itemId}" title="Toggle" alt="Toggle wether or not this tag is active" class="button white toggle"><i class="${base_button_class}"></i></button>
                        <button type="submit" name="<%= Actions.EDIT %>" value="${itemId}" title="Edit" alt="Edit this tag" class="button white"><i class="${base_button_class} fa-pencil"></i></button>
                        <button type="submit" name="<%= Actions.DELETE %>" value="${itemId}" title="Delete" alt="Delete this tag" class="button white"><i class="${base_button_class} fa-times-circle"></i></button>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="black">
            <div class="black col-12 noJavaScript-hidden">
                <c:set var="selector" value="div:not(.hidden):not([style='display: none;']) > input[name='${checkName}']" />
                <button type="button" class="selectAll" data-select="${selector}">Selecteer alles</button>
                <button type="button" class="deselectAll" data-select="${selector}">Deselecteer alles</button>
            </div>
            <div class="black col-12">
                <button type="submit" name="<%= Actions.MERGE %>" value="<%= Selections.SELECTION %>" title="Merge" alt="Merge the selected items" class="button"><i class="${base_button_class} fa-compress"></i> combineer geselecteerde tags</button>
                <button type="submit" name="<%= Actions.DELETE %>" value="<%= Selections.SELECTION %>" title="Delete" alt="Delete the selected items" class="button"><i class="${base_button_class} fa-times-circle"></i> verwijder geselecteerde tags</button>
                <button type="submit" name="<%= Actions.ENABLE %>" value="<%= Selections.SELECTION %>" title="Enable" alt="Activate the selected items" class="button"><i class="${base_button_class} fa-toggle-off"></i>activeer geselecteerde tags</button>
                <button type="submit" name="<%= Actions.DISABLE %>" value="<%= Selections.SELECTION %>" title="Disable" alt="Deactivate the selected items" class="button"><i class="${base_button_class} fa-toggle-on"></i>deactiveer geselecteerde tags</button>
            </div>
        </div>
    </form>
</div>
