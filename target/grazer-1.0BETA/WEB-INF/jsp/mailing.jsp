<%@ page import="nl.impressie.grazer.model.User.Profile" %>
<%@ page import="nl.impressie.grazer.model.User.CustomUser" %>
<%@ page import="nl.impressie.grazer.util.AuthenticationUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Roma
  Date: 27.07.2022
  Time: 12:59
  To change this template use File | Settings | File Templates.
--%>
<%@include file="/WEB-INF/jsp/includes/MailingHeader.jsp" %>
<div class="screen">
    <div class="${content_size_class} shadowed white">
        <div class="col-12" style="color: #2a7eaf;">
            <h1 class="${page_title_class}" style="color: #3AAEE0">
                E-mailattendering(en)
            </h1>
            <div style="width: 60%; margin: auto">
            <p style="color: #1b1e21">U kunt in Grazer ® Legal NL e-mailattenderingen toevoegen voor elk profiel dat u heeft gemaakt. U ontvangt dan volgens de door u aangegeven frequentie een e-mailattendering van de nieuwsitems die verschenen zijn. Het aantal nieuwsitems per e-mailattendering is gelimiteerd tot 20. Wanneer er meer zijn, dan kunt u door middel van een link onderaan de e-mailattendering direct naar Grazer® Legal NL gaan om de overige nieuwsberichten te bekijken.</p>
            </div>
                <c:forEach items="${listOfProfiles}" var="profile">
                    <form style="margin: auto" class="col-12 ${flexwrap} justify-content-center mb-1 black p-0" action="<c:url value="/mailing/save"/>"
                    >
                        <table style="border-collapse: collapse; width: 50%;" border="0">

                        <tbody>
                        <tr>
                            <td style="width: 25%;">
                                <div style="margin: auto">
                                     <p style=" text-align: left">    ${profile.getName()}<br/> </p>
                                </div>
                            </td>
                            <td style="width: 25%;">
                                <div  style="margin: auto">
                                    <select id="select_profile" name="${profile.id}" onchange="this.form.submit()">

                                        <option  id="${profile.id}" value="Daily" >Dagelijks
                                        </option>
                                        <option  id="${profile.id}" value="Every 3 days" >Elke 3 dagen
                                        </option>
                                        <option  id="${profile.id}" value="Weekly" >Wekelijks
                                        </option>
                                        <option  id="${profile.id}" value="Don`t receive" >Geen attenderingen
                                        </option>
                                        <option  style="display: none" id="${profile.id}" value="${profile.getMailOption()}" selected >${profile.getMailOption()}</option>

                                    </select>

                              </div>
                            </td>
                        </tr>

                        </tbody>
                        </table>



                            <br/>

                    </form>
                </c:forEach>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>

