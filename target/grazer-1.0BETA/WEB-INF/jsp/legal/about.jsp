<%@ page import="nl.impressie.grazer.factories.CollectionFactory" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen" style="padding-bottom: 15px;">
    <div class="${content_size_class} shadowed p-0">
        <div class="col-12">

            <div>
                    <div id="about-main-photo">
                        <div id="about-main-photo-text">
                        </div>
                    </div>
                </div>

                <div>
                    <span style="color:#84b5df;font-weight:bold;">
                        Hoe blijf ik op de hoogte van al het nieuws over mijn vak?
                    </span>
                </div>

                <div>
                    <p>
                        Bent u jurist en heeft u behoefte aan ondersteuning bij het op de hoogte blijven van het vele nieuws binnen uw vakgebied? Dan is <i>Grazer® Legal NL</i> een betaalbare én handige oplossing. Als praktiserend jurist ervaart u dagelijks dat bijblijven een hele opgaaf is. Daarom graast <i>Grazer® Legal NL</i> voor u tweemaal daags alleen het voor u relevante nieuws bij elkaar en presenteert dat overzichtelijk. Hierdoor blijft u niet alleen op de hoogte van het laatste nieuws, maar bespaart u bovendien veel tijd, doordat u niet zelf alle relevante websites hoeft te bezoeken.
                    </p>

                    <p>
                        <span style="color:#84b5df;font-weight:bold;">Welk nieuws vind ik in <i>Grazer® Legal NL</i>?</span><br/>
                        <i>Grazer® Legal NL</i> graast ruim duizend publieke juridische bronnen af naar nieuwsberichten. U kunt zelf aangeven welke websites worden bezocht aan de hand van de door u gekozen Rechtsgebieden, Nieuwszenders en Informatiesoorten.
                        U kunt ook suggesties voor websites aan ons doorgeven.
                    </p>

                    <div class="row">
                        <div class="col-md-6">
                            <ul>
                                <li>Betaalbaar</li>
                                <li>Honderden publieke juridische bronnen</li>
                                <li>Overzichtelijk</li>
                                <li>Persoonlijk instelbaar</li>
                                <li>
                                    <a href="//www.rechtspraak.nl/Uitspraken/Paginas/Hulp-bij-zoeken.aspx#1ab85aa0-e737-4b56-8ad5-d7cb7954718d77a998be-3c73-40e3-90f7-541fceeb00fd7" target="_blank">
                                        Rechtsgebiedenindeling Rechtspraak.nl
                                    </a>
                                </li>
                                <li>Tijdbesparend</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <a id="about-register-circle" href="/register">
                                Registreer u
                            </a>
                        </div>
                    </div>
                    <p>
                        <span style="color:#84b5df;font-weight:bold;">Welke functionaliteit biedt <i>Grazer® Legal NL</i>?</span><br/>
                        Alle mogelijkheden van <i>Grazer®</i> vindt u ook terug in <i>Grazer® Legal NL</i>. Klik op het <i>Grazer®</i> -logo voor een overzicht.
                    </p>

                    <p>
                        <a href="<c:url value="/about/grazer" />">
                            <img src="<c:url value="/img/logos/default-logo.svg"/> " style="display:block;max-height:100px;" />
                        </a>
                    </p>

                    <p>
                        <span style="color:#84b5df;font-weight:bold;">Hierna vindt u praktische informatie over <i>Grazer® Legal NL</i>.</span><br/>
                        Door op de uitklapmenu’s hierna te klikken vindt u praktische informatie over <i>Grazer® Legal NL</i>, zoals: de prijzen, de rechtsgebiedenindeling, de nieuwszenders en informatiesoorten die <i>Grazer® Legal NL</i> kent.
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>

<c:set var="temp_button_classes" value="expand-label col-12 button white bgc-light-primary sel-bgc-dark-primary" />
<c:set var="temp_input_classes" value="hidden expand-check mobile-button-font-smaller" />
<c:set var="temp_div_classes" value="col-12 mb-3" />
<div class="screen" style="padding-top: 0px !important;">
    <div class="${content_size_class} shadowed p-0">

        <div class="${temp_div_classes}">
            <input id="show-prize-text" type="checkbox" class="${temp_input_classes}" />
            <ctags:checkboxButton
                    forid="show-prize-text"
                    text="ABONNEMENTEN EN PRIJZEN"
                    base_class="${temp_button_classes} mobile-text-smaller"
            />
            <div class="expand">
                <div>
                    <br/>
                    <i>Grazer® Legal NL</i> kent twee abonnementsvormen.
                    Een kwartaalabonnement van € 59,95 of een jaarabonnement van € 199,95 waarmee u bijna 20% bespaart ten opzichte van het kwartaalabonnement. Alle prijzen zijn exclusief 21% btw. Het jaarabonnement is per maand opzegbaar ná verloop van het eerste abonnementsjaar.

                </div>

                <div>
                    <br/>
                    Neem contact op met ons indien u een maatwerkofferte wilt voor bijvoorbeeld een concernabonnement voor meer dan vijf abonnees.
                    Op al onze prijzen en offertes zijn onze
                    <a href="<c:url value="/pdf/terms_and_conditions"/>">Algemene voorwaarden</a> van toepassing.
                </div>
            </div>
        </div>

        <div class="${temp_div_classes}">
            <input id="show-juris-text" type="checkbox" class="${temp_input_classes}" />
            <ctags:checkboxButton
                    forid="show-juris-text"
                    text="RECHTSGEBIEDENINDELING"
                    base_class="${temp_button_classes} mobile-text-smaller"
            />
            <div class="expand">
                <div>
                    <br/>
                    U kunt via <i>Grazer® Legal NL</i> het voor u relevante tuchtrecht volgen. <i>Grazer® Legal NL</i> hanteert daarnaast dezelfde rechtsgebiedenindeling als Rechtspraak.nl. U kunt één, meerdere of alle rechtsgebieden kiezen tijdens het maken van uw interesseprofielen.
                    <br/><br/>
                    Klik
                    <a href="//www.rechtspraak.nl/Uitspraken/Paginas/Hulp-bij-zoeken.aspx#1ab85aa0-e737-4b56-8ad5-d7cb7954718d77a998be-3c73-40e3-90f7-541fceeb00fd7" target="_blank">hier</a>
                    voor een overzicht van alle beschikbare rechtsgebieden.

                </div>
    <%--           ToDo: Insert code here to automatically generate the lists, making on per parent category--%>
            </div>
        </div>

        <div class="${temp_div_classes}">
            <input id="show-maatschap-text" type="checkbox" class="${temp_input_classes}" />
            <ctags:checkboxButton
                    forid="show-maatschap-text"
                    text="Nieuwszenders"
                    base_class="${temp_button_classes} mobile-text-smaller"
            />
            <div class="expand">
                <div>
                    <br/>
                    Bij Nieuwszenders kunt u de maatschappelijke sector kiezen waarin de afzender van het nieuws actief is. Wilt u bijvoorbeeld alleen nieuws ontvangen dat afkomstig is van de Overheid dan klikt u alleen ‘Overheid’ aan. U kunt ook hier, evenals bij de Rechtsgebieden, één, meerdere of álle Nieuwszenders kiezen. De Nieuwszenders waaruit u kunt kiezen zijn:
                    <br/><br/>
                        <ul>
                            <li>Advocatuur en Notariaat</li>
                            <li>Brancheorganisaties</li>
                            <li>Internationale en niet-gouvernementele organisaties</li>
                            <li>Journalistiek</li>
                            <li>Onderzoeksinstellingen</li>
                            <li>Overheid</li>
                            <li>Rechterlijke macht</li>
                            <li>Rechtsbijstandsverzekeraars</li>
                            <li>Specialistenverenigingen</li>
                            <li>Zakelijke juridische dienstverlening</li>
                    </ul>

                </div>
    <%--           ToDo: Insert code here to automatically generate the list--%>
            </div>
        </div>

        <div class="${temp_div_classes}">
            <input id="show-types-text" type="checkbox" class="${temp_input_classes}" />
            <ctags:checkboxButton
                    forid="show-types-text"
                    text="INFORMATIESOORTEN"
                    base_class="${temp_button_classes} mobile-text-smaller"
            />
            <div class="expand">
                <div>
                    <br/>
                    Door middel van de rubriek informatiesoorten kunt u kiezen voor de soort nieuwsberichten dat u wilt ontvangen. U heeft de volgende keuzemogelijkheden:
                    <br/><br/>
                    <ul>
                        <li>Blogs</li>
                        <li>Boeken</li>
                        <li>Evenementen en opleidingen</li>
                        <li>Jurisprudentie</li>
                        <li>Nieuws- en persberichten</li>
                        <li>Onderzoek en publicaties</li>
                        <li>Vacatures</li>
                    </ul>
                </div>
    <%--           ToDo: Insert code here to automatically generate the list--%>
            </div>
        </div>

        <div class="col-12 d-flex flex-wrap mt-4 p-0">
            <div class="col-12 col-lg-6">
                <c:if test="${not empty legal_article}">
                    <c:set var="article" value="${legal_article}"/>
                    <%@include file="/WEB-INF/jsp/article/view.jsp" %>
                </c:if>
            </div>

            <div class="col-12 col-lg-6">
                <%@include file="/WEB-INF/jsp/homepage_onderdelen/demo.jsp" %>
            </div>
        </div>

    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>