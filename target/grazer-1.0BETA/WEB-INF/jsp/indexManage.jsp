<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen bgc-grey">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">
        <h1>Index</h1>
        <c:set value="light-primary col-12 text-center d-block p-0" var="color" />
        <p class="dark-primary text-center"><span>De index bevat</span> <span>${articles.size()}</span> <span>artikellen</span></p>
        <p><a href="${base_url}index/retag" class="${color}">Retag index</a></p>
        <c:choose>
            <c:when test="${running}">
                <p><a href="${base_url}index/stop" class="${color}">Onderbreek het indexeren</a></p>
            </c:when>
            <c:otherwise>
                <p><a href="${base_url}index/start" class="${color}">Start indexeren</a></p>
            </c:otherwise>
        </c:choose>
        <p><a href="${base_url}index/download" class="${color}">Download index</a></p>
        <p><a href="${base_url}index/reset" class="${color}">Reset index</a></p>
        <div class="text-center">
            <p class="dark-primary">
                Upload een nieuw index bestand
            </p>
            <form action="/index/upload" class="col-12" method="POST" enctype='multipart/form-data'>
                <input type="file" name="index" class="black" />
                <button type="submit" class="black">laad zip bestand</button>
            </form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>