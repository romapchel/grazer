<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 2018-07-17
  Time: 11:06
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<html>
<head>
    <title>error</title>
</head>
<body>
<p>If you're seeing this that means you don't have JavaScript enabled, please use the back button to return to the
    previous page as that's what this page would have done if your JavaScript was working</p>
<script>
    history.go(-1);
</script>
</body>
</html>
