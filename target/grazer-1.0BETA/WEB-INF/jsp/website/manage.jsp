<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<style>
    .content {
        display: flex;
        flex-wrap: wrap;
    }
    .content > * {
        width: 100%;
    }
    .margin-bottom {
        margin-bottom: 3em;
    }
</style>
<c:set var="selected_page" value="${selected}" scope="page" />
<c:set var="sites" value="${items}" scope="page" />

<div class="screen bgc-grey">
    <div class="${content_size_class} shadowed white">
        <c:if test="${not empty downloadLink}">
            <div class="col-12 white">
                <a href="${base_url}download?path=${downloadLink}&name=file.txt">
                    Download resultaten
                </a>
            </div>
        </c:if>
        <%@include file="/WEB-INF/jsp/website/includes/select.jsp" %>
    </div>
</div>

<div class="screen bgc-light-primary">
    <div class="${content_size_class} shadowed white">
        <c:if test="${not empty page}">
            <%@include file="/WEB-INF/jsp/website/includes/edit.jsp" %>
        </c:if>
    </div>
    <div class="white">
        <div class="fixed foreground">
            <div class="margin-bottom">
                <label for="website-submit-button" class="button bordered">Wijzigingen opslaan</label>
            </div>
            <label id="addButton" class="button bordered">Knop toevoegen</label>
            <label id="addPage" class="button bordered">Extra pagina toevoegen</label>
            <label id="addBefore" class="button bordered">Pre selector toevoegen</label>
            <label id="addReplacement" class="button bordered">Tag vervanging toevoegen</label>
            <label id="addLink" class="button bordered">Extra link toevoegen</label>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#addPage').on('click', addAltPage);
        $('#addBefore').on('click', addBefore);
        $('#addReplacement').on('click', addReplacement);
        $('#addLink').on('click', addLink);
    });
    function addReplacement() {
        var container = $('#replacements');
        var index = $('#replacements .replacement').length;
        addNew(index, "<%= Types.TAG_REPLACEMENT %>", container);
    }
    function addAltPage() {
        var container = $('#altPages');
        var index = $('#altPages .altPage').length;
        addNew(index, "<%= Types.ALT_PAGE %>", container);
    }

    function addBefore() {
        var container = $('#befores');
        var index = $('#befores .before').length;
        addNew(index, "<%= Types.BEFORES %>", container);
    }

    function addLink() {
        var container = $('#additional_links');
        var index = $('#additional_links .additional_source_link').length;
        console.log('index: ' + index);
        addNew(index, "<%= Types.ADDITIONAL_SOURCE_LINK %>", container);
    }

    function addNew(index, type, container) {
        $.ajax({
            url: '${base_url}website/new',
            type: 'POST',
            data: {index: index, type: type},
            success: function(data) {
                container.append(data);
                container.prevAll('input').prop('checked', false);
                /*scrollToElement(container);*/
            },
            error: function(error) {
                log("error");
                log(error);
            }
        });
    }
    function addButton() {
        var buttonCon = $('#button_container');
    }
</script>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>