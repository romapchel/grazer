<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<%@ page import="nl.impressie.grazer.model.DB.PageSearch" %>
<%@ page import="nl.impressie.grazer.model.DB.CookieWall" %>

<style>
    .grouping {
        margin-bottom: 1.5em;
    }
    .item > form input {
        background: inherit;
        border: 1px solid white;
        padding: 0.2em 0.5em;
        width: 100%;
    }
    .grouping {
        border: 1px solid white;
    }
    .validatable,
    .grouping {
        overflow: hidden;
    }
    .grouping > h4 {
        margin-top: 0;
    }
    .grouping > div > *:last-child {
        margin-bottom: 0;
    }
    .grouping > .expand-label {
        width: 100%;
    }
</style>

<div class="website edit">
    <form action="${base_url}website/save" class="validatable" method="POST">

        <c:set var="id" value="${selected_page.getId()}" />
        <c:set var="current" value="${selected_page.getCurrent()}" scope="request" />
        <c:set var="currentTags" value="${selected_page.getTags()}" scope="request" />

        <c:choose>
            <c:when test="${empty id || id eq '0'}">
                <div>
                    <span>Nieuwe website</span>
                </div>
            </c:when>
            <c:otherwise>
                <input type="hidden" name="<%= WebsiteDao.ID %>" value="${id}" />
                <div>
                    <span>${current.get('article-count')}</span> <span>articles indexed</span>
                    <p>
                        <span>ID: ${id}</span>
                    </p>
                </div>
            </c:otherwise>
        </c:choose>

        <ctags:input name="<%= WebsiteDao.NAME %>" title="Naam website" placeholder="Voorbeeld naam" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
        <ctags:input name="<%= WebsiteDao.NIEUWSZENDER %>" title="Nieuwszender website"
                     placeholder="Nieuwszender waar deze website bij hoort" values="${current}" errors="${errors}"
                     onInputFocus="$(this).select();"/>
        <ctags:input name="<%= WebsiteDao.DESCRIPTION %>" type="textarea" title="Beschrijving website" placeholder="Uitleg over wat voor artikkelen op deze pagina voorkomen" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
        <ctags:input name="<%= WebsiteDao.URL %>" title="URL website" placeholder="www.voorbeeld.nl" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
        <ctags:input name="<%= WebsiteDao.MAX_DEPTH %>" type="number" title="Maximale diepte" placeholder="3" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />

        <c:set var="showAll" value="${logged_in_user.isAdmin()}" />
        <c:if test="${showAll}">
            <ctags:input name="<%= WebsiteDao.MORE_INFO %>" title="Meer informatie" placeholder="a.meer-inforamtie" values="${current}" errors="${errors}" onInputFocus="$(this).select();" tooltip="Sommige artikellen hebben links naar andere pagina's met extra indormatie over dit artikel, deze links kan je via deze selector toevoegen" />

            <div class="grouping">
                <c:set var="item" value="${selected.getNavigation()}" scope="request" />
                <input id="navigation_expand_check" type="checkbox" class="expand-check hidden"<c:if test="${empty item}"> checked="checked"</c:if> />
                <ctags:checkboxButton forid="navigation_expand_check" text="Navigation" additional_label_class="reverse-icons" />
                <div id="navigation_container" class="hideable">
                    <jsp:include page="/WEB-INF/jsp/includes/website/navigationView.jsp" />
                </div>
            </div>

            <div class="grouping">
                <c:set var="altpages" value="${selected_page.getAltPages()}" />
                <input id="altpages_expand_check" type="checkbox" class="expand-check hidden"<c:if test="${empty altpages || 0 == altpages.size()}"> checked="checked"</c:if> />
                <ctags:checkboxButton forid="altpages_expand_check" text="Extra pagina's" additional_label_class="reverse-icons" />
                <div id="altPages" class="hideable">
                    <c:forEach var="item" items="${altpages}" varStatus="loop">
                        <c:set var="index" value="${loop.index + 1}" scope="request"/>
                        <c:set var="item" value="${item}" scope="request" />
                        <jsp:include page="/WEB-INF/jsp/includes/website/altPageView.jsp" />
                    </c:forEach>
                    <c:set var="index" value="0" scope="request"/>
                    <c:set var="item" value="" scope="request"/>
                    <jsp:include page="/WEB-INF/jsp/includes/website/altPageView.jsp"/>
                </div>
            </div>

            <div class="grouping">
                <c:set var="befores" value="${selected_page.getBeforeSelectors()}"/>
                <input id="befores_expand_check" type="checkbox" class="expand-check hidden"<c:if
                        test="${empty befores || 0 == befores.size()}"> checked="checked"</c:if> />
                <ctags:checkboxButton forid="befores_expand_check" text="Selectors voor de link pagina"
                                      additional_label_class="reverse-icons"/>
                <div id="befores" class="hideable">
                    <c:forEach var="item" items="${befores}" varStatus="loop">
                        <c:set var="item" value="${item}" scope="request"/>
                        <c:set var="index" value="${loop.index + 1}" scope="request"/>
                        <jsp:include page="/WEB-INF/jsp/includes/website/beforeView.jsp"/>
                    </c:forEach>
                    <c:set var="item" value="" scope="request"/>
                    <c:set var="index" value="0" scope="request"/>
                    <jsp:include page="/WEB-INF/jsp/includes/website/beforeView.jsp"/>
                </div>
            </div>

            <div class="grouping">
                <c:set var="replacements" value="${selected_page.getReplacements()}"/>
                <input id="replacements_expand_check" type="checkbox" class="expand-check hidden"<c:if
                        test="${empty replacements || 0 == replacements.size()}"> checked="checked"</c:if> />
                <ctags:checkboxButton forid="replacements_expand_check" text="Regex replacements in tag texts"
                                      additional_label_class="reverse-icons"/>
                <div id="replacements" class="hideable">
                    <c:forEach var="item" items="${replacements}" varStatus="loop">
                        <c:set var="item" value="${item}" scope="request"/>
                        <c:set var="index" value="${loop.index + 1}" scope="request"/>
                        <jsp:include page="/WEB-INF/jsp/includes/website/tagReplacementView.jsp"/>
                    </c:forEach>
                    <c:set var="item" value="" scope="request"/>
                    <c:set var="index" value="0" scope="request"/>
                    <jsp:include page="/WEB-INF/jsp/includes/website/tagReplacementView.jsp"/>
                </div>
            </div>

            <div class="grouping">
                <c:set var="additional_links" value="${selected_page.getAddtionalLinkSources()}"/>
                <input id="additional_links_expand_check" type="checkbox" class="expand-check hidden"<c:if
                        test="${empty additional_links || 0 == additional_links.size()}"> checked="checked"</c:if> />
                <ctags:checkboxButton forid="additional_links_expand_check" text="Additional article source pages"
                                      additional_label_class="reverse-icons"/>

                <div id="additional_links" class="hideable">
                    <c:forEach var="item" items="${additional_links}" varStatus="loop">
                        <c:set var="item" value="${item}" scope="request"/>
                        <c:set var="index" value="${loop.index + 1}" scope="request"/>
                        <jsp:include page="/WEB-INF/jsp/includes/website/additionalSourceLinkView.jsp"/>
                    </c:forEach>
                    <c:set var="item" value="" scope="request"/>
                    <c:set var="index" value="0" scope="request"/>
                    <jsp:include page="/WEB-INF/jsp/includes/website/additionalSourceLinkView.jsp"/>
                </div>
            </div>

            <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
                <jsp:param name="seReDeName" value="<%= WebsiteDao.DATE %>" />
                <jsp:param name="seReDeTitle" value="Datum" />
            </jsp:include>
            <ctags:input name="<%= WebsiteDao.DATE_FORMAT %>" title="Datum formaat" placeholder="dd-MM-yyyy" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />

            <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
                <jsp:param name="seReDeName" value="<%= WebsiteDao.CRAWL %>" />
                <jsp:param name="seReDeTitle" value="Artikel links" />
            </jsp:include>

            <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
                <jsp:param name="seReDeName" value="<%= WebsiteDao.ARTICLE %>" />
                <jsp:param name="seReDeTitle" value="Artikel" />
            </jsp:include>

            <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
                <jsp:param name="seReDeName" value="<%= WebsiteDao.TITLE %>" />
                <jsp:param name="seReDeTitle" value="Titel" />
            </jsp:include>

            <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
                <jsp:param name="seReDeName" value="<%= WebsiteDao.CONTENT %>" />
                <jsp:param name="seReDeTitle" value="Inhoud" />
            </jsp:include>

            <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
                <jsp:param name="seReDeName" value="<%= WebsiteDao.SNIPPET %>" />
                <jsp:param name="seReDeTitle" value="Beschrijving" />
            </jsp:include>

            <div class="grouping">
                <input id="search_expand_check" type="checkbox" class="expand-check hidden"<c:if test="${empty current || not current.containsKey('search')}"> checked="checked"</c:if> />
                <ctags:checkboxButton forid="search_expand_check" text="Zoeken" additional_label_class="reverse-icons"/>
                <div class="hideable">
                    <ctags:input name="<%= WebsiteDao.SEARCH_KEY %>" title="Zoek sleutel" placeholder="query" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
                    <ctags:input name="<%= WebsiteDao.JSON_URL %>" title="JSON URL" placeholder="www.example.com/json?" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
                    <ctags:input name="<%= WebsiteDao.PAGE_KEY %>" title="Paginatie sleutel" placeholder="page" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />

                    <ctags:input name="<%= WebsiteDao.SEARCH_HEADER %>" title="Header"
                                 placeholder="X-Requested-With=XMLHttpRequest" values="${current}" errors="${errors}"
                                 onInputFocus="$(this).select();"/>
                    <ctags:input name="<%= WebsiteDao.SEARCH_DATA %>" title="Data"
                                 placeholder="X-Requested-With=XMLHttpRequest, search=term" values="${current}"
                                 errors="${errors}" onInputFocus="$(this).select();"/>

                    <c:set var="name" value="<%= WebsiteDao.SEARCH_METHOD %>"/>
                    <ctags:input name="${uid}${name}" value="<%= PageSearch.METHOD_GET %>"
                                 title="Laad pagina's via een GET request"
                                 type="radio" values="${current}" errors="${errors}"/>
                    <ctags:input name="${uid}${name}" value="<%= PageSearch.METHOD_POST %>"
                                 title="Laad pagina's via een POST request"
                                 type="radio" values="${current}" errors="${errors}"/>

                </div>
            </div>

            <div class="grouping">
                <input id="js_expand_check" type="checkbox" class="expand-check hidden"<c:if test="${empty current || not current.containsKey('javascript')}"> checked="checked"</c:if> />
                <ctags:checkboxButton forid="js_expand_check" text="JavaScript" additional_label_class="reverse-icons"/>
                <div class="hideable">
                    <ctags:input name="<%= WebsiteDao.JS_TEST_ID %>" title="Test voor dit ID" placeholder="main-content" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
                    <ctags:input name="<%= WebsiteDao.JS %>" title="Javascript code" placeholder="getElementById(button).click();" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
                </div>
            </div>

            <div class="grouping">
                <input id="cookie_expand_check" type="checkbox" class="expand-check hidden"<c:if
                        test="${empty current || not current.containsKey('cookies')}"> checked="checked"</c:if> />
                <ctags:checkboxButton forid="cookie_expand_check" text="Cookie wall"
                                      additional_label_class="reverse-icons"/>
                <div class="hideable">
                    <ctags:input name="<%= WebsiteDao.COOKIE_URL %>" title="Cookie destination url"
                                 placeholder="www.site.nl/acceptcookies?cookies=yes" values="${current}"
                                 errors="${errors}" onInputFocus="$(this).select();"/>
                    <ctags:input name="<%= WebsiteDao.COOKIE_DATA %>" title="Data voor cookie pagina"
                                 placeholder="accept=yes, which=all" values="${current}" errors="${errors}"
                                 onInputFocus="$(this).select();"/>

                    <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp">
                        <jsp:param name="seReDeName" value="<%= WebsiteDao.COOKIE_CSS %>"/>
                        <jsp:param name="seReDeTitle" value="Identificieer cookie pagina"/>
                    </jsp:include>

                    <c:set var="name" value="<%= WebsiteDao.COOKIE_METHOD %>"/>
                    <ctags:input name="${uid}${name}" value="<%= CookieWall.METHOD_GET %>"
                                 title="Laad pagina via een GET request"
                                 type="radio" values="${current}" errors="${errors}"/>
                    <ctags:input name="${uid}${name}" value="<%= CookieWall.METHOD_POST %>"
                                 title="Laad pagina via een POST request"
                                 type="radio" values="${current}" errors="${errors}"/>

                </div>
            </div>
        </c:if>

        <c:if test="${not empty page}"><c:set var="selectedItems" value="${selected_page.getTags()}" scope="request" /></c:if>

        <c:set var="name"><%= WebsiteDao.RECHTSGEBIED %></c:set>
        <c:set var="code"><%= Tag.RECHTSGEBIED %></c:set>
        <c:set var="title"><ctags:message key="${code}.display"/></c:set>
        <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
            <jsp:param name="seReDeName" value="${name}" />
            <jsp:param name="seReDeTitle" value="${title}" />
            <jsp:param name="showAll" value="${showAll}" />
            <jsp:param name="dropdown" value="True"/>
        </jsp:include>

        <c:set var="name"><%= WebsiteDao.NIEUWS_SOORT %></c:set>
        <c:set var="code"><%= Tag.NIEUWSSOORT %></c:set>
        <c:set var="title"><ctags:message key="${code}.display"/></c:set>
        <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
            <jsp:param name="seReDeName" value="${name}" />
            <jsp:param name="seReDeTitle" value="${title}" />
            <jsp:param name="showAll" value="${showAll}" />
            <jsp:param name="dropdownOnly" value="True"/>
        </jsp:include>

        <c:set var="name"><%= WebsiteDao.RUBRIEKEN %></c:set>
        <c:set var="code"><%= Tag.RUBRIEK %></c:set>
        <c:set var="title"><ctags:message key="${code}.display"/></c:set>
        <jsp:include page="/WEB-INF/jsp/includes/website/SeReDeView.jsp" >
            <jsp:param name="seReDeName" value="${name}" />
            <jsp:param name="seReDeTitle" value="${title}" />
            <jsp:param name="showAll" value="${showAll}" />
            <jsp:param name="dropdownOnly" value="True"/>
        </jsp:include>

        <button id="website-submit-button" type="submit" class="hidden"></button>
    </form>
</div>