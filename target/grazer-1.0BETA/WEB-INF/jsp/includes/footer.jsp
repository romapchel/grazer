<%@ page contentType="text/html;charset=UTF-8" language="java" %>

</div>

<div id="footer" class="vertically-centered col-12 flex-wrap justify-content-around">
    <c:set var="column_classes" value="col-12 col-lg-3 white text-lg-center" />
    <c:set var="pdf_url" value="${base_url}pdf/" />
    <div class="${column_classes}">
        <a target="_blank" href="${pdf_url}terms_and_conditions">
            <ctags:message key="menu.button.terms"/>
        </a>
    </div>
    <div class="${column_classes}">
        <a target="_blank" href="${pdf_url}privacy">
            <ctags:message key="menu.button.privacy"/>
        </a>
    </div>
    <div class="${column_classes}">
        <a href="${base_url}suggestion">
            <ctags:message key="menu.button.suggestion"/>
        </a>
    </div>
    <div class="${column_classes}">
        <a href="${base_url}faq">
            <ctags:message key="menu.button.faq"/>
        </a>
    </div>
</div>


<c:if test="${empty user}">
    <div id="bottomfixregister" class="fixed up register">
        <a href="${base_url}register" class="${temp_button_class} left"><ctags:message key="header.link.register"/></a>
    </div>
</c:if>

<div id="scrolltopfix" class="fixed up">
    <a href="#fb-root" class="right">
        <i class="fa fa-chevron-up col-12 p-0"></i>
        <br />
        <span class="mobile-hide">Omhoog</span>
    </a>
</div>


</body>
</html>