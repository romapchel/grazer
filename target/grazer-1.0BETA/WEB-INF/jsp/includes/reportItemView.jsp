<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>
<div class="flex">
    <div class="col-6">
        ${item.getTitle()}
    </div>
    <div class="col-6">
        <span>
            ${item.getHealthy()} / ${item.getTotal()} (<fmt:formatNumber value="${item.getPercentage() / 100}" type="percent" maxFractionDigits="2" />)
        </span>
    </div>
</div>