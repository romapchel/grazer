<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 14-3-2018
  Time: 8:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<%@ page import="nl.impressie.grazer.dao.AltPageDao" %>
<%@ page import="nl.impressie.grazer.model.DB.AltPage" %>
<%@ page import="nl.impressie.grazer.model.DB.Website" %>

<c:set var="temp_type_name" value="<%= Website.Types.ALT_PAGE %>"/>

<c:set var="uid" value="${temp_type_name}-${index}-"/>
<c:set var="current" value="" />
<c:set var="id" value="0" />
<c:if test="${not empty item}">
    <c:set var="current" value="${item.getCurrent(uid)}" />
    <c:set var="id" value="${item.getId()}" />
</c:if>

<div class="altPage grouping">
    <c:set var="name" value="<%= AltPageDao.ID %>" />
    <ctags:input name="${uid}${name}" type="hidden" />

    <c:set var="name" value="<%= AltPageDao.URL %>" />
    <ctags:input name="${uid}${name}" title="URL" placeholder="www.example.com/artikelen" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />

    <c:set var="name" value="<%= AltPageDao.METHOD %>"/>
    <ctags:input name="${uid}${name}" value="<%= AltPage.METHOD_GET %>" title="Laad pagina via een GET request"
                 type="radio" values="${current}" errors="${errors}"/>
    <ctags:input name="${uid}${name}" value="<%= AltPage.METHOD_POST %>" title="Laad pagina via een POST request"
                 type="radio" values="${current}" errors="${errors}"/>

    <c:set var="name" value="<%= AltPageDao.DATA %>"/>
    <ctags:input name="${uid}${name}" title="data" placeholder="target=/, origin=google" values="${current}"
                 errors="${errors}" onInputFocus="$(this).select();"/>

    <c:set var="name" value="<%= AltPageDao.MOMENT %>" />
    <ctags:input name="${uid}${name}" value="<%= AltPage.PRE %>" title="Bezoek vooraf" type="radio" values="${current}"
                 errors="${errors}"/>
    <ctags:input name="${uid}${name}" value="<%= AltPage.POST %>" title="Bezoek achteraf" type="radio"
                 values="${current}" errors="${errors}"/>
</div>