<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 5-7-2018
  Time: 10:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<%@ page import="nl.impressie.grazer.model.DB.Website" %>
<%@ page import="nl.impressie.grazer.dao.*" %>

<c:set var="temp_type_name" value="<%= Types.TAG_REPLACEMENT %>"/>
<c:set var="uid" value="${temp_type_name}_${index}_"/>
<c:set var="before_current" value=""/>
<c:set var="before_id" value=""/>
<c:if test="${not empty item}">
    <c:set var="before_current" value="${item.getCurrent(uid)}"/>
    <c:set var="before_id" value="${item.getId()}"/>
</c:if>

<div class="replacement grouping">
    <c:set var="name" value="<%= TagReplacementDao.ID %>"/>
    <ctags:input name="${uid}${name}" type="hidden" value="${before_id}"/>

    <label class="col-12 validatable-input-label">
        <div class="vertically-centered d-flex flex-wrap ">


            <div class="col-12 col-lg-6 button">Type of tag</div>
            <c:set var="temp" value="<%= TagReplacementDao.TAG_TYPE %>" />
            <c:set var="temp" value="${uid}${temp}" />
            <c:set var="temp2"><c:catch var="exception">${before_current.containsKey(temp)}</c:catch></c:set>
            <c:choose>
                <c:when test="${empty exception && before_current.containsKey(temp)}">
                    <c:set var="cur" value="${before_current.get(temp)}" />
                </c:when>
                <c:otherwise>
                    <c:set var="cur" value="${Tag.RECHTSGEBIED}" />
                </c:otherwise>
            </c:choose>
            <select name="${uid}<%= TagReplacementDao.TAG_TYPE %>" class="col validatable">
                <c:forEach var="tag" items="${Tag.getTypes()}">
                    <option value="${tag}"<c:if test="${tag.equals(cur)}"> selected</c:if>>
                        <ctags:message key="${tag}.display" defaultValue="${tag}" />
                    </option>
                </c:forEach>
            </select>
        </div>
    </label>
    <c:set var="name" value="<%= TagReplacementDao.REPLACE_TEXT %>"/>
    <ctags:input name="${uid}${name}" title="Replace this text" placeholder="Straf\(proces\)recht" values="${before_current}"
                 errors="${errors}" onInputFocus="$(this).select();"/>

    <c:set var="name" value="<%= TagReplacementDao.REPLACE_WITH %>"/>
    <ctags:input name="${uid}${name}" title="With this text" placeholder="Strafrecht" values="${before_current}"
                 errors="${errors}" onInputFocus="$(this).select();"/>
</div>