<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 14-3-2018
  Time: 8:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<%@ page import="nl.impressie.grazer.dao.NavigationDao" %>

<c:set var="uid" value="navigation-${index}-" />
<c:set var="current" value="" />
<c:set var="id" value="0" />
<c:if test="${not empty item}">
    <c:set var="current" value="${item.getCurrent(uid)}" />
    <c:set var="id" value="${item.getId()}" />
</c:if>
<div class="navigation grouping">
    <c:set var="col" value="<%= NavigationDao.URL %>" />
    <ctags:input name="${uid}${col}" title="Navigatie URL" placeholder="www.example.com/artikelen" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
    <c:set var="col" value="<%= NavigationDao.PAGING %>" />
    <ctags:input name="${uid}${col}" title="Paginatie deel URL" placeholder="?pagina=%s" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
    <c:set var="col" value="<%= NavigationDao.AFTER %>" />
    <ctags:input name="${uid}${col}" title="URL na de paginatie" placeholder="&cat=nieuws" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
    <c:set var="col" value="<%= NavigationDao.START %>" />
    <ctags:input name="${uid}${col}" type="number" title="Begin waarde paginatie" placeholder="0" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
    <c:set var="col" value="<%= NavigationDao.JUMP %>" />
    <ctags:input name="${uid}${col}" type="number" title="sprong grote" placeholder="1" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
    <c:set var="col" value="<%= NavigationDao.END %>" />
    <ctags:input name="${uid}${col}" type="number" title="Hoeveelheid sprongen" placeholder="3" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
</div>