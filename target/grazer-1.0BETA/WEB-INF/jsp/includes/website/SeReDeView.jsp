<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<c:set var="showAll" value="True" />
<c:if test="${not empty param.showAll}"><c:set var="showAll" value="${param.showAll}" /></c:if>

<div class="serede grouping">
    <c:set var="uid" value="serede-${param.seReDeName}-" />
    <input id="${uid}expand_check" type="checkbox" class="expand-check hidden"<c:if test="${empty current || not current.containsKey(param.seReDeName)}"> checked="checked"</c:if> />
    <label class="expand-label button reverse-icons" for="${uid}expand_check">
        <span class="col">${param.seReDeTitle}</span>
    </label>
    <div class="hideable">
        <c:if test="${showAll && not param.dropdownOnly}">
            <ctags:input name="${uid}selector" title="Css selector" placeholder="div#main.text" values="${current}" errors="${errors}" labelLengthClasses="full-width" onInputFocus="$(this).select();" />
            <ctags:input name="${uid}property" title="Atribuut" placeholder="content" values="${current}" errors="${errors}" labelLengthClasses="full-width" onInputFocus="$(this).select();" />
            <ctags:input name="${uid}regex" title="Regular expresion" placeholder="\d{1,2}\s*[a-zA-Z]*\s*\d{4}" values="${current}" errors="${errors}" labelLengthClasses="full-width" onInputFocus="$(this).select();" />
        </c:if>
        <%--[${not empty allTags && param.dropdown}][${not empty allTags}][${param.dropdown}]--%>
        <c:choose>
            <c:when test="${not empty allTags && (param.dropdown || param.dropdownOnly)}">
                <%--<ctags:select items="${allTags.get(param.seReDeName)}" id="${uid}regex" name="${uid}regex" title="Regular expresion" />--%>
                <c:set var="autocompleteItems" value="${allTags.get(param.seReDeName)}" scope="request" />

                <ctags:tagAutocomplete name="${uid}default" />
            </c:when>
            <c:otherwise>
                <ctags:input name="${uid}default" title="Standaard waarde" placeholder="Standaard" values="${current}" errors="${errors}" onInputFocus="$(this).select();" />
            </c:otherwise>
        </c:choose>
    </div>
</div>