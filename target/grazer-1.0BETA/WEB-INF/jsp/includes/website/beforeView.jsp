<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 5-7-2018
  Time: 10:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<%@ page import="nl.impressie.grazer.dao.BeforeSelectorDao" %>
<%@ page import="nl.impressie.grazer.model.DB.Website" %>

<c:set var="temp_type_name" value="<%= Website.Types.BEFORES %>"/>
<c:set var="uid" value="${temp_type_name}-${index}-"/>
<c:set var="before_current" value=""/>
<c:set var="before_id" value=""/>
<c:if test="${not empty item}">
    <c:set var="before_current" value="${item.getCurrent(uid)}"/>
    <c:set var="before_id" value="${item.getId()}"/>
</c:if>

<div class="before grouping">
    <c:set var="name" value="<%= BeforeSelectorDao.ID %>"/>
    <ctags:input name="${uid}${name}" type="hidden" value="${before_id}"/>

    <c:set var="name" value="<%= BeforeSelectorDao.NAME %>"/>
    <ctags:input name="${uid}${name}" title="Selector naam" placeholder="date" values="${before_current}"
                 errors="${errors}" onInputFocus="$(this).select();"/>

    <c:set var="name" value="<%= BeforeSelectorDao.SELECTOR %>"/>
    <ctags:input name="${uid}${name}" title="CSS selector" placeholder="link > artikel" values="${before_current}"
                 errors="${errors}" onInputFocus="$(this).select();"/>
</div>