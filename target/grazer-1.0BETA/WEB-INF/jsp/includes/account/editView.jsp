<%@ page import="nl.impressie.grazer.dao.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/libraries.jsp" %>

<div class="account edit">
    <c:if test="${not empty selected}">
        <c:if test="${!selected.isActive()}">
            <div class="error">
                Het e-mail adress van deze gebruiker is nog niet geactiveerd
            </div>
        </c:if>
    </c:if>
    <form class="validatable" method="POST">

        <c:set var="curr_id" value="-1" />
        <c:set var="current" value="${cfn:emptyMap()}" scope="request" />
        <c:if test="${not empty selected}">
            <c:set var="curr_id" value="${selected.getId()}" />
            <c:if test="${empty previous}">
                <c:set var="current" value="${selected.getCurrent()}" scope="request" />
            </c:if>
        </c:if>
        <c:if test="${not empty previous}">
            <c:set var="current" value="${previous}" scope="request" />
        </c:if>

        <c:set var="values" value="${current}" scope="request" />
        <c:set var="errors" value="${errors}" scope="request" />
        <%@include file="/WEB-INF/jsp/user/fields.jsp" %>

        <ctags:field fieldMap="${fields.get(UserDao.USERNAME)}" />
        <ctags:field fieldMap="${fields.get(UserDao.MAIL)}" />

        <ctags:field fieldMap="${fields.get(UserDao.INITIALS)}" />
        <ctags:field fieldMap="${fields.get(UserDao.TUSSENVOEGSELS)}" />
        <ctags:field fieldMap="${fields.get(UserDao.LASTNAME)}" />

        <ctags:field fieldMap="${fields.get(UserDao.STREET)}" />
        <ctags:field fieldMap="${fields.get(UserDao.HOUSENUMBER)}" />
        <ctags:field fieldMap="${fields.get(UserDao.ZIP)}" />
        <ctags:field fieldMap="${fields.get(UserDao.CITY)}" />

        <c:set var="id_key" value="<%= AccountTypeDao.ID %>" />
        <c:set var="name_key" value="<%= AccountTypeDao.NAME %>" />
        <c:set var="selectedType" value="<%= AccountTypeDao.DEFAULT_TYPE_ID %>" />

        <c:if test="${not empty selected}">
            <c:set var="selectedAccount" value="${selected.getAccountType()}" />
            <c:set var="selectedType" value="${selectedAccount.getId()}" />
        </c:if>

        <label class="col-12 validatable-input-label">
            <div class="vertically-centered d-flex flex-wrap  required">
                <div class="col-12 col-lg-6 button ">
                    <ctags:message key="user.input.type.title" />
                </div>
                <select name="${UserDao.TYPE_ID}">
                    <c:forEach var="type" items="${types}">
                        <option value="${type.getId()}"<c:if test="${selectedType == type.getId()}"> selected</c:if>>${type.getName()}</option>
                    </c:forEach>
                </select>
                <div class="mouse_overs d-flex"></div>
                <span>
                    <c:if test="${not empty selectedAccount}">
                        <c:choose>
                            <c:when test="${selectedAccount.getEndDate() <= 0}">
                                No expiration date
                            </c:when>
                            <c:otherwise>
                                Valid until: <input name="${RoleDao.END_DATE}" type="date" value="${selectedAccount.getEndDate("yyyy-MM-dd")}" />
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </span>
            </div>
        </label>

        <div>
            <p>
                <ctags:message key="user.subheader.3" />
            </p>
            <ctags:field fieldMap='<%= ((FieldMap)
                                                    ((Map)
                                                        request.getAttribute("fields"))
                                                    .get(UserDao.PASSWORD))
                                            .put(FieldMap.Attribute.REQUIRED, false) %>' />
            <ctags:field fieldMap='<%= ((FieldMap)
                                                    ((Map)
                                                        request.getAttribute("fields"))
                                                    .get(UserDao.PASSWORD + "-repeat"))
                                            .put(FieldMap.Attribute.REQUIRED, false) %>' />
        </div>

        <button id="account-submit-button" type="submit" class="hidden"></button>
    </form>
</div>