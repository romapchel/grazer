<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<script>
    $(document).ready(function(){
        $('input[name="opened_screen"]').on('change', function(){
            markLabel($(this));
        });

        $('input[name="opened_screen"]').each(function(){
            markLabel($(this));
        });

        function markLabel(input, initialCall) {
            if(initialCall === undefined)
                initialCall = true;

            var id = input.prop('id');
            var checked = input.is(':checked');
            var radio = input.prop('type') === 'radio';
            var name = input.prop('name');
            if(id) {
                var labels = $('.' + id);
                var selectedClass = 'selected';
                if(checked)
                    labels.addClass(selectedClass);
                else
                    labels.removeClass(selectedClass);

                if(radio && initialCall) {
                    $('input[name="' + name + '"]').each(function(){
                        markLabel($(this), false);
                    });
                }
            }
        }
    });
</script>
<style>
    #subscription-data-checkbox:checked ~ #confirm-submision {
        display: none;
    }
</style>

<div class="screen">
    <div class="content col-12 col-lg-10 col-xl-8 shadowed">
        <h1 class="page_title light-primary">Instellingen</h1>
        <c:set var="subheader_class" value="font-weight-bold"/>
        <form action="${base_url}user/edit" class="validatable col-12" method="POST">
            <input type="hidden" name="id" value="${uid}" />

            <div class="${flexwrap} justify-content">
                <c:set var="temp_button_classes" value="col-12 col-lg-4 button white bgc-light-primary sel-bgc-dark-primary " />
                <ctags:checkboxButton
                        forid="personal-data-checkbox"
                        text="Persoonsgegevens"
                        base_class="personal-data-checkbox ${temp_button_classes} mobile-margin-bottom-1"
                />
                <div class="col-lg-4 pl-2 pr-2 m-0 mobile-p-0 mobile-margin-bottom-1">
                <ctags:checkboxButton
                        forid="password-data-checkbox"
                        text="Wachtwoord"
                        base_class="password-data-checkbox col-12 button white bgc-light-primary sel-bgc-dark-primary"
                />
                </div>
                <ctags:checkboxButton
                        forid="subscription-data-checkbox"
                        text="Abonnementsgegevens"
                        base_class="subscription-data-checkbox ${temp_button_classes}"
                />
            </div>

            <c:set var="temp_selected_id" value="personal-data-checkbox" />
            <c:if test="${not empty selected_id}"><c:set var="temp_selected_id" value="${selected_id}" /></c:if>

            <c:set var="temp_current_id" value="personal-data-checkbox" />
            <input id="${temp_current_id}" type="radio" name="opened_screen" value="${temp_current_id}" class="hidden expand-check"<c:if test="${temp_current_id eq temp_selected_id}"> checked="checked"</c:if> />
            <div class="expand">
                <%@include file="/WEB-INF/jsp/user/parts/personal.jsp" %>
            </div>

            <c:set var="temp_current_id" value="password-data-checkbox" />
            <input id="${temp_current_id}" type="radio" name="opened_screen" value="${temp_current_id}" class="hidden expand-check"<c:if test="${temp_current_id eq temp_selected_id}"> checked="checked"</c:if> />
            <div class="expand">
                <%@include file="/WEB-INF/jsp/user/parts/password.jsp" %>
            </div>

            <c:set var="temp_current_id" value="subscription-data-checkbox" />
            <input id="${temp_current_id}" type="radio" name="opened_screen" value="${temp_current_id}" class="hidden expand-check"<c:if test="${temp_current_id eq temp_selected_id}"> checked="checked"</c:if> />
            <div class="expand">
                <div class="col-12 p-0 mt-4 ${flexwrap}">
                    <%@include file="/WEB-INF/jsp/user/parts/subscription.jsp" %>
                </div>
            </div>

            <div id="confirm-submision">
                <p class="${subheader_class}">
                    <ctags:message key="user.subheader.4"/>
                </p>
                <c:set var="title"><ctags:message key="user.input.password.current.title"/></c:set>
                <c:set var="tooltip">
                    <ctags:message key="tooltip.required"/>
                    <ctags:message key="user.input.password.current.tooltip"/>
                </c:set>
                <ctags:input name="oldpassword" title="${title}" placeholder="${title}" type="password" required="true"
                             tooltip="${tooltip}" values="${current}" errors="${errors}"/>

                <button type="submit" class="button submit-button bordered col-12 centered bgc-light-primary text-white">
                    <ctags:message key="user.button.submit"/>
                </button>
            </div>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>