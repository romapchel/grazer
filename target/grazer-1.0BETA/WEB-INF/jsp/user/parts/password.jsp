<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-12 p-0 mt-4 ${flexwrap}">
    <p class="${subheader_class}">
        <ctags:message key="user.subheader.3" />
    </p>
    <ctags:field fieldMap='<%= ((FieldMap)
                                        ((Map)
                                            request.getAttribute("fields"))
                                        .get(UserDao.PASSWORD))
                                .put(FieldMap.Attribute.REQUIRED, false) %>' />
    <ctags:field fieldMap='<%= ((FieldMap)
                                        ((Map)
                                            request.getAttribute("fields"))
                                        .get(UserDao.PASSWORD + "-repeat"))
                                .put(FieldMap.Attribute.REQUIRED, false) %>' />
</div>