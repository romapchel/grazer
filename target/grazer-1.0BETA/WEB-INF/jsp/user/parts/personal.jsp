<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-12 p-0 mt-4 ${flexwrap}">
    <p class="${subheader_class}">
        <ctags:message key="user.subheader.1" />
    </p>
    <c:set var="values" value="${current}" scope="request" />
    <c:set var="errors" value="${errors}" scope="request" />
    <%@include file="/WEB-INF/jsp/user/fields.jsp" %>

    <ctags:field fieldMap="${fields.get(UserDao.ORGANISATION)}" />
    <ctags:field fieldMap="${fields.get(UserDao.INITIALS)}" />
    <ctags:field fieldMap="${fields.get(UserDao.TUSSENVOEGSELS)}" />
    <ctags:field fieldMap="${fields.get(UserDao.LASTNAME)}" />

    <p class="${subheader_class}">
        <ctags:message key="user.subheader.2" />
    </p>
    <ctags:field fieldMap="${fields.get(UserDao.MAIL)}" />
    <ctags:field fieldMap="${fields.get(UserDao.STREET)}" />
    <ctags:field fieldMap="${fields.get(UserDao.HOUSENUMBER)}" />
    <ctags:field fieldMap="${fields.get(UserDao.ZIP)}" />
    <ctags:field fieldMap="${fields.get(UserDao.CITY)}" />
</div>