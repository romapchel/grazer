<%--
  Created by IntelliJ IDEA.
  User: Impressie
  Date: 21-3-2018
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<c:set var="page" value="${selected}" scope="page" />
<c:set var="sites" value="${sites}" scope="page" />

<div class="screen bgc-grey">
    <div class="content col-12 col-sm-8 col-md-6 shadowed">
        <%@include file="/WEB-INF/jsp/website/includes/select.jsp" %>
    </div>
</div>
<div class="screen bgc-light-primary">
    <div class="content col-12 col-sm-8 col-md-6 shadowed white">
        <%@include file="/WEB-INF/jsp/includes/website/health.jsp" %>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>