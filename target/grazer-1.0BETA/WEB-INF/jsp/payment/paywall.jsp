<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<style>
    form#new-bundle > * {
        margin-bottom: 0.5em;
    }
    #bundles {
        margin-bottom: 0.5em;
        overflow: hidden;
    }
    #bundles .bundle .times:after {
        display: inline-block;
        content: "X";
    }
    #bundles .bundle .total {
        display: block;
    }
    #bundles .bundle .total:before {
        display: inline-block;
        content: "Totaal:";
    }
</style>

<div class="screen vertically-centered bgc-grey">
    <div class="${content_size_class} shadowed bgc-light-primary white rounded justify-content-center p-0">
        <div class="role_remaining col-12 text-center">
            <h2><ctags:message key="paywall.header" /></h2>
        </div>

        <div id="bundles" class="text-center col-12">
            <c:forEach var="bundle" items="${bundles}">
                <a class="bundle" href="${base_url}mollie/subscribe?bundle=${bundle.getId()}">
                    <div class="bordered bgc-white light-primary col-12 vertically-centered button">
                        <div class="col-12 col-sm-8">
                            <p class="description nomargin">
                                ${bundle.getDescription()}
                            </p>
                        </div>
                        <div class="col-12 col-sm-4">
                            <span>
                                <ctags:message key="paywall.price" />:
                                €<fmt:formatNumber value="${bundle.getPriceAsDouble()}" maxFractionDigits="2" minFractionDigits="2" minIntegerDigits="1" />
                            </span>
                        </div>
                    </div>
                </a>
            </c:forEach>
        </div>
    </div>
</div>
<c:if test="${user.isAdmin()}">
    <div class="screen vertically-centered bgc-light-primary">
        <div class="content col-12 shadowed bgc-white light-primary rounded centered">
            <form id="new-bundle" action="${base_url}mollie/bundle/create" method="POST">
                <h1>Nieuwe bundel toevoegen</h1>

                <div class="flex col-12 p-0">
                    <label for="new-bundle-price" class="col-12 col-sm-4 button" >Prijs in cent</label>
                    <input id="new-bundle-price" type="number" name="price" class="col-12 col-sm-6" />
                </div>

                <div class="flex col-12 p-0">
                    <label for="new-bundle-times" class="col-12 col-sm-4 button">Hoe vaak wordt er afgeboekt</label>
                    <input id="new-bundle-times" type="number" name="times" class="col-12 col-sm-6" />
                </div>

                <div class="flex col-12 p-0">
                    <label for="new-bundle-interval" class="col-12 col-sm-4 button">Hoe veel tijd zit er tussen afboekingen</label>
                    <input id="new-bundle-interval" type="number" name="intervalNmbr" class="col-6 col-sm-4" />
                    <select name="intervalType" class="col-6 col-sm-4 p-0">
                        <option value="d">dag(en)</option>
                        <option value="m">maand(en)</option>
                        <option value="y">jaar</option>
                    </select>
                </div>

                <label for="new-bundle-description" class="col-12 button">Beschrijving</label>
                <textarea id="new-bundle-description" name="description" class="col-12"></textarea>

                <div class="col-12">
                    <button type="submit">Voeg toe</button>
                </div>
            </form>
        </div>
    </div>
</c:if>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>