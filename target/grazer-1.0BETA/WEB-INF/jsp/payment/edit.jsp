<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/includes/header.jsp" %>

<div class="screen">
    <div class="${content_size_class} shadowed">
        <form class="col-12 p-0" method="POST">
            <a href="${base_url}${AdminController.ADMIN_BUNDLE_URL}">Bekijke alle bundels</a>
            <ctags:input name="${BundleDao.PRICE}" title="Prijs in cent" type="number" hideMouseOvers="true" value="${bundle.getPrice()}" />

            <input id="repetition-check" name="limited-repitition" value="true" type="checkbox" class="hidden expand-check"<c:if test="${bundle.getTimes() > 0}"> checked="checked"</c:if> />
            <ctags:checkboxButton forid="repetition-check" text="Limiteer aantal herhalingen" additional_label_class="bgc-light-primary no-icon col-12" />
            <div class="expand bgc-light-primary border-square">
                <ctags:input name="${BundleDao.TIMES}" title="Aantal keer herhaald" type="number" value="${bundle.getTimes()}" />
            </div>
            <div class="col-12 p-0 ${flexwrap} align-items-center">
                <label for="${BundleDao.INTERVAL_NUMBER}" class="col-6 button">Herhaal na</label>
                <div class="col-6 p-0 ${flexwrap}">
                    <input class="col-6" id="${BundleDao.INTERVAL_NUMBER}" name="${BundleDao.INTERVAL_NUMBER}" type="number" value="${bundle.getIntervalNmbr()}" />
                    <select class="col-6" name="${BundleDao.INTERVAL_TYPE}">
                        <c:forEach var="key" items="${repetitions.keySet()}">
                            <option value="${key}"<c:if test="${bundle.getIntervalType() == key}"> selected="selected"</c:if>>
                                ${repetitions.get(key)}
                            </option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <ctags:input title="Beschrijving" name="${BundleDao.DESCRIPTION}" type="textarea" hideMouseOvers="true" value="${bundle.getDescription()}" />

            <button type="submit" class="button white bgc-light-primary">Wijzigingen opslaan</button>
            <button type="submit" class="button white bgc-light-primary" name="<%= Selections.ACTION %>" value="<%= Actions.DELETE %>">Verwijder bundel</button>
        </form>
    </div>
</div>
<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>