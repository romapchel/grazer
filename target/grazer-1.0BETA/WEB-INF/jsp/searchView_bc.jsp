<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/header.jsp" %>

<ctags:resourceImport type="css" path="css/search.css" />
<ctags:resourceImport type="css" path="css/profile.css"/>
<ctags:resourceImport type="js" path="js/search.js" />

<div class="screen">
    <div class="col-12 col-lg-10 col-xl-8 align-items-center">


        <div class="advanced-search mouseover tip" id="toggle_button_eenvoudig" style="display:none;float:right;width:auto;">
            <span class="advanced-search-button-top" onclick="toggleAdvanced();$('#search-check').click()">
                <ctags:message key="Eenvoudig zoeken" />
                <div class="mouseover-msg  below light-primary" style="bottom:unset">
                    <ctags:message key="
                        Eenvoudig zoeken biedt de gebruiker de mogelijkheid om op basis van het actieve profiel de zoekresultaten te verfijnen.
                    " />
                </div>
            </span>
        </div>

        <div class="advanced-search mouseover tip" id="toggle_button_advanced" style="float:right;width: auto;">
            <span class="advanced-search-button-top" onclick="toggleAdvanced();$('#search-check').click()">
                <ctags:message key="Geavanceerd zoeken" />
                <div class="mouseover-msg  below light-primary" style="bottom:unset">
                    <ctags:message key="
                        Geavanceerd zoeken biedt de gebruiker de mogelijkheid om op basis van het actieve profiel de zoekresultaten te verfijnen.
" />
                </div>

            </span>
            <script>
                function toggleAdvanced() {
                    if ($("#search-check").prop('checked')) {
                        $("div#toggle_button_eenvoudig").css("display", "none");
                        $("div#toggle_button_advanced").css("display", "block");
                    } else {
                        $("div#toggle_button_advanced").css("display", "none");
                        $("div#toggle_button_eenvoudig").css("display", "block");
                    }
                }
            </script>
        </div>
        <div style="clear: both;margin-bottom:7px;">
        </div>

<form id="search-form" action="${base_url}search" method="GET" class="ajaxable col-12 p-0">
    <c:set var="search_form_default_button_name" value="<%= Selections.ACTION %>" />
    <c:set var="search_form_default_button_value" value="<%= Actions.CREATE %>" />
    <button class="hidden" name="${search_form_default_button_name}" value="${search_form_default_button_value}" type="submit">default action(search)</button>

    <input type="hidden" name="new_search" value="${new_search}" />

    <div>
        <div class="shadowed p-0 search-form-container">
            <div class="">

                <input id="search-check" type="checkbox" name="advanced_search" value="True" class="expand-check hidden"<c:if test="${search.isAdvanced()}"> checked="checked"</c:if> />

                <div class="hideable">
                    <button class="main-search-button button bgc-override white h-lg-75" name="${search_form_default_button_name}"
                            value="${search_form_default_button_value}" type="submit">
                        <ctags:message key="search.settings.button.search"/>
                    </button>

                    <c:set var="temp_select_dropdown_pre" scope="page">
                        <ctags:message key="search.settings.paragraph.1.before.select"/>
                    </c:set>
    <%--                <c:set var="temp_select_dropdown_post" scope="page">--%>
    <%--                    <ctags:message key="search.settings.paragraph.1.after.select"/>--%>
    <%--                </c:set>--%>
                    <%@ include file="/WEB-INF/jsp/profile/select_dropdown.jsp" %>


                    <div class="col-9 p-0 search-button-container">
                            <ctags:input name="<%= Article.GENERIC_SEARCH %>" value="${search.getContent()}" title="search.settings.label.search.generic"
                                     inputclass="" labelLengthClasses="col-12" inputLengthClasses="col-12 col-sm" titleLengthClasses="inline label"
                                     labelclass="search-button mb-0 p-0 pr-1" validatable="false" requiredInfoShow="false" placeholder="Zoeken" />
                            ${temp_search_check_label}
                    </div>

                </div>



                <div class="col-12 mb-4 p-0 expand flexwrap justify-content-end">
<%--                    ${temp_search_check_label}--%>
                    <div id="searching" class="col-12 expand p-0 m-0">

                        <%--
                        <div class="col-12 justify-content-end p-0 m-0">
                            <button type="submit" name="<%= Selections.ACTION %>" value="<%= Actions.RESET %>"
                                    class="col-12 col-lg-12 button bgc-light-primary white mt-2 mb-2">
                                <ctags:message key="search.settings.button.reset"/>
                            </button>
                        </div>--%>

                        <c:set var="containerClass" value="col-12 p-0 m-0" />
                        <c:set var="inputClass" value="p-0 m-0 " />
                        <c:set var="labelLengthClasses" value=" " />
                        <c:set var="lengthClass" value="col-12 col-sm-6" />
                        <c:set var="inputClass" value="${lengthClass} p-0 m-0" />
                        <div class="col-12 p-0 m-0">
                            <c:set var="text">
                                <ctags:message key="search.settings.label.search.title"/>
                            </c:set>
                            <ctags:input name="<%= Article.TITLE_SEARCH %>" value="${search.getTitle()}" title="${text}"
                                         inputclass="m-0 p-0" labelLengthClasses="${labelLengthClasses}"
                                         labelclass="${containerClass}" validatable="false" requiredInfoShow="false"/>
                            <c:set var="text">
                                <ctags:message key="search.settings.label.search.content"/>
                            </c:set>
                            <ctags:input name="<%= Article.CONTENT_SEARCH %>" value="${search.getContent()}" title="${text}"
                                         inputclass="" labelLengthClasses="${labelLengthClasses}"
                                         labelclass="${containerClass}" validatable="false" requiredInfoShow="false" />

                            <c:set var="containerClass" value="${containerClass} ${flexwrap}" />
                            <div class="${containerClass}">
                                <label for="order" class="${inputClass}">
                                    <ctags:message key="search.settings.label.sort"/>
                                </label>
                                <select id="order" class="${lengthClass}" name="order">
                                    <option value="date:DESC"<c:if
                                            test="${search.isSortOrder('date:DESC')}"> selected="true"</c:if>>
                                        <ctags:message key="search.settings.label.sort.date"/>
                                    </option>
                                    <option value="score:DESC"<c:if
                                            test="${search.isSortOrder('score:DESC')}"> selected="true"</c:if>>
                                        <ctags:message key="search.settings.label.sort.score"/>
                                    </option>
                                </select>
                            </div>
                            <input id="filter-op-publicatie" type="checkbox" class="expand-check hidden" />
    <%--                            <c:set var="text">--%>
    <%--                                <ctags:message key="search.settings.button.toggle.date"/>--%>
    <%--                            </c:set>--%>
    <%--                            <ctags:checkboxButton forid="filter-op-publicatie" text="${text}"--%>
    <%--                                                  additional_label_class="no-caps submit-button mb-3 no-icon bgc-light-primary col-12 justify-content-center" />--%>
                            <div class="${containerClass} expand">
                                <label for="from" class="${inputClass}">
                                    <ctags:message key="search.settings.label.date"/>
                                </label>
                                <div class="${lengthClass} ${flexwrap} p-0">
                                    <div class="${lengthClass} px-1">
                                        <label for="from" class="col-2 m-0 p-0">
                                            <span>
                                                <ctags:message key="search.settings.label.date.from"/>
                                            </span>
                                        </label>
                                        <input id="from" type="date" name="from" value="${search.getStartDate('yyyy-MM-dd')}" />
                                    </div>
                                    <div class="${lengthClass} px-1">
                                        <label for="to" class="col-2 m-0 p-0">
                                            <span>
                                                <ctags:message key="search.settings.label.date.till"/>
                                            </span>
                                        </label>
                                        <input id="to" type="date" name="to" value="${search.getEndDate('yyyy-MM-dd')}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="advanced-search" class="expandable col-12 p-0">
                            <c:set var="rechtsgebiedId" value="<%= Tag.RECHTSGEBIED %>" />
                            <c:set var="rubriekId" value="<%= Tag.RUBRIEK %>" />
                            <c:set var="nieuwssoortId" value="<%= Tag.NIEUWSSOORT %>" />

                            <c:set var="text">
                                <ctags:message key="${rechtsgebiedId}.display"/>
                            </c:set>
                            <ctags:select id="recht" title="${text}" name="${rechtsgebiedId}" items="${rechtsgebieden}" aditional_container_classes="bgc-dark-primary strongly-rounded" />

                            <c:set var="text">
                                <ctags:message key="${nieuwssoortId}.display"/>
                            </c:set>
                            <ctags:select id="soort" title="${text}" name="${nieuwssoortId}" items="${soorten}" aditional_container_classes="submit-button bgc-dark-primary strongly-rounded" />

                            <c:set var="text">
                                <ctags:message key="${rubriekId}.display"/>
                            </c:set>
                            <ctags:select id="rubrieken" title="${text}" name="${rubriekId}" items="${rubrieken}" aditional_container_classes="submit-button bgc-dark-primary strongly-rounded"/>
                        </div>

                        <div class="col-12 mb-3 mt-3 search-button-container m-1 p-0 centered">
                            <button class="button bgc-override white" name="${search_form_default_button_name}"
                                    value="${search_form_default_button_value}" type="submit">
                                <ctags:message key="search.settings.button.search"/>
                            </button>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>



    <div id="articles" class="screen vertically-centered p-0">
        <c:set var="display">
            <ctags:message key="search.format.articles.all.show"/>
        </c:set>

        <c:set var="button">
            <ctags:message key="search.format.articles.feed.switch"/>
        </c:set>
        <c:if test="${search.isFeedSearch()}">
            <c:set var="display">
                <ctags:message key="search.format.articles.feed.show"/>
            </c:set>
            <c:set var="button">
                <ctags:message key="search.format.articles.all.switch"/>
            </c:set>
        </c:if>

        <div id="feed" class="col-12 mt-1 mb-3 ${flexwrap} p-0">
            <p class="col-12 text-center m-0 p-0">
                <button name="feed" value="${not search.isFeedSearch()}" class="col-12 button white p-3 mp-2 bgc-dark-primary">
                    <ctags:message key="${button}" />
                </button>
            </p>
        </div>
        <input type="hidden" name="feed" value="${search.isFeedSearch()}" />

<%--        <label>--%>
<%--            <input type="checkbox" class="autosubmit" value="true" name="exlcude_jurisprudentie"<c:if test="${search.excludesJurisprudentie()}"> checked="checked"</c:if> />--%>
<%--            <span>Verberg uitspraken</span>--%>
<%--        </label>--%>
        <input type="checkbox" id="exlcude_jurisprudentie" class="hidden toggles_button_text autosubmit" name="exlcude_jurisprudentie" value="true"<c:if test="${search.excludesJurisprudentie()}"> checked="checked"</c:if> />

        <c:set var="buttonText">
            <ctags:message key="<span class=\"button_text toggle_off\">Verberg</span><span class=\"button_text toggle_on\">Toon</span> <span>Jurisprudentie</span>" />
            <div class="mouseover-msg light-primary">
                <ctags:message key="De informatiesoort Jurisprudentie maakt vaak een groot deel van de gevonden nieuwsberichten uit waardoor het kan voorkomen dat de eerste tientallen getoonde nieuwsberichten alleen daaruit bestaan. Daarom kunt u de jurisprudentie onderdrukken zodat u ook meteen de andere informatiesoorten in het getoonde overzicht ziet." />
            </div>
        </c:set>

            <ctags:checkboxButton
                    forid="exlcude_jurisprudentie"
                    text="${buttonText}"
                    base_class="col-12 button white bgc-dark-primary mb-4 lh-high pl-0 pr-0"
                    span_class="mouseover mouseover-below tip d-block text-white"
            />

<%--        <c:set var="buttonText">--%>
<%--            <span>--%>
<%--                <ctags:message key="<span class=\"button_text toggle_off\">Verberg</span><span class=\"button_text toggle_on\">Toon</span> <span>uitspraken</span>" />--%>
<%--                --%>
<%--                <span style="position: absolute;">--%>
<%--                    <ctags:info message="De informatiesoort Jurisprudentie maakt vaak een groot deel van de gevonden nieuwsberichten uit waardoor het kan voorkomen dat de eerste tientallen getoonde nieuwsberichten alleen daaruit bestaan. Daarom kunt u de jurisprudentie onderdrukken zodat u ook meteen de andere informatiesoorten in het getoonde overzicht ziet." />--%>
<%--                </span>--%>
<%--            </span>--%>
<%--        </c:set>--%>
<%--        <ctags:checkboxButton--%>
<%--                forid="exlcude_jurisprudentie"--%>
<%--                text="${buttonText}"--%>
<%--                base_class="col-12 button white bgc-light-primary"--%>
<%--        />--%>

        <div class="col-12 text-center p-0 mobile-text-justify">
            <p class="col-12 text-center m-0 mobile-text-justify mobile-p-0 mobile-p-b-1">
                <ctags:message key="${display}" />
            </p>

            <span >
                <ctags:message key="search.articles.results.display.before"/>
            </span>
            <span id="current">${currentHits}</span>
            <span>
                <ctags:message key="search.articles.results.display.midle"/>
            </span>
            <span id="total">${totalHits}</span>
            <span>
                <ctags:message key="search.articles.results.display.after"/>
            </span>
        </div>

        <div class="content mt-4 ${flexwrap}">

            <c:forEach var="article" items="${artikels}">
                <div class="col-12 col-lg-6">
                    <%@include file="/WEB-INF/jsp/article/view.jsp" %>
                </div>
            </c:forEach>
            <div id="empty" class="col-12 text-center font-weight-bold">
                <ctags:message key="search.articles.noresults"/>
            </div>
        </div>
        <button id="next-page" name="<%= Selections.PAGE %>" value="<%= Actions.NEXT_PAGE %>" type="submit" class="hidden"></button>
        <label id="load-more" for="next-page" class="col-12 bgc-dark-primary text-white button">
            <ctags:message key="search.button.more"/>
            <i class="fa fa-plus-circle" aria-hidden="true"></i>
        </label>
    </div>
</form>

    </div>
</div>

<%@include file="/WEB-INF/jsp/includes/footer.jsp" %>