
/* onload functions */
window.onload = function() {
    $("#s\\.q").autocomplete({
        source: "autocomplete?field=contents",
        minLength:0
    });

    $("#s\\.publisher").autocomplete({
        source: "autocomplete?field=publisher",
        minLength:0
    }).focus(function() {
        $(this).autocomplete("search", $(this).val());
    });

    $("#s\\.publication").autocomplete({
        source: "autocomplete?field=publication",
        minLength:0
    }).focus(function() {
        $(this).autocomplete("search", $(this).val());
    });

};