$(document).ready(function(){
    $('.expand-check').on('change', function() {
        if(this.checked) {
            var bla = $(this);
            $('.expand-check:checked').each(function(){
                if(!bla.is($(this))) {
                    $(this).click();
                }
            })
        }
    });
    $('.item-input').on('input propertychange', function() {
        autoComp($(this));
    });

    $('.item-input').each(function() {
        autoComp($(this));
    });

    $('.item-input').on('keydown', function(e) {
        var id = $(this).closest('form').attr('id');
        switch(e.which) {
            case 13: //enter
                var active = $('#{0} .autocomplete-option.active'.format(id));
                if(active.length) {
                    active.click();
                } else {
                    $('#{0}'.format(id)).submit();
                }
                break;
            case 38: // up
                selectSuggestion(id, false);
                break;
            case 40: // down
                selectSuggestion(id, true);
                break;
            default: return; // exit this handler for other keys
        }
        e.preventDefault();
    });
    $('.save-settings').on('submit', function(e) {
        $.ajax({
            type: "post",
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json'
        }).done(function(data) {
                var id = data['id'][0];
                var items = data['items'][0];
                $('#{0} .selectedItems'.format(id)).html('');
                $.each(items, function(index, item) {
                    addToSelection(id, item['name'], item['value']);
                    primeSelected();
                });
            }).fail(function(jqXHR, textStatus, errorThrown) {
                log(errorThrown);
            });
        e.preventDefault();
    });
    primeSelected();
});

function autoComp(item)
{
    var typing = item.val().trim();
    var id = item.closest('.save-settings').attr('id');
    autocomplete(id, typing);
}
function createAutoCompleteSpan(name, value)
{
    var span = '<span class="autocomplete-option col-xs-12 button" data-value="{0}">{1}</span>';
    return span.format(value, name);
}
function createSelectedItem(id, index, name, value)
{
    var openContainer = '<div class="selectedContainer">';
    var input = '<input name="items[]" id="{0}-{1}" type="checkbox" value="{2}" checked="checked" class="hidden selected-check" />'.format(id, index, value);
    var label = '<label for="{0}-{1}" class="selected-label button">{2}</label>'.format(id, index, name);
    return '{0}{1}{2}</div>'.format(openContainer, input, label);
}
function addToSelection(id, name, value)
{
    var lastSelected = $('#{0} .selectedContainer:last-of-type'.format(id));
    var index = 0;
    if(lastSelected.length) {
        index = lastSelected.find('input').attr('id').split('-')[1];
        index = Number(index) + 1;
    }
    var newItem = createSelectedItem(id, index, name, value);
    $('#{0} .selectedItems'.format(id)).append(newItem);
    var input = $('#{0} .item-input'.format(id));
    resetAutocomplete(id);
    input.val('');
    input.focus();
    autoComp(input);
}
function primeSelected()
{
    var checks = $('.selected-check');
    checks.off();
    checks.on('change', function() {
        var id = $(this).closest('.save-settings').attr('id');
        $(this).parent().remove();
        $('#{0}'.format(id)).submit();
    });
}
function selectSuggestion(id, next)
{
    var options = $('#{0} .autocomplete-option'.format(id));
    var remove, add;
    if(options.length == 0) {
        return;
    }
    var active = $('#{0} .autocomplete-option.active'.format(id));
    if(active.length) {
        remove = active;
    }
    if(next && active.next().length) {
        add = active.next();
    } else if(next) {
        add = options.first();
    } else if(active.prev().length) {
        add = active.prev();
    } else {
        add = options.last();
    }
    if(typeof remove !== 'undefined')
        remove.removeClass('active');
    if(typeof add !== 'undefined') {
        add.addClass('active');
        var container = $('#{0} .autocomplete'.format(id));
        container.scrollTop(
            add.offset().top - container.offset().top + container.scrollTop()
        );
    }
}
function primeAutocomplete(id)
{
    $('#{0} .autocomplete-option'.format(id)).off();
    $('#{0} .autocomplete-option'.format(id)).on('click', function() {
        addToSelection(id, $(this).html(), $(this).data('value'));
        $('#{0}'.format(id)).submit();
    });
}
function resetAutocomplete(id)
{
    var complete = $('#{0} .autocomplete'.format(id));
    complete.html('');
    complete.attr('style', 'displaySearch: none;');
}
function autocomplete(id, typing)
{
    var values, names;
    if(typeof auto_values !== 'undefined') {
        if(id in auto_values) {
            values = JSON.parse(auto_values[id]);
            names = JSON.parse(auto_names[id]);
        }
    }
    if(typeof values === 'undefined') {
        console.error('Could not recognize the id: {0}'.format(id));
        return;
    }
    var lastIndex = typing.lastIndexOf(',');
    if(lastIndex != -1) {
        typing = typing.substring(lastIndex + 1).trim().toLowerCase();
    }
    var typingLength = typing.length;
    var index, value;
    var max = values.length;
    var newComplete = '';
    if(typingLength >= 0 && max > 0) {
        for(index = 0; index < max; ++index) {
            var name = names[index];
            value = values[index];
            if (name.substring(0, typingLength).toLowerCase() === typing) {
                newComplete = newComplete.concat(createAutoCompleteSpan(name, value));
            }
        }
    }
    resetAutocomplete(id);
    var complete = $('#{0} .autocomplete'.format(id));
    if(newComplete.length > 0) {
        complete.attr('style', '');
        complete.html(newComplete);
    }
    primeAutocomplete(id);
}