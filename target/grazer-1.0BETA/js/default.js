$(document).ready(onDocumentReady);

function onDocumentReady() {
    fixLinkedInButtonStyleAttribute();
    JSEnabled();
    prepDisplayFilters();
    prepAjaxForms();
    // setHeaderScrollToFixed();
    setScrollToTopButton();
    $('.websites > .website > *:not(.inactive)').on('click', function(){
        $(this).addClass('loading');
    });

    $('.selectAll').on('click', function(){ checkall($(this), true); });
    $('.deselectAll').on('click', function(){ checkall($(this), false); });

    $('.selectOnFocus').on('focus', function() { $(this).select(); });

    $('.radio-check').on('change', function() {
        var checked = $(this).prop('checked');
        if(checked) {
            var name = $(this).attr('name');
            $('input.radio-check[name="{0}"]'.format(name)).prop('checked', !checked);
            $(this).prop('checked', checked);
        }
    });
}

function fixLinkedInButtonStyleAttribute() {
    var button = $('span.IN-widget > span');
    if(button.length === 0)
        setTimeout(fixLinkedInButtonStyleAttribute, 1000);
    else
        button.css('vertical-align', 'top');
}

//debounce is copied from https://davidwalsh.name/javascript-debounce-function
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};


function setScrollToTopButton() {
    var scrollTopElement = $("#scrolltopfix");
    $(document).on("scroll", debounce(function() {
        if($(window).scrollTop() > 5) {
            scrollTopElement.removeClass("hidden");
        } else {
            scrollTopElement.addClass("hidden");
        }
    }));
}

function setHeaderScrollToFixed() {
    var header = $("#menuBar");
    var content = $("#page-content");
        $(document).on("scroll", debounce(function() {
            if($(window).scrollTop() > header.height()) {
                header.addClass("floating");
                content.css('margin-top', header.height());
            } else {
                header.removeClass("floating");
                content.css('margin-top', '');
            }
        }, 250));
}

function prepAjaxForms() {
    $('form.ajaxable :submit').off('click');
    $('form.ajaxable :submit').on('click', function(e) {
        if(!$(this).hasClass('no-ajax')) {
            e.preventDefault();
            ajaxForm($(this));
        }
    });

    $('.autosubmit').on('change', function() {
        var form = $(this).closest('form');
        if(form.hasClass('ajaxable')) {
            ajaxForm(form);
        } else {
            form.submit();
        }
    });
}

function ajaxForm(button) {
    var form = button.closest('form.ajaxable');
    form.prepend(
        $("<input type='hidden'>").attr( {
            name: button.attr('name'),
            value: button.attr('value') })
    );

    var url = form.attr("action");

    var updateSelector = form.data('updates');
    if (typeof updateSelector === 'undefined') {
        updateSelector = '#' + form.attr('id');
    }
    if (1 == updateSelector.length) {
        updateSelector = "form.ajaxable";
    }

    if (typeof formsBeingUpdated === 'undefined') {
        formsBeingUpdated = [];
    }

    if (formsBeingUpdated[updateSelector]) {
        return;
    }

    formsBeingUpdated[updateSelector] = true;

    var data = form.serialize();

    $(updateSelector).addClass('loading');

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'html',
        success: function(data) {
            var result = $(data);
            var doc = $(document);

            var found = false;
            updateSelector.split(",").forEach(function (item) {
                item = item.trim();
                var replacement = result.find(item);
                var current = doc.find(item);

                if (replacement.length > 0 && current.length > 0) {
                    found = true;
                    current.replaceWith(replacement);
                }
            });
            if (found) {
                if(button.hasClass('update_page')) {
                    var page_input = $(button.data('page'));
                    setUrlParameter(page_input.attr('name'), page_input.val());
                }
                prepAjaxForms();
                prepDisplayFilters();
            } else {
                log('wrong page returned');
            }
        },
        error: function(error) {
            var responseText = error.responseText;
            log("error");
            log(error);
            log(responseText);
            setMessage(responseText, "fail")
        }
    }).always(function () {
        $(updateSelector).removeClass('loading');
        delete formsBeingUpdated[updateSelector];
    });
}

function ajaxCall(url, data, succes, datatype) {
    datatype = typeof datatype !== 'undefined' ? datatype : 'json';
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: datatype,
        success: function(data) {
            if(data.hasOwnProperty('message')) {
                var content = data.message.content;
                var type = data.message.type;
                setMessage(content, type);
            }
            succes(data);
        },
        error: function(error) {
            var responseText = error.responseText;
            log("error");
            log(error);
            log(responseText);
            // setMessage(responseText, "fail")
        }
    });
}

function setMessage(content, type) {
    $('#hide_messageBox_check').prop("checked", false);
    $('#messageBox > .content').html(content);
    $('#messageBox').attr("class", type);
}
function log(data) {
    console.log(data);
}

function decode(string) {
    return decodeURIComponent(decodeURIComponent(string));
}

function JSEnabled() {
    $('html').removeClass('noJavaScript');
    Cookies.set('javascript', true);
}

function checkall(checkbox, check) {
    var select = checkbox.data('select');
    if(select.length == 0) {
        select = 'input[type="checkbox"]';
    }
    $(select).prop("checked", check);
}

function hideByCheck(input) {
    var selector = input.data('select');
    var items = $(selector);
    var show = !input.prop('checked');

    display(items, show);
}

function prepDisplayFilters() {
        $('.display-filter-check').each(function () {
            hideByCheck($(this));
            $(this).on('click', function(){
                hideByCheck($(this));
            });
        });

        $('.display-filter-input').each(function () {
            if(!hasFilterableList($(this)))
                return;

            search($(this));
            $(this).on('keyup', function () {
                search($(this));
            });
        });
    }

function getNearestList(bf) {
    if(bf.hasClass("validatable"))
        bf = bf.closest(".validatable-input-label");

    return bf.next('.display-filterable');
}
function hasFilterableList(bf) {
    var next = getNearestList(bf);

    return next.length != 0;
}

function search(input) {
    var value = input.val();
    value = cleanString(value);

    var next = getNearestList(input);

    var items = next.find('.filterable');

    if(items.length == 0) {
        items = next.children();
    }
    var filterAtrribute = input.data('filter-attribute');

    items.each(function () {
        var item = $(this);
        var displayText = item.text();
        var attributeText = '';

        if (typeof filterAtrribute !== 'undefined') {
            filterAtrribute.toString().split(", ").forEach(function (attribute) {
                if (attributeText.length > 0) {
                    attributeText += " ";
                }
                var at = item.attr(attribute);
                if (typeof at === 'undefined')
                    at = '';
                attributeText += at;
            });
        }

        if (typeof attributeText === 'undefined')
            attributeText = '';

        displayText = cleanString(displayText);
        attributeText = cleanString(attributeText);

        var displayContainsText = attributeText.length == 0 && displayText.includes(value);
        var attributeContainsText = attributeText.includes(value);

        var parent = item.parent();
        if (parent.hasClass('display-filterable') && !parent.hasClass('item-list')) {
            item = parent;
        }

        var disp = displayContainsText || attributeContainsText;

        display(item, disp);
    });

    var parents = next.find('.filterable-display-parent');
    parents.each(function () {
        var parent = $(this);
        if(parent.find('.hidden.display-filterable').length != parent.find('.display-filterable').length) {
            parent.removeClass('hidden');
        } else {
            parent.addClass('hidden');
        }
    });
}

function display(item, show) {
    var hiddenClass = 'hidden';
    if(show)
        item.removeClass(hiddenClass);
    else
        item.addClass(hiddenClass);
}

function cleanString(toClean) {
    toClean = toClean.toLowerCase();

    toClean = toClean.replace(/[\\\^\$\*\+\?\|\{\}:;,\.<>\(\)\[\]]/gi, "");

    toClean = toClean.trim();

    return toClean;
}

function scrollToElement(element, parentSelector) {
    parentSelector = typeof parentSelector !== 'undefined' ? parentSelector : 'html';

    if(0 < element.length) {
        var list = element.closest(parentSelector);
        var offset = element.offset().top - list.offset().top;
        list.animate({
            scrollTop: offset + 'px'
        }, 'fast');
    }
}

function setUrlParameter(name, value) {
    var currentParams = getAllUrlParameters();
    currentParams[name] = value;
    var newUrl = '?' + $.param(currentParams);
    history.pushState([name, value], 'update', newUrl);
}
function getAllUrlParameters() {
    var queryString = window.location.search.slice(1);

    var paramArray = {};
    if(queryString) {
        queryString = queryString.split('#')[0];
        var queryStringParams = queryString.split("&");
        $(queryStringParams).each(function() {
            var keyValue = this.split("=");
            var key = keyValue[0];
            var value = '';
            if(keyValue[1]) {
                value = keyValue[1];
            }
            if(key.endsWith('[]')) {
                key = key.slice(0, -2);
            }
            if(paramArray[key]) {
                var current = paramArray[key];
                if(typeof current === 'string') {
                    var list = {};
                    list.push(current);
                    current = list;
                }
                current.push(value);
            } else {
                paramArray[key] = value;
            }
        });
    }

    return paramArray;
}

$.fn.goTo = function() {
    $('html, body').animate({
        scrollTop: $(this).offset().top + 'px'
    }, 'fast');
    return this; // for chaining...
};

String.prototype.format = String.prototype.f = function() {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};