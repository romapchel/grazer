$(document).ready(function(){
    var textinputs = $('#searching input[type="text"]');
    var sortorder = $('#searching #order');

    sortorder.on('change', function(){
        Search.disableSortChange(textinputs, sortorder);
    });
    textinputs.on('change', function(){
        Search.disableSortChange(textinputs, sortorder);
        sortorder.val('score:DESC');
    });
});

var Search = {
    disableSortChange: function disableSortChange(textinputs, sortorder) {
        sortorder.off('change');
        textinputs.off('change');
    }
};