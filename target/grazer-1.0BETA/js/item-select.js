$(document).ready(function(){
    var firstSelected = $('.selected:checked + .item').eq(0);
    scrollToElement(firstSelected, '.item-list');

    $('.validatable.not working the way I want it to, get this to work later.a').on('submit', function(e) {

        var id = $(this).attr('id');
        var action = $(this).attr('action');
        var original_form = $(this);

        e.preventDefault();

        $.ajax({
            url: action,
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'html',
            success: function(data)
            {
                var form = $(data).find('#'+id);
                if(form.length == 0) {
                    form = $(data).find('form.validatable');
                }
                if(form.length > 0) {
                    original_form.replaceWith(form.html());
                } else {
                    log('wrong page returned');
                }
            },
            error: function(error) {
                var responseText = error.responseText;
                log("error");
                log(error);
                log(responseText);
                setMessage(responseText, "fail")
            }
        });
    });
});