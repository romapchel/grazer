$( document ).ready(
    function()
    {
        $('#addButton').on('click', function() {
            addButton();
        });
        $('#addLink').on('click', function() {
            addLink();
        });
    }
);
function addLink()
{
    if($('#links .link').length > 4) {
        return;
    }
    var html = buildLinkHTML($('#links .link').length);
    $('#linkContainer').append(html);
}
function buildLinkHTML(index)
{
    var html = ""
        + '<div class="link">'
            + '<input type="text" name="altlink[]" placeholder="link naar pagina" />'
            + '<select name="altpre[]">'
                + '<option value="True">Vooraf</option>'
                + '<option value="False">Achteraf</option>'
            + '</select>'
        + '</div>';
    return html;
}

function addButton()
{
    if($('#buttons .button').length > 4) {
        return;
    }
    var html = buildButtonHTML($('#buttons .button').length);
    $('#buttonContainer').append(html);
}

function buildButtonHTML(index)
{
    var html = ""
        + '<div class="button">'
        + '<div class="testID">'
        + '<label for="testid' + index + '">ID van een element dat aanwezig moet zijn daarmee op de knop wordt gedrukt</label>'
        + '<input type="text" id="testid' + index + '" name="testid[]" placeholder="ID test element" />'
        + '</div>'
        + '<div class="selector">'
        + '<label for="selector' + index + '">CSS selector van de knop</label>'
        + '<textarea id="selector' + index + '" name="selector[]" placeholder="CSS selector van de knop"></textarea>'
        + '</div>'
        + '<div>'
        + '<label for="clicks' + index + '">Hoe vaak te knop moet worden ingedrukt</label>'
        + '<input type="number" id="clicks' + index + '" name="clicks[]" placeholder="klikken" />'
        + '</div>'
        + '<div>'
        + '<label for="order' + index + '">Dit getal geeft aan in welke volgorde de knoppen moeten worden ingedrukt(laag naar hoog)</label>'
        + '<input type="number" id="order' + index + '" name="order[]" placeholder="volgorde"/>'
        + '</div>'
        + '<div>'
        + '<label for="indexAfter' + index + '">moet de crawler elke keer worden uitgevoerd nadat er op de knop is gedrukt?</label>'
        + '<input type="checkbox" id="indexAfter' + index + '" name="indexAfter[]" />'
        + '</div>'
        + '</div>';
    return html;
}
function buildSeReDeHTML(name, selector, regex, alwaysregex, defaultval, alwaysdefault)
{
    var alwaysReg = "";
    if(alwaysregex) {
        alwaysReg = "checked";
    }
    var alwaysDef = "";
    if(alwaysdefault) {
        alwaysDef = "checked";
    }

    var html = ""
        + '<div id="' + name + '" class="serede">'
        + '<h2>' + capitalize(name) + '</h2>'
        + '<div class="selector">'
        + '<label for="' + name + '_selector">CSS selector voor de ' + name + ': </label>'
        + '<textarea id="' + name + '_selector" name="' + name + '_selector" placeholder="CSS selector">' + decode(selector) + '</textarea>'
        + '</div>'
        + '<div class="regex">'
        + '<label for="' + name + '_regex">Regex voor de ' + name + ': </label>'
        + '<input type="text" id="' + name + '_regex" name="' + name + '_regex" placeholder="regex" value="' + decode(regex) + '" />'
        + '<label for="' + name + '_regex_always">Gebruik de regex altijd</label>'
        + '<input type="checkbox" id="' + name + '_regex_always" name="' + name + '_regex_always" value="True" ' + alwaysReg + ' />'
        + '</div>'
        + '<div class="default">'
        + '<label for="' + name + '_default">Standaard waarde voor de ' + name + ': </label>'
        + '<input type="text" id="' + name + '_default" name="' + name + '_default" placeholder="standaard waarde" value="' + decode(defaultval) + '" />'
        + '<label for="' + name + '_default_always">Gebruik de standaard waarde altijd</label>'
        + '<input type="checkbox" id="' + name + '_default_always" name="' + name + '_default_always" value="True" ' + alwaysDef + ' />'
        + '</div>'
        + '</div>';

    return html;
}
function capitalize(string)
{
    return string.trim().charAt(0).toUpperCase() + string.slice(1);
}